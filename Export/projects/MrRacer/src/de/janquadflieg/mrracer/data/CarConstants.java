/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package de.janquadflieg.mrracer.data;

/**
 *
 * @author quad
 */
public interface CarConstants {
    public static final double CAR_WIDTH = 1.94;
    public static final double CAR_HALF_WIDTH = 0.97;
    public static final double CAR_LENGTH = 4.52;
    public static final double CAR_HALF_LENGTH = 2.26;
}
