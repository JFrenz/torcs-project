/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.classification.filter;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.telemetry.ModifiableSensorData;
import de.janquadflieg.mrracer.telemetry.SensorData;
import flanagan.analysis.Regression;
import java.awt.geom.Point2D;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;

/**
 *
 * @author quad
 */
public class Filter {    
    public static void filterApacheCommons(double[] track) {
        apache2(track);
    }
    public static void apache2(double[] track){
        // Calculate coordinates
        ModifiableSensorData data = new ModifiableSensorData();
        data.setTrackEdgeSensors(track);        
        Point2D[] points = SensorData.calculateTrackPoints(data, false);

        // Find the indeces of the biggest sensor value
        int leftIndex = SensorData.maxTrackIndexLeft(data);
        int rightIndex = SensorData.maxTrackIndexRight(data);

        OLSMultipleLinearRegression reg = new OLSMultipleLinearRegression();

        if (leftIndex > 2) {
            double[][] matrix = new double[leftIndex][3];
            double[] constants = new double[leftIndex];
            for (int i = 0; i < leftIndex; ++i) {
                matrix[i][0] = 1.0;
                matrix[i][1] = points[i].getY();
                matrix[i][2] = points[i].getY() * points[i].getY();
                constants[i] = points[i].getX();
            }
            /*System.out.println("--------------------------------");
            for(int i=0; i < matrix.length; ++i){
                System.out.println(Utils.dTS(matrix[i][0])+" "+
                        Utils.dTS(matrix[i][1])+" "+Utils.dTS(matrix[i][2]));
            }
            System.out.println("--------------------------------");*/
            reg.newSampleData(constants, matrix);
            reg.setNoIntercept(true);
            reg.newSampleData(constants, matrix);
            double[] beta = reg.estimateRegressionParameters();
            /*for (double d : beta) {
                System.out.println("D: " + d);
            }*/
            for(int i=0; i < leftIndex; ++i){
                double x = beta[0] + beta[1] * points[i].getY() + beta[2] * points[i].getY() * points[i].getY();
                track[i] = Math.sqrt(x * x + points[i].getY() * points[i].getY());
            }                    
        }
        
        if (rightIndex < track.length - 3) {
            double[][] matrix = new double[track.length - (rightIndex+1)][3];
            double[] constants = new double[track.length - (rightIndex+1)];
            for (int i = 0; i < matrix.length; ++i) {
                matrix[i][0] = 1.0;
                matrix[i][1] = points[rightIndex+1+i].getY();
                matrix[i][2] = points[rightIndex+1+i].getY() * points[rightIndex+1+i].getY();
                constants[i] = points[rightIndex+1+i].getX();
            }
            reg.newSampleData(constants, matrix);
            reg.setNoIntercept(true);
            reg.newSampleData(constants, matrix);
            double[] beta = reg.estimateRegressionParameters();            
            for(int i=0; i < matrix.length; ++i){
                double x = beta[0] + beta[1] * points[rightIndex+1+i].getY() + beta[2] * points[rightIndex+1+i].getY() * points[rightIndex+1+i].getY();
                track[rightIndex+1+i] = Math.sqrt(x * x + points[rightIndex+1+i].getY() * points[rightIndex+1+i].getY());                
            }
        }
    }
    
    public static void apache1(double[] track) {
        // Calculate coordinates
        ModifiableSensorData data = new ModifiableSensorData();
        data.setTrackEdgeSensors(track);

        // Find the indeces of the biggest sensor value
        int leftIndex = SensorData.maxTrackIndexLeft(data);
        int rightIndex = SensorData.maxTrackIndexRight(data);

        OLSMultipleLinearRegression reg = new OLSMultipleLinearRegression();

        if (leftIndex > 2) {
            double[][] matrix = new double[leftIndex][3];
            double[] constants = new double[leftIndex];
            for (int i = 0; i < leftIndex; ++i) {
                matrix[i][0] = 1.0;
                matrix[i][1] = i;
                matrix[i][2] = i * i;
                constants[i] = track[i];
            }
            /*System.out.println("--------------------------------");
            for(int i=0; i < matrix.length; ++i){
                System.out.println(Utils.dTS(matrix[i][0])+" "+
                        Utils.dTS(matrix[i][1])+" "+Utils.dTS(matrix[i][2]));
            }
            System.out.println("--------------------------------");*/
            reg.newSampleData(constants, matrix);
            reg.setNoIntercept(true);
            reg.newSampleData(constants, matrix);
            double[] beta = reg.estimateRegressionParameters();
            /*for (double d : beta) {
                System.out.println("D: " + d);
            }*/
            for(int i=0; i < leftIndex; ++i){
                track[i] = beta[0] + beta[1] * i + beta[2] * i*i;
            }
        }
        
        if (rightIndex < track.length - 3) {
            double[][] matrix = new double[track.length - (rightIndex+1)][3];
            double[] constants = new double[track.length - (rightIndex+1)];
            for (int i = 0; i < matrix.length; ++i) {
                matrix[i][0] = 1.0;
                matrix[i][1] = i;
                matrix[i][2] = i * i;
                constants[i] = track[rightIndex+1+i];
            }
            reg.newSampleData(constants, matrix);
            reg.setNoIntercept(true);
            reg.newSampleData(constants, matrix);
            double[] beta = reg.estimateRegressionParameters();            
            for(int i=0; i < matrix.length; ++i){
                track[rightIndex+1+i] = beta[0] + beta[1] * i + beta[2] * i*i;
            }
        }        
    }
    
    public static void filterFlanaganRegression(double[] track) {
        // Calculate coordinates
        ModifiableSensorData data = new ModifiableSensorData();
        data.setTrackEdgeSensors(track);
        Point2D[] points = SensorData.calculateTrackPoints(data, false);

        // Find the indeces of the biggest sensor value
        int leftIndex = SensorData.maxTrackIndexLeft(data);
        int rightIndex = SensorData.maxTrackIndexRight(data);

        if (leftIndex > 2) {
            double[] x = new double[leftIndex];
            double[] y = new double[leftIndex];

            for (int i = 0; i < x.length; ++i) {
                x[i] = points[i].getY();
                y[i] = points[i].getX();
            }

            Regression reg = new Regression(x, y);

            try {
                reg.polynomial(2);
                double[] values = reg.getYcalc();

                for (int i = 0; i < values.length; ++i) {
                    points[i].setLocation(values[i], points[i].getY());
                    track[i] = Math.sqrt(values[i] * values[i] + points[i].getY() * points[i].getY());
                }

            } catch (IllegalArgumentException e) {
                e.printStackTrace(System.out);
            }
        } else {
            /*for (int i = 0; i < leftIndex; ++i) {
                track[i] = 0.0;
            }*/
        }

        if (rightIndex < points.length - 3) {
            double[] x = new double[points.length - (rightIndex + 1)];
            double[] y = new double[points.length - (rightIndex + 1)];

            for (int i = 0; i < x.length; ++i) {
                x[i] = points[points.length - (1 + i)].getY();
                y[i] = points[points.length - (1 + i)].getX();
            }

            Regression reg = new Regression(x, y);
            try {
                reg.polynomial(2);
                double[] values = reg.getYcalc();

                for (int i = 0; i < values.length; ++i) {
                    points[points.length - (1 + i)].setLocation(values[i], points[points.length - (1 + i)].getY());

                    track[track.length - (1 + i)] = Math.sqrt(values[i] * values[i]
                            + points[points.length - (1 + i)].getY() * points[points.length - (1 + i)].getY());
                }

            } catch (IllegalArgumentException e) {
                e.printStackTrace(System.out);
            }
        }

    }    
    
    
}