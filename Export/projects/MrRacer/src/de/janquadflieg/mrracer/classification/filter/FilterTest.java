/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.classification.filter;

import java.util.Random;

import de.janquadflieg.mrracer.telemetry.*;

/**
 *
 * @author quad
 */
public class FilterTest {
    
    public static void test(){
        final double EPSILON = 0.0000000001;
        Telemetry telemetry = new Telemetry();
        telemetry.load("f:\\test.zip");
        telemetry.shutdown();
        System.out.println(telemetry.size()+" entries");
        
        Random rand = new Random();
        rand.setSeed(System.nanoTime());
        
        int notOK = 0;
        
        for(int i=0; i < telemetry.size(); ++i){
            SensorData data = telemetry.getSensorData(i);
            
            // make noisy
            double[] trackSensor = data.getTrackEdgeSensors();
            double[] noisyTrackSensor = new double[trackSensor.length];
            System.arraycopy(trackSensor, 0, noisyTrackSensor, 0, trackSensor.length);
            
            for(int k=0; k < noisyTrackSensor.length; ++k){
                noisyTrackSensor[k] *= 1.0+(rand.nextGaussian()*0.1);
            }
            
            double[] flanagan = new double[noisyTrackSensor.length];
            System.arraycopy(noisyTrackSensor, 0, flanagan, 0, flanagan.length);
            
            Filter.filterFlanaganRegression(flanagan);
            
            double[] apache = new double[noisyTrackSensor.length];
            System.arraycopy(noisyTrackSensor, 0, apache, 0, apache.length);
            
            Filter.filterApacheCommons(apache);
            
            // compare
            boolean ok = true;
            for(int k=0; k < apache.length; ++k){
                ok &= Math.abs(apache[k]-flanagan[k]) < EPSILON;
            }
            
            if(!ok){
                ++notOK;
            }
        }
        
        System.out.println(notOK+" not ok ");
    }
    
    public static void main(String[] args){
        try{
            test();
        } catch(RuntimeException e){
            e.printStackTrace(System.out);
        }
    }
    
}
