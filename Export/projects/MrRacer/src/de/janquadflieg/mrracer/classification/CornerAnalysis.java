/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.classification;

import static de.janquadflieg.mrracer.classification.Situations.TYPE_FULL;
import static de.janquadflieg.mrracer.classification.Situations.TYPE_HAIRPIN;
import static de.janquadflieg.mrracer.classification.Situations.TYPE_MEDIUM;
import static de.janquadflieg.mrracer.classification.Situations.TYPE_SLOW;
import java.io.File;
import java.util.HashMap;
import java.util.Properties;

import de.janquadflieg.mrracer.functions.GeneralisedLogisticFunction;
import de.janquadflieg.mrracer.telemetry.*;
import de.janquadflieg.mrracer.track.*;

/**
 *
 * @author quad
 */
public class CornerAnalysis {

    private final static boolean INFO = true;
    private String[] TRACKS = {"etrack2", "etrack3", "etrack4", "etrack6", "forza", "cgspeed", "polimi", "wheel2"};
    public static final int[] TYPES = {
        TYPE_HAIRPIN, TYPE_SLOW, TYPE_MEDIUM, TYPE_FULL,};
    private static final String BASE_DIR = "Z:\\Experimente\\Evaluation\\MrRacer";
    private HashMap<String, Telemetry> telemetry = new HashMap<>();
    private HashMap<String, TrackModel> trackModels = new HashMap<>();
    private static final char DELIM = ' ';
    private GeneralisedLogisticFunction f = new GeneralisedLogisticFunction();
    private static final double PCF = 0.3586022350646899;

    public CornerAnalysis()
            throws Exception {
        for (String track : TRACKS) {
            if (INFO) {
                System.out.println("Loading Track " + track);
            }
            Telemetry t = new Telemetry();
            t.shutdown();
            t.load(BASE_DIR + File.separator + track + ".data");
            if (INFO) {
                System.out.println(t.size() + " entries");
            }
            telemetry.put(track, t);
            TrackModel model = TrackModel.load(BASE_DIR + File.separator + track + TrackModel.TM_EXT);
            if (INFO) {
                System.out.println(model.size() + " segments");
            }
            trackModels.put(track, model);
        }
        Properties p = new Properties();
        p.setProperty("-GLF.A-", "0.0");
        p.setProperty("-GLF.B-", "6.940896455228012");
        p.setProperty("-GLF.F-", "1.0");
        p.setProperty("-GLF.K-", "1.0");
        p.setProperty("-GLF.M-", "0.3094200836528687");
        p.setProperty("-GLF.Q-", "0.6171537458627748");
        p.setProperty("-GLF.V-", "0.6490062775650256");
        f.setParameters(p, "");
    }

    public void run()
            throws Exception {

        // HEADER
        // Name
        System.out.print("TrackName");
        System.out.print(DELIM);
        // Width
        System.out.print("TrackWidth");
        System.out.print(DELIM);
        // RHO
        System.out.print("Rho");
        System.out.print(DELIM);
        // VGLF            
        System.out.print("Vglf");
        System.out.print(DELIM);
        // VTRACKMODEL
        System.out.print("Vopt");
        System.out.print(DELIM);
        // vmin & vmax
        System.out.print("Vmin");
        System.out.print(DELIM);
        System.out.print("Vmax");
        System.out.print(DELIM);
        // DtoApex
        System.out.print("DistToApex");
        System.out.print(DELIM);

        // Length Full Subsegment
        //if (type == TYPE_MEDIUM || type == TYPE_SLOW || type == TYPE_HAIRPIN) {
        System.out.print("Full");
        System.out.print(DELIM);
        //}

        // Length Medium Subsegment
        //if (type == TYPE_SLOW || type == TYPE_HAIRPIN) {
        System.out.print("Medium");
        System.out.print(DELIM);
        //}

        // Length Slow Subsegment
        //if (type == TYPE_HAIRPIN) {
        System.out.print("Slow");
        System.out.print(DELIM);
        //}
        
        System.out.print("Hairpin");
        System.out.print(DELIM);
        
        System.out.print("NFull");
        System.out.print(DELIM);
        //}

        // Length Medium Subsegment
        //if (type == TYPE_SLOW || type == TYPE_HAIRPIN) {
        System.out.print("NMedium");
        System.out.print(DELIM);
        //}

        // Length Slow Subsegment
        //if (type == TYPE_HAIRPIN) {
        System.out.print("NSlow");
        System.out.print(DELIM);
        //}
        
        System.out.print("NHairpin"); 
        System.out.print(DELIM);
        
        // Overall length
        System.out.print("Length");
        System.out.print(DELIM);
        
        // Context
        System.out.print("Prev");        
        System.out.print(DELIM);
        
        System.out.print("PrevDist");        
        System.out.print(DELIM);
        
        System.out.print("Next");        
        System.out.print(DELIM);
        
        System.out.print("NextDist");
        System.out.print(DELIM);
        
        System.out.print("Result");

        System.out.println("");

        for (int type : TYPES) {
            //System.out.println(Situations.toString(type));


            for (String track : TRACKS) {
                TrackModel model = trackModels.get(track);
                Telemetry t = telemetry.get(track);
                for (int i = 0; i < model.size(); ++i) {
                    TrackSegment segment = model.getSegment(i);
                    if (segment.isCorner() && segment.getType() == type) {
                        analyse(i, segment, t, model);
                    }
                }
            }
        }
    }

    private void analyse(int index, TrackSegment s, Telemetry t, TrackModel model) {
        if (INFO) {
            //System.out.println(s);
        }
        TrackSegment.Apex[] apexes = s.getApexes();
        if (apexes.length > 1) {
            return;
        }
        // Name
        System.out.print(model.getName());
        System.out.print(DELIM);
        // Width
        System.out.print(model.getWidth());
        System.out.print(DELIM);
        // RHO
        System.out.print(String.valueOf(Math.abs(apexes[0].value)));
        System.out.print(DELIM);
        // VGLF
        double vglf = f.getMirroredValue(Math.abs(apexes[0].value / 100.0)) * 280.0 + 50.0;
        System.out.print(String.valueOf(vglf));
        System.out.print(DELIM);
        // VTRACKMODEL
        System.out.print(String.valueOf(apexes[0].targetSpeed));
        System.out.print(DELIM);
        // vmin & vmax
        TrackSubSegment subsegment = s.getSubSegment(apexes[0].position);
        double vmin = Double.POSITIVE_INFINITY;
        double vmax = Double.NEGATIVE_INFINITY;
        double start = (2 * model.getLength()) - t.getSensorData(0).getDistanceFromStartLine();
        for (int i = 0; i < t.size(); ++i) {
            SensorData data = t.getSensorData(i);
            if (data.getDistanceRaced() >= start
                    && data.getDistanceFromStartLine() >= subsegment.getStart()
                    && data.getDistanceFromStartLine() <= subsegment.getEnd()) {
                if (data.getSpeed() > vmax) {
                    vmax = data.getSpeed();
                }
                if (data.getSpeed() < vmin) {
                    vmin = data.getSpeed();
                }
            }
        }

        System.out.print(String.valueOf(vmin));
        System.out.print(DELIM);
        System.out.print(String.valueOf(vmax));
        System.out.print(DELIM);

        // distances
        double distToApex = 0.0;
        double full = 0.0;
        double medium = 0.0;
        double slow = 0.0;
        double hairpin = 0.0;        

        // Length Full Subsegment
        if (s.getType() == TYPE_MEDIUM || s.getType() == TYPE_SLOW || s.getType() == TYPE_HAIRPIN) {
            full = s.getSubSegment(0).getLength();            
            
        } else {
            full = s.getSubSegment(0).getLength()*0.5*PCF;            
        }
        distToApex += full;
        

        // Length Medium Subsegment
        if (s.getType() == TYPE_SLOW || s.getType() == TYPE_HAIRPIN) {
            medium = s.getSubSegment(1).getLength();            
            
        } else if(s.getType() == TYPE_MEDIUM){
            medium = s.getSubSegment(1).getLength()*0.5*PCF;                        
        }
        distToApex += medium;

        // Length Slow Subsegment
        if (s.getType() == TYPE_HAIRPIN) {
            slow = s.getSubSegment(2).getLength();            
            
        } else if(s.getType() == TYPE_SLOW){
            slow = s.getSubSegment(2).getLength()*0.5*PCF;            
        }
        distToApex += slow;
        
        // Length Hairpin Subsegment
        if (s.getType() == TYPE_HAIRPIN) {
            hairpin = s.getSubSegment(3).getLength()*0.5*PCF;   
        }    
        distToApex += hairpin;
        
        System.out.print(String.valueOf(distToApex));
        System.out.print(DELIM);
        
        System.out.print(String.valueOf(full));
        System.out.print(DELIM);
        
        System.out.print(String.valueOf(medium));
        System.out.print(DELIM);
        
        System.out.print(String.valueOf(slow));
        System.out.print(DELIM);
        
        System.out.print(String.valueOf(hairpin));
        System.out.print(DELIM);
        
        System.out.print(String.valueOf(full/distToApex));
        System.out.print(DELIM);
        
        System.out.print(String.valueOf(medium/distToApex));
        System.out.print(DELIM);
        
        System.out.print(String.valueOf(slow/distToApex));
        System.out.print(DELIM);
        
        System.out.print(String.valueOf(hairpin/distToApex));
        System.out.print(DELIM);
        
        // Overall length
        System.out.print(String.valueOf(s.getLength()));
        System.out.print(DELIM);
        
        // Context        
        TrackSegment prev = model.getSegment(model.decIdx(index));
        if(prev.isStraight()){
            System.out.print("ST");
            System.out.print(DELIM);
            System.out.print(String.valueOf(0.0));
            System.out.print(DELIM);
            
        } else {
            if(prev.isFull()){
                System.out.print('F');
                System.out.print(DELIM);
            }
            if(prev.isMedium()){
                System.out.print('M');
                System.out.print(DELIM);
            }
            if(prev.isSlow()){
                System.out.print('S');
                System.out.print(DELIM);
            }
            if(prev.isHairpin()){
                System.out.print('H');
                System.out.print(DELIM);
            }
            System.out.print(String.valueOf((s.getStart()-prev.getApexes()[0].position+model.getLength())%model.getLength()));
            System.out.print(DELIM);
        }
        
        TrackSegment next = model.getSegment(model.incIdx(index));
        if(next.isStraight()){
            System.out.print("ST");
            System.out.print(DELIM);
            System.out.print(String.valueOf(0.0));
            System.out.print(DELIM);
            
        } else {
            if(next.isFull()){
                System.out.print('F');
                System.out.print(DELIM);
            }
            if(next.isMedium()){
                System.out.print('M');
                System.out.print(DELIM);
            }
            if(next.isSlow()){
                System.out.print('S');
                System.out.print(DELIM);
            }
            if(next.isHairpin()){
                System.out.print('H');
                System.out.print(DELIM);
            }
            System.out.print(String.valueOf((next.getApexes()[0].position-s.getEnd()+model.getLength())%model.getLength()));
            System.out.print(DELIM);
        }
        
        // Result
        // Corners which are not relevant
        if(vmax < s.getApexes()[0].targetSpeed){
            System.out.print("UnLimited");
            
        } else if(s.getApexes()[0].targetSpeed > vglf){
            System.out.print("++++");
            
        } else if(s.getApexes()[0].targetSpeed < vglf){
            System.out.print("----");
            
        } else {
            System.out.print("====");
        }

        System.out.println("");
    }

    public static void main(String[] args) {
        try {
            CornerAnalysis s = new CornerAnalysis();

            s.run();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
}