/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.telemetry;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.controller.MrRacer2013;
import scr.MessageBasedSensorModel;

import java.io.*;
import java.util.ArrayList;
import java.util.zip.*;

/**
 * Helper class to create the precompile data
 * @author quad
 */
public class JITData {

    
    /*public static ArrayList<String> getPrecompile() {
        try {
            FileInputStream fi = new FileInputStream("f:\\test_data.zip");
            GZIPInputStream zi = new GZIPInputStream(fi);
            ObjectInputStream is = new ObjectInputStream(zi);
            @SuppressWarnings("unchecked")
            ArrayList<String> result = (ArrayList<String>) is.readObject();
            is.close();
            zi.close();
            fi.close();

            return result;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }*/
    
    public static ArrayList<scr.SensorModel> getPrecompile() {
        try {
            FileInputStream fi = new FileInputStream("f:\\test_data.zip");
            GZIPInputStream zi = new GZIPInputStream(fi);
            ObjectInputStream is = new ObjectInputStream(zi);
            @SuppressWarnings("unchecked")
            ArrayList<scr.SensorModel> result = (ArrayList<scr.SensorModel>) is.readObject();
            is.close();
            zi.close();
            fi.close();

            return result;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void test() {
        long start = System.nanoTime();
        MrRacer2013 controller = new MrRacer2013();
        long end = System.nanoTime();

        System.out.println(Utils.dTS((end - start) / 1000000000.0));
    }

    private static void telemetryToStringDataFile() {
        Telemetry t = Telemetry.createPrecompile();
        ArrayList<String> list = new ArrayList<>();
        //scr.Action a = control(data.getSensorData(i).getSensorModel());
        for (int i = 0; i < t.size(); ++i) {
            String s = t.getSensorData(i).getSensorModelString();
            list.add(s);
        }
        try {
            FileOutputStream fo = new FileOutputStream("f:\\test_data.zip", false);
            GZIPOutputStream zo = new GZIPOutputStream(fo);
            ObjectOutputStream os = new ObjectOutputStream(zo);
            os.writeObject(list);
            os.flush();
            os.close();
            fo.close();

            FileInputStream fi = new FileInputStream("f:\\test_data.zip");
            GZIPInputStream zi = new GZIPInputStream(fi);
            ObjectInputStream is = new ObjectInputStream(zi);
            @SuppressWarnings("unchecked")
            ArrayList<String> l2 = (ArrayList<String>) is.readObject();
            for (int i = 0; i < l2.size(); ++i) {
                System.out.println(l2.get(i));
            }

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
    
    private static void telemetryToSensorModelFile() {
        Telemetry t = Telemetry.createPrecompile();
        ArrayList<scr.SensorModel> list = new ArrayList<>();
        //scr.Action a = control(data.getSensorData(i).getSensorModel());
        for (int i = 0; i < t.size(); ++i) {
            scr.SensorModel s = t.getSensorData(i).getSensorModel();
            list.add(s);
        }
        try {
            FileOutputStream fo = new FileOutputStream("f:\\test_data.zip", false);
            GZIPOutputStream zo = new GZIPOutputStream(fo);
            ObjectOutputStream os = new ObjectOutputStream(zo);
            os.writeObject(list);
            os.flush();
            os.close();
            fo.close();

            FileInputStream fi = new FileInputStream("f:\\test_data.zip");
            GZIPInputStream zi = new GZIPInputStream(fi);
            ObjectInputStream is = new ObjectInputStream(zi);
            @SuppressWarnings("unchecked")
            ArrayList<scr.SensorModel> l2 = (ArrayList<scr.SensorModel>) is.readObject();
            for (int i = 0; i < l2.size(); ++i) {
                System.out.println(l2.get(i));
            }

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
    
    public static void main(String[] args) {
        test();
        //telemetryToStringDataFile();
        //telemetryToSensorModelFile();
    }
}
