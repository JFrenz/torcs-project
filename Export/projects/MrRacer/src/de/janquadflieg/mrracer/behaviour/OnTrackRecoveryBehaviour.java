/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.behaviour;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.telemetry.*;

/**
 *
 * @author Jan Quadflieg
 */
public class OnTrackRecoveryBehaviour
        implements Behaviour {

    private static final boolean TEXT_DEBUG = false;
    /**
     * Gearchange behaviour.
     */
    private Behaviour gearChange = new StandardGearChangeBehaviour();
    /**
     * A counter to determine if the car is stuck.
     */
    private int speedCounter = 0;
    private boolean forwardAcc = true;
    /**
     * Should the car drive backwards?
     */
    private boolean reverse = false;
    /**
     * The last speed value.
     */
    private double lastSpeed = 0;
    /**
     * Angles.
     */
    private float[] angles = new float[19];
    /**
     * Desired position when driving backwards.
     */
    private static final double BACK_POS = -0.7;
    /**
     * Desired angle when driving backwards.
     */
    private static final double BACK_ANGLE_D = -20.0;
    private boolean firstCall = true;

    public OnTrackRecoveryBehaviour() {
        // init angles
        for (int i = 0; i < 19; ++i) {
            angles[i] = -90 + i * 10;
        }
    }

    public OnTrackRecoveryBehaviour(float[] f) {
        System.arraycopy(f, 0, angles, 0, f.length);
    }

    public void setStuck() {
        speedCounter = 200;
    }

    public void paint(String baseFileName, java.awt.Dimension d) {
    }

    @Override
    public void execute(SensorData data, ModifiableAction action) {
        //System.out.println("ONTR");
        double trackAngleD = Math.toDegrees(data.getAngleToTrackAxis());
        // looking to the right is the same as looking left mirrored
        boolean mirror = trackAngleD > 0;
        double steeringD = 0;

        if (mirror) {
            if (TEXT_DEBUG) {
                System.out.println("Mirror input true");
            }
            trackAngleD *= -1.0;
        }

        if (firstCall) {
            lastSpeed = data.getSpeed();
            firstCall = false;
            reverse = data.getSpeed() < 0.0;
        }

        if (reverse) {
            if (TEXT_DEBUG) {
                System.out.println("Backwards");
            }
            driveBackward(data, action);

        } else {
            if (TEXT_DEBUG) {
                System.out.println("Forward");
            }
            //System.out.println("F");
            driveForward(data, action);
        }

        // steering basically just depends on the direction in which we're driving
        if (data.getSpeed() < 0.0) {
            if (trackAngleD <= -90) {
                steeringD = 45;

            } else {
                //steeringD = (1.0 - ((trackAngleD + 90) / 90)) * 45;
                // can be reduced to
                steeringD = -(trackAngleD / 2.0);
            }
            
            if (mirror) {
                steeringD *= -1;
            }

        } else {
            // try to drive forward, life can be so simple
            if (TEXT_DEBUG) {
                System.out.println("Steering forward");
            }
            if (trackAngleD <= -90.0) {
                steeringD = -45;
                
                if (mirror) {
                    steeringD *= -1;
                }

            } else if (trackAngleD > -90 && trackAngleD <= -9) {
                //steeringD = (1.0 - ((trackAngleD + 90) / 90)) * -45.0;
                // can be reduced to:
                steeringD = trackAngleD / 2.0;
                
                if (mirror) {
                    steeringD *= -1;
                }

            } else if (trackAngleD > -9 && trackAngleD <= 9) {
                if (TEXT_DEBUG) {
                    System.out.println("steering based on the \"biggest sensor value\" heuristic");
                }
                int index = SensorData.maxTrackIndexLeft(data);
                steeringD = -angles[index];
                
                // no mirroring required, sensor points in the right direction anyway
                
                if (TEXT_DEBUG) {
                    System.out.println(steeringD);
                }
            }
        }

        action.setSteering(steeringD / 45);

        action.setClutch(0.0);
        action.limitValues();
        if (TEXT_DEBUG) {
            System.out.println("Final steering: "+action.getSteeringS());
        }
    }

    private void driveBackward(SensorData data, ModifiableAction action) {
        double trackAngleD = Math.toDegrees(data.getAngleToTrackAxis());
        double trackPos = data.getTrackPosition();

        forwardAcc = false;

        // looking to the right is the same as looking left mirrored
        boolean mirror = trackAngleD > 0;

        if (mirror) {
            trackAngleD *= -1;
            trackPos *= -1;
        }

        boolean reachedPosition = data.getTrackEdgeSensors()[9] > 5.0;

        if ((reachedPosition && trackAngleD > BACK_ANGLE_D)
                || trackPos < -0.5) {
            //System.out.println("reachedPosition && trackAngleD > BACK_ANGLE_D? "+((reachedPosition && trackAngleD > BACK_ANGLE_D)));
            //System.out.println("trackPos < -0.5? "+(trackPos < -0.5));
            reverse = false;
            speedCounter = 0;
            lastSpeed = data.getSpeed();
            driveForward(data, action);
        }

        // normierte distanz beim winkel
        double distanceAngle = ((trackAngleD - BACK_ANGLE_D) * (trackAngleD - BACK_ANGLE_D))
                / ((-180.0 - BACK_ANGLE_D) * (-180.0 - BACK_ANGLE_D));
        // normierte distanz bei der position
        double distancePos = ((trackPos - BACK_POS) * (trackPos - BACK_POS))
                / ((1.0 - BACK_POS) * (1.0 - BACK_POS));
        distancePos = Math.sqrt(distancePos);
        distanceAngle = Math.sqrt(distanceAngle);

        double distance = Math.min(distanceAngle, distancePos);
        double targetSpeed = -15.0 + -20.0 * distance;

        //System.out.println("Speed: "+Utils.dTS(data.getSpeed()));
        //System.out.println("DA: "+Utils.dTS(distanceAngle)+" DP: "+Utils.dTS(distancePos)+" D: "+Utils.dTS(distance)+" - "+Utils.dTS(targetSpeed));

        if (data.getSpeed() > targetSpeed) {
            action.setAcceleration(0.8);
            action.setBrake(0.0);
            action.setGear(-1);

        } else {
            action.setAcceleration(0);
            action.setBrake(0.8);
            action.setGear(0);
        }
    }

    private void driveForward(SensorData data, ModifiableAction action) {
        double trackAngleD = Math.toDegrees(data.getAngleToTrackAxis());

        // looking to the right is the same as looking left mirrored
        boolean mirror = trackAngleD > 0;

        if (mirror) {
            trackAngleD *= -1;
        }

        // when facing away from the track, we might hit an obstacle and get stuck
        if ((data.getSpeed() <= lastSpeed + 2.0 && data.getSpeed() < 10)) {
            //|| Math.abs(lastRaceDistance - data.getDistanceRaced()) < 0.2) {
            ++speedCounter;
        }

        lastSpeed = data.getSpeed();

        // check if stuck
        if (speedCounter > 50 * 2) {
            reverse = true;
            speedCounter = 0;
            driveBackward(data, action);
            return;
        }

        if (data.getSpeed() < -1.0 && !forwardAcc) {
            action.setGear(0);
            action.setAcceleration(0);
            action.setBrake(1);

        } else {
            forwardAcc = true;
            action.setGear(1);
            action.setAcceleration(0.8);
            action.setBrake(0);

            if (data.getSpeed() > 30) {
                gearChange.execute(data, action);
            } else {
                gearChange.reset();
            }
        }
    }

    /**
     * The current situation of the car, as classified by an appropriate
     * classifier chosen by the controller which uses this behaviour.
     */
    @Override
    public void setSituation(de.janquadflieg.mrracer.classification.Situation s) {
    }

    @Override
    public void reset() {
        reverse = false;
        gearChange.reset();
        firstCall = true;
        speedCounter = 0;
    }

    @Override
    public void shutdown() {
        gearChange.shutdown();
    }

    @Override
    public void setParameters(java.util.Properties params, String prefix) {
    }

    @Override
    public void getParameters(java.util.Properties params, String prefix) {
    }
}
