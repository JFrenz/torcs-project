/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.behaviour;

import de.janquadflieg.mrracer.classification.*;
import de.janquadflieg.mrracer.telemetry.*;

import java.util.*;

/**
 *
 *
 * @author Jan Quadflieg
 */
public class DefensiveFallbackBehaviour
        implements Behaviour {

    private static final boolean TEXT_DEBUG = false;
    private Situation s;
    private CorrectingPureHeuristicSteering steering;
    /**
     * Gear change behaviour.
     */
    private StandardGearChangeBehaviour gearChange = new StandardGearChangeBehaviour();
    private OnTrackRecoveryBehaviour onTrackRecovery;
    private OffTrackRecoveryBehaviour offTrackRecovery = new OffTrackRecoveryBehaviour();
    private StuckDetection stuck = new StuckDetection();
    public static final String OFF_TRACK = "-DFBB.offTrack-";
    private boolean firstCall = true;

    public DefensiveFallbackBehaviour(float[] f) {
        steering = new CorrectingPureHeuristicSteering(f);
        onTrackRecovery = new OnTrackRecoveryBehaviour(f);
    }

    @Override
    public void setParameters(Properties params, String prefix) {
        offTrackRecovery.setParameters(params, prefix + OFF_TRACK);
    }

    @Override
    public void getParameters(Properties params, String prefix) {
        offTrackRecovery.getParameters(params, prefix + OFF_TRACK);
    }

    @Override
    public void paint(String baseFileName, java.awt.Dimension d) {
    }

    @Override
    public void execute(SensorData data, ModifiableAction action) {
        if (TEXT_DEBUG) {
            System.out.println(data.getDistanceRacedS() + "m ------FALLBACK--------------");
        }
        if (Math.abs(data.getTrackPosition()) > 1) {
            if (TEXT_DEBUG) {
                System.out.println("Using offTrack");
            }
            offTrackRecovery.execute(data, action);
            onTrackRecovery.reset();
            stuck.reset();
            firstCall = false;
            return;
        }

        if (Math.toDegrees(data.getAngleToTrackAxis()) < -80
                || Math.toDegrees(data.getAngleToTrackAxis()) > 80
                || data.getSpeed() < 0 || Math.abs(data.getTrackPosition()) > 0.9) {
            if (TEXT_DEBUG) {
                System.out.println("Using onTrack");
            }
            //System.out.println("Using on track");


            onTrackRecovery.execute(data, action);
            offTrackRecovery.reset();
            stuck.reset();
            firstCall = false;

            return;
        }

        if (TEXT_DEBUG) {
            System.out.println("Defensive");
        }


        stuck.execute(data, action);
        if (stuck.isStuck()) {
            if(TEXT_DEBUG){
                System.out.println("Stuck");
            }
            

            onTrackRecovery.execute(data, action);
            offTrackRecovery.reset();
            firstCall = false;
            return;
        }


        offTrackRecovery.reset();
        onTrackRecovery.reset();

        if (s.hasError()) {
            onTrackRecovery.execute(data, action);
            offTrackRecovery.reset();
            firstCall = false;
            return;
        }

        // gear
        if (data.getGear() < 1) {
            action.setGear(1);

        } else {
            if (firstCall) {
                gearChange.reset();
            }
            gearChange.execute(data, action);
        }

        firstCall = false;

        // acc / brake
        if (s.isStraight()) {
            if (data.getSpeed() < 68.0) {
                action.setAcceleration(1.0);
            } else {
                action.setAcceleration(0);
            }
            action.setBrake(0);

        } else if (s.isStraightAC()) {
            if (data.getSpeed() < 68.0) {
                action.setAcceleration(1);
                action.setBrake(0);

            } else {
                action.setAcceleration(0);
                action.setBrake(0.5);
                if (data.getSpeed() > 150) {
                    action.setBrake(1.0);
                }
            }

        } else {
            if (data.getSpeed() < 68.0) {
                action.setAcceleration(1.0);
            } else {
                action.setAcceleration(0);
            }
            action.setBrake(0);
        }

        // steering based on the "biggest sensor value" heuristic
        if(TEXT_DEBUG){
            System.out.println("Calling Steering");
        }
        steering.setSituation(s);
        steering.execute(data, action);


        action.setClutch(0.0);

        action.limitValues();
    }

    /**
     * The current situation of the car, as classified by an appropriate
     * classifier chosen by the controller which uses this behaviour.
     */
    @Override
    public void setSituation(de.janquadflieg.mrracer.classification.Situation s) {
        this.s = s;
    }

    public void setStuck() {
        stuck.setStuck();
        onTrackRecovery.setStuck();
    }

    public void setTrackWidth(double d) {
        steering.setWidth(d);
    }

    @Override
    public void reset() {
        gearChange.reset();
        offTrackRecovery.reset();
        onTrackRecovery.reset();
        firstCall = true;
        stuck.reset();
    }

    @Override
    public void shutdown() {
        gearChange.shutdown();
        offTrackRecovery.shutdown();
        onTrackRecovery.shutdown();
    }
}