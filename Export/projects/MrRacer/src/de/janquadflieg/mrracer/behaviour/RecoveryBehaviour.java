/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.behaviour;

import de.janquadflieg.mrracer.classification.*;
import de.janquadflieg.mrracer.telemetry.*;

import java.util.*;

/**
 *
 *
 * @author Jan Quadflieg
 */
public class RecoveryBehaviour
        implements Behaviour {
    
    protected enum STATE {OFF_TRACK_BACKWARD, OFF_TRACK_FORWARD, ON_TRACK_CRITICAL};
    
    protected STATE state = STATE.ON_TRACK_CRITICAL;

    private double safetyMargin = 0.0;
    private static final boolean TEXT_DEBUG = false;
    private Situation situation;
    private Steering2015 steering;
    private double trackwidth = 0.0;    
    private StuckDetection stuck;
    public static final String OFF_TRACK = "-DFBB.offTrack-";
    
    private boolean firstCall = true;

    public RecoveryBehaviour(float[] f, StuckDetection stuck, double margin) {
        this.stuck = stuck;
        this.safetyMargin = margin;
        //onTrackRecovery = new OnTrackRecoveryBehaviour(f);
    }

    @Override
    public void setParameters(Properties params, String prefix) {
        //offTrackRecovery.setParameters(params, prefix + OFF_TRACK);
    }

    @Override
    public void getParameters(Properties params, String prefix) {
        //offTrackRecovery.getParameters(params, prefix + OFF_TRACK);
    }

    @Override
    public void paint(String baseFileName, java.awt.Dimension d) {
    }

    @Override
    public void execute(SensorData data, ModifiableAction action) {
        if (TEXT_DEBUG) {
            System.out.println(data.getDistanceRacedS() + "m ------RECOVERY_BEHAVIOR ["+state+"]--------------");
        }
        
        if(firstCall){
            // 
        }
        
        
        


        action.setClutch(0.0);

        action.limitValues();
    }

    /**
     * The current situation of the car, as classified by an appropriate
     * classifier chosen by the controller which uses this behaviour.
     */
    @Override
    public void setSituation(de.janquadflieg.mrracer.classification.Situation s) {
        this.situation = s;
        steering.setSituation(s);
    }

    

    public void setTrackWidth(double d) {
        this.trackwidth = d;
        steering.setWidth(d);
    }

    @Override
    public void reset() {
        steering.reset();
    }

    @Override
    public void shutdown() {
        steering.shutdown();        
    }
}