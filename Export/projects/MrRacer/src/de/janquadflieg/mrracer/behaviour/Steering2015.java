/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.behaviour;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.classification.*;
import de.janquadflieg.mrracer.controller.BaseController;
import de.janquadflieg.mrracer.functions.CubicSplineTwoPoints;
import de.janquadflieg.mrracer.telemetry.ModifiableAction;
import de.janquadflieg.mrracer.telemetry.SensorData;
import de.janquadflieg.mrracer.track.*;
import static de.janquadflieg.mrracer.data.CarConstants.CAR_HALF_WIDTH;
import static de.janquadflieg.mrracer.data.CarConstants.CAR_WIDTH;
import static de.janquadflieg.mrracer.data.CarConstants.CAR_LENGTH;
import static de.janquadflieg.mrracer.data.CarConstants.CAR_HALF_LENGTH;
import de.janquadflieg.mrracer.functions.GeneralCubicSpline;
import de.janquadflieg.mrracer.plan.Plan;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JComponent;
import javax.vecmath.Vector2d;

/**
 *
 * @author quad
 */
public class Steering2015
        implements SteeringBehaviour, de.janquadflieg.mrracer.gui.GraphicDebugable {

    public static final boolean TEXT_DEBUG = true;
    public static final boolean GRAPHIC_DEBUG = true;
    /**
     * The angles of the track sensors.
     */
    private float[] angles = new float[19];
    private Situation situation;
    private TrackSegment segment;
    private double trackWidth;
    private DebugPainter painter;
    private GraphicsMetaData graphicsMetaData;
    private double lastDelta = Double.NaN;
    private BaseController c;
    private double lastFactor = 1.0;
    private GeneralCubicSpline mapping;
    
    /**
     * Target position.
     */
    private final Point2D targetPosition = new Point2D.Double();

    public Steering2015(float[] f, BaseController c) {
        this.c = c;
        System.arraycopy(f, 0, angles, 0, f.length);
        if (GRAPHIC_DEBUG) {
            painter = new DebugPainter();
            graphicsMetaData = new GraphicsMetaData();
            painter.setName("Steering2015");
        }
        
        Point2D[] spoints = new Point2D[12];
        spoints[0] = new Point2D.Double(-5.0, -5.0);
        spoints[1] = new Point2D.Double(0.0, 0.0);
        spoints[2] = new Point2D.Double(5.0, 2.0);
        spoints[3] = new Point2D.Double(10.0, 3.0);
        spoints[4] = new Point2D.Double(15.0, 4.0);
        spoints[5] = new Point2D.Double(20.0, 15.0);
        spoints[6] = new Point2D.Double(25.0, 24.0);
        spoints[7] = new Point2D.Double(30.0, 31.0);
        spoints[8] = new Point2D.Double(35.0, 34.0);
        spoints[9] = new Point2D.Double(40.0, 41.0);
        spoints[10] = new Point2D.Double(45.0, 44.0);
        spoints[11] = new Point2D.Double(50.0, 50.0);
        
        mapping = new GeneralCubicSpline(spoints);
        
    }

    /**
     * First version: Calculates lines between opposite points, degenerates when
     * the track shape is asymmetric (in corners), when is indexLongest very
     * small or very high.
     *
     * @return Points
     */
    private ArrayList<Point2D> calculateConnectingPoints1(Point2D[] trackPoints,
            double targetFactor, int index) {
        ArrayList<Point2D> result = new ArrayList<>();
        for (int left = index - 1, right = index + 1; left >= 0 && right <= 18; --left, ++right) {


            Point2D p = new Point2D.Double(
                    targetFactor * (trackPoints[left].getX() + trackPoints[right].getX()),
                    targetFactor * (trackPoints[left].getY() + trackPoints[right].getY()));
            result.add(p);

        }

        return result;
    }

    /**
     * Complete version: Calculates all possible lines between opposite points.
     *
     * @return Points
     */
    private ArrayList<Point2D> calculateConnectingComplete(Point2D[] trackPoints,
            double targetFactor, int index) {
        ArrayList<Point2D> result = new ArrayList<>();
        for (int left = 0; left < index; ++left) {
            for (int right = 18; right > index; --right) {
                if (trackPoints[left].getX() < trackPoints[right].getX()) {
                    Point2D p = new Point2D.Double(
                            targetFactor * (trackPoints[left].getX() + trackPoints[right].getX()),
                            targetFactor * (trackPoints[left].getY() + trackPoints[right].getY()));
                    result.add(p);
                }
            }
        }

        return result;
    }

    /*private boolean withinCorner() {
     return (segment != null && !segment.isUnknown() && segment.isCorner())
     || situation.isCorner();
     }

     private boolean withinRightCorner() {
     return (segment != null && !segment.isUnknown() && segment.isRight())
     || situation.isRight();
     }*/
    @Override
    public void execute(SensorData data, ModifiableAction action) {
        if (GRAPHIC_DEBUG && painter != null) {
            paintInit(graphicsMetaData, data);
        }
        
        
        // steering based on the "biggest sensor value" heuristic
        double[] rawSensors = data.getRawTrackEdgeSensors();
        int index = 0;
        double longest = rawSensors[0];
        for (int i = 1; i < rawSensors.length; ++i) {
            if (rawSensors[i] > longest) {
                index = i;
                longest = rawSensors[i];
            }
        }

        if (index == 0 || index == 18) {
            action.setSteering(-angles[index] / 45.0);
            action.limitValues();
            
            return;
        }


        

        Point2D[] trackPoints = SensorData.calculateTrackPoints(data, true);

        if (GRAPHIC_DEBUG && painter != null) {
            paintTrackPoints(graphicsMetaData, trackPoints, index);
        }

        /*ArrayList<Point2D> connectionPoints;
         // Calculate the points on the connections between left and right track points
         {
         double targetFactor = 0.5;
         if (targetPosition.getX() != Plan.NO_RACE_LINE) {
         targetFactor = (-0.5 * targetPosition.getX()) + 0.5;
         }

         //connectionPoints = calculateConnectingPoints1(trackPoints, targetFactor, index);
         connectionPoints = calculateConnectingComplete(trackPoints, targetFactor, index);

         Collections.sort(connectionPoints, new java.util.Comparator<Point2D>() {
         @Override
         public int compare(Point2D o1, Point2D o2) {
         if (o1.getY() < o2.getY()) {
         return -1;
         } else if (o2.getY() < o1.getY()) {
         return 1;
         }
         return 0;
         }                
         });            
         }*/


        Vector2d targetPoint = new Vector2d();
        /*{
         final double LOOK_AHEAD = 5.0;

         double distance = 0.0;
         int deleteIndex = 0;
            
            
         for (int i = 1; i < connectionPoints.size() && distance < LOOK_AHEAD; ++i) {
         distance += connectionPoints.get(i-1).distance(connectionPoints.get(i));
         deleteIndex = i+1;
         }
            
         for(int i=connectionPoints.size()-1;i >= deleteIndex; --i){
         connectionPoints.remove(i);
         }
            
         Point2D last = connectionPoints.get(connectionPoints.size()-1);
            
         targetPoint.set(last.getX(), last.getY());
            
         if (GRAPHIC_DEBUG && painter != null) {
         paintConnectingPoints(graphicsMetaData, connectionPoints);
         }
         }*/

        Point2D left = trackPoints[index - 1];
        Point2D right = trackPoints[index + 1];
        if(index >= 2 && index <= 16){
            left = trackPoints[index - 2];
            right = trackPoints[index + 2];
        }
        Point2D trackVector = new Point2D.Double(right.getX() - left.getX(),
                right.getY() - left.getY());
        double targetFactor = 0.5;
        if (targetPosition.getX() != Plan.NO_RACE_LINE) {
            targetFactor = (-0.5 * targetPosition.getX()) + 0.5;
        }
        
        targetPoint.x = left.getX() + (targetFactor * trackVector.getX());
        targetPoint.y = left.getY() + (targetFactor * trackVector.getY());

        Vector2d reference = new Vector2d(0.0, 1.0);

        double angleToTargetVectorD = Math.toDegrees(reference.angle(targetPoint));

        double steeringAngleD = angleToTargetVectorD;
        double crossZ = (reference.getX() * targetPoint.getY())
                - (reference.getY() * targetPoint.getX());
        double sign = Math.signum(crossZ);
        steeringAngleD *= sign;


        if (GRAPHIC_DEBUG && painter != null) {
            paintTargetVector(graphicsMetaData, targetPoint);
            graphicsMetaData.g.setColor(Color.BLACK);
            graphicsMetaData.g.drawString("Angle to targetVector: " + Utils.dTS(angleToTargetVectorD), 20, 200);
        }

        /*{
            double absMeasure = Math.abs(situation.getMeasure());

            if (absMeasure >= 20.0 && absMeasure < 35.0) {
                angleD *= 1.0 + ((absMeasure - 20.0) / (35.0 - 20.0));

            } else if (absMeasure >= 35.0 && absMeasure < 60.0) {
                angleD *= 2.0 + ((absMeasure - 35.0) / (60.0 - 35.0));

            } else if (absMeasure >= 60.0) {
                angleD *= 3.0;
            }

            if (GRAPHIC_DEBUG && painter != null) {
                graphicsMetaData.g.setColor(Color.BLACK);
                graphicsMetaData.g.drawString("Measure: "+Utils.dTS(absMeasure)+", AngleD after " + Utils.dTS(angleD), 20, 220);
            }
        }*/
        
        
        
        {
            angleToTargetVectorD = Math.min(45.0, angleToTargetVectorD);
        
            steeringAngleD = mapping.interpolate(angleToTargetVectorD);
            
            steeringAngleD *= sign;

            
            if (GRAPHIC_DEBUG && painter != null) {
                int gwidth = graphicsMetaData.buffer.getWidth();
                graphicsMetaData.g.setColor(Color.BLACK);
                graphicsMetaData.g.fillRect(gwidth-220, 20, 200, 200);                
                graphicsMetaData.g.setColor(Color.WHITE);
                graphicsMetaData.g.fillRect(gwidth-219, 21, 198, 198);
                graphicsMetaData.g.setColor(Color.BLACK);
                for(int x=1; x <= 200; ++x){
                    double x0 = (x-1)/200.0*45.0;
                    double x1 = (x)/200.0*45.0;
                    double y0 = mapping.interpolate(x0);
                    double y1 = mapping.interpolate(x1);
                    graphicsMetaData.g.drawLine(gwidth-220+x-1, 220-(int)Math.round(y0/45.0*200.0),
                            gwidth-220+x, 220-(int)Math.round(y1/45.0*200.0));
                }
                graphicsMetaData.g.setColor(Color.RED);
                graphicsMetaData.g.drawLine(gwidth-220+(int)Math.round(angleToTargetVectorD/45.0*200.0), 220,
                            gwidth-220+(int)Math.round(angleToTargetVectorD/45.0*200.0), 220-(int)Math.round(23/45.0*200.0));
            }
        }



        double delta;
        if (targetPosition.getX() != Plan.NO_RACE_LINE) {
            delta = data.calcAbsoluteTrackPosition(trackWidth)
                    - SensorData.calcAbsoluteTrackPosition(targetPosition.getX(), trackWidth);
        } else {
            delta = data.calcAbsoluteTrackPosition(trackWidth)
                    - (trackWidth * 0.5);
        }




        if (GRAPHIC_DEBUG && painter != null) {
            graphicsMetaData.g.setColor(Color.BLACK);
            graphicsMetaData.g.drawString("Delta : " + Utils.dTS(delta) + "m", 20, 240);
        }


        double deltaSign = Math.signum(delta);
        delta = Math.abs(delta);
        Point2D[] spoints = new Point2D[4];
        spoints[0] = new Point2D.Double(0.0, 0.0);
        spoints[1] = new Point2D.Double(1.0, 1.0);
        spoints[2] = new Point2D.Double(1.25, 1.5);
        spoints[3] = new Point2D.Double(1.5, 2.5);
        GeneralCubicSpline spline = new GeneralCubicSpline(spoints);        
        double factor = 2.5;
        if (delta < 1.5) {
            factor = spline.interpolate(delta);
        }
        factor *= deltaSign;        
        
        //steeringAngleD += factor;
        
        if (GRAPHIC_DEBUG && painter != null) {
            graphicsMetaData.g.setColor(Color.BLACK);
            graphicsMetaData.g.drawString("Delta factor: " + Utils.dTS(factor), 20, 260);
            graphicsMetaData.g.drawString("Modified steeringAngleD: " + Utils.dTS(steeringAngleD), 20, 280);
        }
        
        




        /*if (TEXT_DEBUG && !c.isPreCompiling() && Math.abs(delta) > 0.1) {
         System.out.println("-------------------------------------------------------------------------------------------");
         System.out.println(Utils.timeToExactString(data.getCurrentLapTime()) + " @ " + data.getDistanceRacedS() + "- track: "
         + data.getTrackPositionS() + ", index: " + index);
         //System.out.println("Left: " + left);
         //System.out.println("Right: " + right);
         System.out.println("Target Position: " + targetPosition);
         //System.out.println("Target Point: " + targetPoint);
         //System.out.println("Target Vector length: " + targetPoint.length());
         if (targetPosition.getX() == Plan.NO_RACE_LINE) {
         System.out.println("No target position planned");
         }
         }*/



        /*if (TEXT_DEBUG && !c.isPreCompiling() && Math.abs(delta) > 0.1) {
         System.out.println("Delta: " + Utils.dTS(delta));
         if (!Double.isNaN(lastDelta)) {
         System.out.println("Delta': " + Utils.dTS(delta - lastDelta));
         }
         }*/

        /*if (Math.signum(delta) == Math.signum(angleD)) {
         if (TEXT_DEBUG && !c.isPreCompiling() && Math.abs(delta) > 0.1) {
         System.out.println("+++steering+++");
         }

         delta = Math.abs(delta);

         CubicSplineTwoPoints targetDeltaDerivationFunction =
         new CubicSplineTwoPoints(new double[]{0.0, 1.0}, new double[]{1.0, 3.0});

         Point2D[] spoints = new Point2D[4];
         spoints[0] = new Point2D.Double(0.0, 1.0);
         spoints[1] = new Point2D.Double(0.5, 1.5);
         spoints[2] = new Point2D.Double(1.0, 2.25);
         spoints[3] = new Point2D.Double(1.5, 4.0);



         GeneralCubicSpline spline = new GeneralCubicSpline(spoints);

         //double factor = targetDeltaDerivationFunction.interpolate(delta);
         double factor = 4.0;
         if (delta < 1.5) {
         factor = spline.interpolate(delta);
         }


         angleD *= factor;

         if (TEXT_DEBUG && !c.isPreCompiling() && Math.abs(delta) > 0.1) {
         System.out.println("Factor: " + Utils.dTS(factor));
         }

         } else {
         if (TEXT_DEBUG && !c.isPreCompiling() && Math.abs(delta) > 0.1) {
         //System.out.println("---steering---");
         }
         }*/









        steeringAngleD = Math.min(45.0, Math.max(-45.0, steeringAngleD));

        action.setSteering(steeringAngleD / 45.0);
        action.limitValues();

        if (GRAPHIC_DEBUG && painter != null) {
            painter.swapBuffers();
            painter.repaint();
        }

        /*if (TEXT_DEBUG && !c.isPreCompiling()) {
         System.out.println("Final value: " + action.getSteeringS());
         System.out.println("-------------------------------------------------------------------------------------------");
         }*/
    }

    private void paintInit(GraphicsMetaData gmd, SensorData data) {
        gmd.buffer = painter.getBackBuffer();
        gmd.g = gmd.buffer.createGraphics();

        BufferedImage buffer = gmd.buffer;
        Graphics2D g = gmd.g;

        g.setColor(Color.WHITE);
        g.fillRect(0, 0, buffer.getWidth(), buffer.getHeight());

        g.setColor(Color.BLACK);
        g.drawString("Steering2015 @" + data.getDistanceRacedS(), 20, 20);

        //g.translate(buffer.getWidth() / 2.0, buffer.getHeight() - 50);

        //g.scale(1, -1);

        double ppm = buffer.getWidth() / 30;
        gmd.ppm = ppm;

        AffineTransform transform = new AffineTransform();
        transform.translate(buffer.getWidth() / 2.0, buffer.getHeight() - 50);
        transform.scale(1, -1);
        transform.scale(ppm, ppm);


        gmd.transform = transform;

        // coordinate system
        g.draw(transform.createTransformedShape(new Line2D.Double(-15, 0, 15, 0)));
        for (int x = -15; x <= 15; x += 5) {
            g.draw(transform.createTransformedShape(new Line2D.Double(x, 1, x, -1)));
        }
        g.draw(transform.createTransformedShape(new Line2D.Double(0, 0, 0, 50)));
        for (int y = 5; y <= 50; y += 5) {
            g.draw(transform.createTransformedShape(new Line2D.Double(-1, y, 1, y)));
        }

        // CAR
        g.setColor(Color.black);
        g.draw(transform.createTransformedShape(new java.awt.geom.Rectangle2D.Double(-CAR_HALF_WIDTH, -CAR_HALF_LENGTH, CAR_WIDTH, CAR_LENGTH)));
    }

    private void paintTrackPoints(GraphicsMetaData gmd, Point2D[] trackPoints, int indexLongest) {
        Graphics2D g = gmd.g;
        AffineTransform transform = gmd.transform;

        Point2D[] trackPointsTransformed = new Point2D[trackPoints.length];
        transform.transform(trackPoints, 0, trackPointsTransformed, 0, trackPoints.length);

        for (int i = 1; i < trackPointsTransformed.length; ++i) {
            if (Utils.odd(i)) {
                g.setColor(Color.BLACK);
            } else {
                g.setColor(Color.ORANGE);
            }
            if (i == indexLongest || i == (indexLongest + 1)) {
                g.setColor(Color.red);
            }
            g.draw(new Line2D.Double(trackPointsTransformed[i - 1], trackPointsTransformed[i]));
        }
    }

    private void paintConnectingPoints(GraphicsMetaData gmd, ArrayList<Point2D> points) {
        Graphics2D g = gmd.g;
        AffineTransform transform = gmd.transform;


        g.setColor(Color.BLACK);
        g.drawString(points.size() + " connecting points", 20, 40);


        Point2D[] trackPointsTransformed = new Point2D[points.size()];
        for (int i = 0; i < points.size(); ++i) {
            trackPointsTransformed[i] = new Point2D.Double();
            transform.transform(points.get(i), trackPointsTransformed[i]);
        }

        for (int i = 1; i < trackPointsTransformed.length; ++i) {
            if (Utils.odd(i)) {
                g.setColor(Color.RED);
            } else {
                g.setColor(Color.GREEN);
            }
            g.draw(new Line2D.Double(trackPointsTransformed[i - 1], trackPointsTransformed[i]));
        }
    }

    private void paintTargetVector(GraphicsMetaData gmd, Vector2d targetVector) {
        Graphics2D g = gmd.g;
        AffineTransform transform = gmd.transform;

        g.setColor(Color.BLACK);
        g.drawString("Length of target vector: " + Utils.dTS(targetVector.length()), 20, 40);

        Point2D origin = new Point2D.Double();
        transform.transform(origin, origin);
        Point2D targetPoint = new Point2D.Double(targetVector.getX(), targetVector.getY());
        transform.transform(targetPoint, targetPoint);
        g.setColor(Color.RED);

        g.draw(new Line2D.Double(origin, targetPoint));
    }

    /**
     * The current situation of the car, as classified by an appropriate
     * classifier chosen by the controller which uses this behaviour.
     */
    @Override
    public void setSituation(Situation s) {
        this.situation = s;
    }

    /**
     * The segment of the trackmodel containing the current position of the car.
     * Might be null, if the trackmodel has not beeen initialized or unknown, if
     * the controller is still learning the track during the first lap.
     *
     * @param s The track segment.
     */
    @Override
    public void setTrackSegment(TrackSegment s) {
        this.segment = s;
    }

    /**
     * The desired target position on the track (1 left edge of the track, 0
     * middle, -1 right edge.
     *
     * @param position The position.
     */
    @Override
    public void setTargetPosition(java.awt.geom.Point2D position) {
        targetPosition.setLocation(position);
    }

    /**
     * The width of the race track in meter.
     *
     * @param width
     */
    @Override
    public void setWidth(double width) {
        trackWidth = width;
    }

    @Override
    public void reset() {
        lastDelta = Double.NaN;
        lastFactor = 1.0;
    }

    @Override
    public void shutdown() {
    }

    @Override
    public String getDebugInfo() {
        return "";
    }

    @Override
    public void setParameters(java.util.Properties params, String prefix) {
    }

    @Override
    public void getParameters(java.util.Properties params, String prefix) {
    }

    public void paint(String baseFileName, java.awt.Dimension d) {
    }

    @Override
    public JComponent[] getComponent() {
        if (GRAPHIC_DEBUG) {
            return new JComponent[]{painter};
        }
        return new JComponent[0];


    }

    private class DebugPainter
            extends javax.swing.JPanel {

        /**
         * First offscreen buffer.
         */
        private BufferedImage buffer1 = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        /**
         * Second offscreen buffer.
         */
        private BufferedImage buffer2 = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        /**
         * The front buffer.
         */
        private BufferedImage front = buffer1;
        /**
         * The back buffer.
         */
        private BufferedImage back = buffer2;

        public DebugPainter() {
            this.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentResized(ComponentEvent e) {
                    componentResizedImpl();
                }
            });
        }

        private synchronized void componentResizedImpl() {
            resetBuffers();
        }

        public BufferedImage getBackBuffer() {
            return back;
        }

        @Override
        public void paintComponent(Graphics graphics) {
            Graphics2D g = (Graphics2D) graphics;

            g.setColor(Color.WHITE);

            g.fillRect(0, 0, getWidth(), getHeight());

            synchronized (this) {
                g.drawImage(front, 0, 0, this);
            }
        }

        private synchronized void resetBuffers() {
            this.buffer1 = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
            this.buffer2 = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
            this.front = buffer1;
            this.back = buffer2;

            Graphics2D g = this.front.createGraphics();
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, getWidth(), getHeight());
            g.setColor(Color.BLACK);
            g.drawString("resetBuffers: buffer 1", 30, 30);
            g.drawString(getWidth() + " / " + getHeight(), 30, 60);


            g = this.back.createGraphics();
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, getWidth(), getHeight());
            g.setColor(Color.BLACK);
            g.drawString("resetBuffers: buffer 2", 30, 30);
            g.drawString(getWidth() + " / " + getHeight(), 30, 60);
        }

        private synchronized void swapBuffers() {
            if (front == buffer1) {
                front = buffer2;
                back = buffer1;

            } else {
                front = buffer1;
                back = buffer2;
            }
        }
    }

    private static class GraphicsMetaData {

        BufferedImage buffer;
        Graphics2D g;
        double ppm;
        AffineTransform transform;
    }

    public static void main(String[] args) {
        for (int index = 1; index < 18; ++index) {
            int left = index;
            int right = 18 - index;
            System.out.println("Index " + index + " " + (left * right) + " points");
        }
    }
}