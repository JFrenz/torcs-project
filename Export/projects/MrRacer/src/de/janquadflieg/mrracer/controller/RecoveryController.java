/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.controller;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.behaviour.*;
import de.janquadflieg.mrracer.classification.*;
import de.janquadflieg.mrracer.gui.GraphicDebugable;
import de.janquadflieg.mrracer.telemetry.*;
import de.janquadflieg.mrracer.track.*;

import java.io.*;

import java.util.*;

import javax.swing.JFileChooser;

/**
 * Controller which simply uses the recovery behavior to drive.
 * @author Jan Quadflieg
 */
public final class RecoveryController
        extends BaseController
        implements GraphicDebugable {

    /**
     * A controller to handle situations not handled by this controller.
     */
    private DefensiveFallbackBehaviour backupBehaviour;
    /** Generate log text for the telemetry? */
    private static final boolean TEXT_LOG = false;
    /** Text debug messages? */
    private static final boolean TEXT_DEBUG = false;
    /** Debug online learning? -> Don't learn a new track model! */
    private static final boolean DEBUG_WARMUP = false;
    /** Debug painter. */
    private TrackModelDebugger debugPainter;
    /** Graphical debug? */
    private static final boolean GRAPHICAL_DEBUG = true;
    /** Save debug telemetry? */
    private static final boolean SAVE_DEBUG_TELEMETRY = false;
    private Telemetry debugT;
    /** Default data path. */
    private static final String DEFAULT_PARAMETERS = "/de/janquadflieg/mrracer/data/2013_1";    
    /** Noisy classifier. */
    private AngleClassifierWithQuadraticRegression noisyClassifier = new AngleClassifierWithQuadraticRegression();    
    /** Angles used for the track edge sensor. */
    private final static float[] angles = new float[19];    
    /** Prefix for properties. */
    public static final String RECOVERY = "-MrRacer2012.Recovery-";    
    /** Start time. */
    private long controllerStartTime = 0;    
    /** Param file identifier. */
    public static final String PARAM_FILE = "Parameters";
    /** Stuck Detection. */
    private StuckDetection stuck = new StuckDetection();

    static {
        // init angles        
        for (int i = 0; i < 19; ++i) {
            angles[i] = -90 + i * 10;
        }
    }

    public RecoveryController() {
        this(null, DEFAULT_PARAMETERS, true);
    }

    public RecoveryController(Telemetry t) {
        this(t, DEFAULT_PARAMETERS, true);
    }

    public RecoveryController(Telemetry t, String s, boolean loadFromCP) {
        super(t);        

        backupBehaviour = new DefensiveFallbackBehaviour(angles);
        noise = new NoiseDetector();
        classifier = new AngleBasedClassifier(angles);       
        
        try {
            if (TEXT_DEBUG) {
                System.out.print("Loading default parameter set " + s);
            }
            InputStream in;

            if (loadFromCP) {
                if (TEXT_DEBUG) {
                    System.out.println(" from class path");
                }
                in = new Object().getClass().getResourceAsStream(s);
            } else {
                if (TEXT_DEBUG) {
                    System.out.println(" from file");
                }
                in = new FileInputStream(s);
            }

            Properties p = new Properties();
            p.load(in);
            in.close();


            //System.out.println(Utils.list(p, "\n"));

            setParameters(p);            

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        precompile();

        if (SAVE_DEBUG_TELEMETRY) {
            debugT = new Telemetry();
        }

        if(TEXT_DEBUG){
            System.out.println("##############################################");
            System.out.println("##############################################");
            System.out.println("##############################################");
            System.out.println("##############################################");
            System.out.println();
            System.out.println();
            System.out.println();
            System.out.println();
            System.out.println();
        }
    }

    @Override
    public void setParameters(Properties params) {        
        backupBehaviour.setParameters(params, RECOVERY);        
    }

    @Override
    public void getParameters(Properties params) {        
        backupBehaviour.getParameters(params, RECOVERY);        
    }

    public void paint(String baseFileName, java.awt.Dimension d) {        
    }


    @Override
    public void setStage(Stage s) {
        super.setStage(s);
    }

    @Override
    public void setTrackName(String s) {
        super.setTrackName(s);              
    }

    @Override
    public float[] initAngles() {
        return angles;
    }

    private boolean canHandle(SensorData data) {
        boolean angleOK = Math.toDegrees(data.getAngleToTrackAxis()) > -45.0 && Math.toDegrees(data.getAngleToTrackAxis()) < 45.0;

        if (wasRecovering) {
            angleOK = Math.toDegrees(data.getAngleToTrackAxis()) > -9 && Math.toDegrees(data.getAngleToTrackAxis()) < 9;
        }

        boolean speedOK = data.getSpeed() >= -0.15;

        if (!wasRecovering) {
            stuck.execute(data, null);
        }

        if (stuck.isStuck() && !wasRecovering) {
            backupBehaviour.setStuck();
        }

        if (TEXT_LOG) {
            controllerLog.append("onTrack? ");
            controllerLog.append(String.valueOf(data.onTrack()).substring(0, 1));
            controllerLog.append(", angle? ");
            controllerLog.append(String.valueOf(angleOK).substring(0, 1));
            controllerLog.append(", speed? ");
            controllerLog.append(String.valueOf(speedOK).substring(0, 1));
            controllerLog.append(", stuck? ").append(stuck.isStuck());
            System.out.println(controllerLog.toString());
        }

        return data.onTrack() && angleOK && speedOK && !stuck.isStuck();
    }

    @Override
    public scr.Action unsaveControl(scr.SensorModel model) {
        long startTime = System.nanoTime();
        controllerLog.delete(0, controllerLog.length());
        ModifiableAction action = new ModifiableAction();
        SensorData data;
        SensorData rawData;        
             
        if(trackModel.initialized()){
            data = new SensorData(model, trackModel.getWidth());
            rawData = new SensorData(model, trackModel.getWidth());
        } else {
            data = new SensorData(model, Utils.NO_DATA_D);
            rawData = new SensorData(model, Utils.NO_DATA_D);
        }

        noise.update(data);        

        if (noise.isNoisy()) {
            noise.filterNoise(data);
        }

        if (firstPacket) {
            controllerStartTime = System.currentTimeMillis();
            firstPacket = false;
        }        

        Situation s;

        if (noise.isNoisy()) {
            s = noisyClassifier.classify(data);
        } else {
            s = classifier.classify(data);
        }

        boolean wasComplete = trackModel.complete();
        trackModel.append(data, s, noise.isNoisy());
        if (!wasComplete && trackModel.complete() && TEXT_DEBUG) {
            System.out.println("TrackModel complete");
            double seconds = (System.currentTimeMillis() - controllerStartTime) / 1000.0;
            System.out.println("Learning took " + Utils.timeToString(seconds));
        }

        backupBehaviour.setSituation(s);
        backupBehaviour.execute(data, action);
        stuck.reset();
        

//        if(!this.precompile){
//            System.out.println(data.getGear()+" "+data.getCurrentLapTimeS()+" "+data.getSpeedS());//+" "+gear.getLog()+" a.gear:"+action.getGearS());
//        }

        // zum testen
        //action.setFocusAngle(-(int) Math.round(data.getAngleToTrackAxis()));

        if (telemetry != null) {
            telemetry.log(data, action, controllerLog.toString());
        }

        if (SAVE_DEBUG_TELEMETRY && debugT != null) {
            debugT.log(rawData, action, String.valueOf(s.getMeasure()));
        }

        if (TEXT_DEBUG) {
            double dT = ((double) (System.nanoTime() - startTime)) / 1000000.0;
            if (dT > 5.0 && !precompile) {
                if (TEXT_DEBUG) {
                    System.out.println("Warning, exceeding 5,0ms");
                    System.out.println(String.format("%.2fms", dT));
                }
            }
        }

        if (GRAPHICAL_DEBUG) {
            debugPainter.update(data.getDistanceFromStartLine());
        }

        return action.getRaceClientAction();
    }  

    @Override
    public javax.swing.JComponent[] getComponent() {
        return new javax.swing.JComponent[0];
    }

    private void print(String s) {
        //System.out.print(s);
    }

    private void println(String s) {
        //System.out.println(s);
    }

    @Override
    public void reset() {
        //System.out.println("RESET");

        // this prevents the loss of all warmup information when using the maxSteps
        // argument of the standard Client combined with the standard server
        RuntimeException e = new RuntimeException();
        StackTraceElement[] list = e.getStackTrace();

        //for(int i=0; i < list.length; ++i){
        //    System.out.println(i+" "+list[i].getClassName()+" "+list[i].getFileName()+
        //            list[i].getMethodName()+" "+list[i].getLineNumber());
        //}

        if(list.length > 1 && list[1].getClassName().equalsIgnoreCase("scr.Client") &&
                list[1].getMethodName().equalsIgnoreCase("main")){
            return;
        }
        
        this.resetFull();
    }

    @Override
    public void resetFull() {
        super.reset();
        backupBehaviour.reset();        
        wasRecovering = false;
        firstPacket = true;
        noise.reset();
        noisyClassifier.reset();
        stuck.reset();
    }

    @Override
    protected void functionalReset() {
        backupBehaviour.reset();        
        stuck.reset();
    }

    private void saveTrackModel(String suffix) {
        String filename = "." + java.io.File.separator + getTrackName() + suffix + TrackModel.TM_EXT;
        try {
            trackModel.setName(trackModel.getName() + suffix);
            trackModel.save(filename);

        } catch (Exception e) {
            System.out.println("Warning, failed to save the trackmodel for track " + getTrackName() + " to file \"" + filename + "\"!");
            System.out.println("Reason:");
            e.printStackTrace(System.out);
        }
        trackModel.print();
    }

    @Override
    public void shutdown() {
        backupBehaviour.shutdown();        

        //System.out.println("SHUTDOWN");

        if (SAVE_DEBUG_TELEMETRY) {
            System.out.println("Stopping telemetry");
            debugT.shutdown();
            System.out.println("Saving telemetry for debugging purposes.");
            String filename = "telemetry-" + (new Date().toString().replace(':', '-').replace(' ', '-')) + ".txt";
            debugT.save(new File(filename));
        }

        if (getStage() == Stage.WARMUP) {
            System.out.println("End of Warmup, saving learned data for track " + getTrackName() + ".");
            String suffix = "";
            if (DEBUG_WARMUP) {
                suffix = new Date().toString().replace(':', '-').replace(' ', '-');
            }
            saveTrackModel(suffix);

        } else if (getStage() == Stage.QUALIFYING || getStage() == Stage.RACE) {
            // nothing todo
        }
    }

    public static void main(String args[]) {
        try {
            JFileChooser fileChooser = new JFileChooser("f:\\quad\\svn\\TORCS");
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.setAcceptAllFileFilterUsed(true);
            fileChooser.setMultiSelectionEnabled(true);


            int result = fileChooser.showOpenDialog(null);
            if (result != JFileChooser.APPROVE_OPTION) {
                System.exit(-1);
            }

            File[] files = fileChooser.getSelectedFiles();

            for (int i = 0; i < files.length; ++i) {
                InputStream in = new FileInputStream(files[i]);

                Properties p = new Properties();
                p.load(in);
                in.close();

                RecoveryController controller = new RecoveryController();
                controller.setParameters(p);

                controller.paint(files[i].getAbsoluteFile().getPath(), new java.awt.Dimension(600, 400));
            }

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }   
}