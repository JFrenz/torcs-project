/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.controller;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.behaviour.*;
import de.janquadflieg.mrracer.classification.*;
import de.janquadflieg.mrracer.gui.GraphicDebugable;
import de.janquadflieg.mrracer.gui.TrackImagePainter;
import de.janquadflieg.mrracer.plan.Plan;
import de.janquadflieg.mrracer.telemetry.*;
import de.janquadflieg.mrracer.track.*;

import de.delbrueg.steering.behaviour.CircleSteeringBehaviour;
import de.janquadflieg.mrracer.plan.Plan2013;

import java.io.*;

import java.util.*;

import javax.swing.JFileChooser;

/**
 *
 * @author Jan Quadflieg
 */
public final class MrRacer2013
        extends BaseController
        implements GraphicDebugable {

    /**
     * A controller to handle situations not handled by this controller.
     */
    private DefensiveFallbackBehaviour backupBehaviour;
    
    private static final boolean USE_STEERING_INTERPOLATION_IN_FRONT_OF_CORNER = true;
    /**
     * Generate log text for the telemetry?
     */
    private static final boolean TEXT_LOG = false;
    /**
     * Text debug messages?
     */
    private static final boolean TEXT_DEBUG = false;
    /**
     * Debug online learning? -> Don't learn a new track model!
     */
    private static final boolean DEBUG_WARMUP = false;
    /**
     * Debug painter for the track model.
     */
    private TrackModelDebugger debugPainter;
    /**
     * Debug painter to draw an image of the track
     */
    private TrackImagePainter debugTrackImgPainter;
    /**
     * Graphical debug?
     */
    private static final boolean GRAPHICAL_DEBUG = true;
    /**
     * Save debug telemetry?
     */
    private static final boolean SAVE_DEBUG_TELEMETRY = false;
    private Telemetry debugT;
    /**
     * Default parameters.
     */
    private static final String DEFAULT_PARAMETERS = "/de/janquadflieg/mrracer/data/smsemoa3d-run09-ind-id5966.params";
    private Properties warmupParams = new Properties();
    /**
     * Parameter set used, if track specific parameter sets are disabled.
     */
    private Properties parameterSet;
    /**
     * Track specific parameter sets.
     */
    private HashMap<String, Properties> trackParameters = new HashMap<>();
    /**
     * Maximum fuel level.
     */
    //private static final double MAX_FUEL = 94.0;
    /**
     * Noisy classifier.
     */
    private AngleClassifierWithQuadraticRegression noisyClassifier = new AngleClassifierWithQuadraticRegression();
    /**
     * Plan.
     */
    public Plan plan;
    /**
     * Clutch Behaviour.
     */
    private Behaviour clutch = new Clutch();
    //private Behaviour clutch = new ClutchMulti();
    //private Behaviour clutch = new ClutchConstant();
    //private Behaviour clutch = new ClutchAutopia();
    /**
     * SteeringBehaviour
     */
    private SteeringBehaviour standardSteering = new CorrectingPureHeuristicSteering(angles);
    private SteeringBehaviour newSteering = new Steering2015(angles, this);
    //private SteeringBehaviour standardSteering = new PureHeuristicSteering(angles);
    /**
     * SteeringBehaviour.
     */
    private CircleSteeringBehaviour circleSteering = new CircleSteeringBehaviour(angles);
    //private SteeringBehaviour circleSteering = new Steering2015(angles);
    /**
     * AccelerationBehaviour.
     */
    private DampedAccelerationBehaviour acceleration = new DampedAccelerationBehaviour();
    /**
     * Gear change behaviour.
     */
    private StandardGearChangeBehaviour gear = new StandardGearChangeBehaviour();
    /**
     * Angles used for the track edge sensor.
     */
    private final static float[] angles = new float[19];
    /**
     * Prefix for properties.
     */
    public static final String PLAN = "-MrRacer2012.Plan-";
    /**
     * Prefix for properties.
     */
    public static final String ACC = "-MrRacer2012.Acc-";
    /**
     * Prefix for properties.
     */
    public static final String RECOVERY = "-MrRacer2012.Recovery-";
    /**
     * Prefix for properties.
     */
    public static final String CLUTCH = "-MrRacer2012.Clutch-";
    /**
     * Only use heuristic steering?
     */
    public static final String HEURISTIC_STEERING = "-MrRacer2012.onlyHeuristicSteering-";
    /**
     * Flag.
     */
    private boolean onlyHeuristicSteering = false;
    /**
     * Flag
     */
    private final static boolean useNewSteering = false;
    /**
     * Start time.
     */
    private long controllerStartTime = 0;
    /**
     * Speed modifier for opponent tests to make this car slower.
     */
    //private final double opponentSpeedModifier = 1.0;
    /**
     * Param file identifier. This can be used on the command line to override
     * the parameter sets within the controller and also the lookup for track
     * specific parameter sets. Usage: -DEnforceParameters=file (jvm option).
     */
    public static final String ENFORCE_PARAMETERS = "EnforceParameters";
    /**
     * Param load option identifier. This can be used to override the
     * loadFromClassPath variable used for the lookup of parameterFiles. Usage:
     * -DLoadFromClassPath=<true,false> (jvm option).
     */
    public static final String LOAD_FROM_CP = "LoadFromClassPath";
    /**
     * Stuck Detection.
     */
    private StuckDetection stuck = new StuckDetection();
    //More Speed in first round of Warmup if straight is long enough
    private static boolean FAST_WARMUP = false;

    static {
        // init angles        
        for (int i = 0; i < 19; ++i) {
            angles[i] = -90 + i * 10;
        }
    }

    public MrRacer2013() {
        this(null, DEFAULT_PARAMETERS, true);
    }

    public MrRacer2013(Telemetry t) {
        this(t, DEFAULT_PARAMETERS, true);
    }

    public MrRacer2013(Telemetry t, String parameterFile, boolean loadFromCP) {
        super(t);

        if (TEXT_DEBUG) {
            System.out.println("---------------------------------------------");
            System.out.println("System properties:");
            System.out.println(Utils.list(System.getProperties(), "\n"));
            System.out.println("---------------------------------------------");
        }

        /*if (System.getProperties().containsKey(PARAM_FILES)) {
         parameterFiles = Utils.toArrayOfStrings(System.getProperty(PARAM_FILES), ",");
         }*/

        if (System.getProperties().containsKey(LOAD_FROM_CP)) {
            if (TEXT_DEBUG) {
                System.out.println("Load from classpath option set, value =  "
                        + System.getProperty(LOAD_FROM_CP));
            }
            loadFromCP = Boolean.parseBoolean(System.getProperty(LOAD_FROM_CP));
        }

        if (System.getProperties().containsKey(ENFORCE_PARAMETERS)) {
            if (TEXT_DEBUG) {
                System.out.println("Enforce Parameters option set, replacing "
                        + "default parameter set " + parameterFile + " with "
                        + System.getProperty(ENFORCE_PARAMETERS));
            }
            parameterFile = System.getProperty(ENFORCE_PARAMETERS);
        }

        if (System.getProperties().containsKey(ENFORCE_PARAMETERS)
                && !System.getProperties().containsKey(Plan2013.ALWAYS_USE_GLF_FOR_TARGET_SPEEDS)) {
            System.out.println("WARNING: You are using the enforce paramers option without "
                    + "Plan2013.ALWAYS_USE_GLF_FOR_TARGET_SPEEDS, is this really what you wanted?!?!");
        }

        /*if (System.getProperties().containsKey(PARAM_FILES)
         && System.getProperties().containsKey(ENFORCE_PARAMETERS)) {
         System.out.println("WARNING: Two conflicting options active:"
         + PARAM_FILES + " and " + ENFORCE_PARAMETERS);
         System.out.println("Expect unforeseen consequences!");
         }*/

        backupBehaviour = new DefensiveFallbackBehaviour(angles);
        noise = new NoiseDetector();
        classifier = new AngleBasedClassifier(angles);

        plan = new de.janquadflieg.mrracer.plan.Plan2013(this);

        if (GRAPHICAL_DEBUG) {
            debugPainter = new TrackModelDebugger();
            debugPainter.setModel(trackModel);
            debugPainter.setName("TrackModel");
            debugTrackImgPainter = new TrackImagePainter();
            debugTrackImgPainter.setTrackModel(trackModel);
            debugTrackImgPainter.setName("Track Image");
        }

        //String modifier = System.getProperty("speedModifier");
        /*if(modifier != null){
         System.out.println("Modifier: "+modifier);
         opponentSpeedModifier = Double.parseDouble(modifier);
         }*/

        try {
            if (TEXT_DEBUG) {
                System.out.println("Loading warmup parameter set");
            }

            InputStream in = new Object().getClass().getResourceAsStream("/de/janquadflieg/mrracer/data/warmup.params");
            warmupParams.load(in);
            in.close();

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }


        try {
            if (TEXT_DEBUG) {
                System.out.println("Loading parameter set " + parameterFile
                        + (loadFromCP ? " from class path" : " from files"));
            }

            InputStream in;
            if (loadFromCP) {
                in = new Object().getClass().getResourceAsStream(parameterFile);
            } else {
                in = new FileInputStream(parameterFile);
            }
            parameterSet = new Properties();
            parameterSet.load(in);
            in.close();

            // Set this parameter set for the precompile process
            setParameters(parameterSet);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        try {
            if (!System.getProperties().contains(BaseController.DO_NOT_LOAD_TRACK_PARAMETERS)) {
                if (TEXT_DEBUG) {
                    System.out.println("Loading track specific parameter sets...");
                }
                File currentDirectory = new File(".");
                File[] files = currentDirectory.listFiles();

                for (File f : files) {
                    if (f.isFile() && f.getName().endsWith(BaseController.PARAMETER_EXT)) {
                        String filename = "." + java.io.File.separator + f.getName();
                        String trackName = f.getName().substring(0, f.getName().length() - BaseController.PARAMETER_EXT.length());
                        if (TEXT_DEBUG) {
                            System.out.println(filename + " - " + trackName);
                        }
                        FileInputStream in = new FileInputStream(filename);
                        Properties p = new Properties();
                        p.load(in);
                        in.close();
                        trackParameters.put(trackName, p);
                    }
                }
            } else {
                if (TEXT_DEBUG) {
                    System.out.println(BaseController.DO_NOT_LOAD_TRACK_PARAMETERS
                            + " option active, ignoring track specific parameter sets");
                }
            }

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        /*if (System.getProperties()
         .containsKey(ENFORCE_PARAMETERS)) {
         if (TEXT_DEBUG) {
         System.out.println(ENFORCE_PARAMETERS + " option active, loading"
         + " requested file " + System.getProperties().getProperty(ENFORCE_PARAMETERS));
         }

         try (FileInputStream in = new FileInputStream(System.getProperties().getProperty(ENFORCE_PARAMETERS))) {
         Properties p = new Properties();
         p.load(in);
         parameterSets.clear();
         parameterSets.add(p);

         } catch (Exception e) {
         e.printStackTrace(System.out);
         }
         }*/
        if (TEXT_DEBUG) {
            System.out.println("############### PRECOMPILE START ############");
        }

        precompile();
        if (TEXT_DEBUG) {
            System.out.println("############### PRECOMPILE END ##############");
            System.out.println("");
        }
        if (SAVE_DEBUG_TELEMETRY) {
            debugT = new Telemetry();
        }
    }

    @Override
    public void setParameters(Properties params) {
        acceleration.setParameters(params, ACC);
        plan.setParameters(params, PLAN);
        backupBehaviour.setParameters(params, RECOVERY);
        clutch.setParameters(params, CLUTCH);

        if (params.containsKey(HEURISTIC_STEERING)) {
            onlyHeuristicSteering = Boolean.parseBoolean(params.getProperty(HEURISTIC_STEERING));
        }
        //new RuntimeException().printStackTrace();
        //System.out.println("onlyHeuristicSteering="+onlyHeuristicSteering);
    }

    @Override
    public void getParameters(Properties params) {
        //System.out.println("Parameters in use:");
        //params.list(System.out);
        acceleration.getParameters(params, ACC);
        plan.getParameters(params, PLAN);
        backupBehaviour.getParameters(params, RECOVERY);
        clutch.getParameters(params, CLUTCH);

        params.setProperty(HEURISTIC_STEERING, String.valueOf(onlyHeuristicSteering));
    }

    public void paint(String baseFileName, java.awt.Dimension d) {
        plan.paint(baseFileName, d);
        acceleration.paint(baseFileName, d);
        clutch.paint(baseFileName, d);
    }

    /*@Override
     public int getParameterSetCount() {
     return parameterSets.size();
     }

     @Override
     public void selectParameterSet(int i) {
     if (i >= 0 && i < parameterSets.size()) {
     setParameters(parameterSets.get(i));
     }
     }*/
    @Override
    public void setStage(Stage s) {
        super.setStage(s);
        plan.setStage(s);
    }

    @Override
    public void setTrackName(String s) {
        super.setTrackName(s);

        if (getStage() == Stage.WARMUP) {
            if (DEBUG_WARMUP) {
                if (!precompile && TEXT_DEBUG) {
                    System.out.println("Warmup, debug mode, getting trackmodel for track " + getTrackName() + ".");
                }
                TrackModel result = trackDB.getByName(getTrackName());
                if (result == null) {
                    if (!precompile && TEXT_DEBUG) {
                        System.out.println("Warning, failed to get the trackmodel for track " + getTrackName() + ".");
                    }
                } else {
                    trackModel = result;
                }

            } else {
                if (!precompile && TEXT_DEBUG) {
                    System.out.println("Warmup, learning the trackmodel for track " + getTrackName() + ".");
                    System.out.println("Selecting warmup parameter set");
                }
                trackModel = new TrackModel(getTrackName());
                setParameters(warmupParams);
            }

        } else if (getStage() == Stage.QUALIFYING || getStage() == Stage.RACE) {
            if (!precompile && TEXT_DEBUG) {
                System.out.println("Qualifying or race, getting trackmodel for track " + getTrackName() + ".");
            }

            TrackModel result = trackDB.getByName(getTrackName());

            if (result == TrackDB.UNKNOWN_MODEL) {
                if (!precompile) {
                    System.out.println("Warning, failed to get the trackmodel for track " + getTrackName() + ".");
                }
                trackModel = new TrackModel(getTrackName());

            } else {
                trackModel = result;
            }

            if (System.getProperties().containsKey(ENFORCE_PARAMETERS)) {
                if (!precompile && TEXT_DEBUG) {
                    System.out.println("Using the given parameter file " + System.getProperty(ENFORCE_PARAMETERS));
                }
                setParameters(parameterSet);
                if (!precompile && TEXT_DEBUG) {
                    System.out.println(Utils.list(parameterSet, "\n"));
                }

            } else if (!System.getProperties().containsKey(BaseController.DO_NOT_LOAD_TRACK_PARAMETERS)) {
                Properties p = trackParameters.get(getTrackName());
                if (p == null && !precompile) {
                    System.out.println("Warning, failed to find a parameter set selected during warmup, using default");
                    setParameters(parameterSet);
                }
                if (p != null) {
                    setParameters(p);
                }
            }

        } else {
            System.out.println("Warning, unknown stage, setting stage to warmup");
            System.out.println("Stage given was: " + getStage());
            setStage(Stage.WARMUP);
            println("Warmup, learning the trackmodel for track " + trackModel.getName() + ".");
            trackModel = new TrackModel(getTrackName());
        }

        setTrackModel(trackModel);
    }

    public void setTrackModel(TrackModel m) {
        trackModel = m;

        plan.setTrackModel(this.trackModel);

        if (GRAPHICAL_DEBUG && !isPreCompiling()) {
            debugPainter.setModel(this.trackModel);
            debugPainter.repaint();
            debugTrackImgPainter.setTrackModel(trackModel);
            debugTrackImgPainter.setTrack(getTrackName());
            debugTrackImgPainter.repaint();
        }
    }

    @Override
    public float[] initAngles() {
        return angles;
    }

    private boolean canHandle(SensorData data) {
        if(data.getCurrentLapTime() <= 3){
            return true;
        }
        
        boolean angleOK = Math.toDegrees(data.getAngleToTrackAxis()) > -45.0 && Math.toDegrees(data.getAngleToTrackAxis()) < 45.0;

        if (wasRecovering) {
            angleOK = Math.toDegrees(data.getAngleToTrackAxis()) > -9 && Math.toDegrees(data.getAngleToTrackAxis()) < 9;
        }

        boolean speedOK = data.getSpeed() >= 10;

        if (!wasRecovering) {
            stuck.execute(data, null);
        }

        if (stuck.isStuck() && !wasRecovering) {
            backupBehaviour.setStuck();
        }

        if (TEXT_LOG) {
            controllerLog.append("onTrack? ");
            controllerLog.append(String.valueOf(data.onTrackCompletely(0.0)).substring(0, 1));
            controllerLog.append(", angle? ");
            controllerLog.append(String.valueOf(angleOK).substring(0, 1));
            controllerLog.append(", speed? ");
            controllerLog.append(String.valueOf(speedOK).substring(0, 1));
            controllerLog.append(", stuck? ").append(stuck.isStuck());
            System.out.println(data.getDistanceRacedS()+" "+controllerLog.toString());
        }

        return data.onTrackCompletely(0.0) && angleOK && speedOK && !stuck.isStuck();
    }

    @Override
    public scr.Action unsaveControl(scr.SensorModel model) {
        long startTime = System.nanoTime();
        controllerLog.delete(0, controllerLog.length());
        ModifiableAction action = new ModifiableAction();
        SensorData data;
        SensorData rawData;

        if (trackModel.initialized()) {
            data = new SensorData(model, trackModel.getWidth());
            rawData = new SensorData(model, trackModel.getWidth());
        } else {
            data = new SensorData(model, Utils.NO_DATA_D);
            rawData = new SensorData(model, Utils.NO_DATA_D);
        }

        noise.update(data);

        circleSteering.setNoisy(noise.isNoisy());

        if (noise.isNoisy()) {
            noise.filterNoise(data);
        }

        if (firstPacket) {
            controllerStartTime = System.currentTimeMillis();
            firstPacket = false;
        }

        if (noise.isNoisy() && data.getCurrentLapTime() < 0.0 && getStage() == Stage.WARMUP) {
            trackModel.adjustWidth(noise.getWidth());
        }

        Situation s;

        if (noise.isNoisy()) {
            s = noisyClassifier.classify(data);
        } else {
            s = classifier.classify(data);
        }

        boolean wasComplete = trackModel.complete();
        trackModel.append(data, s, noise.isNoisy());
        if (!wasComplete && trackModel.complete() && TEXT_DEBUG) {
            System.out.println("TrackModel complete");
            double seconds = (System.currentTimeMillis() - controllerStartTime) / 1000.0;
            System.out.println("Learning took " + Utils.timeToString(seconds));
        }

        if (canHandle(data)) {
            controllerLog.delete(0, controllerLog.length());
            backupBehaviour.reset();
            if (wasRecovering) {
                plan.functionalReset();
                newSteering.reset();
                noisyClassifier.reset();
                noise.clearBuffer();
                gear.reset();
                stuck.reset();
            }
            wasRecovering = false;

            TrackSegment current = null;
            TrackSegment next = null;

            if (trackModel.initialized()) {
                int index = trackModel.getIndex(data.getDistanceFromStartLine());
                current = trackModel.getSegment(index);
                next = trackModel.getSegment(trackModel.incIdx(index));

                if (TEXT_LOG) {
                    logTrackSegment(data, index, current);
                }
            }

            if (s.hasError()) {
                if (TEXT_LOG) {
                    controllerLog.append("?");
                }
                if (TEXT_DEBUG) {
                    System.out.println("Classifier error: " + s.toString());
                }

            } else {
                if (TEXT_LOG) {
                    logSituation(s);
                }
            }

            action.reset();

            plan.update(data, s);

            gear.setSituation(s);
            gear.execute(data, action);


            acceleration.setSituation(s);
            acceleration.setWidth(trackModel.getWidth());
            //double speedModifier = 1.0 - (0.1 * Math.max(Math.min((MAX_FUEL - data.getFuelLevel()) / MAX_FUEL, 1.0), 0.0));
            //speedModifier *= opponentSpeedModifier;

            acceleration.setTargetSpeed(plan.getTargetSpeed());
            //System.out.println("Trackmodel vollständig? "+ trackModel.complete() );
            if (FAST_WARMUP && getStage() == Stage.WARMUP && current != null
                    && current.isUnknown()) {
                double[] sensors = data.getTrackEdgeSensors();
                double[] copy = new double[sensors.length];
                System.arraycopy(sensors, 0, copy, 0, sensors.length);
                java.util.Arrays.sort(copy);

                if (copy[copy.length - 1] >= 50.0) {
                    acceleration.setTargetSpeed(0.75 * copy[copy.length - 1] + 0.25 * copy[copy.length - 2]);
                }
            }

            acceleration.setTrackSegment(current);
            acceleration.execute(data, action);

            if (useNewSteering) {
                SteeringBehaviour steering = newSteering;
                steering.setSituation(s);
                steering.setWidth(trackModel.getWidth());
                steering.setTargetPosition(plan.getTargetPosition());
                steering.setTrackSegment(current);
                steering.execute(data, action);
                
            } else {
                SteeringBehaviour steering = circleSteering;
                double weight = 0.0;
                double standardSteeringValue = 0.0;
                if (plan.getTargetPosition().getX() == Plan.NO_RACE_LINE || onlyHeuristicSteering) {
                    steering = standardSteering;

                } else if (USE_STEERING_INTERPOLATION_IN_FRONT_OF_CORNER
                        && plan.getTargetPosition().getX() != Plan.NO_RACE_LINE && !onlyHeuristicSteering
                        && trackModel.complete() && current != null && next != null && next.isCorner()
                        && current.getEnd() - data.getDistanceFromStartLine() <= 10.0) {

                    weight = (10 - (current.getEnd() - data.getDistanceFromStartLine())) / 10.0;

                    if (!precompile && TEXT_DEBUG) {
                        System.out.println(Utils.dTS(data.getDistanceRaced()) + " Close to corner, interpolating steering " + Utils.dTS(weight));
                    }

                    standardSteering.setSituation(s);
                    standardSteering.setWidth(trackModel.getWidth());
                    standardSteering.setTargetPosition(plan.getTargetPosition());
                    standardSteering.setTrackSegment(current);
                    standardSteering.execute(data, action);
                    standardSteeringValue = action.getSteering();
                }

                steering.setSituation(s);
                steering.setWidth(trackModel.getWidth());
                steering.setTargetPosition(plan.getTargetPosition());
                steering.setTrackSegment(current);
                steering.execute(data, action);

                if (weight > 0.0) {
                    if (!precompile && TEXT_DEBUG) {
                        System.out.print(Utils.dTS(weight) + " * " + Utils.dTS(standardSteeringValue)
                                + " + " + Utils.dTS(1.0 - weight) + " * " + Utils.dTS(action.getSteering()));
                    }
                    double weightedSteering = (weight * standardSteeringValue)
                            + ((1.0 - weight) * action.getSteering());
                    action.setSteering(weightedSteering);
                    if (!precompile && TEXT_DEBUG) {
                        System.out.println(" = " + Utils.dTS(action.getSteering()));
                    }
                }

            }

            acceleration.execute(data, action);

            clutch.execute(data, action);

            if (TEXT_LOG) {
                controllerLog.append("-");
            }

        } else {
            plan.updateOffTrack(data);
            wasRecovering = true;
            backupBehaviour.setSituation(s);
            if (trackModel.initialized()) {
                backupBehaviour.setTrackWidth(trackModel.getWidth());
            } else {
                backupBehaviour.setTrackWidth(10.0);
            }
            backupBehaviour.execute(data, action);
            stuck.reset();
        }

//        if(!this.precompile){
//            System.out.println(data.getGear()+" "+data.getCurrentLapTimeS()+" "+data.getSpeedS());//+" "+gear.getLog()+" a.gear:"+action.getGearS());
//        }

        // zum testen
        //action.setFocusAngle(-(int) Math.round(data.getAngleToTrackAxis()));

        if (telemetry != null) {
            telemetry.log(data, action, controllerLog.toString());
        }

        if (SAVE_DEBUG_TELEMETRY && debugT != null) {
            debugT.log(rawData, action, String.valueOf(s.getMeasure()));
        }

        if (TEXT_DEBUG) {
            double dT = ((double) (System.nanoTime() - startTime)) / 1000000.0;
            if (dT > 5.0 && !precompile) {
                if (TEXT_DEBUG) {
                    System.out.println("Warning, exceeding 5ms");
                    System.out.println(String.format("%.2fms", dT));
                }
            }
        }

        /*if (!precompile && tcounter == 0) {
         try {
         Thread.sleep(100);
         ++tcounter;
         } catch (Exception e) {
         e.printStackTrace(System.out);
         }
         }*/

        if (GRAPHICAL_DEBUG) {
            debugPainter.update(data.getDistanceFromStartLine());
        }

        return action.getRaceClientAction();
    }

    @Override
    public javax.swing.JComponent[] getComponent() {
        if (GRAPHICAL_DEBUG) {
            javax.swing.JComponent[] planComponents = plan.getComponent();
            javax.swing.JComponent[] steeringComponents = new javax.swing.JComponent[0];
            if(useNewSteering){
                steeringComponents = ((GraphicDebugable)newSteering).getComponent();
            }
            
            javax.swing.JComponent[] result = new javax.swing.JComponent[planComponents.length + steeringComponents.length+ 2];

            System.arraycopy(planComponents, 0, result, 0, planComponents.length);
            System.arraycopy(steeringComponents, 0, result, planComponents.length, steeringComponents.length);
            result[result.length - 2] = debugPainter;
            result[result.length - 1] = debugTrackImgPainter;

            return result;

        } else {
            return new javax.swing.JComponent[0];
        }
    }

    private void print(String s) {
        //System.out.print(s);
    }

    private void println(String s) {
        //System.out.println(s);
    }

    @Override
    public void reset() {
        //System.out.println("RESET");

        // this prevents the loss of all warmup information when using the maxSteps
        // argument of the standard Client combined with the standard server
        RuntimeException e = new RuntimeException();
        StackTraceElement[] list = e.getStackTrace();

        //for(int i=0; i < list.length; ++i){
        //    System.out.println(i+" "+list[i].getClassName()+" "+list[i].getFileName()+
        //            list[i].getMethodName()+" "+list[i].getLineNumber());
        //}

        if (list.length > 1 && list[1].getClassName().equalsIgnoreCase("scr.Client")
                && list[1].getMethodName().equalsIgnoreCase("main")) {
            return;
        }

        this.resetFull();
    }

    @Override
    public void resetFull() {
        super.reset();
        backupBehaviour.reset();
        gear.reset();
        acceleration.reset();
        circleSteering.reset();
        standardSteering.reset();
        wasRecovering = false;
        firstPacket = true;
        plan.resetFull();
        noise.reset();
        noisyClassifier.reset();
        clutch.reset();
        stuck.reset();
    }

    @Override
    protected void functionalReset() {
        backupBehaviour.reset();
        gear.reset();
        acceleration.reset();
        circleSteering.reset();
        standardSteering.reset();
        plan.functionalReset();
        clutch.reset();
        stuck.reset();
    }

    private void saveTrackModel(String suffix) {
        String filename = "." + java.io.File.separator + getTrackName() + suffix + TrackModel.TM_EXT;
        try {
            trackModel.setName(trackModel.getName() + suffix);
            trackModel.save(filename);

        } catch (Exception e) {
            System.out.println("Warning, failed to save the trackmodel for track " + getTrackName() + " to file \"" + filename + "\"!");
            System.out.println("Reason:");
            e.printStackTrace(System.out);
        }
        trackModel.print();
    }

    @Override
    public void shutdown() {
        backupBehaviour.shutdown();
        gear.shutdown();
        acceleration.shutdown();
        circleSteering.shutdown();
        standardSteering.shutdown();
        clutch.shutdown();

        //System.out.println("SHUTDOWN");

        if (SAVE_DEBUG_TELEMETRY) {
            System.out.println("Stopping telemetry");
            debugT.shutdown();
            System.out.println("Saving telemetry for debugging purposes.");
            String filename = "telemetry-" + (new Date().toString().replace(':', '-').replace(' ', '-')) + ".txt";
            debugT.save(new File(filename));
        }

        if (getStage() == Stage.WARMUP) {
            System.out.println("End of Warmup, saving learned data for track " + getTrackName() + ".");
            String suffix = "";
            if (DEBUG_WARMUP) {
                suffix = new Date().toString().replace(':', '-').replace(' ', '-');
            }
            plan.saveOnlineData(suffix);
            saveTrackModel(suffix);

        } else if (getStage() == Stage.QUALIFYING || getStage() == Stage.RACE) {
            // nothing todo
        }

        if (!System.getProperties().containsKey(BaseController.DO_NOT_LOAD_TRACK_PARAMETERS)) {
            System.out.println("MrRacer2013 by Team Dortmund");
            System.out.println("Team Dortmund: Jan Quadflieg, Tim Delbruegger, Kai Verlage and Mike Preuss");
            System.out.println("Thanks to Michael Flanagan for his Java Scientific Library");
            if (GRAPHICAL_DEBUG) {
                System.out.println("Thanks to http://cemagraphics.deviantart.com for Car Icon");
            }
        }

        /*Properties p = new Properties();
         this.getParameters(p);
         p.list(System.out);*/
    }

    public static void main(String args[]) {
        try {
            JFileChooser fileChooser = new JFileChooser("f:\\quad\\svn\\TORCS");
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.setAcceptAllFileFilterUsed(true);
            fileChooser.setMultiSelectionEnabled(true);


            int result = fileChooser.showOpenDialog(null);
            if (result != JFileChooser.APPROVE_OPTION) {
                System.exit(-1);
            }

            File[] files = fileChooser.getSelectedFiles();

            for (int i = 0; i < files.length; ++i) {
                InputStream in = new FileInputStream(files[i]);

                Properties p = new Properties();
                p.load(in);
                in.close();

                MrRacer2013 controller = new MrRacer2013();
                controller.setParameters(p);

                controller.paint(files[i].getAbsoluteFile().getPath(), new java.awt.Dimension(600, 400));
            }

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
}