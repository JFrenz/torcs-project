/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.controller;

import scr.Controller;

/**
 *
 * @author quad
 */
public class DebugTorcsController 
extends BaseController{
    
    int counter = 0;
    
    public DebugTorcsController(de.janquadflieg.mrracer.telemetry.Telemetry t) {
        super(t);        
    }
    
    public scr.Action unsaveControl(scr.SensorModel m){
        scr.Action result = new scr.Action();
        result.accelerate = 0.0;
        result.steering = 0.0;
        result.brake = 1.0;
        result.gear = 0;
        result.restartRace = false;
        
        ++counter;
        
        if(counter > 20*50){
            result.restartRace = true;
        }
        
        return result;
    }
    
    public void reset(){
        
    }
    
    public void shutdown(){
        
    }    
}