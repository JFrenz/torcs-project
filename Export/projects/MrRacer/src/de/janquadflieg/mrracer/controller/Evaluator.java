/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.controller;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.telemetry.*;
import de.janquadflieg.mrracer.track.*;

/**
 *
 * @author Jan Quadflieg
 */
public class Evaluator
        extends scr.Controller
        implements de.janquadflieg.mrracer.evo.EvaluationData {

    /**
     * Constant indicating no maximum.
     */
    public static final int NO_MAXIMUM = Integer.MIN_VALUE;
    /**
     * The controller to evaluate.
     */
    private BaseController controller;
    /**
     * Distance raced.
     */
    private double distanceRaced = 0;
    /**
     * Number of gameTicks off the track.
     */
    private int offTrackCtr = 0;
    /**
     * Damage of the car.
     */
    private double damage = 0;
    /**
     * Damage of the car.
     */
    private double maxDamage = Utils.NO_DATA_D;
    /**
     * Number of gameticks after which a restart should be requested.
     */
    private int maxGameTicks = 0;
    /**
     * Counter for gameticks.
     */
    private int gameTickCtr = 0;
    /**
     * Counter for laps.
     */
    private int lapCtr = -1;
    /**
     * Number of laps after which a restart should be requested.
     */
    private int maxLaps = NO_MAXIMUM;
    /**
     * Last sensor data.
     */
    private SensorData lastData = null;
    /**
     * Time in seconds for the fastest lap;
     */
    private double fastestLap = 60 * 100;
    private double time = 0.0;
    private double sumTime = 0.0;
    private double latSpeedIntegral = 0.0;
    private boolean aborted = false;
    private boolean stop = false;
    /**
     * Counter for the number of overtakings.
     */
    private int overtakingCtr = 0;
    /**
     * Trackmodel.
     */
    private TrackModel trackModel = null;
    /**
     * Collection of track models.
     */
    private TrackDB trackDB;
    /**
     * How many gameticks have we been above the target speed?
     */
    private int[] tsData;
    private de.janquadflieg.mrracer.plan.Plan plan = null;

    public Evaluator(BaseController c, int i) {
        this.controller = c;
        this.maxGameTicks = i;
        trackDB = TrackDB.create();

        if (c instanceof de.janquadflieg.mrracer.controller.MrRacer2013) {
            plan = ((de.janquadflieg.mrracer.controller.MrRacer2013) c).plan;
        }
    }

    public void setMaxLaps(int d) {
        this.maxLaps = d;
    }

    @Override
    public void setStage(Stage s) {
        controller.setStage(s);
    }

    @Override
    public void setTrackName(String s) {
        controller.setTrackName(s);

        System.out.println(s);

        trackModel = trackDB.getByName(s);

        System.out.println(trackModel);

        if (trackModel != null) {
            tsData = new int[trackModel.size()];
            java.util.Arrays.fill(tsData, 0);
        }
    }

    public void setAborted() {
        System.out.println("-----------Torcs crashed-------------");
        postEvaluate();
        System.out.println("-------------------------------------");
    }

    private void postEvaluate() {
        //System.out.println("lastData null? " + (lastData==null));
        //System.out.println("trackmodel null? "+ (controller.trackModel == null));
        if (maxLaps != NO_MAXIMUM && lapCtr != maxLaps) {
            System.out.println("Abort@ Laps[" + lapCtr + "/" + (maxLaps != NO_MAXIMUM ? String.valueOf(maxLaps) : "No Maximum") + "] "
                    + "Ticks[" + gameTickCtr + "/" + (maxGameTicks != NO_MAXIMUM ? String.valueOf(maxGameTicks) : "No Maximum") + "] "
                    + "Damage: " + lastData.getDamage() + " "
                    + "Time raced: " + Utils.timeToExactString(time));
            double overallDist = (maxLaps * controller.trackModel.getLength())
                    + ((lastData.getDistanceRaced() - lastData.getDistanceFromStartLine()) % controller.trackModel.getLength());
            System.out.println(maxLaps + " Laps: " + Utils.dTS(maxLaps * controller.trackModel.getLength())
                    + ", " + Utils.dTS(overallDist));
            System.out.print("Adjusting time (Max laps not reached), distance["
                    + Utils.dTS(Math.max(1.0, lastData.getDistanceRaced())) + "/" + Utils.dTS(overallDist) + "] " + Utils.timeToExactString(time));
            double ratio = Math.max(1.0, lastData.getDistanceRaced()) / overallDist;
            time *= 1.0 / ratio;
            System.out.println(" -> " + Utils.timeToExactString(time));
            aborted = true;
        }
        
        System.out.println("Stopped? " + stop);
    }

    // Methods for the new interface
    @Override
    public double getAdjustedTime(int byWhat) {
        double result = time;

        if ((byWhat & BY_DAMAGE) == BY_DAMAGE) {
            System.out.print("Adjusting time according to " + lastData.getDamageS() + " damage " + Utils.timeToExactString(result));
            //time *= (1.0 + (2.0 * (lastData.getDamage() / 5000.0)));
            result += lastData.getDamage() / 100.0;
            System.out.println(" -> " + Utils.timeToExactString(result));
        }

        if ((byWhat & BY_LAT_SPEED) == BY_LAT_SPEED) {
            System.out.print("Adjusting time further according to " + Utils.dTS(latSpeedIntegral) + " lateralSpeedIntegral " + Utils.timeToExactString(result));
            result += latSpeedIntegral / 1000.0;
            System.out.println(" -> " + Utils.timeToExactString(result));
        }

        return result;
    }

    public double getDamage() {
        return this.damage;
    }

    @Override
    public double getDistanceRaced() {
        return this.distanceRaced;
    }

    public double getFastestLap() {
        return this.fastestLap;
    }

    public int getLapCtr() {
        return lapCtr;
    }

    public double getLateralSpeedIntegral() {
        return latSpeedIntegral;
    }

    @Override
    public double getOverallTime() {
        return this.time;
    }

    public int getOvertakingCtr() {
        return overtakingCtr;
    }

    /*    result.damage = controller.getDamage();
     result.distance = controller.getDistanceRaced();
     result.offTrack = controller.getOffTrackCtr();*/
    /*public boolean aborted() {
     return aborted;
     }

      

     public int getGameTickCtr() {
     return gameTickCtr;
     }

     

     

     public int getOffTrackCtr() {
     return this.offTrackCtr;
     }

     */
    @Override
    public scr.Action control(scr.SensorModel m) {
        if (aborted) {
            scr.Action action = new scr.Action();
            action.restartRace = true;
            return action;
        }

        SensorData data = new SensorData(m, Utils.NO_DATA_D);
        ++gameTickCtr;
        if (lastData != null && data.getDistanceFromStartLine() < lastData.getDistanceFromStartLine()
                && data.getDistanceRaced() > lastData.getDistanceRaced()) {
            ++lapCtr;
            if (lapCtr > 0) {
                fastestLap = Math.min(fastestLap, data.getLastLapTime());
                sumTime += data.getLastLapTime();
                //System.out.println(sumTime);
                //System.out.println(fastestLap);
            }
        }

        if (lastData != null) {
            latSpeedIntegral += Math.abs(data.getLateralSpeed())
                    * Math.abs(data.getDistanceRaced() - lastData.getDistanceRaced());
        }

        if (lastData != null && data.getRacePosition() != lastData.getRacePosition()) {
            // question? record absolute values?
            overtakingCtr += lastData.getRacePosition() - data.getRacePosition();
        }

        time = sumTime + data.getCurrentLapTime();

        if (!data.onTrack()) {
            ++offTrackCtr;
        }

        distanceRaced = data.getDistanceRaced();
        damage = data.getDamage();

        if (trackModel != null && lapCtr == 1) {
            int index = trackModel.getIndex(data.getDistanceFromStartLine());
            TrackSegment segment = trackModel.getSegment(index);

            if (segment.isCorner() && plan != null) {
                if (plan.getTargetSpeed() < data.getSpeed()) {
                    tsData[index]++;
                }
            }
        }

        scr.Action action = controller.control(m);

        boolean stopMaxTicks = maxTicksReached();
        boolean stopMaxDamage = maxDamageReached();
        boolean stopMaxLaps = maxLapsReached();

        // end of evaluation?
        if (stopMaxTicks || stopMaxDamage || stopMaxLaps || stop) {
            action.restartRace = true;
            postEvaluate();
        }

        lastData = data;

        return action;
    }

    private boolean maxTicksReached() {
        boolean result = (maxGameTicks != NO_MAXIMUM) && gameTickCtr >= maxGameTicks;
        if (result) {
            System.out.println("Reached MaxTicks");
        }
        return result;
    }

    public boolean maxDamageReached() {
        boolean result = (maxDamage != Utils.NO_DATA_D) && damage > maxDamage;
        if (result) {
            System.out.println("Reached MaxDamage");
        }
        return result;
    }

    private boolean maxLapsReached() {
        boolean result = (maxLaps != NO_MAXIMUM) && lapCtr == maxLaps;
        if (result) {
            System.out.println("Reached MaxLaps");
        }
        return result;
    }

    public void resetFull() {
        controller.resetFull();
    }

    @Override
    public void reset() {
        controller.reset();
    }

    @Override
    public void shutdown() {
        controller.shutdown();
        if (tsData != null && trackModel != null) {
            for (int i = 0; i < trackModel.size(); ++i) {
                TrackSegment segment = trackModel.getSegment(i);
                if (segment.isCorner()) {
                    if (tsData[i] == 0) {
                        System.out.println("Segment " + segment.toString(true));
                        System.out.println("Not important");
                    } else {
                        System.out.println("Segment " + segment.toString(true));
                        System.out.println("Important");
                    }
                }
            }
        }
    }

    public void stop() {
        stop = true;
    }
}