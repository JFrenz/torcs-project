/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;

/**
 *
 * @author quad
 */
public class DebugJPanel
    extends javax.swing.JPanel {        
        /**
         * First offscreen buffer.
         */
        private BufferedImage buffer1 = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        /**
         * Second offscreen buffer.
         */
        private BufferedImage buffer2 = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        /**
         * The front buffer.
         */
        private BufferedImage front = buffer1;
        /**
         * The back buffer.
         */
        private BufferedImage back = buffer2;

        public DebugJPanel(String name){
            setName(name);
            this.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentResized(ComponentEvent e) {
                    resetBuffers();
                }
            });
        }   
        
        public Graphics2D getBackbufferGraphics(){
            return back.createGraphics();
        }

        @Override
        public void paintComponent(Graphics graphics) {
            Graphics2D g = (Graphics2D) graphics;

            g.setColor(Color.WHITE);

            g.fillRect(0, 0, getWidth(), getHeight());

            synchronized (this) {
                g.drawImage(front, 0, 0, this);
            }
        }

        private synchronized void resetBuffers() {
            this.buffer1 = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
            this.buffer2 = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
            this.front = buffer1;
            this.back = buffer2;

            Graphics2D g = this.front.createGraphics();
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, getWidth(), getHeight());
            g.setColor(Color.BLACK);
            g.drawString("resetBuffers: buffer 1", 30, 30);
            g.drawString(getWidth() + " / " + getHeight(), 30, 60);


            g = this.back.createGraphics();
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, getWidth(), getHeight());
            g.setColor(Color.BLACK);
            g.drawString("resetBuffers: buffer 2", 30, 30);
            g.drawString(getWidth() + " / " + getHeight(), 30, 60);
        }

        public synchronized void swapBuffers() {
            if (front == buffer1) {
                front = buffer2;
                back = buffer1;

            } else {
                front = buffer1;
                back = buffer2;
            }
        }        
    }