/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.gui;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.functions.GeneralisedLogisticFunction;
import de.janquadflieg.mrracer.track.TrackModel;
import de.janquadflieg.mrracer.track.TrackSegment;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.imageio.ImageIO;

/**
 *  Class to show an image of the racetrack with additional information.
 * 
 * @author quad
 */
public class TrackImagePainter
extends javax.swing.JPanel {
        private BufferedImage img = null;
        private static final double OFFSET = 0.1;
        private Point2D offset = new Point2D.Double();
        private Point pixOffset = new Point();
        private int imgWidth;
        private int imgHeight;
        private int width;
        private int height;
        private int drawWidth;
        private int drawHeight;
        private double ratioPanel;
        private double ratioImg;
        private double ratioImgArea;
        private Rectangle imgArea = new Rectangle();
        private Point mouseClick = new Point(-1, -1);
        private ArrayList<Point2D> cornerPoints = new ArrayList<>();
        private ArrayList<Point2D> labelPoints = new ArrayList<>();
        private ArrayList<Point2D> target = labelPoints;
        
        private TrackModel trackModel;
        
        private GeneralisedLogisticFunction function = null;        
        
        private static final boolean DEFINE_POINTS = false;

        public TrackImagePainter() {
            super();           

            setPreferredSize(new Dimension(400, 300));

            addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if(!DEFINE_POINTS){
                        return;
                    }
                    mouseClick.setLocation(e.getX(), e.getY());                    

                    if (e.getButton() == MouseEvent.BUTTON1) {
                        Point2D p = toNormalizedImgCoords(mouseClick.x, mouseClick.y);
                        target.add(p);


                    } else if (e.getButton() == MouseEvent.BUTTON3 && !target.isEmpty()) {
                        target.remove(target.size() - 1);
                    }

                    listPoints();
                    repaint();
                }
            });
        }
        
        public void setTrack(String track){
            try {
                File f = new File("." + File.separator + track + ".png");
                if(f.exists()){
                    img = ImageIO.read(f);                
                }

            } catch (Exception e) {
                e.printStackTrace(System.out);
            }

            try {
                File f = new File("." + File.separator + track + ".png.corner_points");
                if (f.exists()) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
                    StringTokenizer tokenizer;
                    String line = reader.readLine();

                    while (line != null && !line.isEmpty()) {
                        tokenizer = new StringTokenizer(line, ";");
                        cornerPoints.add(new Point2D.Double(Double.parseDouble(tokenizer.nextToken()),
                                Double.parseDouble(tokenizer.nextToken())));
                        line = reader.readLine();
                    }
                } else {
                    System.out.println("Corner Points not found");
                }
            } catch (IOException | NumberFormatException e) {
                e.printStackTrace(System.out);
            }
            
            try {
                File f = new File("." + File.separator + track + ".png.label_points");
                if (f.exists()) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"));
                    StringTokenizer tokenizer;
                    String line = reader.readLine();

                    while (line != null && !line.isEmpty()) {
                        tokenizer = new StringTokenizer(line, ";");
                        labelPoints.add(new Point2D.Double(Double.parseDouble(tokenizer.nextToken()),
                                Double.parseDouble(tokenizer.nextToken())));
                        line = reader.readLine();
                    }
                } else {
                    System.out.println("Label Points not found");
                }
            } catch (IOException | NumberFormatException e) {
                e.printStackTrace(System.out);
            }
            
        }
        
        public void setFunction(GeneralisedLogisticFunction f){
            function = f;
        }
        
        public void setTrackModel(TrackModel model){
            trackModel = model;            
            //model.print();
        }

        @Override
        public void paintComponent(Graphics graphics) {
            Graphics2D g = (Graphics2D) graphics;
            
            if(img == null){
                g.setColor(Color.LIGHT_GRAY);
                g.fillRect(0, 0, getWidth(), getHeight());
                
                g.setColor(Color.BLACK);
                g.drawString("No image", getWidth()/2, getHeight()/2);
                return;
            }
            
            if(trackModel == null || !trackModel.initialized()){
                g.setColor(Color.LIGHT_GRAY);
                g.fillRect(0, 0, getWidth(), getHeight());
                
                g.setColor(Color.BLACK);
                g.drawString("No trackmodel", getWidth()/2, getHeight()/2);
                return;
            }

            imgWidth = img.getWidth();
            imgHeight = img.getHeight();

            width = getWidth();
            height = getHeight();

            ratioPanel = 1.0 * width / height;

            if (ratioPanel < 1.0) {
                offset.setLocation(OFFSET, OFFSET * ratioPanel);

            } else {
                offset.setLocation(OFFSET / ratioPanel, OFFSET);
            }

            pixOffset.setLocation(width * offset.getX(), height * offset.getY());

            drawWidth = (int) Math.round((1.0 - 2 * offset.getX()) * width);
            drawHeight = (int) Math.round((1.0 - 2 * offset.getY()) * height);


            ratioImg = 1.0 * imgWidth / imgHeight;
            ratioImgArea = 1.0 * drawWidth / drawHeight;


            g.setColor(Color.WHITE);
            g.fillRect(0, 0, getWidth(), getHeight());

            g.setColor(Color.WHITE);
            g.fillRect(pixOffset.x, pixOffset.y, drawWidth, drawHeight);

            int drawX = pixOffset.x;
            int drawY = pixOffset.y;

            if (ratioImg < ratioImgArea) {
                drawWidth = (int) Math.round(ratioImg * drawHeight);
                drawX = (int) Math.round((width - 2 * offset.getX()) / 2 - drawWidth / 2);

            } else {
                drawHeight = (int) Math.round(drawWidth / ratioImg);
                drawY = (int) Math.round((height - 2 * offset.getY()) / 2 - drawHeight / 2);
            }

            imgArea.setRect(drawX, drawY, drawWidth, drawHeight);
            g.setColor(Color.lightGray);
            g.fill(imgArea);

            g.drawImage(img, imgArea.x, imgArea.y, imgArea.width, imgArea.height, this);            

            if(DEFINE_POINTS){
                g.setColor(Color.CYAN);
                for (Point2D p : target) {
                    Point r = toImgPixCoords(p.getX(), p.getY());
                    int x = r.x;
                    int y = r.y;
                    g.drawLine(x, y - 5, x, y + 5);
                    g.drawLine(x - 5, y, x + 5, y);
                }
            }
            
            int index = 0;
            for (int i = 0; i < cornerPoints.size() && i < labelPoints.size();) {
                TrackSegment seg = trackModel.getSegment(index);

                if (seg.isCorner()) {
                    TrackSegment.Apex[] apexes = seg.getApexes();
                    for (int j = 0; j < apexes.length; ++j) {
                        if (i < cornerPoints.size() && i < labelPoints.size()) {                            
                            Point c = toImgPixCoords(cornerPoints.get(i).getX(), cornerPoints.get(i).getY());
                            Point l = toImgPixCoords(labelPoints.get(i).getX(), labelPoints.get(i).getY());
                            g.setColor(Color.BLACK);
                            g.drawLine(c.x, c.y, l.x, l.y);
                            
                            String label;
                            
                            if(function != null){
                                double speed = 50 + 270 * function.getMirroredValue(Math.abs(apexes[j].value) / 100.0);
                                label = Utils.dTS(speed);
                                
                            } else {
                                if(apexes[j].targetSpeed == TrackSegment.DEFAULT_SPEED){
                                    label = "???";
                                    
                                } else {
                                    label = Utils.dTS(apexes[j].targetSpeed);
                                }                                
                            }                            
                            
                            label += " km/h";
                            
                            int lwidth = g.getFontMetrics().stringWidth(label);
                            int lheight = g.getFontMetrics().getHeight();
                            
                            Point topLeft = new Point(l.x-(lwidth/2), l.y-(lheight/2));
                            
                            g.setColor(Color.BLACK);
                            g.fillRect(topLeft.x, topLeft.y, lwidth+4, lheight+4);
                            g.setColor(Color.WHITE);
                            g.fillRect(topLeft.x+1, topLeft.y+1, lwidth+2, lheight+2);
                            g.setColor(Color.BLACK);
                            g.drawString(label, topLeft.x+2, topLeft.y+2+g.getFontMetrics().getMaxAscent());
                        }
                        ++i;
                    }
                }

                index = trackModel.incIdx(index);
            }

            /*
            g.setColor(Color.PINK);
            g.drawLine(mouseClick.x, mouseClick.y - 5, mouseClick.x, mouseClick.y + 5);
            g.drawLine(mouseClick.x - 5, mouseClick.y, mouseClick.x + 5, mouseClick.y);

            g.setColor(Color.BLACK);
            g.drawString(Utils.dTS(ratioPanel), 10, 10);
            g.drawString(Utils.dTS(ratioImg), 10, 20);
            g.drawString(Utils.dTS(ratioImgArea), 10, 30);*/

            /*if (imgArea.contains(mouseClick)) {
                Point2D p = toNormalizedImgCoords(mouseClick.x, mouseClick.y);
                g.drawString(Utils.dTS(p.getX()) + "-" + Utils.dTS(p.getY()), 10, 50);
            }*/
        }

        Point2D toNormalizedImgCoords(int x, int y) {
            double rx = (x - imgArea.x) * 1.0 / imgArea.width;
            double ry = (y - imgArea.y) * 1.0 / imgArea.height;

            return new Point2D.Double(rx, ry);
        }

        Point toImgPixCoords(double x, double y) {
            int rx = imgArea.x + (int) Math.round(x * imgArea.width);
            int ry = imgArea.y + (int) Math.round(y * imgArea.height);

            return new Point(rx, ry);
        }

        public void listPoints() {
            System.out.println("Corner Points");
            for (Point2D p : cornerPoints) {
                System.out.println(p.getX() + ";" + p.getY());
            }
            System.out.println("Label Points");
            for (Point2D p : labelPoints) {
                System.out.println(p.getX() + ";" + p.getY());
            }
        }
    
}
