/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.gui;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.classification.AngleBasedClassifier;
import de.janquadflieg.mrracer.classification.Situation;
import de.janquadflieg.mrracer.classification.filter.Filter;
import de.janquadflieg.mrracer.telemetry.SensorData;
import de.janquadflieg.mrracer.telemetry.ModifiableSensorData;

import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;

/**
 *
 * @author Jan Quadflieg
 */
public class TrackSensorDisplay
        extends JComponent {

    private static final Color DARK_GREEN = new Color(30, 181, 0);
    private static final Color PURPLE = new Color(0xbd0ebf);
    private ModifiableSensorData m = new ModifiableSensorData();
    private static final int CAR_WIDTH = 2;
    private static final int CAR_LENGTH = 5;
    /**
     * Pixel per meter.
     */
    private static final int PPM = 3;
    private boolean drawSensors = true;
    private boolean drawFocus = true;
    private boolean drawOutline = true;
    private boolean drawCoordinates = true;
    private boolean drawCapOff = true;
    private boolean drawFiltered = true;
    private boolean drawAsParallel = false;
    private float[] angles = new float[19];
    private int focusAngle = 0;
    private double[] focusSensors = new double[5];

    public TrackSensorDisplay() {
        for (int i = 0; i < 19; ++i) {
            angles[i] = -90 + i * 10;
        }
    }

    public void setAngles(float[] angles) {
        for (int i = 0; i < 19; ++i) {
            this.angles[i] = angles[i];
        }
    }

    @Override
    public void paintComponent(Graphics graphics) {
        Graphics2D g = (Graphics2D) graphics;

        g.setColor(Color.WHITE);

        g.fillRect(0, 0, getWidth(), getHeight());

        if (m.getAngleToTrackAxis() != Utils.NO_DATA_D) {
            if (this.drawAsParallel) {
                drawTackSensorsParallel(g);
            } else {
                drawTrackSensors(g);
            }

            // my car
            //drawCar(g);

        } else {
            g.setColor(Color.BLACK);
            g.drawString("No data available", 30, 30);
        }
    }

    private void drawCar(Graphics2D g) {
        AffineTransform backup = g.getTransform();

        g.translate(getWidth() / 2, getHeight() - (CAR_LENGTH * PPM));
        g.setColor(Color.BLACK);
        g.fillRect(-(CAR_WIDTH * PPM / 2), 0, CAR_WIDTH * PPM, CAR_LENGTH * PPM);

        g.setTransform(backup);
    }

    private void drawTackSensorsParallel(Graphics2D g) {
        final Color COL_RAW = Color.red;
        g.setColor(COL_RAW);
        g.fillRect(5, 5, 15, 15);
        g.setColor(Color.BLACK);
        g.drawString("Raw Sensors", 20, 20);
        
        int halfHeight = getHeight() / 2;

        int sensorHeight = halfHeight - 50;
        final double ppm = sensorHeight / 200.0;
        // width of a sensor
        int w = (getWidth() - 20 * 5) / 19;

        double[] s = new double[19];
        System.arraycopy(m.getTrackEdgeSensors(), 0, s, 0, 19);
        int offset = 5;

        for (int i = 0; i < s.length; ++i) {
            int height = (int) Math.round(s[i] * ppm);
            g.setColor(Color.BLUE);
            if (i == 9) {
                g.setColor(DARK_GREEN);
            }
            g.fillRect(offset, sensorHeight, w, -height);

            g.setColor(Color.BLACK);
            g.drawString(String.valueOf(i), offset, halfHeight + 10);
            offset += 5 + w;
        }

        if(!m.onTrack()){
            return;
        }        

        final int ALTERNATIVES = 3;
        System.arraycopy(m.getRawTrackEdgeSensors(), 0, s, 0, 19);
        int smallw = (w - 4) / ALTERNATIVES;
        offset = 5;
        double[] rawSensors = m.getRawTrackEdgeSensors();

        for (int i = 0; i < rawSensors.length; ++i) {
            int height = (int) Math.round(rawSensors[i] * ppm);
            g.setColor(COL_RAW);
            g.fillRect(offset + 2, sensorHeight, smallw, -height);

            offset += 5 + w;
        }

        double[] filteredFlanagan = new double[rawSensors.length];
        System.arraycopy(rawSensors, 0, filteredFlanagan, 0, rawSensors.length);
        Filter.filterFlanaganRegression(filteredFlanagan);

        offset = 5;

        for (int i = 0; i < filteredFlanagan.length; ++i) {
            int height = (int) Math.round(filteredFlanagan[i] * ppm);
            g.setColor(Color.ORANGE);
            g.fillRect(offset + 2 + smallw, sensorHeight, smallw, -height);

            offset += 5 + w;
        }
        
        double[] filteredApache = new double[rawSensors.length];
        System.arraycopy(rawSensors, 0, filteredApache, 0, rawSensors.length);
        Filter.filterApacheCommons(filteredApache);

        offset = 5;

        for (int i = 0; i < filteredApache.length; ++i) {
            int height = (int) Math.round(filteredApache[i] * ppm);
            g.setColor(Color.MAGENTA);
            g.fillRect(offset + 2 + smallw+ smallw, sensorHeight, smallw, -height);

            offset += 5 + w;
        }        
        
        AngleBasedClassifier classifier = new AngleBasedClassifier(angles);
        
        ModifiableSensorData copy = new ModifiableSensorData();
        copy.setData(m);
        
        Situation situation = classifier.classify(copy);
        
        g.setColor(Color.BLACK);
        g.drawString("Classification Original: "+situation.toString(), 20, halfHeight+50);
        
        copy.setTrackEdgeSensors(filteredFlanagan);
        situation = classifier.classify(copy);
        g.drawString("Classification Flanagan: "+situation.toString(), 20, halfHeight+65);
    }

    private void drawTrackSensors(Graphics2D g) {
        AffineTransform backup = g.getTransform();

        g.translate(getWidth() / 2, getHeight() - (CAR_LENGTH * PPM));

        g.setColor(Color.RED);

        double[] sensors = new double[19];
        System.arraycopy(m.getTrackEdgeSensors(), 0, sensors, 0, 19);


        // coordinate system
        if (drawCoordinates) {
            g.setColor(Color.BLACK);
            // x-line
            g.drawLine(-(25 * PPM), 0, 25 * PPM, 0);
            g.drawString("X", (25 * PPM) - 5, 20);
            g.drawLine(-(20 * PPM), -5, -(20 * PPM), 5);
            g.drawString("-20m", -(20 * PPM) - 12, 20);
            g.drawLine(-(10 * PPM), -5, -(10 * PPM), 5);
            g.drawString("-10m", -(10 * PPM) - 12, 20);
            g.drawString("0m", -5, 20);
            g.drawLine((10 * PPM), -5, (10 * PPM), 5);
            g.drawString("10m", (10 * PPM) - 10, 20);
            g.drawLine((20 * PPM), -5, (20 * PPM), 5);
            g.drawString("20m", (20 * PPM) - 10, 20);
            // y line
            g.drawLine(0, PPM, 0, -220 * PPM);
            g.drawString("Y", 5, -220 * PPM);
            g.drawLine(-5, -50 * PPM, 5, -50 * PPM);
            g.drawString("50m", 20, -50 * PPM);
            g.drawLine(-5, -100 * PPM, 5, -100 * PPM);
            g.drawString("100m", 20, -100 * PPM);
            g.drawLine(-5, -150 * PPM, 5, -150 * PPM);
            g.drawString("150m", 20, -150 * PPM);
            g.drawLine(-5, -200 * PPM, 5, -200 * PPM);
            g.drawString("200m", 20, -200 * PPM);
        }

        int last_x = 0;
        int last_y = 0;

        int max_l = SensorData.maxTrackIndexLeft(m);
        int max_r = SensorData.maxTrackIndexRight(m);

        for (int i = 0; i < sensors.length; ++i) {
            double angle = Math.toRadians(angles[i]);
            int x_offset = (int) Math.round(Math.sin(angle) * sensors[i] * PPM);
            int y_offset = (int) Math.round(-Math.cos(angle) * sensors[i] * PPM);

            //g.setColor(Color.RED);
            //g.fillRect(x_offset - 1, y_offset - 1, 2, 2);
            if (drawSensors) {
                g.setColor(Color.BLUE);
                if (x_offset < 0) {
                    if (i % 2 == 0) {
                        g.drawString(Integer.toString(i), x_offset - 10, y_offset + (PPM));
                    } else {
                        g.drawString(Integer.toString(i), x_offset - 20, y_offset + (PPM));
                    }

                } else if (x_offset > 0) {
                    if (i % 2 == 0) {
                        g.drawString(Integer.toString(i), x_offset, y_offset);
                    } else {
                        g.drawString(Integer.toString(i), x_offset + 20, y_offset);
                    }

                } else {
                    g.drawString(Integer.toString(i), x_offset, y_offset - 20);
                }

                g.drawLine(0, 0, x_offset, y_offset);
            }

            if (i > 0 && drawOutline) {
                if (drawCapOff) {
                    if (i < max_l || i > max_r + 1) {
                        g.setColor(Color.GREEN);
                        if (i < max_l) {
                            int cor = 3;
                            if (i % 2 == 0) {
                                cor = -10;
                            }
                            g.drawString(String.valueOf(i - 1), last_x + ((x_offset - last_x) / 2) + cor, last_y + ((y_offset - last_y) / 2));

                        } else {
                            int z = 18 - i;
                            int cor = 3;
                            if (i % 2 == 0) {
                                cor = -10;
                            }
                            g.drawString(String.valueOf(z), last_x + ((x_offset - last_x) / 2) + cor, last_y + ((y_offset - last_y) / 2));
                        }

                    } else {
                        g.setColor(Color.RED);
                    }
                    g.drawLine(last_x, last_y, x_offset, y_offset);

                } else {
                    g.setColor(DARK_GREEN);
                    g.drawLine(last_x, last_y, x_offset, y_offset);
                }
            }


            last_x = x_offset;
            last_y = y_offset;
        }

        if (drawFocus) {
            g.setColor(PURPLE);
            for (int i = -2; i < 3; ++i) {
                double angle = Math.toRadians(focusAngle + i);
                int x_offset = (int) Math.round(Math.sin(angle) * focusSensors[i + 2] * PPM);
                int y_offset = (int) Math.round(-Math.cos(angle) * focusSensors[i + 2] * PPM);

                g.drawLine(0, 0, x_offset, y_offset);
            }
        }

        /*if (this.drawFiltered) {
            Filter.filterFlanaganRegression(sensors);

            for (int i = 0; i < sensors.length; ++i) {
                double angle = Math.toRadians(angles[i]);
                int x_offset = (int) Math.round(Math.sin(angle) * sensors[i] * PPM);
                int y_offset = (int) Math.round(-Math.cos(angle) * sensors[i] * PPM);

                //g.setColor(Color.RED);
                //g.fillRect(x_offset - 1, y_offset - 1, 2, 2);

                g.setColor(Color.PINK);
                g.drawLine(0, 0, x_offset, y_offset);
            }
        }*/

        g.setTransform(backup);
    }    

    public void setDrawOutline(boolean b) {
        this.drawOutline = b;
        repaint();
    }

    public void setDrawCoordinates(boolean b) {
        this.drawCoordinates = b;
        repaint();
    }

    public void setDrawCoordinatesParallel(boolean b) {
        this.drawAsParallel = b;
        repaint();
    }

    public void setDrawCapOff(boolean b) {
        this.drawCapOff = b;
        repaint();
    }

    public void setDrawSensors(boolean b) {
        this.drawSensors = b;
        repaint();
    }

    public void setDrawFocus(boolean b) {
        this.drawFocus = b;
        repaint();
    }

    public void setDrawFiltered(boolean b) {
        this.drawFiltered = b;
        repaint();
    }

    public void setSensorData(SensorData model, int focusAngle) {
        if (model.focusAvailable()) {
            this.focusAngle = focusAngle;
            System.arraycopy(model.getFocusSensors(), 0, this.focusSensors, 0, this.focusSensors.length);
        }
        this.m.setData(model);
        repaint();
    }
}
