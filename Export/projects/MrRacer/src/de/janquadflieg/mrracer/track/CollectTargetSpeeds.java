/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.track;

import java.io.*;

/**
 *
 * @author quad
 */
public class CollectTargetSpeeds {

    public static void run()
            throws Exception {
        //String path = "Z:\\Experimente\\Evaluation\\Vergleich\\MrRacer\\";
        String path = "Z:\\Experimente\\2013-11-18-LongWarmup\\BrakeSteering\\";
        File directory = new File(path);
        File[] files = directory.listFiles();

        for (int i = 0; i < files.length; ++i) {
            if (files[i].isFile() && files[i].getName().endsWith(TrackModel.TM_EXT)) {
                TrackModel model = TrackModel.load(files[i].getAbsolutePath());
                
                FileOutputStream fo = new FileOutputStream(new File(path+"ts_"+model.getName()+".txt"));
                OutputStreamWriter w = new OutputStreamWriter(fo, "UTF-8");
                
                for(int idxs = 0; idxs < model.size(); ++idxs){
                    TrackSegment seg = model.getSegment(idxs);
                    
                    if(seg.isCorner()){
                        TrackSegment.Apex[] apexes = seg.getApexes();
                        
                        for(int idxa = 0; idxa < apexes.length; ++idxa){
                            w.write(String.valueOf(Math.abs(apexes[idxa].value)));
                            w.write(";");
                            w.write(String.valueOf(Math.abs(apexes[idxa].targetSpeed)));
                            w.write("\n");
                        }
                    }
                }                

                w.flush();
                fo.flush();
                w.close();
                fo.close();
            }
        }
    }

    public static void main(String[] args) {
        try {
            run();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
}
