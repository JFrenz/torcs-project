/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.plan;

import de.janquadflieg.mrracer.data.CarConstants;
import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.classification.Situation;
import de.janquadflieg.mrracer.classification.Situations;
import de.janquadflieg.mrracer.controller.BaseController;
import de.janquadflieg.mrracer.functions.ConstantValue;
import de.janquadflieg.mrracer.functions.CubicSplineTwoPoints;
import de.janquadflieg.mrracer.functions.GeneralisedLogisticFunction;
import de.janquadflieg.mrracer.functions.GeneralisedLogisticFunction.XAxis;
import de.janquadflieg.mrracer.functions.GeneralisedLogisticFunction.YAxis;
import de.janquadflieg.mrracer.gui.GraphicDebugable;
import de.janquadflieg.mrracer.gui.TelemetryDisplay;
import de.janquadflieg.mrracer.opponents.Observer2013;
import de.janquadflieg.mrracer.opponents.OpponentObserver;
import static de.janquadflieg.mrracer.plan.Plan.TEXT_DEBUG;
import de.janquadflieg.mrracer.telemetry.ModifiableSensorData;
import de.janquadflieg.mrracer.telemetry.SensorData;
import de.janquadflieg.mrracer.track.*;

import java.util.ArrayList;
import java.util.Properties;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author quad
 */
public final class Plan2013
        implements Plan {

    private static final boolean USE_SPEED_CHECK_POINTS = true;
    private static final boolean USE_SPEED_AT_APEX_VERIFIER = true;
    private static final boolean LIMIT_TARGET_SPEED_AT_TRACK_EDGE = true;
    private double saveSpeed = Plan.MAX_SPEED;
    
    private ArrayList<SpeedVerifier> speedVerifiers = new ArrayList<>();
    /**
     * Constant indicating that there is no prefered anchor point.
     */
    public static final double NO_PREFERED_ANCHOR_POINT = 777.0;
    /**
     * Graphics Debug?
     */
    private static final boolean GRAPHICAL_DEBUG = true;
    /**
     * Debug painter.
     */
    private DebugPainter debugPainter;
    /**
     * In this mode, we ignore the target speeds saved in the track model and
     * always use the logistic function.
     */
    private boolean alwaysUseGLF = false;
    /**
     * Maximum deviation allowed before a replan occurs.
     */
    private final static double MAX_POS_DEVIATION_M = 1.0;
    /**
     * Modifier for the target speeds.
     */
    private final static double SPEED_MODIFIER = 1.0;
    /**
     * Look ahead.
     */
    private final static double LOOK_AHEAD = 100.0;
    /**
     * The stage of the race.
     */
    private scr.Controller.Stage stage = scr.Controller.Stage.QUALIFYING;
    /**
     * Online learning data during the warmup.
     */
    private OnlineLearning2013 onlineLearner = new OnlineLearning2013();
    /**
     * Observer for opponents.
     */
    private OpponentObserver observer;
    /**
     * Last target point.
     */
    private Point2D lastTargetPoint = OpponentObserver.NO_RECOMMENDED_POINT;
    /**
     * The Trackmodel.
     */
    private TrackModel trackModel = new TrackModel();
    /**
     * Debug Info.
     */
    private StringBuilder info = new StringBuilder();
    /**
     * The plan.
     */
    private PlanContent plan = new PlanContent();
    /**
     * Point at which we want to create a new plan.
     */
    private double replanAt = 0.0;
    /**
     * Planning module for straights.
     */
    private PlanSubModule moduleStraight;
    /**
     * String identifier.
     */
    public static final String MODULE_STRAIGHT = "-PLAN.straight-";
    /**
     * Planning module for corners.
     */
    private PlanSubModule moduleCorner;
    /**
     * Planning module for the race line.
     */
    private PlanRaceLineJan moduleRaceLine;
    /**
     * String identifier.
     */
    public static final String MODULE_CORNER = "-PLAN.corner-";
    /**
     * The estimated next race distance.
     */
    private double nextDistance = 0.0;
    /**
     * The current race distance.
     */
    private double currentDistance = 0.0;
    /**
     * The desired position for approching corners and at their apex.
     */
    private double offset = 0.0;
    /**
     * Last data packet.
     */
    //private ModifiableSensorData lastData = new ModifiableSensorData();
    /**
     * Brake coefficient for corners.
     */
    private double brakeCornerCoeff = 0.5;
    /**
     * String Identifier.
     */
    public static final String BRAKE_CORNER_COEFF = "-PLAN.BCC-";
    /**
     * Friction value mu.
     */
    private double mu = 1.1;
    /**
     * String Identifier.
     */
    public static final String MU = "-PLAN.MU-";
    /**
     * Target Speeds.
     */
    private GeneralisedLogisticFunction targetSpeeds = new GeneralisedLogisticFunction();
    /**
     * String Identifier.
     */
    public static final String TARGET_SPEEDS = "-PLAN.targetSpeeds-";
    /**
     * String Identifier.
     */
    public static final String OBSERVER = "-PLAN.observer-";
    /**
     * String Identifier.
     */
    public static final String BRAKE_PREDICTOR = "-PLAN.brakepredictor-";
    /**
     * Controller.
     */
    private BaseController controller;
    /**
     * String Identifier.
     */
    public static final String SPEED_PREDICTOR = "-PLAN.speedpredictor-";
    /**
     * Speed predictor.
     */
    private SpeedPredictor speedPredictor = new SpeedPredictor();
    /**
     * Brake Predictor.
     */
    private BrakePredictor brakePredictor = new BrakePredictor();
    /**
     * Constant which can be used as a system property. This causes the plan to
     * always use the generalized logistic function. Useful for optimization.
     */
    public static final String ALWAYS_USE_GLF_FOR_TARGET_SPEEDS = "Plan2013AlwaysUseGLF";

    public Plan2013(BaseController c) {
        this.observer = new Observer2013(this);
        //this.observer = new de.janquadflieg.mrracer.opponents.DebugObserverBlock(this);
        //this.observer = new de.janquadflieg.mrracer.opponents.DebugObserverOvertake(this, observer);
        this.moduleStraight = new PlanStraight2013(this, speedPredictor);
        this.moduleCorner = new PlanCorner2013(this);
        this.moduleRaceLine = new PlanRaceLineJan(this);
        this.controller = c;
        if (GRAPHICAL_DEBUG) {
            debugPainter = new DebugPainter();
            debugPainter.setName("Plan");
        }

        if (System.getProperties().containsKey(Plan2013.ALWAYS_USE_GLF_FOR_TARGET_SPEEDS)) {
            System.out.println("Plan: Using GLF for target speeds");
            alwaysUseGLF = true;
        }
    }

    @Override
    public void addSpeedVerifier(SpeedVerifier sv) {
        speedVerifiers.add(sv);
    }

    @Override
    public void setParameters(Properties params, String prefix) {
        targetSpeeds.setParameters(params, prefix + TARGET_SPEEDS);
        observer.setParameters(params, prefix + OBSERVER);
        brakePredictor.setParameters(params, prefix + Plan2013.BRAKE_PREDICTOR);
        speedPredictor.setParameters(params, prefix + Plan2013.SPEED_PREDICTOR);
        moduleCorner.setParameters(params, prefix + Plan2013.MODULE_CORNER);
        moduleStraight.setParameters(params, prefix + Plan2013.MODULE_STRAIGHT);

        if (params.containsKey(prefix + Plan2013.MU)) {
            mu = Double.parseDouble(params.getProperty(prefix + Plan2013.MU));

        } else {
            System.out.println("WARNING: properties do not contain " + prefix + MU);
            System.out.println("Using: " + mu);
        }

        if (params.containsKey(prefix + Plan2013.BRAKE_CORNER_COEFF)) {
            brakeCornerCoeff = Double.parseDouble(params.getProperty(prefix + Plan2013.BRAKE_CORNER_COEFF));

        } else {
            System.out.println("WARNING: properties do not contain " + prefix + Plan2013.BRAKE_CORNER_COEFF);
            System.out.println("Using: " + brakeCornerCoeff);
        }

    }

    @Override
    public void getParameters(Properties params, String prefix) {
        targetSpeeds.getParameters(params, prefix + TARGET_SPEEDS);
        observer.getParameters(params, prefix + OBSERVER);
        brakePredictor.getParameters(params, prefix + Plan2013.BRAKE_PREDICTOR);
        speedPredictor.getParameters(params, prefix + Plan2013.SPEED_PREDICTOR);
        moduleCorner.getParameters(params, prefix + Plan2013.MODULE_CORNER);
        moduleStraight.getParameters(params, prefix + Plan2013.MODULE_STRAIGHT);
        params.setProperty(prefix + Plan2013.BRAKE_CORNER_COEFF, String.valueOf(brakeCornerCoeff));
        params.setProperty(prefix + Plan2013.MU, String.valueOf(mu));
    }

    @Override
    public void paint(String baseFileName, java.awt.Dimension d) {
        java.awt.image.BufferedImage img = new java.awt.image.BufferedImage(d.width, d.height, java.awt.image.BufferedImage.TYPE_INT_RGB);
        XAxis x = new XAxis();
        YAxis y = new YAxis();
        x.labelMin = 0.0;
        x.labelMax = 100.0;
        x.xmin = 0.0;
        x.xmax = 100.0;
        x.ticks = 10.0;
        x.unit = "°";

        y.mirror = true;
        y.labelMin = 0.0;
        y.y0 = 50;
        y.labelMax = 330.0;
        y.y1 = 330.0;
        y.ticks = 50.0;
        y.unit = "km/h";
        targetSpeeds.paint(img.createGraphics(), d, x, y);
        try {
            javax.imageio.ImageIO.write(img, "PNG", new java.io.File(baseFileName + "-speeds.png"));
        } catch (java.io.IOException e) {
            e.printStackTrace(System.out);
        }
    }

    private void clear() {
        plan.clear();
        speedVerifiers.clear();
    }

    @Override
    public void setStage(scr.Controller.Stage s) {
        stage = s;
        observer.setStage(s);
        //System.out.println("Plan, stage="+stage.toString());
    }

    @Override
    public void replan() {
        clear();
    }

    @Override
    public void resetFull() {
        reset();
        onlineLearner.resetFull();
    }

    public void reset() {
        observer.reset();
        lastTargetPoint = OpponentObserver.NO_RECOMMENDED_POINT;
        clear();
        replanAt = 0.0;
        onlineLearner.reset();
    }

    private double toDistanceFromStartLine(SensorData data, double d) {
        if (!trackModel.initialized()) {
            return d;
        }

        //System.out.println("Data: "+data.getDistanceRaced()+" "+data.getDistanceFromStartLine()+", d: "+d);
        //System.out.println(trackModel.getLength());

        final double startDistance = (trackModel.getLength() - (data.getDistanceRaced() - data.getDistanceFromStartLine())) % trackModel.getLength();

        //System.out.println("startD: "+startDistance);

        double result = (d + startDistance) % trackModel.getLength();

        //System.out.println("Result: "+result);

        return result;
    }

    @Override
    public void functionalReset() {
        clear();
        observer.reset();
        lastTargetPoint = OpponentObserver.NO_RECOMMENDED_POINT;
    }

    public final double calcApproachSpeedCorner(double targetSpeed, double distance, double bcc) {
        if (bcc == TrackSegment.DEFAULT_BCC) {
            bcc = brakeCornerCoeff;
        }

        return brakePredictor.calcApproachSpeed(targetSpeed, distance, mu, bcc);
    }

    public final double calcApproachSpeedStraight(double targetSpeed, double distance) {
        return brakePredictor.calcApproachSpeed(targetSpeed, distance, mu, 1.0);
    }

    public final double calcBrakeDistanceStraight(double speed, double targetSpeed) {
        return brakePredictor.calcBrakeDistance(speed, targetSpeed, mu, 1.0);
    }

    public final double calcBrakeDistanceCorner(double speed, double targetSpeed, double bcc) {
        if (bcc == TrackSegment.DEFAULT_BCC) {
            bcc = brakeCornerCoeff;
        }
        return brakePredictor.calcBrakeDistance(speed, targetSpeed, mu, bcc);
    }

    public final double calcBrakingZoneStraight(double refSpeed, double length, double targetSpeed) {
        return calcBrakingZone(refSpeed, length, targetSpeed, 1.0);
    }

    public final double calcBrakingZoneCorner(double refSpeed, double length, double targetSpeed, double bcc) {
        if (bcc == TrackSegment.DEFAULT_BCC) {
            bcc = brakeCornerCoeff;
        }
        return calcBrakingZone(refSpeed, length, targetSpeed, bcc);
    }

    /**
     * This method calculates the length of the braking zone needed to brake
     * down to the given target speed, assuming that the car is driving with the
     * given reference speed and length m of track is available. The method
     * takes into account that the available track can be used to further
     * accelerate.
     *
     * @param refSpeed
     * @param length
     * @param targetSpeed
     * @param bcc
     * @return
     */
    private double calcBrakingZone(double refSpeed, double length, double targetSpeed, double bcc) {
        //System.out.println("Calculate braking zone: length=" + length + ", refSpeed=" + refSpeed + ", targetSpeed="+targetSpeed);
        double result = brakePredictor.calcBrakeDistance(speedPredictor.predictSpeed(refSpeed, length), targetSpeed, mu, bcc);

        for (int i = 0; i < 3; ++i) {
            //System.out.println(i + " " + result);
            result = brakePredictor.calcBrakeDistance(speedPredictor.predictSpeed(refSpeed, length - result), targetSpeed, mu, bcc);
        }

        return Math.max(result, 0.0);
    }

    protected double calcSpeed(double v) {
        double value = Math.abs(v) / 100.0;
        double result = this.targetSpeeds.getMirroredValue(value);
        result *= 330 - 50;
        result += 50;

        return result;
    }
    
    @Override
    public void createAndAddCheckPoints(double start, double distance, double initialSpeed){        
        final double CHECK_POINT_INTERVALL = 50;
        double remainingDistance = distance;
        double coveredDistance = CHECK_POINT_INTERVALL;
        while(remainingDistance >= CHECK_POINT_INTERVALL){
            double predictedSpeed = speedPredictor.predictSpeed(initialSpeed, coveredDistance);
            plan.addSpeedCheckPoint(new Point2D.Double(start+coveredDistance, predictedSpeed));
            remainingDistance -= CHECK_POINT_INTERVALL;
            coveredDistance += CHECK_POINT_INTERVALL;
        }
    }

    @Override
    public javax.swing.JComponent[] getComponent() {
        if (GRAPHICAL_DEBUG) {
            ArrayList<javax.swing.JComponent> list = new ArrayList<>();
            list.add(debugPainter);
            if (observer instanceof GraphicDebugable) {
                javax.swing.JComponent[] c = ((GraphicDebugable) observer).getComponent();
                for (int i = 0; i < c.length; ++i) {
                    list.add(c[i]);
                }
            }

            if (moduleRaceLine instanceof GraphicDebugable) {
                javax.swing.JComponent[] c = ((GraphicDebugable) moduleRaceLine).getComponent();
                for (int i = 0; i < c.length; ++i) {
                    list.add(c[i]);
                }
            }

            javax.swing.JComponent[] result = new javax.swing.JComponent[list.size()];

            for (int i = 0; i < list.size(); ++i) {
                result[i] = list.get(i);
            }

            return result;

        } else {
            return new javax.swing.JComponent[0];
        }
    }

    @Override
    public java.awt.geom.Point2D getTargetPosition() {
        double p = plan.getPosition(nextDistance);

        double y = nextDistance - currentDistance;

        return new java.awt.geom.Point2D.Double(p, y);
    }

    @Override
    public double getTargetSpeed() {
        double speed = plan.getSpeed(nextDistance);

        if (observer.otherCars() && observer.getRecommendedSpeed() != OpponentObserver.NO_RECOMMENDED_SPEED) {
            speed = Math.min(speed, observer.getRecommendedSpeed());
        }
        
        speed = Math.min(speed, saveSpeed);        

        return speed;
    }

    public boolean isPreCompiling() {
        return controller.isPreCompiling();
    }

    public String getInfo() {
        return info.toString();
    }

    /**
     * Calculates the difference between the current position and the planned
     * target position in meter.
     *
     * @param data Current sensor data.
     * @return Deviation from the planned racing line.
     */
    private double getDeviation(SensorData data) {
        if (plan.contains(nextDistance)
                && trackModel != null && trackModel.initialized()) {

            if (plan.getPosition(nextDistance) == NO_RACE_LINE) {
                return 0.0;

            } else {
                double tw_half = trackModel.getWidth() * 0.5;
                double planned = plan.getPosition(nextDistance) * tw_half;
                double current = data.getTrackPosition() * tw_half;
                return Math.abs(current - planned);
            }
        }

        return 0.0;
    }

    /**
     * Updates the plan when the car is not on the track. This is only necessary
     * during the warmup.
     *
     * @param data
     */
    @Override
    public final void updateOffTrack(SensorData data) {
        if (stage != scr.Controller.Stage.WARMUP) {
            return;
        }

        if (!onlineLearner.active()) {
            if (data.getCurrentLapTime() > -0.1) {
                onlineLearner.init(trackModel, this, data, controller);
            }

            return;
        }
        onlineLearner.update(data);
    }

    @Override
    public final void update(SensorData data, Situation situation) {
        boolean repaint = false;

        this.currentDistance = data.getDistanceRaced();

        observer.update(data, situation);
        
        saveSpeed = Plan.MAX_SPEED;
        
        if(Plan2013.LIMIT_TARGET_SPEED_AT_TRACK_EDGE && trackModel.initialized()){
            double distanceLeft = data.calcAbsoluteTrackPosition(trackModel.getWidth())-CarConstants.CAR_HALF_WIDTH;
            double distanceRight = trackModel.getWidth()-(data.calcAbsoluteTrackPosition(trackModel.getWidth())+CarConstants.CAR_HALF_WIDTH);
            //System.out.println(Utils.dTS(distanceLeft)+" - "+Utils.dTS(distanceRight));
            if(distanceLeft < 0.1 || distanceRight < 0.1){
                saveSpeed = Math.max(50.0, data.getSpeed());
            }
        }

        if (GRAPHICAL_DEBUG) {
            repaint = Math.abs(data.getDistanceRaced() - debugPainter.lastRepaintAt) > 10;
        }

        info = new StringBuilder();

        nextDistance = data.getDistanceRaced() + Math.max(0.1, (data.getSpeed() / 3.6) / 50);

        // handle online learning
        if (stage == scr.Controller.Stage.WARMUP) {
            if (!onlineLearner.active()) {
                if (data.getCurrentLapTime() > -0.1) {
                    onlineLearner.init(trackModel, this, data, controller);
                }
            } else {
                onlineLearner.update(data);
            }
        }

        for (SpeedVerifier sv : speedVerifiers) {
            sv.update(data);
        }

        if (trackModel.initialized()) {
            offset = ((trackModel.getWidth() - 4) / 2) / (trackModel.getWidth() / 2);

            boolean newTargetPoint = lastTargetPoint == OpponentObserver.NO_RECOMMENDED_POINT && observer.getRecommendedPosition() != OpponentObserver.NO_RECOMMENDED_POINT;
            boolean targetPointChanged = lastTargetPoint != OpponentObserver.NO_RECOMMENDED_POINT && observer.getRecommendedPosition() != OpponentObserver.NO_RECOMMENDED_POINT && lastTargetPoint.distanceSq(observer.getRecommendedPosition()) != 0.0;

            boolean reactToOtherCars = observer.otherCars() && (newTargetPoint || targetPointChanged);

            boolean scheduledReplan = nextDistance > replanAt;

            double raceLineDeviation = getDeviation(data);
            boolean offRaceLine = raceLineDeviation > MAX_POS_DEVIATION_M
                    && ((stage == scr.Controller.Stage.WARMUP && !trackModel.complete()) || (trackModel.complete()));// && trackModel.getSegment(data.getDistanceFromStartLine()).isStraight()));

            boolean speedDeviation = false;
            if (USE_SPEED_CHECK_POINTS) {
                Point2D checkPoint = plan.getNextSpeedCheckPoint();
                if (checkPoint != null && checkPoint.getX() < data.getDistanceRaced()) {
                    if (TEXT_DEBUG && !controller.isPreCompiling()) {
                        println("Checking speed @"+data.getDistanceRacedS()+"m: " + Utils.dTS(checkPoint.getX())
                                + "m / " + Utils.dTS(checkPoint.getY())
                                +"km/h, true speed: "+data.getSpeedS()+"km/h"+", planned target speed: "+Utils.dTS(getTargetSpeed())+"km/h");
                    }
                    
                    speedDeviation = Math.abs(checkPoint.getY()-data.getSpeed()) > 5.0;
                    
                    plan.removeNextSpeedCheckPoint();
                }
            }

            if (!plan.contains(nextDistance) || reactToOtherCars || offRaceLine || scheduledReplan || speedDeviation) {
                if (TEXT_DEBUG && !controller.isPreCompiling()) {
                    println("");
                    println("");
                    println("##################################################################################");
                    println("##################################################################################");
                }
                if (TEXT_DEBUG && !controller.isPreCompiling() && reactToOtherCars) {
                    println("Need to to replan because of other cars");
                }

                if (TEXT_DEBUG && !controller.isPreCompiling() && offRaceLine) {
                    println("Need to replan because I left the racing line");
                }

                if (TEXT_DEBUG && !controller.isPreCompiling() && scheduledReplan) {
                    println("Need to replan because I reached the scheduled replan point");
                }

                if (TEXT_DEBUG && !controller.isPreCompiling() && !plan.contains(nextDistance)) {
                    println("Need to replan because nextDistance is not covered by the plan");
                }
                
                if (TEXT_DEBUG && !controller.isPreCompiling() && speedDeviation) {
                    println("Need to replan because the real speed differs too much from the predicted speed");
                }

                plan(data);
                
                if (TEXT_DEBUG && !controller.isPreCompiling()) {
                    println("##################################################################################");
                    println("##################################################################################");
                    println("");
                    println("");
                }

                lastTargetPoint = observer.getRecommendedPosition();
                repaint = true;
                if (GRAPHICAL_DEBUG) {
                    debugPainter.clearData();
                }
            }

        } else {
            clear();
            plan.setCoverage(data.getDistanceRaced(), data.getDistanceRaced() + 25);
            replanAt = plan.getEnd();
            if (Math.abs(data.getTrackPosition()) < 0.1) {
                plan.attachPosition(new ConstantValue(data.getDistanceRaced(), data.getDistanceRaced() + 25.0, 0.0));

            } else {
                double[] x = {data.getDistanceRaced(), data.getDistanceRaced() + 25.0};
                double[] y = {data.getTrackPosition(), 0};
                CubicSplineTwoPoints spline = new CubicSplineTwoPoints(x, y);
                plan.attachPosition(spline);
            }
            plan.attachSpeed(new ConstantValue(data.getDistanceRaced(), data.getDistanceRaced() + 25, 50.0));

            info.append("Model not initialized");
            repaint = true;

            if (GRAPHICAL_DEBUG) {
                debugPainter.clearData();
            }
        }
        if (GRAPHICAL_DEBUG && repaint) {
            debugPainter.paintPlan(data);
        }
    }

    private void plan(SensorData data) {
        int index = trackModel.getIndex(data.getDistanceFromStartLine());
        TrackSegment current = trackModel.getSegment(index);

        if (current.isUnknown()) {
            planUnknown(data, current);

        } else {
            planNormal(data);
        }
    }

    private void planNormal(SensorData data) {
        int index = trackModel.getIndex(data.getDistanceFromStartLine());
        TrackSegment current = trackModel.getSegment(index);

        if (TEXT_DEBUG && !controller.isPreCompiling()) {
            println("");
            println("*****************************************************");
            println("** planNormal @" + Utils.dTS(data.getDistanceRaced()) + "m/"
                    + Utils.dTS(data.getDistanceFromStartLine()) + "m, " + data.getSpeedS() + "km/h");
            println("*****************************************************");
            println(current.toString());
        }

        // Clear current plan
        clear();

        // PlanStackData Object to control the planning process
        PlanStackData planData = new PlanStackData(data.getDistanceRaced());

        // the next corner
        TrackSegment corner = null;

        // estimated speed at the corner
        double speedAtCorner;
        // distance to accelerate
        double accDist = 0.0;

        // add the current element
        accDist += current.getEnd() - data.getDistanceFromStartLine();
        speedAtCorner = speedPredictor.predictSpeed(data.getSpeed(), accDist);
        planData.addSegment(index,
                current.getEnd() - data.getDistanceFromStartLine(),
                data.getSpeed());

        // search the next corner
        int searchIndex = trackModel.incrementIndex(index);
        int cornerIndex = searchIndex;

        while (corner == null && searchIndex != index) {
            TrackSegment element = trackModel.getSegment(searchIndex);

            planData.addSegment(searchIndex, element.getLength(),
                    speedAtCorner);

            if (element.isCorner()) {
                corner = element;
                cornerIndex = searchIndex;

            } else if (element.isUnknown()) {
                corner = element;
                cornerIndex = searchIndex;
                speedAtCorner = 50.0;

            } else if (element.isStraight()) {
                accDist += element.getLength();
                speedAtCorner = speedPredictor.predictSpeed(data.getSpeed(), accDist);
            }

            searchIndex = trackModel.incrementIndex(searchIndex);
        }

        if (corner == null) {
            if (TEXT_DEBUG && !controller.isPreCompiling()) {
                println("corner == null, setze letztes stack element");
            }
            corner = trackModel.getSegment(planData.currentSegment());
        }

        replanAt = planData.planEnd();

        // search the segment which contains the point planEnd + LOOK_AHEAD
        double searchLength = 0.0;
        do {
            TrackSegment element = trackModel.getSegment(searchIndex);
            planData.addSegment(searchIndex, element.getLength(),
                    speedAtCorner);
            searchLength += element.getLength();
            searchIndex = trackModel.incIdx(searchIndex);
        } while (searchLength < LOOK_AHEAD);

        planData.approachSpeed = Plan.MAX_SPEED;

        if (TEXT_DEBUG && !controller.isPreCompiling()) {
            println("");
            println("Starting to plan, start=" + Utils.dTS(planData.planStart()) + ", end=" + Utils.dTS(planData.planEnd()));
            println("A replan will be scheduled for " + Utils.dTS(replanAt));
            println("I'm at [" + index + "], planning towards [" + cornerIndex + "] where I will arrive with " + Utils.dTS(speedAtCorner) + "km/h");

            if (!corner.isUnknown()) {
                println(corner.toString(true));
                /*TrackSegment.Apex[] apexes = corner.getApexes();
                 for (TrackSegment.Apex a : apexes) {
                 println("Apex: " + Utils.dTS(a.position) + ", " + Situations.toString(a.type) + ", speed: " + Utils.dTS(getTargetSpeed(a)) + "km/h");
                 }*/

            } else {
                println("The corner is an unknown segment.");
            }
            println("");
        }

        plan.setCoverage(planData.planStart(), planData.planEnd());

        if (TEXT_DEBUG && !controller.isPreCompiling()) {
            println("----------------------------------");
            println("Forward planning of the race line...");
        }

        moduleRaceLine.plan(planData, data, trackModel, observer, plan);

        if (TEXT_DEBUG && !controller.isPreCompiling()) {
            println("----------------------------------");
            println("");
            println("Backward planning of target speeds...");
        }

        // plan back to front, corner entry
        while (planData.hasMoreSegments()) {
            if (TEXT_DEBUG && !controller.isPreCompiling()) {
                println("Planning for tracksegment " + planData.currentSegment());
            }

            current = trackModel.getSegment(planData.currentSegment());

            if (current.isUnknown()) {
                if (TEXT_DEBUG && !controller.isPreCompiling()) {
                    println("Unknown");
                }
                plan.attachSpeed(new ConstantValue(planData.start(), planData.end(), 50.0));
                planData.approachSpeed = 50.0;

            } else if (current.isStraight()) {
                moduleStraight.planTargetSpeeds(planData, data, trackModel, observer, plan);

            } else { // corner
                moduleCorner.planTargetSpeeds(planData, data, trackModel, observer, plan);
            }

            planData.popSegment();
        }

        if (TEXT_DEBUG && !controller.isPreCompiling()) {
            println("finished");
        }

        if (TEXT_DEBUG && !controller.isPreCompiling()) {
            println("Verifying plan");
            plan.checkIntegrity();
            println("done");
            println("");
            plan.printSpeeds();
        }

        if (GRAPHICAL_DEBUG) {
            debugPainter.repaint();
        }
    }

    private void planUnknown(SensorData data, TrackSegment current) {
        if (TEXT_DEBUG && !controller.isPreCompiling()) {
            println("");
            println("*****************************************************");
            println("**                  Within unknown segment         **");
            println("*****************************************************");
        }

        clear();
        double remaining = current.getLength() - (data.getDistanceFromStartLine() - current.getStart());
        double end = data.getDistanceRaced() + remaining;

        if (TEXT_DEBUG && !controller.isPreCompiling()) {
            println("Remaning: " + remaining + "m");
            println("End: " + end + "m");
            println("NextDistance: " + nextDistance);
        }

        if (end <= nextDistance + 10.0) {
            end = nextDistance + 10.0;
            if (TEXT_DEBUG && !controller.isPreCompiling()) {
                println("Too close to nextDistance, moving end to: " + end + "m");
            }
        }

        plan.setCoverage(data.getDistanceRaced(), end);
        replanAt = plan.getEnd();

        if (Math.abs(data.getTrackPosition()) < 0.1) {
            plan.attachPosition(new ConstantValue(data.getDistanceRaced(), end, 0.0));

        } else if (remaining < 10.0) {
            double[] x = {data.getDistanceRaced(), end};
            double[] y = {data.getTrackPosition(), 0.0};
            CubicSplineTwoPoints spline = new CubicSplineTwoPoints(x, y);
            plan.attachPosition(spline);

        } else {
            double[] x = {data.getDistanceRaced(), data.getDistanceRaced() + 9.0};
            double[] y = {data.getTrackPosition(), 0.0};
            CubicSplineTwoPoints spline = new CubicSplineTwoPoints(x, y);
            plan.attachPosition(spline);
            plan.attachPosition(new ConstantValue(data.getDistanceRaced() + 9.0, end, 0.0));
        }
        plan.attachSpeed(new ConstantValue(data.getDistanceRaced(), end, 50.0));
    }

    public double getTargetSpeed(TrackSegment.Apex a) {
        if (a.unknown) {
            return 50.0;
        }

        if (a.targetSpeed == TrackSegment.DEFAULT_SPEED || alwaysUseGLF) {
            return calcSpeed(a.value) * SPEED_MODIFIER;

        } else {
            return a.targetSpeed * SPEED_MODIFIER;
        }
    }

    @Override
    public void setTrackModel(TrackModel t) {
        this.trackModel = t;
        observer.setTrackModel(t);
    }

    private void print(String s) {
        System.out.print(s);
    }

    protected void println(String s) {
        System.out.println(s);
    }

    public double getAnchorPoint(TrackSegment first, TrackSegment second) {
        if (first.getDirection() == Situations.DIRECTION_FORWARD && second.getDirection() == Situations.DIRECTION_RIGHT && !second.isFull()) {
            //System.out.println("F / R "+offset);

            return offset;

        } else if (first.getDirection() == Situations.DIRECTION_FORWARD && second.getDirection() == Situations.DIRECTION_LEFT && !second.isFull()) {
            //System.out.println("F / L");

            return -offset;

        } else if (first.getDirection() == Situations.DIRECTION_RIGHT && second.getDirection() == Situations.DIRECTION_FORWARD && !first.isFull()) {
            //System.out.println("R / F");

            return offset;

        } else if (first.getDirection() == Situations.DIRECTION_LEFT && second.getDirection() == Situations.DIRECTION_FORWARD && !first.isFull()) {
            //System.out.println("L / F");

            return -offset;

        } else if (first.getDirection() == Situations.DIRECTION_LEFT && second.getDirection() == Situations.DIRECTION_LEFT) {
            //System.out.println("L / L");

            return -offset;

        } else if (first.getDirection() == Situations.DIRECTION_RIGHT && second.getDirection() == Situations.DIRECTION_RIGHT) {
            //System.out.println("R / R");

            return offset;
        }
        //System.out.println("sonst");
        return 0;
    }

    @Override
    public void saveOnlineData(String suffix) {
        System.out.println("Plan.saveOnlineData");
        System.out.println("Stage: " + stage);
        System.out.println("OnlineLearner: " + onlineLearner);
        System.out.println("EA MODE: " + alwaysUseGLF);
        if (stage == scr.Controller.Stage.WARMUP && onlineLearner.active() && !alwaysUseGLF) {
            onlineLearner.saveData(suffix);
        }
    }

    /**
     * Calculates the distance needed to switch the position on the track by
     * delta meter.
     *
     * @param delta
     * @return
     */
    public static double calcSwitchDistance(SensorData data, double delta) {
        return calcSwitchDistance(data.getSpeed(), delta);
    }

    public static double calcSwitchDistance(double speed, double delta) {
        int index = Math.max(0, Math.min(M.length - 1, (int) Math.floor((speed - 50.0 + 12.5) / 25.0)));
        return M[index] * delta + B[index];
    }

    /**
     * Calculates the possible absolute change in trackposition.
     *
     * @param data
     * @param length
     * @return
     */
    public static double calcPossibleSwitchDelta(SensorData data, double length) {
        int index = Math.max(0, Math.min(M.length - 1, (int) Math.floor((data.getSpeed() - 50.0 + 12.5) / 25.0)));
        return (length - B[index]) / M[index];
    }
    private static final double[] M = {13.809, 17.959, 23.32, 28.857,
        34.277, 40.947, 51.484, 60.293, 60.839, 62.842};
    private static final double[] B = {-8.1055, -6.8262, 6.9141, -6.7871,
        -7.6758, -8.9551, -17.891, -9.668, -7.5175, -10.205};

    private class DebugPainter
            extends javax.swing.JPanel {

        /**
         * Debug data.
         */
        private SensorData[] data = new SensorData[10];
        private int dataEndIndex = -1;
        private int dataStartIndex = -1;
        /*private final Color DARK_GREEN = new Color(0, 129, 36);
         private final Color ORANGE = new Color(255, 154, 23);*/
        private double lastRepaintAt = 0.0;
        /**
         * First offscreen buffer.
         */
        private BufferedImage buffer1 = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        /**
         * Second offscreen buffer.
         */
        private BufferedImage buffer2 = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        /**
         * The front buffer.
         */
        private BufferedImage front = buffer1;
        /**
         * The back buffer.
         */
        private BufferedImage back = buffer2;

        public DebugPainter() {
            this.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentResized(ComponentEvent e) {
                    componentResizedImpl();
                }
            });
        }

        private synchronized void componentResizedImpl() {
            resetBuffers();
        }

        public void clearData() {
            dataEndIndex = -1;
            dataStartIndex = -1;
        }

        public void paintPlan(SensorData data) {
            long startTime = System.nanoTime();

            Graphics2D g = back.createGraphics();

            long endTime = System.nanoTime();
            //System.out.println("Geeting g took " + (endTime - startTime) / 1000 + "mikros");
            startTime = endTime;

            if (dataStartIndex == -1) {
                this.data[0] = data;
                dataEndIndex = 0;
                dataStartIndex = 0;

            } else if (data.getDistanceRaced() - this.data[dataEndIndex].getDistanceRaced() > 1.0) {
                dataEndIndex = (dataEndIndex + 1) % this.data.length;
                this.data[dataEndIndex] = data;
                if (dataEndIndex == dataStartIndex) {
                    dataStartIndex = (dataStartIndex + 1) % this.data.length;
                }
            }


            paintComponentUnsaveEDT(g);

            endTime = System.nanoTime();
            //System.out.println("Drwaing took " + (endTime - startTime) / 1000 + "mikros");
            startTime = endTime;

            synchronized (this) {
                swapBuffers();
            }

            repaint();

            endTime = System.nanoTime();
            //System.out.println("Swapping buffers took " + (endTime - startTime) / 1000 + "mikros");
        }

        @Override
        public void paintComponent(Graphics graphics) {
            Graphics2D g = (Graphics2D) graphics;

            g.setColor(Color.WHITE);

            g.fillRect(0, 0, getWidth(), getHeight());

            synchronized (this) {
                g.drawImage(front, 0, 0, this);
            }
        }

        private synchronized void resetBuffers() {
            this.buffer1 = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
            this.buffer2 = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
            this.front = buffer1;
            this.back = buffer2;

            Graphics2D g = this.front.createGraphics();
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, getWidth(), getHeight());
            g.setColor(Color.BLACK);
            g.drawString("resetBuffers: buffer 1", 30, 30);
            g.drawString(getWidth() + " / " + getHeight(), 30, 60);


            g = this.back.createGraphics();
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, getWidth(), getHeight());
            g.setColor(Color.BLACK);
            g.drawString("resetBuffers: buffer 2", 30, 30);
            g.drawString(getWidth() + " / " + getHeight(), 30, 60);
        }

        private synchronized void swapBuffers() {
            if (front == buffer1) {
                front = buffer2;
                back = buffer1;

            } else {
                front = buffer1;
                back = buffer2;
            }
        }

        public void paintComponentUnsaveEDT(Graphics2D g) {
            if (plan.getLength() == 0.0 || !Plan2013.this.trackModel.initialized() || dataStartIndex == -1) {
                g.setColor(Color.WHITE);
                g.fillRect(0, 0, getWidth(), getHeight());

                g.setColor(Color.BLACK);
                if (!Plan2013.this.trackModel.initialized()) {
                    g.drawString("Trackmodel not initialized", 20, 20);
                }
                if (plan.getLength() == 0.0) {
                    g.drawString("Plan is empty", 20, 40);
                }
                if (dataStartIndex == -1) {
                    g.drawString("Debug data is empty", 20, 60);
                }
                return;
            }

            SensorData lastData = data[dataEndIndex];
            lastRepaintAt = lastData.getDistanceRaced();
            Dimension size = getSize();

            g.setColor(Color.WHITE);
            g.fillRect(0, 0, getWidth(), getHeight());

            g.setColor(Color.BLACK);

            g.translate(10, 10);

            double totalLength = 0.0;
            double rest;

            {
                double d1 = Plan2013.this.toDistanceFromStartLine(lastData, plan.getStart());
                int index = Plan2013.this.trackModel.getIndex(d1);


                TrackSegment s1 = Plan2013.this.trackModel.getSegment(index);
                totalLength += s1.getLength();

                rest = d1 - s1.getStart();
                d1 = plan.getStart() + (s1.getEnd() - d1) + 0.5;

                while (plan.contains(d1)) {
                    index = Plan2013.this.trackModel.incIdx(index);
                    s1 = Plan2013.this.trackModel.getSegment(index);
                    totalLength += s1.getLength();
                    d1 += s1.getLength();
                }
            }

            double ppm = (1.0 * (size.width - 20)) / totalLength;

            {
                int offset = 0;

                double d1 = Plan2013.this.toDistanceFromStartLine(lastData, plan.getStart());
                int index = Plan2013.this.trackModel.getIndex(d1);

                TrackSegment s1 = Plan2013.this.trackModel.getSegment(index);

                Dimension d = TrackSegment.Painter.draw(s1, g, ppm,
                        TrackSegment.Painter.DRAW_SUBSEGMENTS | TrackSegment.Painter.DRAW_START_END);
                offset += d.width;
                g.translate(d.width, 0);

                d1 = plan.getStart() + (s1.getEnd() - d1) + 0.5;

                while (plan.contains(d1)) {
                    index = Plan2013.this.trackModel.incIdx(index);
                    s1 = Plan2013.this.trackModel.getSegment(index);

                    d = TrackSegment.Painter.draw(s1, g, ppm,
                            TrackSegment.Painter.DRAW_SUBSEGMENTS | TrackSegment.Painter.DRAW_START_END);
                    offset += d.width;
                    g.translate(d.width, 0);

                    d1 += s1.getLength();
                }

                g.translate(-offset, d.height);
            }

            int offset = (int) Math.round(rest * ppm);

            g.translate(offset, 5);

            PlanContent.Painter.draw(plan, g, ppm, lastData, this.data, this.dataStartIndex, this.dataEndIndex);
        }
    }

    public boolean ObsResponsForSpeed() {
        double speed = plan.getSpeed(nextDistance);
        boolean help;
        if (observer.otherCars() && observer.getRecommendedSpeed() != OpponentObserver.NO_RECOMMENDED_SPEED) {
            speed = Math.min(speed, observer.getRecommendedSpeed());
            if (speed == observer.getRecommendedSpeed()) {
                help = true;
            } else {
                help = false;
            }
        } else {
            help = false;
        }

        return help;
    }
}