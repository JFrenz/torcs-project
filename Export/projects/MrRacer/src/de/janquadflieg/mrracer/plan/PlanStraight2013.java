/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.plan;

import de.janquadflieg.mrracer.functions.LinearInterpolator;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Properties;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.functions.ConstantValue;
import de.janquadflieg.mrracer.functions.CubicSplineTwoPoints;
import de.janquadflieg.mrracer.functions.Interpolator;
import de.janquadflieg.mrracer.opponents.OpponentObserver;
import de.janquadflieg.mrracer.telemetry.SensorData;
import de.janquadflieg.mrracer.track.*;

/**
 *
 * @author quad
 */
public final class PlanStraight2013
        implements PlanSubModule {

    private Plan2013 plan;
    private SpeedPredictor speedPredictor;

    public PlanStraight2013(Plan2013 p, SpeedPredictor sp) {
        this.plan = p;
        speedPredictor = sp;
    }

    public void setParameters(Properties params, String prefix) {
    }

    public void getParameters(Properties params, String prefix) {
    }

    public void paint(String baseFileName, java.awt.Dimension d) {
    }

    @Override
    public final void planTargetSpeeds(PlanStackData planData,
            SensorData data, TrackModel trackModel, OpponentObserver observer,
            PlanContent planContent) {
        int index = planData.currentSegment();
        TrackSegment current = trackModel.getSegment(index);
        TrackSegment next = trackModel.getSegment(trackModel.incrementIndex(index));
        TrackSegment prev = trackModel.getSegment(trackModel.decrementIndex(index));

        double end = planData.end();
        double start = planData.start();

        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("");
            plan.println("------------ PlanStraight TargetSpeeds------------");
            plan.println("Start/End: " + Utils.dTS(start) + ", " + Utils.dTS(end) + " [" + Utils.dTS(end - start) + "m]" + " - ");
            planData.print();
            plan.println("");
        }

        // plan like the last element?
        boolean planLikeLast = current.contains(data.getDistanceFromStartLine());

        if (planLikeLast) {
            start = data.getDistanceRaced();

            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("This is the last segment to plan for...");
            }
        }

        // check if we can combine the planning of this segment and the previous
        if (!planLikeLast && prev.isStraight()) {
            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("Previous is also straight, checking further...");
            }
            if (start - prev.getLength() <= data.getDistanceRaced()) {
                if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                    plan.println("Prev is the last segment to plan for");
                }
                start = data.getDistanceRaced();
                planLikeLast = true;

            } else {
                if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                    plan.println("Prev is also a middle segment");
                }
                start -= prev.getLength();
                int prevIndex = trackModel.decrementIndex(index);
                int prevPrevIndex = trackModel.decrementIndex(prevIndex);
                if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                    plan.println("Switching prev from " + prevIndex + " to " + prevPrevIndex);
                }
                prev = trackModel.getSegment(prevPrevIndex);
            }

            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("Moving start to " + Utils.dTS(start) + ", new length " + Utils.dTS(end - start) + "m");
            }
            planData.popSegment();
        }

        double remainingLength = end - start;   // remaining length in this segment        

        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println(Utils.dTS(remainingLength) + "m remain in this segment...");
        }

        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("Checking the race line at the end of the segment");
        }

        double raceLine = planContent.getPosition(end);
        if (raceLine != Plan.NO_RACE_LINE) {
            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("Race line planned");
            }
            double absFinalPosition = SensorData.calcAbsoluteTrackPosition(planContent.getPosition(end), trackModel.getWidth());
            double absDesiredPosition = SensorData.calcAbsoluteTrackPosition(plan.getAnchorPoint(current, next), trackModel.getWidth());

            if (Math.abs(absFinalPosition - absDesiredPosition) > 1.0) {
                if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                    plan.println("Difference > 1.0");
                }

                if (next.isFull()) {
                    if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                        plan.println("Next segment is a full speed corner, nothing to do");
                    }

                } else {
                    if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                        plan.println("Reducing approach speed from " + Utils.dTS(planData.approachSpeed) + "km/h");
                    }
                    if (planData.approachSpeed == Plan2013.MAX_SPEED) {
                        planData.approachSpeed = 240.0;

                    } else {
                        planData.approachSpeed *= 0.8;
                    }

                    if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                        plan.println("to " + Utils.dTS(planData.approachSpeed) + "km/h");
                    }
                }
            } else {
                if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                    plan.println("Difference <=  1.0, everything OK");
                }
            }

        } else {
            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("No race line planned, nothing to check");
            }
        }

        // target speeds
        double brakeDistance = 0.0;

        if (planData.approachSpeed != Plan2013.MAX_SPEED) {
            if (planData.first()) {
                brakeDistance = plan.calcBrakingZoneStraight(data.getSpeed(), end - start,
                        planData.approachSpeed);
            } else {
                brakeDistance = plan.calcBrakingZoneStraight(planData.speed(), end - start,
                        planData.approachSpeed);
            }
        }

        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("Brake distance: " + Utils.dTS(brakeDistance) + "m");
        }

        double[] xS;
        double[] yS;

        if (brakeDistance >= end - start || brakeDistance == 0.0) {
            xS = new double[3];
            yS = new double[3];

            xS[0] = start;
            xS[2] = end;
            xS[1] = (xS[0] + xS[2]) / 2.0;

            if (brakeDistance == 0.0) {
                // create checkpoints
                plan.createAndAddCheckPoints(start, (end - start),
                        (planData.first() ? data.getSpeed() : planData.speed()));

                yS[0] = Plan2013.MAX_SPEED;
                yS[1] = Plan2013.MAX_SPEED;
                yS[2] = Plan2013.MAX_SPEED;
                planData.approachSpeed = Plan2013.MAX_SPEED;

            } else {
                yS[0] = planData.approachSpeed;
                yS[1] = planData.approachSpeed;
                yS[2] = planData.approachSpeed;
                planData.approachSpeed = plan.calcApproachSpeedStraight(planData.approachSpeed, end - start);
            }

        } else {
            xS = new double[4];
            yS = new double[4];            

            xS[0] = start;
            xS[1] = end - brakeDistance;
            xS[2] = end - (brakeDistance * 0.99);
            xS[3] = end;

            yS[0] = Plan2013.MAX_SPEED;
            yS[1] = Plan2013.MAX_SPEED;
            yS[2] = planData.approachSpeed;
            yS[3] = planData.approachSpeed;

            // create checkpoints
            plan.createAndAddCheckPoints(start, (xS[1] - start),
                    (planData.first() ? data.getSpeed() : planData.speed()));

            planData.approachSpeed = Plan2013.MAX_SPEED;
        }

        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("Speed:");
            for (int i = 0; i < xS.length; ++i) {
                plan.println(xS[i] + " , " + yS[i]);
            }
        }

        LinearInterpolator speed;

        try {
            speed = new LinearInterpolator(xS, yS);

        } catch (RuntimeException e) {
            System.out.println("*****************EXCEPTION**************");
            System.out.println("Start/End: " + Utils.dTS(start) + ", " + Utils.dTS(end) + " [" + Utils.dTS(end - start) + "m]" + " - ");
            System.out.println("Segment: " + current.toString());
            System.out.println("Start: " + start);
            System.out.println("End: " + end);
            System.out.println("BrakeDistance: " + brakeDistance);
            System.out.println("SensorData:");
            try {
                java.io.OutputStreamWriter osw = new java.io.OutputStreamWriter(System.out);
                SensorData.writeHeader(osw);
                osw.append('\n');
                data.write(osw);
                osw.flush();

            } catch (Exception schwupp) {
            }
            System.out.println("");
            System.out.println("Speed:");
            for (int i = 0; i < xS.length; ++i) {
                System.out.println(xS[i] + " , " + yS[i]);
            }
            System.out.println("Complete Model:");
            trackModel.print();

            throw e;
        }

        planContent.attachSpeed(speed);
    }
}