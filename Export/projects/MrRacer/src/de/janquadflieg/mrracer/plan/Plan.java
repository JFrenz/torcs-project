/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package de.janquadflieg.mrracer.plan;

import java.awt.geom.Point2D;

/**
 * Public interface for a plan. Used by external code like controllers, etc.
 *
 * @author quad
 */
public interface Plan
extends de.janquadflieg.mrracer.behaviour.Component, de.janquadflieg.mrracer.gui.GraphicDebugable{
    /** Debug in text mode? */
    public static final boolean TEXT_DEBUG = false;    
    /** Maximum possible speed. */
    public static final double MAX_SPEED = 330.0;
    /** Constant indicating that no racing line is available. */
    public static final double NO_RACE_LINE = 777.0;
    
    /**
     * Method to clear the current plan and set the implementing module into
     * a save, consistent state.
     */
    public void functionalReset();
    
    public double getTargetSpeed();    
    
    /**
     * Method called by the controller at the end of the warmup to save 
     * data.
     * @param suffix Suffix for the filename.
     */
    public void saveOnlineData(String suffix);
    
    /**
     * Method to set the current stage (warmup, qualifying, race).
     * @param s Stage.
     */
    public void setStage(scr.Controller.Stage s);    
    
    /**
     * Method to set the track model.
     * @param tm The trackmodel.
     */
    public void setTrackModel(de.janquadflieg.mrracer.track.TrackModel tm);
    
    
    public void resetFull();
    public void update(de.janquadflieg.mrracer.telemetry.SensorData data, 
            de.janquadflieg.mrracer.classification.Situation s);
    
    public java.awt.geom.Point2D getTargetPosition();
    public void updateOffTrack(de.janquadflieg.mrracer.telemetry.SensorData data);
    
    
    public void addSpeedVerifier(SpeedVerifier sv);    

    public void replan();       
    
    public void createAndAddCheckPoints(double start, double distance, double initialSpeed);        
}