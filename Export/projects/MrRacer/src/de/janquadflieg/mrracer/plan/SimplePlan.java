/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.plan;


import de.janquadflieg.mrracer.classification.Situation;
import de.janquadflieg.mrracer.functions.GeneralisedLogisticFunction;
import de.janquadflieg.mrracer.functions.GeneralisedLogisticFunction.XAxis;
import de.janquadflieg.mrracer.functions.GeneralisedLogisticFunction.YAxis;
import de.janquadflieg.mrracer.telemetry.SensorData;
import de.janquadflieg.mrracer.track.*;

import java.util.Properties;

/**
 *
 * @author quad
 */
public final class SimplePlan
        implements Plan {        
    /**
     * Target Speeds.
     */
    private GeneralisedLogisticFunction targetSpeeds = new GeneralisedLogisticFunction();
    /**
     * String Identifier.
     */
    public static final String TARGET_SPEEDS = "-PLAN.targetSpeeds-";    
    
    private double speed = 50.0;
    
    public void addSpeedVerifier(SpeedVerifier s){
        
    }
    
    public void addSpeedCheckPoint(java.awt.geom.Point2D p){
        
    }
    
    public void createAndAddCheckPoints(double start, double distance, double initialSpeed){
        
    }

    public SimplePlan() {
        Properties params = new Properties();
        params.setProperty(GeneralisedLogisticFunction.M, String.valueOf(0.2));
        targetSpeeds.setParameters(params, "");
    }

    @Override
    public void setParameters(Properties params, String prefix) {
        targetSpeeds.setParameters(params, prefix + TARGET_SPEEDS);        
    }

    @Override
    public void getParameters(Properties params, String prefix) {
        targetSpeeds.getParameters(params, prefix + TARGET_SPEEDS);        
    }
    
    @Override
    public javax.swing.JComponent[] getComponent() {
        return new javax.swing.JComponent[0];
    }

    @Override
    public void paint(String baseFileName, java.awt.Dimension d) {
        java.awt.image.BufferedImage img = new java.awt.image.BufferedImage(d.width, d.height, java.awt.image.BufferedImage.TYPE_INT_RGB);
        XAxis x = new XAxis();
        YAxis y = new YAxis();
        x.labelMin = 0.0;
        x.labelMax = 100.0;
        x.xmin = 0.0;
        x.xmax = 100.0;
        x.ticks = 10.0;
        x.unit = "°";

        y.mirror = true;
        y.labelMin = 0.0;
        y.y0 = 50;
        y.labelMax = 330.0;
        y.y1 = 330.0;
        y.ticks = 50.0;
        y.unit = "km/h";
        targetSpeeds.paint(img.createGraphics(), d, x, y);
        try {
            javax.imageio.ImageIO.write(img, "PNG", new java.io.File(baseFileName + "-speeds.png"));
        } catch (java.io.IOException e) {
            e.printStackTrace(System.out);
        }
    }

    @Override
    public void setStage(scr.Controller.Stage s) {        
    }

    @Override
    public void replan() {
        speed = 50.0;
    }

    @Override
    public void resetFull() {
        reset();
    }

    public void reset() {
        speed = 50.0;
    }       

    @Override
    public void functionalReset() {
        speed = 50.0;
    }    

    protected double calcSpeed(double v) {
        double value = Math.abs(v) / 100.0;
        double result = this.targetSpeeds.getMirroredValue(value);
        result *= 330 - 50;
        result += 50;

        return result;
    }    

    @Override
    public java.awt.geom.Point2D getTargetPosition() {
        double p = Plan.NO_RACE_LINE;

        double y = 1.0;

        return new java.awt.geom.Point2D.Double(p, y);
    }

    @Override
    public double getTargetSpeed() {
        return speed;        
    }   

    /**
     * Updates the plan when the car is not on the track. This is only necessary
     * during the warmup.
     *
     * @param data
     */
    @Override
    public final void updateOffTrack(SensorData data) {    
    }

    @Override
    public final void update(SensorData data, Situation situation) {
        if(data.onTrack()){
            double measure = situation.getMeasure();
            speed = calcSpeed(measure);
            
        } else {
            speed = 50.0;
        }
    }            

    @Override
    public void setTrackModel(TrackModel t) {     
    }    

    @Override
    public void saveOnlineData(String suffix) {     
    }    
}