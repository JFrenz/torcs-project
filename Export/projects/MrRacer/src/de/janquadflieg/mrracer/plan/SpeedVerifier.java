/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.plan;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.telemetry.SensorData;

/**
 *
 * @author quad
 */
public class SpeedVerifier {
    private double start;
    private double end;
    private double speed;   
    
    public SpeedVerifier(double s, double e, double speed){
        start = s;
        end = e;
        this.speed = speed;
    }
    
    public void update(SensorData d){
        if(start <= d.getDistanceRaced() && d.getDistanceRaced() <= end){
            //System.out.println("Checking: "+Utils.dTS(d.getSpeed())+"km/h, "+
            //        "should be "+Utils.dTS(speed)+"km/h");
        }
    }    
}