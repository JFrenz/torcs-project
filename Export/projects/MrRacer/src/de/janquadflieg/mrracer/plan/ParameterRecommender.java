/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.plan;

import de.janquadflieg.mrracer.Utils;
import java.util.*;

import de.janquadflieg.mrracer.telemetry.*;
import de.janquadflieg.mrracer.track.*;
import de.janquadflieg.mrracer.torcs.TrackDataList;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 *
 * @author quad
 */
public class ParameterRecommender {

    public static final String DESERT = "Desert";
    public static final String HILL = "Hill";
    public static final String MOUNTAIN = "Mountain";
    public static final String CITY = "City";
    private Properties baseCity;
    private Properties glfCityMueda;
    private Properties glfCityZongxoi;
    private Properties baseDesert;
    private Properties glfDesertKumbharli;
    private Properties glfDesertSandager;
    private Properties baseHill;
    private Properties glfHillBukavu;
    private Properties glfHillPonnesen;
    private Properties baseMountain;
    private Properties glfMountainR491;
    private Properties glfMountainTiasnimbas;
    private StringBuffer decisionLog = new StringBuffer();
    /**
     * Param file identifier. This can be used on the command line to override
     * the parameter sets used by the recommender. This only affects the warmup.
     * If this parameter is used, the controller still looks for a track
     * specific parameter set for the qualifying and the race. Usage:
     * -DParameters=file1,file2,file3 (jvm option).
     */
    public static final String PARAM_FILES = "RecommenderParameters";
    //public static final String LOAD_FROM_CLASSPATH = "RecommenderLoadFromClassPath";
    /**
     * Buckets for the middle sensor.
     */
    private double[] sensorBuckets = new double[21];
    /**
     * Counter.
     */
    private int counter = 0;
    /**
     *
     */
    private int zheightSignChanges = 0;
    private double zheightSignChangesNorm = 0.0;
    private double lastZHeightDelta = 0.0;
    private double zHeightDeltaSum = 0.0;
    private double zHeightDeltaSumNorm = 0.0;
    private SensorData lastData = null;
    private double distanceTo20 = 0.0;
    private double distanceTo30 = 0.0;
    private double distanceTo40 = 0.0;
    private double distanceTo50 = 0.0;
    private double zSpeedSum = 0.0;
    private double zSpeedSumNorm = 0.0;
    private boolean normalized = false;
    private ArrayList<Properties> recommendation = new ArrayList<>();

    public ParameterRecommender() {
        Arrays.fill(sensorBuckets, 0);

        if (System.getProperties().containsKey(PARAM_FILES)) {
            System.out.println(PARAM_FILES + " system property set");
            String file = System.getProperty(PARAM_FILES);
            System.out.println("Loading " + file + " and using this as the baseparameters for all four surfaces");
            try {
                baseCity = Utils.loadProperties(file, false);
                baseDesert = Utils.loadProperties(file, false);
                baseHill = Utils.loadProperties(file, false);
                baseMountain = Utils.loadProperties(file, false);
                
            } catch (Exception e) {
                System.out.println("Fatal: Failed to load " + file + " from classpath");
                System.out.println("Reason:");
                e.printStackTrace(System.out);
                System.exit(1);
            }

        } else {
            initNormal();
        }
    }

    private void initNormal() {
        String file = "/de/janquadflieg/mrracer/data/mobasecity.params";

        try {
            baseCity = Utils.loadProperties(file, true);
            /*file = "/de/janquadflieg/mrracer/data/glfmuedacity.params";
             glfCityMueda = Utils.loadProperties(file, true);
             file = "/de/janquadflieg/mrracer/data/glfzongxoicity.params";
             glfCityZongxoi = Utils.loadProperties(file, true);*/

            file = "/de/janquadflieg/mrracer/data/mobasedesert.params";
            baseDesert = Utils.loadProperties(file, true);
            /*file = "/de/janquadflieg/mrracer/data/glfkumbharlidesert.params";
             glfDesertKumbharli = Utils.loadProperties(file, true);
             file = "/de/janquadflieg/mrracer/data/glfsandagerdesert.params";
             glfDesertSandager = Utils.loadProperties(file, true);*/

            file = "/de/janquadflieg/mrracer/data/mobasehill.params";
            baseHill = Utils.loadProperties(file, true);
            /*file = "/de/janquadflieg/mrracer/data/glfbukavuhill.params";
             glfHillBukavu = Utils.loadProperties(file, true);
             file = "/de/janquadflieg/mrracer/data/glfponnesenhill.params";
             glfHillPonnesen = Utils.loadProperties(file, true);*/

            file = "/de/janquadflieg/mrracer/data/mobasemountain.params";
            baseMountain = Utils.loadProperties(file, true);
            /*file = "/de/janquadflieg/mrracer/data/glfr491mountain.params";
             glfMountainR491 = Utils.loadProperties(file, true);
             file = "/de/janquadflieg/mrracer/data/glftiasnimbasmountain.params";
             glfMountainTiasnimbas = Utils.loadProperties(file, true);*/

        } catch (Exception e) {
            System.out.println("Fatal: Failed to load " + file + " from classpath");
            System.out.println("Reason:");
            e.printStackTrace(System.out);
            System.exit(1);
        }
    }

    public int getRecommendationCount() {
        return recommendation.size();
    }

    public Properties getRecommmendation(int i) {
        return recommendation.get(i);
    }

    private void normalize(double trackLength) {
        if (normalized) {
            return;
        }

        for (int i = 0; i < sensorBuckets.length; ++i) {
            sensorBuckets[i] = sensorBuckets[i] / trackLength;
        }

        zheightSignChangesNorm = (1.0 * zheightSignChanges / trackLength);
        zHeightDeltaSumNorm = zHeightDeltaSum / trackLength;
        zSpeedSumNorm = zSpeedSum / trackLength;

        normalized = true;
    }

    public void analyse(double trackLength) {
        normalize(trackLength);
        System.out.print((counter / trackLength) + " ");
        for (int i = 0; i < sensorBuckets.length; ++i) {
            System.out.print(sensorBuckets[i] + " ");
        }
        System.out.print(zheightSignChangesNorm + " " + zheightSignChanges + " " + zHeightDeltaSum + " " + zHeightDeltaSumNorm + " ");

        System.out.print(distanceTo20 + " " + distanceTo30 + " " + distanceTo40 + " " + distanceTo50 + " ");

        System.out.print(zSpeedSumNorm);


        System.out.println("");
    }

    public String getLog() {
        return decisionLog.toString();
    }

    public Properties recommend(TrackModel tm) {
        normalize(tm.getLength());

        decisionLog = new StringBuffer();

        decisionLog.append("Normalized sum of |deltaZ| = ").append(zHeightDeltaSumNorm);
        if (zHeightDeltaSumNorm <= 0.0025) {
            decisionLog.append(" indicates city or hill track\n");

            decisionLog.append("Normalized sum of |zSpeed| = ").append(zSpeedSumNorm);
            if (zSpeedSumNorm <= 2.5) {
                decisionLog.append(" indicates city track\n");

                //decisionLog.append("Recommendation 1: Mueda glf\n");
                Properties result = new Properties();
                result.putAll(baseCity);
                //result.putAll(glfCityMueda);
                recommendation.add(result);

                /*decisionLog.append("Recommendation 2: Zongxoi glf\n");
                 result = new Properties();
                 result.putAll(baseCity);
                 result.putAll(glfCityZongxoi);
                 recommendation.add(result);*/

            } else {
                decisionLog.append(" indicates hill track\n");

                /*decisionLog.append("Recommendation 1: Bukavu glf\n");
                 Properties result = new Properties();
                 result.putAll(baseHill);
                 result.putAll(glfHillBukavu);
                 recommendation.add(result);*/

                //decisionLog.append("Recommendation 2: Ponnesen glf\n");
                Properties result = new Properties();
                result.putAll(baseHill);
                //result.putAll(glfHillPonnesen);
                recommendation.add(result);
            }

        } else if (zHeightDeltaSumNorm <= 0.0064) {
            decisionLog.append(" indicates desert track\n");

            //decisionLog.append("Recommendation 1: Kumbharli glf\n");
            Properties result = new Properties();
            result.putAll(baseDesert);
            //result.putAll(glfDesertKumbharli);
            recommendation.add(result);

            /*decisionLog.append("Recommendation 2: Sandager glf\n");
             result = new Properties();
             result.putAll(baseDesert);
             result.putAll(glfDesertSandager);
             recommendation.add(result);*/

        } else {
            decisionLog.append(" indicates mountain track\n");

            /*decisionLog.append("Recommendation 1: R491 glf\n");
             Properties result = new Properties();
             result.putAll(baseMountain);
             result.putAll(glfMountainR491);
             recommendation.add(result);*/

            //decisionLog.append("Recommendation 2: Tiasnimbas glf\n");
            Properties result = new Properties();
            result.putAll(baseMountain);
            //result.putAll(glfMountainTiasnimbas);
            recommendation.add(result);
        }

        return recommendation.get(0);
    }

    public void update(SensorData d) {
        if (!d.onTrack()) {
            return;
        }

        double sensorValue = Math.max(0.0, Math.min(200.0, d.getTrackEdgeSensors()[9]));
        int bucket = (int) Math.floor(sensorValue / 10.0);
        sensorBuckets[bucket]++;
        ++counter;

        if (lastData != null) {
            double delta = d.getTrackHeight() - lastData.getTrackHeight();
            if (delta * lastZHeightDelta < 0) {
                zheightSignChanges++;
            }
            zHeightDeltaSum += Math.abs(delta);
            lastZHeightDelta = delta;
        }

        if (distanceTo20 == 0.0 && d.getSpeed() >= 20.0) {
            distanceTo20 = d.getDistanceRaced();
        }

        if (distanceTo30 == 0.0 && d.getSpeed() >= 30.0) {
            distanceTo30 = d.getDistanceRaced();
        }

        if (distanceTo40 == 0.0 && d.getSpeed() >= 40.0) {
            distanceTo40 = d.getDistanceRaced();
        }

        if (distanceTo50 == 0.0 && d.getSpeed() >= 50.0) {
            distanceTo50 = d.getDistanceRaced();
        }

        zSpeedSum += Math.abs(d.getZSpeed());

        lastData = d;
    }

    public static void run(String[] tracks, String path, boolean noisy)
            throws Exception {
        TrackDataList trackDataList = new TrackDataList();
        for (int i = 0; i < tracks.length; ++i) {
//            if (i == 0) {
//                System.out.print("track ppm ");
//                for (int k = 0; k < 21; ++k) {
//                    System.out.print("sb" + k + " ");
//                }
//
//                System.out.print("zHSCnorm zHSC zHDS zHDSnorm ");
//
//                System.out.println("");
//            }

            System.out.print(tracks[i] + " ");
            ParameterRecommender pr = new ParameterRecommender();
            String fileName = path + trackDataList.getTrack(tracks[i]).controllerTrackName + "-1.telemetry";
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));

            String line = null;
            while ((line = reader.readLine()) != null) {
                pr.update(new SensorData(line, ";"));
            }
            pr.analyse(trackDataList.getTrack(tracks[i]).length);
        }
    }

    public static void main(String[] args) {
        String path = "F:/Quad/Experimente/2015-06-11-WarmupDaten/MrRacer/";

        String[] mountain = {"Berhet-mountain", "Bukavu-mountain", "Makowiec-mountain", "Noye-mountain", "Pinneo-mountain", "Quedam-mountain", "R491-mountain", "Sandager-mountain", "Shenkursk-mountain", "Sirevag-mountain", "Starlight-mountain", "Tailwinds-mountain", "Thornbridge-mountain", "TiasNimbas-mountain", "Torovo-mountain", "Watorowo-mountain", "Westplex-mountain", "Wildno-mountain", "Zdzieszulice-mountain", "Zorgvlied-mountain", "Zvolenovice-mountain"};
        String[] mountainSnow = {"Berhet-mountain-snow", "Bukavu-mountain-snow", "Kumbharli-mountain-snow", "Makowiec-mountain-snow", "Noye-mountain-snow", "Petrinja-mountain-snow", "Pinneo-mountain-snow", "Quedam-mountain-snow", "Rauleswor-mountain-snow", "Sirevag-mountain-snow", "Zdzieszulice-mountain-snow", "Zongxoi-mountain-snow"};
        String[] city = {"Kumbharli-city", "Makowiec-city", "Mueda-city", "Pinneo-city", "Quedam-city", "R491-city", "Revercombs-city", "Sancassa-city", "Sandager-city", "Sirevag-city", "Tailwinds-city", "Teruoka-city", "Torovo-city", "Watorowo-city", "Zongxoi-city"};
        String[] desert = {"Arraias-desert", "Kerang-desert", "Kumbharli-desert", "Makowiec-desert", "Noye-desert", "Petrinja-desert", "Pinneo-desert", "Ponnesen-desert", "Quedam-desert", "R491-desert", "Rauleswor-desert", "Sandager-desert", "Sirevag-desert", "Starlight-desert", "Wildno-desert"};
        String[] hill = {"Alsoujlak-hill", "Berhet-hill", "Bukavu-hill", "Kumbharli-hill", "Makowiec-hill", "Noye-hill", "Petrinja-hill", "Pinneo-hill", "Ponnesen-hill", "Quedam-hill", "R491-hill", "Rauleswor-hill", "Revercombs-hill", "Sandager-hill", "Shenkursk-hill", "Sirevag-hill", "Starlight-hill", "Tailwinds-hill", "Teruoka-hill", "Thornbridge-hill", "TiasNimbas-hill", "Tsaritsani-hill", "Volcan-hill", "Watorowo-hill", "Westplex-hill", "Wildno-hill", "Zbydniow-hill", "Zdzieszulice-hill", "Zihoun-hill", "Zongxoi-hill", "Zorgvlied-hill"};

        System.out.print("track ppm ");
        for (int k = 0; k < 21; ++k) {
            System.out.print("sb" + k + " ");
        }

        System.out.print("zHSCnorm zHSC zHDS zHDSnorm distTo20 distTo30 distTo40 distTo50 zSpeedSum");

        System.out.println("");

        try {


            //System.out.println("city");
            run(city, path, false);

            //System.out.println("desert");
            run(desert, path, false);

            //System.out.println("hill");
            run(hill, path, false);

            //System.out.println("mountain");
            run(mountain, path, false);

            //System.out.println("mountainsnow");
            run(mountainSnow, path, false);

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
}
