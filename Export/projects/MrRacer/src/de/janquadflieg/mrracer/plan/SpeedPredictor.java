/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.plan;

import de.janquadflieg.mrracer.behaviour.Component;

import java.awt.geom.Point2D;
import java.io.*;
import java.util.*;

/**
 *
 * @author quad
 */
public class SpeedPredictor
implements Component {

    private ArrayList<Point2D> list;
    private HashMap<String, ArrayList<Point2D>> data = new HashMap<>();    
    public static final String TABLE_NAME = "-SpeedPredictor.table-";
    private String table = "speedprediction-city";
    
    public SpeedPredictor() {
        data.put("speedprediction-city", loadTable("speedprediction-city"));
        data.put("speedprediction-desert", loadTable("speedprediction-desert"));
        data.put("speedprediction-hill", loadTable("speedprediction-hill"));
        data.put("speedprediction-mountain", loadTable("speedprediction-mountain"));
        data.put("speedprediction-mountainsnow", loadTable("speedprediction-mountainsnow"));
        data.put("speedprediction-old", loadTable("speedprediction-old"));
        list = data.get(table);
    }
    
    public void setParameters(Properties params, String prefix){
        if(params.containsKey(prefix+TABLE_NAME)){
            table = params.getProperty(prefix+TABLE_NAME);
            if(!data.containsKey(table)){
                System.out.println("WARNING: requested table "+table+" not available");
                System.out.println("Using default: speedprediction-city");
                table = "speedprediction-city";            
            }
            
        } else {
            System.out.println("WARNING: properties do not contain "+prefix+TABLE_NAME);
            System.out.println("Using default: speedprediction-city");
            table = "speedprediction-city";            
        }
        list = data.get(table);
    }

    public void getParameters(Properties params, String prefix){
        params.put(prefix+TABLE_NAME, table);
    }

    public void paint(String baseFileName, java.awt.Dimension d){
        
    }
    
    private static ArrayList<Point2D> loadTable(String name){
        try {
            ArrayList<Point2D> list = new ArrayList<>();
            
            InputStream in = list.getClass().getResourceAsStream("/de/janquadflieg/mrracer/data/"+name);

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;

            while ((line = reader.readLine()) != null) {
                line = line.trim();

                if (!line.isEmpty()) {
                    double distance = Double.parseDouble(line.substring(0, line.indexOf(";")));
                    double speed = Double.parseDouble(line.substring(line.indexOf(";") + 1, line.length()));

                    list.add(new Point2D.Double(distance, speed));
                }
            }           

            reader.close();
            return list;            
            
        } catch (Exception e) {
            e.printStackTrace(System.out);
            return null;
        }
    }

    public final double predictSpeed(final double currentSpeed, double accDistance) {
        //System.out.println("predictSpeed: current="+currentSpeed+", accDist="+accDistance);
        accDistance = Math.max(0.0, accDistance);

        if (accDistance == 0.0) {
            return currentSpeed;
        }

        int bestIdx = 0;
        double delta = Math.abs(list.get(0).getY() - currentSpeed);

        for (int i = 1; i < list.size(); ++i) {
            double delta2 = Math.abs(list.get(i).getY() - currentSpeed);
            if (delta2 < delta) {
                delta = delta2;
                bestIdx = i;
            } else {
                break;
            }
        }

        //System.out.println(list.get(bestIdx));

        double distance = list.get(bestIdx).getX() + accDistance;
        delta = Double.POSITIVE_INFINITY;

        if (distance > list.get(list.size() - 1).getX()) {
            return list.get(list.size() - 1).getY();
        }

        for (int i = bestIdx; i < list.size(); ++i) {
            double delta2 = Math.abs(list.get(i).getX() - distance);



            if (delta2 < delta) {
                delta = delta2;
                bestIdx = i;
            } else {
                break;
            }
        }

        //System.out.println(list.get(bestIdx));

        return list.get(bestIdx).getY();
    }

    private static ArrayList<Point2D> createFilteredList(String file) {
        ArrayList<Point2D> result = new ArrayList<>();
        ArrayList<Point2D> helper = new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(file))));

            String line;

            while ((line = reader.readLine()) != null) {
                line = line.trim();

                if (!line.isEmpty()) {
                    double distance = Double.parseDouble(line.substring(0, line.indexOf(";")));
                    double speed = Double.parseDouble(line.substring(line.indexOf(";") + 1, line.length()));

                    helper.add(new Point2D.Double(distance, speed));
                }
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        for (int distance = 0; distance <= 2000;) {
            Point2D bestMatch = new Point2D.Double(0.0, 0.0);

            for (int i = 0; i < helper.size(); ++i) {
                if (Math.abs(helper.get(i).getX() - distance)
                        < Math.abs(bestMatch.getX() - distance)) {
                    bestMatch.setLocation(helper.get(i));
                }
            }

            result.add(bestMatch);

            if (distance >= 800) {
                distance += 40;

            } else if (distance >= 400) {
                distance += 20;

            } else if (distance >= 200) {
                distance += 10;
            } else {
                distance += 5;
            }
        }

        for (int i = 1; i < result.size(); ++i) {
            if (result.get(i).getY() <= result.get(i - 1).getY()) {
                System.out.println(result.get(i - 1));
                System.out.println(result.get(i));                
                result.get(i).setLocation(result.get(i).getX(), result.get(i).getY()+0.2);
                System.out.println("-> "+result.get(i));                        
            }            
        }

        return result;
    }

    public static void main(String[] args) {
        ArrayList<Point2D> list = createFilteredList("f:/speedmountainsnow");
        
        for (int i = 0; i < list.size(); ++i) {
            System.out.println(list.get(i).getX()+";"+list.get(i).getY());
        }
    }
}