/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.plan;

import java.awt.Dimension;
import java.util.Properties;
import javax.swing.JComponent;

/**
 *
 * @author quad
 */
public class DummyPlan 
implements Plan{
    
    /**
     * Method to clear the current plan and set the implementing module into
     * a save, consistent state.
     */
    public void functionalReset(){
    }
    
    public double getTargetSpeed(){
        return 300;
    }  
    
    public void addSpeedVerifier(SpeedVerifier s){
        
    }
    
    public void createAndAddCheckPoints(double start, double distance, double initialSpeed){
        
    }
    
    /**
     * Method called by the controller at the end of the warmup to save 
     * data.
     * @param suffix Suffix for the filename.
     */
    public void saveOnlineData(String suffix){
        
    }
    
    /**
     * Method to set the current stage (warmup, qualifying, race).
     * @param s Stage.
     */
    public void setStage(scr.Controller.Stage s){
        
    }    
    
    /**
     * Method to set the track model.
     * @param tm The trackmodel.
     */
    public void setTrackModel(de.janquadflieg.mrracer.track.TrackModel tm){
        
    }
    
    
    public void resetFull(){
        
    }
    public void update(de.janquadflieg.mrracer.telemetry.SensorData data, 
            de.janquadflieg.mrracer.classification.Situation s){
        
    }
    
    public java.awt.geom.Point2D getTargetPosition(){
        return null;
    }
    
    public void updateOffTrack(de.janquadflieg.mrracer.telemetry.SensorData data){        
    }   

    public void replan(){
        
    }    

    @Override
    public void setParameters(Properties params, String prefix) {
        
    }

    @Override
    public void getParameters(Properties params, String prefix) {
        
    }

    @Override
    public void paint(String baseFileName, Dimension d) {
        
    }

    @Override
    public JComponent[] getComponent() {
        return new javax.swing.JComponent[0];
    }
}