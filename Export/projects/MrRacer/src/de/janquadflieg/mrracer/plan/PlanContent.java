/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.plan;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.telemetry.SensorData;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import de.janquadflieg.mrracer.functions.Interpolator;
import java.awt.AlphaComposite;
import java.awt.geom.Point2D;

/**
 *
 * @author Jan Quadflieg
 */
public class PlanContent {

    private double start, end;
    private ArrayList<Interpolator> positions = new ArrayList<>();
    private ArrayList<Interpolator> speeds = new ArrayList<>();
    private static final boolean DO_CHECKS = false;
    private static final double EPSILON = 0.00001;
    /**
     * List of positions, at which we want to verify, if the current speed
     * matches the predicted speed.
     */
    private ArrayList<Point2D> speedCheckPoints = new ArrayList<>();

    public PlanContent() {
        start = -1.0;
        end = -1.0;
    }

    public void addSpeedCheckPoint(java.awt.geom.Point2D p) {
        speedCheckPoints.add(p);
        Collections.sort(speedCheckPoints, new Comparator<Point2D>() {
            @Override
            public int compare(Point2D p1, Point2D p2) {
                if (p1.getX() < p2.getX()) {
                    return -1;

                } else if (p1.getX() > p2.getX()) {
                    return 1;

                } else {
                    return 0;
                }
            }
        });
    }    
    
    public Point2D getNextSpeedCheckPoint(){
        if(speedCheckPoints.size() > 0){
            return speedCheckPoints.get(0);
        }
        return null;
    }
    
    public void removeNextSpeedCheckPoint(){
        if(speedCheckPoints.size() > 0){
            speedCheckPoints.remove(0);
            if(Plan.TEXT_DEBUG){
                System.out.println(speedCheckPoints.size()+" checkpoints remain");
            }
        }        
    }

    public void clear() {
        positions.clear();
        speeds.clear();
        speedCheckPoints.clear();
        start = -1.0;
        end = -1.0;
    }

    public void checkIntegrity() {
        if (!DO_CHECKS) {
            return;
        }

        Collections.sort(positions);
        Collections.sort(speeds);

        if (positions.isEmpty()) {
            System.out.println("WARNING-NO RACELINE PLANNED!");
        }

        if (positions.size() > 0 && positions.get(0).getXmin() > start
                && Math.abs(positions.get(0).getXmin() - start) > EPSILON) {
            System.out.println("WARNING-GAP IN RACELINE: First part starts at "
                    + positions.get(0).getXmin() + "m but the plan starts at "
                    + start + "m");
        }

        if (positions.size() > 0 && positions.get(positions.size() - 1).getXmax() < end
                && Math.abs(positions.get(positions.size() - 1).getXmax() - end) > EPSILON) {
            System.out.println("WARNING-GAP IN RACELINE: Last part ends at "
                    + positions.get(positions.size() - 1).getXmax() + "m but the plan ends at "
                    + end + "m");
        }

        for (int i = 1; i < positions.size(); ++i) {
            // luecke zwischen zwei teilen?
            if (positions.get(i).getXmin() > positions.get(i - 1).getXmax()
                    && Math.abs(positions.get(i).getXmin() - positions.get(i - 1).getXmax()) > EPSILON) {
                System.out.println("WARNING-GAP IN RACELINE: Part[" + (i - 1) + "] ends at "
                        + positions.get(i - 1).getXmax() + "m and part[" + i + "] starts at "
                        + positions.get(i).getXmin() + "m");
            }
            // zwei teile ueberlappen sich?
            if (positions.get(i).getXmin() < positions.get(i - 1).getXmax()
                    && Math.abs(positions.get(i).getXmin() - positions.get(i - 1).getXmax()) > EPSILON) {
                System.out.println("WARNING-OVERLAP IN RACELINE: Part[" + (i - 1) + "] ends at "
                        + positions.get(i - 1).getXmax() + "m and part[" + i + "] starts at "
                        + positions.get(i).getXmin() + "m");
            }
            // zwei teile passen nicht aneinander?
            if (Math.abs(positions.get(i).getXmin() - positions.get(i - 1).getXmax()) < EPSILON) {
                double trackPosition1 = positions.get(i - 1).interpolate(positions.get(i - 1).getXmax());
                double trackPosition2 = positions.get(i).interpolate(positions.get(i).getXmin());

                if (Math.abs(trackPosition1 - trackPosition2) > EPSILON) {
                    System.out.println("WARNING-RACELINES DO NOT CONNECT: Part[" + (i - 1)
                            + "] ends with trackPosition " + trackPosition1 + " at "
                            + positions.get(i - 1).getXmax() + " and part[" + i + "] starts "
                            + "with trackPosition " + trackPosition2 + " at "
                            + positions.get(i).getXmin());

                }
            }
        }

        if (speeds.isEmpty()) {
            System.out.println("WARNING-NO TARGET-SPEEDS PLANNED!");
        }

        for (int i = 0; i < speeds.size(); ++i) {
            // luecke zwischen zwei teilen?
            if (i > 0 && speeds.get(i).getXmin() > speeds.get(i - 1).getXmax()
                    && Math.abs(speeds.get(i).getXmin() - speeds.get(i - 1).getXmax()) > EPSILON) {
                System.out.println("WARNING-GAP IN TARGETSPEEDS: Part[" + (i - 1) + "] ends at "
                        + speeds.get(i - 1).getXmax() + "m and part[" + i + "] starts at "
                        + speeds.get(i).getXmin() + "m");
            }
            // zwei teile ueberlappen sich?
            if (i > 0 && speeds.get(i).getXmin() < speeds.get(i - 1).getXmax()
                    && Math.abs(speeds.get(i).getXmin() - speeds.get(i - 1).getXmax()) > EPSILON) {
                System.out.println("WARNING-OVERLAP IN TARGETSPEEDS: Part[" + (i - 1) + "] ends at "
                        + speeds.get(i - 1).getXmax() + "m and part[" + i + "] starts at "
                        + speeds.get(i).getXmin() + "m");
            }
        }
    }

    public void setCoverage(double s, double e) {
        start = s;
        end = e;
    }

    public boolean contains(double d) {
        return (d >= start && d <= end);
    }

    public double getStart() {
        return start;
    }

    public double getEnd() {
        return end;
    }

    public double getLength() {
        return getEnd() - getStart();
    }

    protected void attachPosition(Interpolator spline) {
        this.positions.add(spline);
    }

    protected void attachSpeed(Interpolator spline) {
        this.speeds.add(spline);
    }

    public double getPosition(double d) {
        if (positions.isEmpty()) {
            return Plan.NO_RACE_LINE;

        } else {
            for (Interpolator position : positions) {
                if (position.getXmin() <= d && position.getXmax() >= d) {
                    return position.interpolate(d);
                }
            }
        }
        return Plan.NO_RACE_LINE;
    }

    public double getSpeed(double d) {
        if (speeds.isEmpty()) {
            return 50.0;

        } else {
            for (Interpolator speed : speeds) {
                if (speed.getXmin() <= d && speed.getXmax() >= d) {
                    return speed.interpolate(d);
                }
            }
        }
        return 50.0;
    }

    public void printSpeeds() {
        System.out.println("Speeds:");
        if (speeds.isEmpty()) {
            System.out.println("50km/h");
        } else {
            Collections.sort(speeds);
            for (Interpolator i : speeds) {
                System.out.println(Utils.dTS(i.getXmin())
                        + ": " + Utils.dTS(i.interpolate(i.getXmin())) + "km/h, "
                        + Utils.dTS(i.getXmax()) + ": "
                        + Utils.dTS(i.interpolate(i.getXmax())) + "km/h");
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append("PlanElement ").append(Utils.dTS(getStart()));
        result.append("-");
        if (getPosition(getStart()) == Plan.NO_RACE_LINE) {
            result.append("NRL");

        } else {
            result.append(Utils.dTS(getPosition(getStart())));
        }
        result.append(" : ").append(Utils.dTS(getEnd())).append("-");
        if (getPosition(getEnd()) == Plan.NO_RACE_LINE) {
            result.append("NRL");

        } else {
            result.append(Utils.dTS(getPosition(getEnd())));
        }

        return result.toString();
    }

    public static class Painter {

        private static final int BLOCK_HEIGHT = 100;
        private static final int BLOCK_DISTANCE = 10;
        private static final Color DARK_GREEN = new Color(0, 129, 36);

        public static Dimension draw(PlanContent e, Graphics2D g, double ppm,
                SensorData data, SensorData[] debugData, int dataStartIndex, int dataEndIndex) {
            Dimension result = new Dimension();

            result.width = (int) Math.round(e.getLength() * ppm);
            result.height = (2 * BLOCK_HEIGHT) + BLOCK_DISTANCE;

            g.setColor(DARK_GREEN);

            g.fillRect(0, 0, result.width, BLOCK_HEIGHT);
            g.fillRect(0, BLOCK_HEIGHT + BLOCK_DISTANCE, result.width, BLOCK_HEIGHT);


            g.setColor(Color.WHITE);
            g.fillRect(4, 4, result.width - 8, BLOCK_HEIGHT - 8);
            g.fillRect(4, BLOCK_HEIGHT + BLOCK_DISTANCE + 4, result.width - 8, BLOCK_HEIGHT - 8);

            final double mpp = e.getLength() / (result.width * 1.0);

            for (int x = 0; x < result.width; ++x) {
                double d = e.getStart() + (mpp * x);
                double v = e.getSpeed(d);

                g.setColor(Color.GREEN);
                int y = BLOCK_HEIGHT - 4 - (int) Math.round((BLOCK_HEIGHT - 8) * (v / Plan.MAX_SPEED));
                g.drawRect(x, y, 1, 1);

                v = e.getPosition(d);
                if (v != Plan.NO_RACE_LINE) {
                    v = Math.max(-1.0, Math.min(1.0, v));
                    y = (2 * BLOCK_HEIGHT) + BLOCK_DISTANCE - 4 - (int) Math.round((BLOCK_HEIGHT - 8) * ((v + 1.0) / 2.0));
                    g.drawRect(x, y, 1, 1);
                }
            }

            if (e.contains(data.getDistanceRaced())) {
                g.setColor(Color.MAGENTA);
                int x = (int) Math.round((data.getDistanceRaced() - e.getStart()) * ppm);
                g.drawLine(x, 0, x, BLOCK_HEIGHT);
                g.drawLine(x, BLOCK_HEIGHT + BLOCK_DISTANCE, x, (2 * BLOCK_HEIGHT) + BLOCK_DISTANCE);
            }

            g.setColor(Color.BLACK);
            String s = Utils.dTS(e.getStart());
            if (result.width >= g.getFontMetrics().stringWidth(s) + 5) {
                g.drawString(s, 5, (2 * BLOCK_HEIGHT) + BLOCK_DISTANCE + g.getFontMetrics().getHeight());
            }

            s = Utils.dTS(e.getEnd());
            if (result.width >= g.getFontMetrics().stringWidth(s) + 5) {
                g.drawString(s, result.width - (g.getFontMetrics().stringWidth(s) + 5),
                        (2 * BLOCK_HEIGHT) + BLOCK_DISTANCE + g.getFontMetrics().getHeight());
            }

            g.setColor(Color.MAGENTA);

            for (int i = dataStartIndex; i != dataEndIndex; i = (i + 1) % debugData.length) {
                SensorData d = debugData[i];

                int x = (int) Math.round((d.getDistanceRaced() - e.getStart()) * ppm);

                if (x < 0 || x > result.width) {
                    continue;
                }

                double v = d.getSpeed();
                v = Math.max(0.0, Math.min(330.0, v));
                int y = BLOCK_HEIGHT - 4 - (int) Math.round((BLOCK_HEIGHT - 8) * (v / Plan.MAX_SPEED));
                g.drawRect(x, y, 1, 1);

                v = d.getTrackPosition();
                v = Math.max(-1.0, Math.min(1.0, v));
                y = (2 * BLOCK_HEIGHT) + BLOCK_DISTANCE - 4 - (int) Math.round((BLOCK_HEIGHT - 8) * ((v + 1.0) / 2.0));
                g.drawRect(x, y, 1, 1);
            }


            return result;
        }
    }
}