/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.plan;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.functions.ConstantValue;
import de.janquadflieg.mrracer.functions.CubicSplineTwoPoints;
import de.janquadflieg.mrracer.functions.Interpolator;
import de.janquadflieg.mrracer.functions.LinearInterpolator;
import de.janquadflieg.mrracer.opponents.OpponentObserver;
import static de.janquadflieg.mrracer.plan.Plan.TEXT_DEBUG;
import de.janquadflieg.mrracer.telemetry.SensorData;
import de.janquadflieg.mrracer.track.TrackModel;
import de.janquadflieg.mrracer.track.TrackSegment;
import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 *
 * @author quad
 */
public class PlanRaceLineJan {
    private Plan2013 plan;
    /** The minimum absolute delta needed to switch the position at all. */
    private static final double MIN_RELEVANT_ABS_DELTA = 0.2;
    /** The minimum length to plan a straight line before the corner. */
    private static final double MIN_RELEVANT_STRAIGHT = 1.0;

    public PlanRaceLineJan(Plan2013 p) {
        this.plan = p;
    }
    
    public final void plan(PlanStackData planData,
            SensorData data, TrackModel trackModel, OpponentObserver observer,
            PlanContent planContent){        
        
        for(int i=0; i < planData.size(); ++i){
            int index = planData.getTrackSegmentIndex(i);
            TrackSegment segment = trackModel.getSegment(index);
            
            boolean skip = false;
            
            if(segment.isUnknown()){
                planRaceLineUnknown(i, planData, data, trackModel, planContent);
                
            } else if(segment.isStraight()){
                skip = planRaceLineStraight(i, planData, data, trackModel, observer,
                    planContent);
                
            } else if(segment.isCorner() && segment.isFull()){
                planRaceLineFullSpeedCorner(i, planData, data, trackModel, observer,
                    planContent);
                
            } else {
                
            }
            
            if(skip){
                ++i;
            }
        }        
    }
    
    private void planRaceLineUnknown(int dataIndex, PlanStackData planData,
            SensorData data, TrackModel trackModel, PlanContent planContent){
        
        int index = planData.getTrackSegmentIndex(dataIndex);
        TrackSegment current = trackModel.getSegment(index);        

        double end = planData.end(dataIndex);
        double start = planData.start(dataIndex);        
        
        if (TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("");
            plan.println("------------ PlanRaceLineUnknown------------");
            plan.println("Start/End: " + Utils.dTS(start) + ", " + Utils.dTS(end) + " [" + Utils.dTS(end - start) + "m]" + " - ");
            planData.print();
            plan.println("");        
        }
        
        // plan like the first element?
        boolean planLikeFirst = current.contains(data.getDistanceFromStartLine());

        if (planLikeFirst) {
            start = data.getDistanceRaced();

            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("This is the first segment to plan for...");
            }
        }
        
        //planContent.attachPosition(new ConstantValue(start, end, 0.0));
        
        double remaining = current.getLength() - (data.getDistanceFromStartLine() - current.getStart());
        
        if ((planLikeFirst && Math.abs(data.getTrackPosition()) < 0.1) || !planLikeFirst) {
            planContent.attachPosition(new ConstantValue(start, end, 0.0));

        } else if (remaining < 10.0) {
            double[] x = {start, end};
            double[] y = {data.getTrackPosition(), 0.0};
            CubicSplineTwoPoints spline = new CubicSplineTwoPoints(x, y);
            planContent.attachPosition(spline);

        } else {
            double[] x = {start, data.getDistanceRaced() + 9.0};
            double[] y = {data.getTrackPosition(), 0.0};
            CubicSplineTwoPoints spline = new CubicSplineTwoPoints(x, y);
            planContent.attachPosition(spline);
            planContent.attachPosition(new ConstantValue(data.getDistanceRaced() + 9.0, end, 0.0));
        }        
    }
    
    private void planRaceLineFullSpeedCorner(int dataIndex, PlanStackData planData,
            SensorData data, TrackModel trackModel, OpponentObserver observer,
            PlanContent planContent){
        int index = planData.getTrackSegmentIndex(dataIndex);
        TrackSegment current = trackModel.getSegment(index);
        TrackSegment next = trackModel.getSegment(trackModel.incrementIndex(index));
        TrackSegment prev = trackModel.getSegment(trackModel.decrementIndex(index));

        double end = planData.end(dataIndex);
        double start = planData.start(dataIndex);        

        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("");
            plan.println("------------ PlanRaceFullSpeedCorner------------");
            plan.println("Start/End: " + Utils.dTS(start) + ", " + Utils.dTS(end) + " [" + Utils.dTS(end - start) + "m]" + " - ");
            planData.print();
            plan.println("");
        }
        
        if(prev.isStraight() && next.isStraight()){
            planContent.attachPosition(new ConstantValue(start, end, 0.0));
        }
    }   
    
    private boolean planRaceLineStraight(int dataIndex, PlanStackData planData,
            SensorData data, TrackModel trackModel, OpponentObserver observer,
            PlanContent planContent){
        int index = planData.getTrackSegmentIndex(dataIndex);
        TrackSegment current = trackModel.getSegment(index);
        TrackSegment next = trackModel.getSegment(trackModel.incrementIndex(index));
        TrackSegment prev = trackModel.getSegment(trackModel.decrementIndex(index));

        double end = planData.end(dataIndex);
        double start = planData.start(dataIndex);
        
        boolean result = false;

        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("");
            plan.println("------------ PlanRaceLineStraight------------");
            plan.println("Start/End: " + Utils.dTS(start) + ", " + Utils.dTS(end) + " [" + Utils.dTS(end - start) + "m]" + " - ");
            planData.print();
            plan.println("");
        }

        // plan like the first element?
        boolean planLikeFirst = current.contains(data.getDistanceFromStartLine());

        if (planLikeFirst) {
            start = data.getDistanceRaced();

            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("This is the first segment to plan for...");
            }
        }

        // check if we can combine the planning of this segment and the next one
        if (next.isStraight() && dataIndex < planData.size()-1) {
            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("Next is also straight");
            }
            
            end = planData.end(dataIndex+1);
            int nextIndex = trackModel.incrementIndex(index);
            int nextNextIndex = trackModel.incIdx(nextIndex);
            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("Switching next from " + nextIndex + " to " + nextNextIndex);
            }
            next = trackModel.getSegment(nextNextIndex);
            

            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("Moving end to " + Utils.dTS(end) + ", new length " + Utils.dTS(end - start) + "m");
            }
            
            result = true;
        }

        Point2D targetPosition = OpponentObserver.NO_RECOMMENDED_POINT;

        if (observer.otherCars() && observer.getRecommendedPosition() != OpponentObserver.NO_RECOMMENDED_POINT) {
            Point2D recommendation = new Point2D.Double();
            recommendation.setLocation(observer.getRecommendedPosition());

            if (start <= recommendation.getY() && recommendation.getY() < end) {
                targetPosition = new Point2D.Double();
                targetPosition.setLocation(recommendation);
            }
        }

        // track positions
        ArrayList<Interpolator> positions = new ArrayList<>();

        double currStart = start;   // current start
        double remainingLength = end - start;   // remaining length in this segment
        double currPosition = plan.getAnchorPoint(prev, current);   // the current position in the track
        if (planLikeFirst) {
            currPosition = data.getTrackPosition();
        }

        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println(Utils.dTS(remainingLength) + "m remain in this segment...");
        }

        if (targetPosition != OpponentObserver.NO_RECOMMENDED_POINT) {
            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("I need to take care of other cars, point is " + targetPosition.toString());
                plan.println("Right now, i'm planning with currStart: " + Utils.dTS(currStart));
                plan.println("Type of the target position: " + observer.getPositionType().toString());
            }
            if (targetPosition.getY() < currStart) {
                if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                    plan.println("Point is before currStart, moving it to +1m");
                }
                targetPosition.setLocation(targetPosition.getX(), currStart + 1.0);
            }

            if (observer.getPositionType() == OpponentObserver.PositionType.OVERTAKING) {
                if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                    plan.println("Checking, if there is enough room...");
                }

                double lengthNeeded = targetPosition.getY() - currStart;

                if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                    plan.println("Observer tells me, that I need at least " + Utils.dTS(lengthNeeded) + "m, " + Utils.dTS(remainingLength) + "m remain");
                }

                if (remainingLength < lengthNeeded) {
                    targetPosition = OpponentObserver.NO_RECOMMENDED_POINT;
                    if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                        plan.println("Cannot overtake, not enough room");
                    }
                }

            } else if (observer.getPositionType() == OpponentObserver.PositionType.BLOCKING) {
                if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                    plan.println("Trying to block");
                }
            }
        }

        if (targetPosition != OpponentObserver.NO_RECOMMENDED_POINT) {
            if (observer.getPositionType() == OpponentObserver.PositionType.OVERTAKING) {
                Point2D planEnd = planRaceLineOvertake(positions, currStart, end,
                        currPosition, targetPosition);
                currStart = planEnd.getY();
                currPosition = planEnd.getX();
                remainingLength = end - currStart;

            } else {
                Point2D planEnd = planRaceLineBlock(positions, currStart, end,
                        currPosition, targetPosition);
                currStart = planEnd.getY();
                currPosition = planEnd.getX();
                remainingLength = end - currStart;
            }
        }

        // simply drive towards the target position for the next corner
        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("Planning towards the next corner...");
            plan.println("Current start: " + Utils.dTS(currStart));
            plan.println("Current position: " + Utils.dTS(currPosition));
            plan.println("Remaining length: " + Utils.dTS(remainingLength));
        }

        // switch position
        if (currStart != end) {
            Point2D planEnd = planSwitchPosition(positions, data, trackModel,
                    current, next, currStart, end, currPosition);
            currStart = planEnd.getY();
            currPosition = planEnd.getX();
            remainingLength = end - currStart;
        }

        if (currStart != end) {
            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("Adding straight part:");
            }

            Interpolator brakeLine = new ConstantValue(currStart, end, currPosition);
            positions.add(brakeLine);
        }               

        for (Interpolator cs : positions) {
            planContent.attachPosition(cs);
        }        
        
        return result;
    }

    /**
     * Plans the normal racing line towards the next corner.
     */
    private Point2D planSwitchPosition(ArrayList<Interpolator> positions,
            SensorData data, TrackModel trackModel, TrackSegment current,
            TrackSegment next,
            double currStart, double end, double currPosition) {
        // the current positon in the absolute coordinate system
        double absCurrPosition = SensorData.calcAbsoluteTrackPosition(currPosition, trackModel.getWidth());
        // the possible delta
        double possibleDelta = Plan2013.calcPossibleSwitchDelta(data, end - currStart);
        // the anchor point towards the next tracksegment
        double anchor = plan.getAnchorPoint(current, next);
        // the anchor in the absolute coordinate system
        double absDesiredPosition = SensorData.calcAbsoluteTrackPosition(anchor, trackModel.getWidth());
        // the length needed to switch to the desired position
        double lengthNeeded = Plan2013.calcSwitchDistance(data, Math.abs(absCurrPosition - absDesiredPosition));
        // remaining length
        double remainingLength = end - currStart;

        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("absCurrPosition: " + Utils.dTS(absCurrPosition));
            plan.println("PossibleDelta: " + Utils.dTS(possibleDelta));
            plan.println("DesiredDelta: " + Utils.dTS(Math.abs(absDesiredPosition - absCurrPosition)));
            plan.println("absDesiredPosition: " + Utils.dTS(absDesiredPosition));
            plan.println("length needed: " + Utils.dTS(lengthNeeded));
        }

        if (lengthNeeded >= remainingLength) {
            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("\tNot enough room, calculating what is possible");
            }

            if (absDesiredPosition < absCurrPosition) {
                // move to the left
                absDesiredPosition = absCurrPosition - possibleDelta;

            } else {
                // move to the right
                absDesiredPosition = absCurrPosition + possibleDelta;
            }

            anchor = SensorData.calcRelativeTrackPosition(absDesiredPosition, trackModel.getWidth());
            lengthNeeded = remainingLength;

            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("\tNew absDesiredPosition: " + Utils.dTS(absDesiredPosition));
                plan.println("\tNew anchor: " + Utils.dTS(anchor));
            }
        }

        // check if we want to switch the position at all
        if (Math.abs(absDesiredPosition - absCurrPosition) < MIN_RELEVANT_ABS_DELTA) {
            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("Close engough to the desired position - nothing to do");
            }
            Point2D.Double result = new Point2D.Double(currPosition, currStart);
            return result;
        }

        double[] xP = new double[2];
        double[] yP = new double[2];

        xP[0] = currStart;
        xP[1] = currStart + lengthNeeded;

        yP[0] = currPosition;
        yP[1] = anchor;

        if (remainingLength - lengthNeeded < MIN_RELEVANT_STRAIGHT) {
            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("So close to the corner, moving end of switch to end of segment");
            }
            xP[1] = end;
        }

        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("Positioning towards the next corner:");
            for (int k = 0; k < xP.length; ++k) {
                plan.println(xP[k] + " , " + yP[k]);
            }
        }

        CubicSplineTwoPoints spline = new CubicSplineTwoPoints(xP, yP);        
        positions.add(spline);
        
        return new Point2D.Double(yP[1], xP[1]);
    }

    /**
     * Plans the racing line to block other cars.
     */
    private Point2D planRaceLineBlock(ArrayList<Interpolator> positions, double currStart, double end, double currentPosition,
            Point2D targetPosition) {
        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("Trying to block");
        }

        // switch Position 1
        double[] xP = new double[2];
        double[] yP = new double[2];

        xP[0] = currStart;
        xP[1] = targetPosition.getY();

        yP[0] = currentPosition;
        yP[1] = targetPosition.getX();

        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("Switch Position 1:");
            for (int k = 0; k < xP.length; ++k) {
                plan.println(xP[k] + " , " + yP[k]);
            }
        }

        CubicSplineTwoPoints spline = new CubicSplineTwoPoints(xP, yP);        
        positions.add(spline);

        currStart = xP[1];
        double remainingLength = end - currStart;
        currentPosition = targetPosition.getX();

        // block line
        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("Block line, remaining length: " + Utils.dTS(remainingLength));
            plan.println("Blocking line is: " + Utils.dTS(currStart) + " " + Utils.dTS(end) + " " + targetPosition.getX());
        }

        positions.add(new ConstantValue(currStart, end, targetPosition.getX()));

        return new Point2D.Double(targetPosition.getX(), end);
    }

    /**
     * Plans the racing line to overtake other cars. Returns the race distance
     * at which this method stopped to plan.
     */
    private Point2D planRaceLineOvertake(ArrayList<Interpolator> positions, double currStart, double end, double currentPosition,
            Point2D targetPosition) {
        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("Trying to overtake");
        }

        // switch Position 1
        double[] xP = new double[2];
        double[] yP = new double[2];

        xP[0] = currStart;
        xP[1] = targetPosition.getY();

        yP[0] = currentPosition;
        yP[1] = targetPosition.getX();

        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("Switch Position 1:");
            for (int k = 0; k < xP.length; ++k) {
                plan.println(xP[k] + " , " + yP[k]);
            }
        }

        CubicSplineTwoPoints spline = new CubicSplineTwoPoints(xP, yP);        
        positions.add(spline);

        currStart = xP[1];
        double remainingLength = end - currStart;
        currentPosition = targetPosition.getX();

        // overtaking line
        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("Overtaking line, remaining length: " + Utils.dTS(remainingLength));
        }

        final double ols = currStart;
        double ole;

        if (remainingLength > 11.0) {
            ole = currStart + 10.0;

        } else {
            ole = end;
        }

        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("Overtaking line: " + Utils.dTS(ols) + " " + Utils.dTS(ole) + " " + targetPosition.getX());
        }

        positions.add(new ConstantValue(ols, ole, targetPosition.getX()));

        return new Point2D.Double(targetPosition.getX(), ole);
    }

    /*public static void main(String[] args) {
    try {
    PlanStackData planData = new PlanStackData(10);

    TrackModel model = TrackModel.load("wheel2.saved_model");



    Point2D.Double point = new Point2D.Double(0.5, 90.0);
    OpponentObserver obs = new de.janquadflieg.mrracer.opponents.DebugObserver(point);

    double dist = 5.0;

    planData.addSegment(0, model.getSegment(0).getLength() - dist, 25.0);

    String msg = "(angle 0)(curLapTime 10.21)(damage 0)(distFromStart " + String.valueOf(dist) + ")(distRaced 10.0)(fuel 94)(gear 5)(lastLapTime 0)(opponents 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200 200)(racePos 1)(rpm 942.478)(speedX 170.0)(speedY 0)(speedZ 0.0196266)(track 4.00001 4.06171 4.25672 4.61881 5.22164 6.22291 8.00001 11.6952 23.0351 200 46.0701 23.3904 16 12.4458 10.4433 9.2376 8.51342 8.12341 7.99999)(trackPos 0.333332)(wheelSpinVel 0 0 0 0)(z 0.339955)(focus -1 -1 -1 -1 -1)";

    PlanStraight2013 planner = new PlanStraight2013(new Plan2011(new de.janquadflieg.mrracer.controller.MrRacer2012()));

    planner.plan(planData, new SensorData(new scr.MessageBasedSensorModel(msg)), model, obs);


    } catch (Exception e) {
    e.printStackTrace(System.out);
    }
    }*/
}