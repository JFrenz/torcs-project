/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.plan;

import de.janquadflieg.mrracer.behaviour.Component;
import de.janquadflieg.mrracer.Utils;

import java.io.*;
import java.util.*;
import javax.vecmath.Point4d;

/**
 *
 * @author quad
 */
public class BrakePredictor
        implements Component {

    private final static double G = 9.81;
    public static final String CW = "-BrakePredictor.CW-";
    private double cw = 0.076771125;
    public static final String CA = "-BrakePredictor.CA-";
    private double ca = 3.64999;
    public static final String MASS = "-BrakePredictor.MASS-";
    private double mass = 1150.0;
    

    public BrakePredictor() {        
    }

    public void paint(String baseFileName, java.awt.Dimension d) {
    }

    public void setParameters(Properties params, String prefix) {
        cw = Double.parseDouble(params.getProperty(prefix + BrakePredictor.CW, String.valueOf(cw)));
        ca = Double.parseDouble(params.getProperty(prefix + BrakePredictor.CA, String.valueOf(ca)));
        mass = Double.parseDouble(params.getProperty(prefix + BrakePredictor.MASS, String.valueOf(mass)));
    }

    public void getParameters(Properties params, String prefix) {
        params.setProperty(prefix + BrakePredictor.CW, String.valueOf(cw));
        params.setProperty(prefix + BrakePredictor.CA, String.valueOf(ca));
        params.setProperty(prefix + BrakePredictor.MASS, String.valueOf(mass));
    }

    public final double calcBrakeDistance(double speed, double targetSpeed, double mu, double cc) {
        if (targetSpeed >= speed) {
            return 0.0;
        }

        return calcBrakeDistanceAnalytical(speed, targetSpeed, mu, cc);
    }

    

    public final double calcBrakeDistanceAnalytical(final double speed, final double targetSpeed, final double MU, double cc) {
        if (targetSpeed >= speed) {
            return 0.0;
        }

        final double c = MU * G;
        final double d = (ca * MU + cw) / mass;
        final double v1sqr = (speed / 3.6) * (speed / 3.6);
        final double v2sqr = (targetSpeed / 3.6) * (targetSpeed / 3.6);
        double brakedist = -Math.log((c + v2sqr * d) / (c + v1sqr * d)) / (2.0 * d);

        return Math.max(0.0, brakedist / cc);
    }

    public static double calcBrakeDistanceSimple(double speed, double targetSpeed, double cc) {
        //System.out.println("calcBrakeDistance speed="+speed+", targetSpeed="+targetSpeed);

        speed = speed / 3.6;
        targetSpeed = targetSpeed / 3.6;

        // 1/2 * erdbeschleunigung * reibungskoeffizient
        double divisor = 2 * 9.81 * 1.1 * cc;

        double result = ((speed * speed) - (targetSpeed * targetSpeed)) / divisor;

        return Math.max(0, result);
    }

    public final double calcApproachSpeed(double targetSpeed, double distance, double mu, double cc) {
        return calcApproachSpeedAnalytical(targetSpeed, distance, mu, cc);
    }

    

    public final double calcApproachSpeedAnalytical(final double targetSpeed, double distance, final double MU, double cc) {
        if (distance <= 0) {
            return targetSpeed;
        }

        distance *= cc;
        final double c = MU * G;
        final double d = (ca * MU + cw) / mass;
        //final double v1sqr = (speed / 3.6) * (speed / 3.6);
        final double v2sqr = (targetSpeed / 3.6) * (targetSpeed / 3.6);
        // 1 / (e^((brakedist * (2.0 * d)) * -1.0)) / (c + v2sqr * d) = (c + v1sqr * d);

        double result = distance * (2.0 * d);
        result *= -1.0;
        result = Math.exp(result);
        result = result / (c + v2sqr * d);
        result = 1.0 / result;
        result = result - c;
        result = result / d;
        result = Math.sqrt(result);
        result = result * 3.6;

        return Math.max(targetSpeed, result);
    }

    public static double calcApproachSpeedSimple(double targetSpeed, double distance, double cc) {
        double c = 2 * 9.81 * 1.1 * cc;

        targetSpeed = targetSpeed / 3.6;

        double result = distance * c + (targetSpeed * targetSpeed);

        result = Math.sqrt(result);

        return result * 3.6;
    }

    public static void main(String[] args) {
        double rearwingarea = 0.7; //GfParmGetNum(car->_carHandle, SECT_REARWING, PRM_WINGAREA, (char*) NULL, 0.0);
        double rearwingangle = 14; // grad! GfParmGetNum(car->_carHandle, SECT_REARWING, PRM_WINGANGLE, (char*) NULL, 0.0);        
        double wingca = 1.23 * rearwingarea * Math.sin(Math.toRadians(rearwingangle));
        double cl = 0.69 + 0.7; //GfParmGetNum(car->_carHandle, SECT_AERODYNAMICS, PRM_FCL, (char*) NULL, 0.0) + GfParmGetNum(car->_carHandle, SECT_AERODYNAMICS, PRM_RCL, (char*) NULL, 0.0);

        double h = 100 + 100 + 105 + 105; // += GfParmGetNum(car->_carHandle, WheelSect[i], PRM_RIDEHEIGHT, (char*) NULL, 0.20);

        h *= 1.5;
        h = h * h;
        h = h * h;
        h = 2.0 * Math.exp(-3.0 * h);
        double CA = h * cl + 4.0 * wingca;

        System.out.println("CA: " + CA);

        double CW = 0.645 * 0.345 * 1.92;

        System.out.println("CW: " + CW);

        BrakePredictor wrong = new BrakePredictor();
        Properties wrongParams = new Properties();
        wrongParams.setProperty("-MrRacer2012.Plan--PLAN.brakepredictor--BrakePredictor.CA-","3.64999");
        wrongParams.setProperty("-MrRacer2012.Plan--PLAN.brakepredictor--BrakePredictor.CW-", "0.076771125");
        wrongParams.setProperty("-MrRacer2012.Plan--PLAN.brakepredictor--BrakePredictor.MASS-", "1150.0");
        wrong.setParameters(wrongParams,"-MrRacer2012.Plan--PLAN.brakepredictor-");

        BrakePredictor right = new BrakePredictor();
        Properties rightParams = new Properties();
        rightParams.setProperty("-MrRacer2012.Plan--PLAN.brakepredictor--BrakePredictor.CA-","0.8331790084452556");
        rightParams.setProperty("-MrRacer2012.Plan--PLAN.brakepredictor--BrakePredictor.CW-", "0.42724799999999996");
        rightParams.setProperty("-MrRacer2012.Plan--PLAN.brakepredictor--BrakePredictor.MASS-", "1150.0");
        right.setParameters(rightParams,"-MrRacer2012.Plan--PLAN.brakepredictor-");

        System.out.println("mu\tspeed\tt_speed\twrong\tright");
        for(double mu = 0.7; mu <= 1.4; mu+=0.02){
            for(double speed=150; speed <= 310; speed+=50){
                System.out.print(Utils.dTS(mu));
                System.out.print("\t"+Utils.dTS(speed));
                System.out.print("\t"+Utils.dTS(100.0));
                System.out.print("\t"+Utils.dTS(wrong.calcBrakeDistance(speed, 100.0, mu, 1.0)));
                System.out.print("\t"+Utils.dTS(right.calcBrakeDistance(speed, 100.0, mu, 1.0)));
                
                System.out.println("");
            }
        }
        
        if (1 == 1) {
            return;
        }

        double bestSqe = Double.POSITIVE_INFINITY;
        double bestCA = 0.0;

        for (double ca = 0.0; ca < 10.0; ca += 0.01) {
            Properties params = new Properties();
            params.setProperty(de.janquadflieg.mrracer.controller.MrRacer2013.PLAN + Plan2013.BRAKE_PREDICTOR + BrakePredictor.CW,
                    String.valueOf(0.645 * 0.345 * 0.345));
            params.setProperty(de.janquadflieg.mrracer.controller.MrRacer2013.PLAN + Plan2013.BRAKE_PREDICTOR + BrakePredictor.CA,
                    String.valueOf(ca));
            params.setProperty(de.janquadflieg.mrracer.controller.MrRacer2013.PLAN + Plan2013.BRAKE_PREDICTOR + BrakePredictor.MASS,
                    String.valueOf(1150.0));

            
        }

        System.out.println("Best: " + bestCA + ", " + bestSqe);
    }
}
