/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.plan;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.classification.Situations;
import de.janquadflieg.mrracer.functions.*;
import de.janquadflieg.mrracer.opponents.OpponentObserver;
import de.janquadflieg.mrracer.telemetry.SensorData;
import de.janquadflieg.mrracer.track.*;

import java.util.Properties;

/**
 *
 * @author quad
 */
public class PlanCorner2013
        implements PlanSubModule {

    private static final boolean REDUCE_APPROACH_SPEED_SHORT_LENGTH_SLOW_OR_HAIRPIN_CORNER = true;
    private Plan2013 plan;
    /**
     * Fraction of the subsegment which should be driven with constant speed. 1
     * will cause the module to plan a constant speed for the whole subsegment,
     * 0 will cause the module to behave like the old 2011 version.
     */
    private double fraction = 1.0;
    public static final String FRACTION = "-PCF.factor-";

    public PlanCorner2013(Plan2013 p) {
        this.plan = p;
    }

    @Override
    public void setParameters(Properties params, String prefix) {
        if (params.containsKey(prefix + FRACTION)) {
            fraction = Double.parseDouble(params.getProperty(prefix + FRACTION));

        } else {
            System.out.println("WARNING: properties do not contain " + prefix + FRACTION);
            System.out.println("Using: " + fraction);
        }
    }

    @Override
    public void getParameters(Properties params, String prefix) {
        params.setProperty(prefix + FRACTION, String.valueOf(fraction));
    }

    @Override
    public void paint(String baseFileName, java.awt.Dimension d) {
    }

    /**
     * New variant: Constant speed within the apex segments.
     */
    @Override
    public final void planTargetSpeeds(PlanStackData planData,
            SensorData data, TrackModel trackModel, OpponentObserver observer,
            PlanContent planContent) {
        TrackSegment currentSegment = trackModel.getSegment(planData.currentSegment());

        double start = planData.start();
        double end = planData.end();

        TrackSegment.Apex[] apexes = currentSegment.getApexes();

        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            plan.println("");
            plan.println("----------  CORNER[Flexible Apex Speed]  ---------------");
            plan.println("Start/End: " + Utils.dTS(start) + ", " + Utils.dTS(end) + " [" + Utils.dTS(end - start) + "m]");
            for (int i = 0; i < apexes.length; ++i) {
                TrackSegment.Apex a = apexes[i];
                plan.println("Apex[" + i + "]: " + Utils.dTS(a.position) + ", " + Situations.toString(a.type) + ", speed: " + Utils.dTS(plan.getTargetSpeed(a)) + "km/h");
            }
        }

        // if this is the first segment to plan for, adjust start
        if (planData.first()) {
            double offsetFromStart = data.getDistanceFromStartLine() - currentSegment.getStart();

            if (offsetFromStart > 0.0) {
                if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                    plan.println("This is the first segment to plan for and i'm "
                            + Utils.dTS(offsetFromStart) + "m into the segment - adjusting start.");
                }
                start -= offsetFromStart;
                if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                    plan.println("New Start/End: " + Utils.dTS(start) + ", " + Utils.dTS(end) + " [" + Utils.dTS(end - start) + "m]");
                }
            }
        }

        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
            planData.print();
            plan.println("");
        }

        double[] apexSpeeds = new double[apexes.length];

        for (int i = 0; i < apexes.length; ++i) {
            apexSpeeds[i] = plan.getTargetSpeed(apexes[i]);
        }

        // handle all apexes, back to front
        for (int i = apexes.length - 1; i >= -1; --i) {
            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                if (i >= 0) {
                    plan.println("Handling Apex " + (i + 1) + "/" + apexes.length);
                } else {
                    plan.println("Handling corner entry");
                }
            }

            TrackSubSegment apexSubsegment = null;
            if (i >= 0) {
                apexSubsegment = currentSegment.getSubSegment(apexes[i].position);
            }

            if (Plan.TEXT_DEBUG && !plan.isPreCompiling() && apexSubsegment != null) {
                plan.println("Part of subsegment: " + Utils.dTS(apexSubsegment.getStart()) + " - " + Utils.dTS(apexSubsegment.getEnd()));
            }



            double length;      // length between the end of the apex subsegment and the next apex or end
            double nextSpeed;   // the next target speed
            double brakeDist;   // brake distance needed to brake down from previousSpeed to targetSpeed
            double previousSpeed;    // preSpeed
            double apexPlanEnd; // end of the planning for this apex

            if (i == apexes.length - 1) {
                // last apex
                length = (currentSegment.getEnd() - apexSubsegment.getEnd())
                        + ((apexSubsegment.getEnd() - apexes[i].position) * (1.0 - fraction));
                nextSpeed = planData.approachSpeed;
                previousSpeed = apexSpeeds[i];
                apexPlanEnd = currentSegment.getEnd();

            } else if (i == -1) {
                // corner entry
                length = (currentSegment.getSubSegment(apexes[i + 1].position).getStart() - currentSegment.getStart())
                        + ((apexes[i + 1].position - currentSegment.getSubSegment(apexes[i + 1].position).getStart()) * (1.0 - fraction));
                nextSpeed = apexSpeeds[i + 1];
                previousSpeed = planData.speed();
                // BUGFIX, allows further acceleration
                if (previousSpeed <= nextSpeed) {
                    previousSpeed = plan.calcApproachSpeedCorner(nextSpeed, length, currentSegment.getBCC());
                }
                apexPlanEnd = currentSegment.getSubSegment(apexes[i + 1].position).getStart()
                        + ((apexes[i + 1].position - currentSegment.getSubSegment(apexes[i + 1].position).getStart()) * (1.0 - fraction));

            } else {
                // within
                length = (currentSegment.getSubSegment(apexes[i + 1].position).getStart() - apexSubsegment.getEnd())
                        + ((apexes[i + 1].position - currentSegment.getSubSegment(apexes[i + 1].position).getStart()) * (1.0 - fraction)) + //
                        ((apexSubsegment.getEnd() - apexes[i].position) * (1.0 - fraction));
                nextSpeed = apexSpeeds[i + 1];
                previousSpeed = apexSpeeds[i];
                apexPlanEnd = currentSegment.getSubSegment(apexes[i + 1].position).getStart()
                        + ((apexes[i + 1].position - currentSegment.getSubSegment(apexes[i + 1].position).getStart()) * (1.0 - fraction));
            }

            brakeDist = plan.calcBrakingZoneCorner(previousSpeed, length, nextSpeed, currentSegment.getBCC());

            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("Previous Speed: " + Utils.dTS(previousSpeed)
                        + "km/h, nextSpeed: " + Utils.dTS(nextSpeed) + "km/h");
                plan.println("Length: " + Utils.dTS(length) + ", brakeDistance: " + Utils.dTS(brakeDist));
            }


            if (brakeDist >= length) {
                double x3 = start + (apexPlanEnd - currentSegment.getStart() - length);
                double x4 = start + (apexPlanEnd - currentSegment.getStart());
                planContent.attachSpeed(new ConstantValue(x3, x4, nextSpeed));
                double saveSpeed = plan.calcApproachSpeedCorner(nextSpeed, length, currentSegment.getBCC());

                if (i == -1) {
                    planData.approachSpeed = saveSpeed;
                    if (REDUCE_APPROACH_SPEED_SHORT_LENGTH_SLOW_OR_HAIRPIN_CORNER
                            && length <= 50.0 && (apexes[0].type == Situations.TYPE_HAIRPIN
                            || apexes[0].type == Situations.TYPE_SLOW)) {
                        if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                            plan.println("Length <= 50.0 in front of " + Situations.toString(apexes[0].type) + " apex, setting approach speed from "
                                    + Utils.dTS(saveSpeed) + "km/h to " + Utils.dTS(nextSpeed) + "km/h");
                        }
                        planData.approachSpeed = nextSpeed;
                    }
                }

            } else {
                if (brakeDist > 0) {
                    double x5 = start + (apexPlanEnd - currentSegment.getStart() - brakeDist);
                    double x6 = start + (apexPlanEnd - currentSegment.getStart());
                    planContent.attachSpeed(new ConstantValue(x5, x6, nextSpeed));
                }
                double x3 = start + (apexPlanEnd - currentSegment.getStart() - length);
                double x4 = start + (apexPlanEnd - currentSegment.getStart() - brakeDist);
                double saveSpeed = Math.min(plan.calcApproachSpeedCorner(nextSpeed, x4 - x3, currentSegment.getBCC()),
                        Plan2013.MAX_SPEED);
                planContent.attachSpeed(new ConstantValue(x3, x4, saveSpeed));
                
                if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                    plan.println("Acceleration Part: x3="+Utils.dTS(x3)+", x4="+Utils.dTS(x4)+", "+Utils.dTS(x4-x3)+"m");                    
                    plan.println("save Speed: "+Utils.dTS(saveSpeed)+"km/h");
                }
                
                plan.createAndAddCheckPoints(x3, (x4-x3), previousSpeed);

                if (i == -1) {
                    planData.approachSpeed = Math.min(plan.calcApproachSpeedCorner(nextSpeed, length, currentSegment.getBCC()),
                            Plan.MAX_SPEED);
                }
            }

            // apex part with constant speed
            if (i >= 0) {
                double frontApex = (apexes[i].position - apexSubsegment.getStart()) * (1.0 - fraction);
                double afterApex = (apexSubsegment.getEnd() - apexes[i].position) * (1.0 - fraction);

                if (brakeDist > length) {
                    double saveSpeed = plan.calcApproachSpeedCorner(nextSpeed, length, currentSegment.getBCC());
                    brakeDist = plan.calcBrakeDistanceCorner(apexSpeeds[i], saveSpeed, currentSegment.getBCC());
                    double usedSubLength = apexSubsegment.getLength() * fraction;

                    if (brakeDist > usedSubLength) {
                        apexSpeeds[i] = saveSpeed;
                        double x1 = start + ((apexSubsegment.getStart() + frontApex) - currentSegment.getStart());
                        double x2 = start + ((apexSubsegment.getEnd() - afterApex) - currentSegment.getStart());
                        planContent.attachSpeed(new ConstantValue(x1, x2, apexSpeeds[i]));

                    } else {
                        double x1 = start + ((apexSubsegment.getStart() + frontApex) - currentSegment.getStart());
                        double x2 = start + ((apexSubsegment.getEnd() - afterApex) - currentSegment.getStart() - brakeDist);
                        planContent.attachSpeed(new ConstantValue(x1, x2, apexSpeeds[i]));

                        double x3 = start + ((apexSubsegment.getEnd() - afterApex) - currentSegment.getStart() - brakeDist);
                        double x4 = start + ((apexSubsegment.getEnd() - afterApex) - currentSegment.getStart());
                        planContent.attachSpeed(new ConstantValue(x3, x4, saveSpeed));
                    }

                } else {
                    double x1 = start + ((apexSubsegment.getStart() + frontApex) - currentSegment.getStart());
                    double x2 = start + ((apexSubsegment.getEnd() - afterApex) - currentSegment.getStart());
                    planContent.attachSpeed(new ConstantValue(x1, x2, apexSpeeds[i]));
                    if (Plan.TEXT_DEBUG && !plan.isPreCompiling()){
                        plan.println("Adding verifier ["+Utils.dTS(x1)+", "+Utils.dTS(x2)+"] "+Utils.dTS(apexSpeeds[i])+"km/h");
                    }
                    plan.addSpeedVerifier(new SpeedVerifier(x1, x2, apexSpeeds[i]));
                }
            }

            if (Plan.TEXT_DEBUG && !plan.isPreCompiling()) {
                plan.println("");
            }
        }
    }
}