/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.janquadflieg.mrracer.functions;

import de.janquadflieg.mrracer.Utils;
import java.awt.geom.Point2D;
import javax.vecmath.Point3d;

/**
 *
 * @author Fabian Bruckner
 */
public class VariableGradientCubicSpline implements Interpolator{

    /**
     * Smallest x coordinate.
     */
    private double xmin;
    /**
     * Largest x coordinate.
     */
    private double xmax;
    /**
     * Smallest y coordinate.
     */
    private double ymin;
    /**
     * Largest y coordinate.
     */
    private double ymax;
    /**
     * Derivation at start.
     */
    private double mS;
    /**
     * Derivation at end.
     */
    private double mE;
    
    private Point2D.Double bounds;
    
    /**
     *
     * @param p0 Vektor der Form (aktuelle xPos, aktuelle yPos, aktuelle Steigung)
     * @param p1 Vektor der Form (naechste xPos, naechste yPos, naechste Steigung)
     */
    public VariableGradientCubicSpline(Point3d p0, Point3d p1, Point2D.Double pBounds) {
        bounds = pBounds;
        xmin = p0.x;
        xmax = p1.x;
        ymin = p0.y;
        ymax = p1.y;
        mS = p0.z;
        mE = p1.z;
    }
    
    @Override
    public double getXmin() {
        return xmin;
    }

    @Override
    public double getXmax() {
        return xmax;
    }

    @Override
    public double interpolate(double v) {
        double t = (v - xmin) / (xmax-xmin);
        double y = (1-t) * ymin + t * ymax + t * (1-t) * ((mS*(xmax-xmin) - (ymax - ymin)) * (1 - t) + (-mE * (xmax-xmin) + (ymax-ymin)) * t);
        return Math.min(bounds.x, Math.max(y, bounds.y));
    }

    @Override
    public int compareTo(Interpolator o) {
        if (getXmin() < o.getXmin()) {
            return -1;
        } else if (getXmin() > o.getXmin()) {
            return 1;
        }
        return 0;
    }
    
    public static void main(String[] args){
        Point3d p0 = new  Point3d(0, 0.33, 0);
        Point3d p1 = new  Point3d(18, 0.9, 0);
        

        VariableGradientCubicSpline spline = new VariableGradientCubicSpline(p0, p1, new Point2D.Double(0.1, 0.9));
        for(double i=p0.x; i <= p1.x; i+=0.1){
            System.out.println(Utils.dTS(i)+" "+Utils.dTS(spline.interpolate(i)));
        }
    }
    
}
