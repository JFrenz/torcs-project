/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.functions;

import de.janquadflieg.mrracer.Utils;

import java.awt.geom.Point2D;

/**
 *
 * @author quad
 */
public class CubicSplineTwoPoints
        implements Interpolator {    
    private double xmin;
    private double xmax;
    
    private double y0;
    private double y1;
    
    private static final double A = -2.0;
    private static final double B = 3.0;
    private static final double C = 0.0;
    private static final double D = 0.0;    
    
    public CubicSplineTwoPoints(double[] x, double[] y) {
        if(x.length != 2 || y.length != 2){
            throw new RuntimeException();
        }
        
        xmin = x[0];
        xmax = x[1];
        
        y0 = y[0];
        y1 = y[1];        
    }
    
    public CubicSplineTwoPoints(Point2D p0, Point2D p1) {        
        xmin = p0.getX();
        xmax = p1.getX();
        
        y0 = p0.getY();
        y1 = p1.getY();        
    }

    /**
     * Compares this object with the specified object for order. Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     */
    public int compareTo(Interpolator other) {
        if (getXmin() < other.getXmin()) {
            return -1;
        } else if (getXmin() > other.getXmin()) {
            return 1;
        }
        return 0;
    }

    @Override
    public double getXmin() {
        return xmin;
    }

    @Override
    public double getXmax() {
        return xmax;
    }

    @Override
    public double interpolate(double v) {
        double x = (v-xmin)/(xmax-xmin);
        
        // naiv
        //double y = A*x*x*x+B*x*x+C*x+D;
        
        // mit Horner Schema
        double y = ((A*x+B)*x+C)*x+D;
        
        return y0+y*(y1-y0);        
    }
    
    public static void main(String[] args){
        double[] x = {0, 10};
        double[] y = {0.9, 0.3};
        
        //double[] x = {0, 1};
        //double[] y = {0, 1};
        
        CubicSplineTwoPoints spline = new CubicSplineTwoPoints(x, y);
        for(double i=x[0]; i <= x[1]; i+=0.1){
            System.out.println(Utils.dTS(i)+" "+Utils.dTS(spline.interpolate(i)));
        }
    }
}
