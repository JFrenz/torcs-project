/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.functions;

import de.janquadflieg.mrracer.Utils;

import java.awt.geom.Point2D;
import org.apache.commons.math3.linear.*;

/**
 * General cubic spline class which uses apache commons math to solve the system
 * of linear equations.
 *
 * @author quad
 */
public class GeneralCubicSpline
        implements Interpolator {

    /**
     * Controls Exceptions.
     */
    private static final boolean USE_EXCEPTIONS = true;
    /**
     * Smallest x coordinate.
     */
    private double xmin;
    /**
     * Largest x coordinate.
     */
    private double xmax;
    /**
     * Coefficients for the cubic terms.
     */
    private double[] A;
    /**
     * Coefficients for the quadratic terms.
     */
    private double[] B;
    /**
     * Coefficients for the linear terms.
     */
    private double[] C;
    /**
     * Coefficients for the constant terms.
     */
    private double[] D;
    /** Normalized X Coordinates. */
    double[] x;
    /** Y Coordinates. */
    double[] y;

    public GeneralCubicSpline(Point2D[] points) {
        if (USE_EXCEPTIONS) {
            if (points.length < 2) {
                throw new RuntimeException("GeneralCubicSpline: Given Array contains less than two points.");
            }
        }
        // Number of points
        final int N = points.length;
        // Number of polynoms
        final int P = N - 1;
        // Number of equations / unknowns
        final int E = 4 * P;

        // Sort Points by their x coordinate
        java.util.Arrays.sort(points, new java.util.Comparator<Point2D>() {
            @Override
            public int compare(Point2D a, Point2D b) {
                if (a.getX() < b.getX()) {
                    return -1;

                } else if (a.getX() > b.getX()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        
        xmin = points[0].getX();
        xmax = points[N-1].getX();

        x = new double[N];
        y = new double[N];
        for (int i = 0; i < N; ++i) {
            x[i] = points[i].getX()-xmin;
            y[i] = points[i].getY();
            //System.out.println(Utils.dTS(x[i]) + " - " + Utils.dTS(y[i]));
        }
        
        
        A = new double[P];
        B = new double[P];
        C = new double[P];
        D = new double[P];        

        // matrix, initially filled with zeros
        double[][] matrix = new double[E][E];
        for (int r = 0; r < E; ++r) {
            for (int c = 0; c < E; ++c) {
                matrix[r][c] = 0.0;
            }
        }
        double[] constants = new double[E];
        java.util.Arrays.fill(constants, 0.0);

        // build linear equations
        for (int i = 0; i < N; ++i) {
            if (i == 0) {
                // 1st. Point
                // p_0(x_0) = y_0
                matrix[0][0] = x[0] * x[0] * x[0];
                matrix[0][1] = x[0] * x[0];
                matrix[0][2] = x[0];
                matrix[0][3] = 1;
                constants[0] = y[0];
                // p_0(x_1) = y_1
                matrix[1][0] = x[1] * x[1] * x[1];
                matrix[1][1] = x[1] * x[1];
                matrix[1][2] = x[1];
                matrix[1][3] = 1;
                constants[1] = y[1];

            } else if (i == N - 1) {
                // last Point -> extra conditions
                // 1. derivation x_0
                matrix[E - 2][0] = 3 * x[0] * x[0];
                matrix[E - 2][1] = 2 * x[0];
                matrix[E - 2][2] = 1;
                constants[E - 2] = 0.0;
                // 1. derivation x_n
                matrix[E - 1][E - 4] = 3 * x[N - 1] * x[N - 1];
                matrix[E - 1][E - 3] = 2 * x[N - 1];
                matrix[E - 1][E - 2] = 1;
                constants[E - 1] = 0.0;

            } else {
                // some other point in between
                // row offset
                final int RO = 2 + ((i - 1) * 4);
                // column offset
                final int CO = ((i - 1) * 4);
                // 1st derivation at point i should be equal for both polynoms
                matrix[RO][CO] = 3 * x[i] * x[i];
                matrix[RO][CO + 1] = 2 * x[i];
                matrix[RO][CO + 2] = 1;
                matrix[RO][CO + 4] = -3 * x[i] * x[i];
                matrix[RO][CO + 5] = -2 * x[i];
                matrix[RO][CO + 6] = -1;
                constants[RO] = 0.0;
                // 2nd derivation at point i should be equal for both polynoms
                matrix[RO + 1][CO] = 6 * x[i];
                matrix[RO + 1][CO + 1] = 2;
                matrix[RO + 1][CO + 4] = -6 * x[i];
                matrix[RO + 1][CO + 5] = -2;
                constants[RO + 1] = 0.0;
                // p_i(x_i) = y_i                
                matrix[RO + 2][CO + 4] = x[i] * x[i] * x[i];
                matrix[RO + 2][CO + 5] = x[i] * x[i];
                matrix[RO + 2][CO + 6] = x[i];
                matrix[RO + 2][CO + 7] = 1;
                constants[RO + 2] = y[i];
                // p_i(x_i+1) = y_i+1
                matrix[RO + 3][CO + 4] = x[i + 1] * x[i + 1] * x[i + 1];
                matrix[RO + 3][CO + 5] = x[i + 1] * x[i + 1];
                matrix[RO + 3][CO + 6] = x[i + 1];
                matrix[RO + 3][CO + 7] = 1;
                constants[RO + 3] = y[i + 1];
            }
        }

        RealMatrix coefficients = new Array2DRowRealMatrix(matrix, false);
        //System.out.println(coefficients.toString());
        DecompositionSolver solver = new LUDecomposition(coefficients).getSolver();

        RealVector solution = solver.solve(new ArrayRealVector(constants, false));

        //System.out.println(solution);

        for (int i = 0; i < P; ++i) {
            A[i] = solution.getEntry(i * 4);
            B[i] = solution.getEntry((i * 4) + 1);
            C[i] = solution.getEntry((i * 4) + 2);
            D[i] = solution.getEntry((i * 4) + 3);
        }
    }
    
    /**
     * Compares this object with the specified object for order. Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     */
    public int compareTo(Interpolator other) {
        if (getXmin() < other.getXmin()) {
            return -1;
        } else if (getXmin() > other.getXmin()) {
            return 1;
        }
        return 0;
    }

    @Override
    public double getXmin() {
        return xmin;
    }

    @Override
    public double getXmax() {
        return xmax;
    }

    @Override
    public double interpolate(double v) {
        if(USE_EXCEPTIONS){
            if (v < xmin || v > xmax) {
                throw new RuntimeException("The given value "+v+
                        " is not covered by ["+xmin+", "+xmax+"]");
            }
        }
        
        v -= xmin;

        int index = -1;

        for (int i = 0; index == -1 && i < A.length; ++i) {
            if (v >= x[i] && v <= x[i + 1]) {
                index = i;
            }
        }
        
        // horner schema
        return ((A[index]*v+B[index])*v+C[index])*v+D[index];

        // naiv
        /*return A[index] * v * v * v
                + B[index] * v * v
                + C[index] * v + D[index];*/
    }
    
    public static void test(){
        // Performance Test
        java.util.Random random = new java.util.Random();
        
        final int ITERATIONS = 10000;
        double[] test = new double[ITERATIONS];
        
        long start = System.nanoTime();        
        
        for(int i=0; i < ITERATIONS; ++i){
            Point2D[] points = new Point2D[10];
            for(int k=0; k < points.length; ++k){
                double x = (k*10)+random.nextDouble()*5.0;
                double y = random.nextGaussian()*10;
                points[k] = new Point2D.Double(x,y);
            }            
            GeneralCubicSpline spline = new GeneralCubicSpline(points);
            test[i] = spline.interpolate(50.0);
        }
        
        long end = System.nanoTime();
        
        System.out.println("This took "+((end-start)/1000000)+"ms");
        
        for(int i=0; i < ITERATIONS; ++i){
            System.out.println(test[i]);
        }
        
        System.out.println("This took "+((end-start)/1000000)+"ms");
        
        System.out.println("This took "+(((end-start)/1000000)*1.0)/ITERATIONS+"ms per iteration");
    }

    public static void main(String[] args) {
        Point2D[] points = new Point2D[4];
        points[0] = new Point2D.Double(0.0, 1.0);
        points[1] = new Point2D.Double(0.5, 1.5);
        points[2] = new Point2D.Double(1.0, 2.25);
        points[3] = new Point2D.Double(1.5, 4.0);
        
        

        GeneralCubicSpline spline = new GeneralCubicSpline(points);
        
        for(double v = spline.getXmin(); v <= spline.getXmax(); v+=0.2){
            System.out.println(Utils.dTS(v)+" "+Utils.dTS(spline.interpolate(v)));
        }
        
        //GeneralCubicSpline.test();
        //GeneralCubicSpline.test();
    }
}