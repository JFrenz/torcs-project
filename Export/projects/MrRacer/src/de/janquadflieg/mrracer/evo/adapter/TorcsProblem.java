/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo.adapter;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.evo.FitnessEvaluator;
import de.janquadflieg.mrracer.evo.Torcs134Executor;
import de.janquadflieg.mrracer.evo.TorcsExecConfig;
import de.janquadflieg.mrracer.behaviour.Clutch;
import de.janquadflieg.mrracer.behaviour.AbstractDampedAccelerationBehaviour;
import de.janquadflieg.mrracer.controller.BaseController;
import de.janquadflieg.mrracer.controller.ClutchTester;
import de.janquadflieg.mrracer.controller.Evaluator;
import de.janquadflieg.mrracer.controller.MrRacer2013;
import de.janquadflieg.mrracer.controller.MrSimpleRacer;
import de.janquadflieg.mrracer.evo.EvaluationData;
import de.janquadflieg.mrracer.torcs.TrackData;
import de.janquadflieg.mrracer.functions.GeneralisedLogisticFunction;
import de.janquadflieg.mrracer.plan.Plan2013;
import de.janquadflieg.mrracer.plan.PlanCorner2013;
import de.janquadflieg.mrracer.torcs.TrackDataList;
import de.janquadflieg.mrracer.track.TrackModel;
import de.janquadflieg.mrracer.track.TrackSegment;

import java.io.*;
import java.util.*;

import optimizer.ea.*;

/**
 * Interface betweeen Mike's ea lib and Torcs.
 *
 * @author quad
 */
public class TorcsProblem
        extends Problem implements ParallelEvaluableProblem {

    private static final String TORCS_IP_PORT = "Torcs.IP_PORT";
    private static final String TORCS_MAX_TICKS = "Torcs.MAX_TICKS";
    private static final String TORCS_MAX_LAPS = "Torcs.MAX_LAPS";
    private static final String TORCS_TRACKS = "Torcs.TRACKS";
    private static final String TORCS_PARAM_FILES = "Torcs.PARAM_FILES";
    private static final String TORCS_OPTIMIZE_PLAN = "Torcs.OPTIMIZE_PLAN";
    private static final String TORCS_OPTIMIZE_ACC_DAMPING = "Torcs.OPTIMIZE_ACC_DAMPING";
    private static final String TORCS_OPTIMIZE_BRAKE_DAMPING = "Torcs.OPTIMIZE_BRAKE_DAMPING";
    private static final String TORCS_OPTIMIZE_CLUTCH = "Torcs.OPTIMIZE_CLUTCH";
    private static final String TORCS_INCLUDE_PLAN_BCC = "Torcs.INCLUDE_PLAN_BCC";
    private static final String TORCS_INCLUDE_PLAN_PCF = "Torcs.INCLUDE_PLAN_PCF";
    private static final String TORCS_OPTIMIZE_TARGET_SPEEDS_INSTEAD_OF_GLF = "Torcs.OPTIMIZE_TARGET_SPEEDS_INSTEAD_OF_GLF";
    private static final String TORCS_TEXT_ONLY = "Torcs.TEXT_ONLY";
    private static final String TORCS_TORCS_PATH = "Torcs.TORCS_PATH";
    private static final String TORCS_TORCS_TIMEOUT = "Torcs.TORCS_TIMEOUT";
    private static final String TORCS_AGGREGATE_RESULTS = "Torcs.AGGREGATE_RESULTS";
    private static final String TORCS_SHARE_DEFAULT_PARAMS = "Torcs.SHARE_DEFAULT_PARAMS";
    private static final String TORCS_NOISY = "Torcs.NOISY";
    private int port = 3001;
    private String host = "127.0.0.1";
    /**
     * Trackdata.
     */
    private ArrayList<TrackData> trackData = new ArrayList<>();
    private TrackDataList tracklist = new TrackDataList();
    /**
     * One list of param files for each track.
     */
    private ArrayList<ArrayList<String>> paramFiles = new ArrayList<>();
    /**
     * Default Properties for each track.
     */
    private ArrayList<Properties> defaultProperties = new ArrayList<>();
    /**
     * Dimensions.
     */
    private int dimensions = 0;
    private int objectiveFunctions = 1;
    private static final int OBJECTIVES_PER_TRACK = 3;
    private boolean optimizePlan = true;
    private boolean optimizeTargetspeeds = false;
    private boolean includePlanBCC = true;
    private boolean includePlanPCF = true;
    private boolean optimizeACCDamp = true;
    private boolean optimizeBrakeDamp = true;
    private boolean optimizeClutch = true;
    private static final int DEFAULT_TICKS = 10000;
    private int maxTicks = DEFAULT_TICKS;
    private int maxLaps = de.janquadflieg.mrracer.controller.Evaluator.NO_MAXIMUM;
    private String experimentName = "";
    private int run = 0;
    private boolean runTorcsTextOnly = true;
    private TrackModel[] trackModels;
    private String torcsPath = "";
    private long torcsTimeout = 10000;
    private boolean aggregateResults = false;
    private boolean shareDefaultParams = false;
    private boolean TEXT_DEBUG = false;
    private boolean noisy = false;
    private boolean multiobjective = false;
    private ArrayList<Integer> objectivesForSelection = new ArrayList<>();
    //private 

    /**
     * Creates a new instance of Torcs Problem.
     */
    public TorcsProblem(Parameters par) {
        System.setProperty(MrRacer2013.DO_NOT_LOAD_TRACK_PARAMETERS, "");

        System.out.println(par.origConfig.getProperty("optimizerClass"));

        multiobjective = par.origConfig.getProperty("optimizerClass").equalsIgnoreCase("optimizer.emoa.smsEmoa.SMSEmoa");

        if (multiobjective) {
            System.out.println("Multi objective optimization");
            if (par.origConfig.containsKey("fitnessFunction")) {
                StringTokenizer tokenizer = new StringTokenizer(par.origConfig.getProperty("fitnessFunction"), ",");
                while (tokenizer.hasMoreTokens()) {
                    objectivesForSelection.add(Integer.parseInt(tokenizer.nextToken().trim()));
                }

            } else {
                System.out.println("You didn't specify the fitness functions to use!");
                System.exit(1);
            }
            
        } else {
            objectivesForSelection.add(0);
        }

        experimentName = par.origConfig.getProperty("experiment", "");
        run = 1 + (int) Double.parseDouble(par.origConfig.getProperty("runOffset", "0"));

        aggregateResults = Boolean.parseBoolean(par.origConfig.getProperty(TORCS_AGGREGATE_RESULTS, "false"));
        shareDefaultParams = Boolean.parseBoolean(par.origConfig.getProperty(TORCS_SHARE_DEFAULT_PARAMS, "false"));
        noisy = Boolean.parseBoolean(par.origConfig.getProperty(TORCS_NOISY, "false"));
        if (shareDefaultParams) {
            System.out.println("Using default parameters for all given tracks");
        }

        if (par.origConfig.containsKey(TORCS_TRACKS)) {
            ArrayList<String> list = new ArrayList<>();
            StringTokenizer tokenizer = new StringTokenizer(par.origConfig.getProperty(TORCS_TRACKS), ",");

            while (tokenizer.hasMoreTokens()) {
                list.add(tokenizer.nextToken().trim());
            }

            // check if all tracks are available            
            boolean success = true;
            for (String s : list) {
                TrackData td = tracklist.getTrack(s);
                if (td == null) {
                    System.out.println("FAILURE: Track \"" + s + "\" not found in TrackDataList!");
                    success = false;
                } else {
                    trackData.add(td);
                }
            }
            if (!success) {
                System.exit(1);
            }

            objectiveFunctions = OBJECTIVES_PER_TRACK * trackData.size();

        } else {
            System.out.println("No tracks given in config file (use parameter " + TORCS_TRACKS + ")");
            System.exit(1);
        }

        if (par.origConfig.containsKey(TORCS_IP_PORT)) {
            String ipport = par.origConfig.getProperty(TORCS_IP_PORT);
            int idx = ipport.indexOf(':');
            host = ipport.substring(0, idx);
            String portAsString = ipport.substring(idx + 1, ipport.length());
            if (portAsString.equalsIgnoreCase("auto")) {
                System.out.print("Using auto config feature for the port number...");
                port = 3001 + tracklist.getTrackIndex(trackData.get(0));
                System.out.println(" port = " + port);

            } else {
                port = Integer.parseInt(ipport.substring(idx + 1, ipport.length()));
            }
        }
        if (par.origConfig.containsKey(TORCS_MAX_TICKS)) {
            maxTicks = Integer.parseInt(par.origConfig.getProperty(TORCS_MAX_TICKS));
        }
        if (par.origConfig.containsKey(TORCS_MAX_LAPS)) {
            maxLaps = Integer.parseInt(par.origConfig.getProperty(TORCS_MAX_LAPS));
        }
        if (par.origConfig.containsKey(TORCS_PARAM_FILES)) {
            StringTokenizer trackTokenizer = new StringTokenizer(par.origConfig.getProperty(TORCS_PARAM_FILES), ";");
            if (!shareDefaultParams && trackTokenizer.countTokens() != trackData.size()) {
                System.out.println("Error: " + trackData.size() + " tracks but default parameter files for only "
                        + trackTokenizer.countTokens() + " tracks!");
                System.out.println("Fix or enable paramter sharing");
                System.exit(-1);
            }

            for (int i = 0; i < (shareDefaultParams ? 1 : trackData.size()); ++i) {
                paramFiles.add(new ArrayList<String>());
                StringTokenizer tokenizer = new StringTokenizer(trackTokenizer.nextToken(), ",");
                while (tokenizer.hasMoreTokens()) {
                    paramFiles.get(i).add(tokenizer.nextToken().trim());
                }
            }
        }

        runTorcsTextOnly = Boolean.parseBoolean(par.origConfig.getProperty(TORCS_TEXT_ONLY, "false"));

        if (runTorcsTextOnly) {
            if (par.origConfig.containsKey(TORCS_TORCS_PATH)) {
                this.torcsPath = par.origConfig.getProperty(TORCS_TORCS_PATH);
                checkDirectory("torcs path", torcsPath);
                checkDirectory("torcs executable path", Torcs134Executor.getExecutablePath(torcsPath));

            } else {
                System.out.println("Torcs cannot be run in textmode without specifying a directory for torcs");
                System.exit(-1);
            }
            if (par.origConfig.containsKey(TORCS_TORCS_TIMEOUT)) {
                this.torcsTimeout = Long.parseLong(par.origConfig.getProperty(TORCS_TORCS_TIMEOUT));
            }
        }

        if (trackData.size() > 1 && !runTorcsTextOnly) {
            System.out.println("Multiobjective optimization on more than one track only supported in text only mode!");
            System.exit(1);
        }

        optimizePlan = Boolean.parseBoolean(par.origConfig.getProperty(TORCS_OPTIMIZE_PLAN, "true"));
        optimizeACCDamp = Boolean.parseBoolean(par.origConfig.getProperty(TORCS_OPTIMIZE_ACC_DAMPING, "true"));
        optimizeBrakeDamp = Boolean.parseBoolean(par.origConfig.getProperty(TORCS_OPTIMIZE_BRAKE_DAMPING, "true"));
        optimizeClutch = Boolean.parseBoolean(par.origConfig.getProperty(TORCS_OPTIMIZE_CLUTCH, "true"));
        includePlanBCC = Boolean.parseBoolean(par.origConfig.getProperty(TORCS_INCLUDE_PLAN_BCC, "true"));
        includePlanPCF = Boolean.parseBoolean(par.origConfig.getProperty(TORCS_INCLUDE_PLAN_PCF, "true"));
        optimizeTargetspeeds = Boolean.parseBoolean(par.origConfig.getProperty(TORCS_OPTIMIZE_TARGET_SPEEDS_INSTEAD_OF_GLF, "false"));

        TEXT_DEBUG = Boolean.parseBoolean(par.origConfig.getProperty("TEXT_DEBUG", "false"));
        if (aggregateResults) {
            objectiveFunctions = OBJECTIVES_PER_TRACK;
        }

        if (optimizePlan) {
            if (optimizeTargetspeeds) {
                // only optimize pcf
                //dimensions += 1;
            } else {
                dimensions += 4;
                System.setProperty(Plan2013.ALWAYS_USE_GLF_FOR_TARGET_SPEEDS, "");
            }
            if (includePlanBCC) {
                ++dimensions;
            }
            if (includePlanPCF) {
                dimensions += 1;
            }
        }
        if (optimizeACCDamp) {
            dimensions += 4;
        }
        if (optimizeBrakeDamp) {
            dimensions += 4;
        }
        if (optimizeClutch) {
            dimensions += 5;
        }

        if (optimizeTargetspeeds) {
            System.out.println("Optimize target speeds currently unsupported");
            System.exit(1);
            /* try {
             TrackModel tm = TrackModel.load(trackName + TrackModel.TM_EXT);

             int speeds = 0;

             for (int i = 0; i < tm.size(); ++i) {
             TrackSegment seg = tm.getSegment(i);
             if (seg.isCorner()) {
             speeds += seg.getApexes().length;
             }
             }

             System.out.println("Need to optimize " + speeds + " target speeds");
             dimensions += speeds;

             int parallelEvals = Integer.parseInt(par.origConfig.getProperty("EA.parallelEvals", "1"));

             if (parallelEvals == 1) {
             trackModels = new TrackModel[]{tm};

             } else {
             trackModels = new TrackModel[parallelEvals];
             trackModels[0] = tm;

             for (int i = 1; i < trackModels.length; ++i) {
             trackModels[i] = TrackModel.load(trackName + TrackModel.TM_EXT);
             }
             }

             } catch (Exception e) {
             e.printStackTrace(System.out);
             System.out.println("Failed to load track model");
             System.exit(-1);
             }*/
        }

        for (int trackIndex = 0; trackIndex < (shareDefaultParams ? 1 : trackData.size()); ++trackIndex) {
            TrackData td = trackData.get(trackIndex);
            if (shareDefaultParams) {
                System.out.println("Loading shared default parameter sets");
            } else {
                System.out.println("Loading default parameter sets for " + td.torcsTrackName);
            }
            Properties props = new Properties();
            for (int i = 0; i < paramFiles.get(trackIndex).size(); ++i) {
                if (new File(paramFiles.get(trackIndex).get(i)).exists()) {
                    System.out.println("    " + paramFiles.get(trackIndex).get(i));

                    try {
                        InputStream in = new FileInputStream(paramFiles.get(trackIndex).get(i));
                        props.load(in);
                        in.close();

                    } catch (Exception e) {
                        e.printStackTrace(System.out);
                        System.exit(1);
                    }
                } else {
                    System.out.println("Error: " + paramFiles.get(trackIndex).get(i) + " not found!");
                    System.exit(-1);
                }
            }
            System.out.println(Utils.list(props, "\n"));
            System.out.println("--------------------------------------");
            defaultProperties.add(props);
        }
    }

    public static void checkDirectory(String description, String path) {
        File check = new File(path);
        System.out.print("Checking " + description + ": " + check.getAbsolutePath() + " ...");
        if (check.exists()) {
            System.out.println("ok");
        } else {
            System.out.println("");
            System.out.println("Does not exist!");
            System.exit(-1);
        }
    }

    @Override
    public String problemRunHeader() {
        StringBuilder result = new StringBuilder();

        result.append("Run torcs in text only mode? ").append(runTorcsTextOnly).append("\n");
        if (runTorcsTextOnly) {
            result.append("Torcs path: ").append(torcsPath).append("\n");
            result.append("Timeout: ").append(torcsTimeout).append("microseconds\n");
            result.append("Noisy sensors: ").append(noisy).append("\n");
        }
        result.append("Tracks: ");
        for (TrackData td : trackData) {
            result.append(td.torcsTrackName).append(", ");
        }
        result.append("\n");
        result.append("Multiobjective optimization: ").append(multiobjective).append("\n");
        if (multiobjective) {
            result.append("  Objectives used for selection: ");
            for (int i = 0; i < objectivesForSelection.size(); ++i) {
                if(i > 0){
                    result.append(", ");
                }
                result.append(objectivesForSelection.get(i));
            }
            result.append("\n");
        }
        result.append("Dimensions: ").append(dimensions).append("\n");
        result.append("Optimize parameters of the planning module? ").append(optimizePlan).append("\n");
        result.append("Include the brake corner coefficient? ").append(includePlanBCC).append("\n");
        result.append("Include the plan corner fraction? ").append(includePlanPCF).append("\n");
        result.append("Optimize parameters for the acceleration damping? ").append(optimizeACCDamp).append("\n");
        result.append("Optimize parameters for the brake damping? ").append(optimizeBrakeDamp).append("\n");
        result.append("Optimize parameters for the clutch? ").append(optimizeClutch).append("\n");
        result.append("Directly optimize target speeds without GLF? ").append(optimizeTargetspeeds).append("\n");
        if (aggregateResults) {
            result.append("Aggregating results from different tracks\n");
        } else {
            result.append("NOT aggregating results from different tracks\n");
        }

        result.append("Default properties in use: \n");
        for (int i = 0; i < (shareDefaultParams ? 1 : trackData.size()); ++i) {
            if (shareDefaultParams) {
                result.append("Shared for all tracks\n");
            } else {
                result.append("For track ").append(trackData.get(i).torcsTrackName).append("\n");
            }
            result.append(Utils.list(defaultProperties.get(i), "\n"));
        }

        return result.toString();
    }

    /**
     * A method called before the next round of parallel evaluations start. This
     * can be used to setup some metadata used to evaluate the n individuals
     * (determine port numbers for network connections of the individuals,
     * prepare different temp directories for the individuals, etc).
     *
     * @param n Number of individuals to be evaluated in parallel during the
     * next batch.
     */
    public void preParallelEvaluations(int n) {
        // nothing to do here
    }

    /**
     * Method called after the parallel evaluation of a number of individuals
     * has finished. Can be used to clean up the stuff generated during the call
     * to preParallelEvaluation().
     */
    public void postParallelEvaluations() {
        // nothing to do here
    }

    /**
     * Method called to start the evaluation of one individual.
     *
     * @param ind The individual to evaluate.
     * @param i The index of the individual, a number between 1 and n.
     * @return The results of the evaluation.
     */
    @Override
    public Evaluation parallelEvaluation(Individual ind, int i) {
        return evaluate(ind, i - 1);
    }

    @Override
    public Evaluation evaluate(Individual variables) {
        return evaluate(variables, 0);
    }

    public Evaluation evaluate(Individual variables, int offset) {
        System.out.println("-- " + variables.id + " / " + offset + " -----------------------------");
        ArrayList<Fitness> results = new ArrayList<>();

        for (int i = 0; i < trackData.size(); ++i) {
            System.out.println("Track " + trackData.get(i).torcsTrackName);
            Fitness[] trackResults = evaluate(variables, offset, trackData.get(i),
                    defaultProperties.get(shareDefaultParams ? 0 : i));
            for (int k = 0; k < trackResults.length; ++k) {
                System.out.println(Utils.toString(trackResults[k]));
                results.add(trackResults[k]);
            }
        }

        Fitness[] result;

        if (aggregateResults) {
            System.out.println("Aggregating...");
            result = new Fitness[objectiveFunctions];
            for (int i = 0; i < result.length; ++i) {
                double value = 0.0;
                for (int k = i; k < results.size(); k += objectiveFunctions) {
                    value += results.get(k).fit;
                    //results[1] = new Fitness(fe.getResult().damage, "Damage", true, "Dam");
                }
                result[i] = new Fitness(value, results.get(i).name,
                        results.get(i).minimization, results.get(i).id);
                System.out.println(Utils.toString(result[i]));
            }

        } else {
            result = new Fitness[results.size()];
            for (int i = 0; i < results.size(); ++i) {
                result[i] = results.get(i);
            }
        }

        synchronized (this) {
            ++evals;
            ++validEvals;
        }

        return new Evaluation(result);
    }

    public Fitness[] evaluate(Individual variables, int offset, TrackData track, Properties defaultProperties) {
        Properties params = new Properties();

        RealVariable v;

        // parameters for the target speed function
        if (optimizePlan && !optimizeTargetspeeds) {
            v = (RealVariable) variables.getAnyVariable("TS_B");
            params.setProperty(MrRacer2013.PLAN + Plan2013.TARGET_SPEEDS + GeneralisedLogisticFunction.GROWTH_RATE_B,
                    String.valueOf(Math.pow(10, v.toDouble())));

            v = (RealVariable) variables.getAnyVariable("TS_M");
            params.setProperty(MrRacer2013.PLAN + Plan2013.TARGET_SPEEDS + GeneralisedLogisticFunction.M,
                    String.valueOf(v.toDouble()));

            v = (RealVariable) variables.getAnyVariable("TS_V");
            params.setProperty(MrRacer2013.PLAN + Plan2013.TARGET_SPEEDS + GeneralisedLogisticFunction.V,
                    String.valueOf(v.toDouble()));

            v = (RealVariable) variables.getAnyVariable("TS_Q");
            params.setProperty(MrRacer2013.PLAN + Plan2013.TARGET_SPEEDS + GeneralisedLogisticFunction.Q,
                    String.valueOf(v.toDouble()));
            params.setProperty(MrRacer2013.PLAN + Plan2013.TARGET_SPEEDS + GeneralisedLogisticFunction.LOWER_ASYMPTOTE_A, String.valueOf(0.0));
            params.setProperty(MrRacer2013.PLAN + Plan2013.TARGET_SPEEDS + GeneralisedLogisticFunction.UPPER_ASYMPTOTE_K, String.valueOf(1.0));
        }

        // parameters for the acceleration damp function
        if (optimizeACCDamp) {
            v = (RealVariable) variables.getAnyVariable("AD_B");
            params.setProperty(MrRacer2013.ACC + AbstractDampedAccelerationBehaviour.ACC_DAMP + GeneralisedLogisticFunction.GROWTH_RATE_B,
                    String.valueOf(Math.pow(10, v.toDouble())));

            v = (RealVariable) variables.getAnyVariable("AD_M");
            params.setProperty(MrRacer2013.ACC + AbstractDampedAccelerationBehaviour.ACC_DAMP + GeneralisedLogisticFunction.M,
                    String.valueOf(v.toDouble()));

            v = (RealVariable) variables.getAnyVariable("AD_V");
            params.setProperty(MrRacer2013.ACC + AbstractDampedAccelerationBehaviour.ACC_DAMP + GeneralisedLogisticFunction.V,
                    String.valueOf(v.toDouble()));

            v = (RealVariable) variables.getAnyVariable("AD_Q");
            params.setProperty(MrRacer2013.ACC + AbstractDampedAccelerationBehaviour.ACC_DAMP + GeneralisedLogisticFunction.Q,
                    String.valueOf(v.toDouble()));

            params.setProperty(MrRacer2013.ACC + AbstractDampedAccelerationBehaviour.ACC_DAMP + GeneralisedLogisticFunction.LOWER_ASYMPTOTE_A, String.valueOf(0.0));
            params.setProperty(MrRacer2013.ACC + AbstractDampedAccelerationBehaviour.ACC_DAMP + GeneralisedLogisticFunction.UPPER_ASYMPTOTE_K, String.valueOf(1.0));
        }

        if (this.optimizeBrakeDamp) {
            // parameters for the brake damp function
            v = (RealVariable) variables.getAnyVariable("BD_B");
            params.setProperty(MrRacer2013.ACC + AbstractDampedAccelerationBehaviour.BRAKE_DAMP + GeneralisedLogisticFunction.GROWTH_RATE_B,
                    String.valueOf(Math.pow(10, v.toDouble())));

            v = (RealVariable) variables.getAnyVariable("BD_M");
            params.setProperty(MrRacer2013.ACC + AbstractDampedAccelerationBehaviour.BRAKE_DAMP + GeneralisedLogisticFunction.M,
                    String.valueOf(v.toDouble()));

            v = (RealVariable) variables.getAnyVariable("BD_V");
            params.setProperty(MrRacer2013.ACC + AbstractDampedAccelerationBehaviour.BRAKE_DAMP + GeneralisedLogisticFunction.V,
                    String.valueOf(v.toDouble()));

            v = (RealVariable) variables.getAnyVariable("BD_Q");
            params.setProperty(MrRacer2013.ACC + AbstractDampedAccelerationBehaviour.BRAKE_DAMP + GeneralisedLogisticFunction.Q,
                    String.valueOf(v.toDouble()));

            params.setProperty(MrRacer2013.ACC + AbstractDampedAccelerationBehaviour.BRAKE_DAMP + GeneralisedLogisticFunction.LOWER_ASYMPTOTE_A, String.valueOf(0.0));
            params.setProperty(MrRacer2013.ACC + AbstractDampedAccelerationBehaviour.BRAKE_DAMP + GeneralisedLogisticFunction.UPPER_ASYMPTOTE_K, String.valueOf(1.0));
        }

        // parameter for the brake coefficient in corners && flexible corner planning
        if (optimizePlan) {
            if (includePlanBCC) {
                v = (RealVariable) variables.getAnyVariable("P_BCC");
                params.setProperty(MrRacer2013.PLAN + Plan2013.BRAKE_CORNER_COEFF,
                        String.valueOf(v.toDouble()));
            }

            if (includePlanPCF) {
                v = (RealVariable) variables.getAnyVariable("P_CPF");
                params.setProperty(MrRacer2013.PLAN + Plan2013.MODULE_CORNER + PlanCorner2013.FRACTION,
                        String.valueOf(v.toDouble()));
            }
        }

        // parameters for the clutch
        if (optimizeClutch) {
            v = (RealVariable) variables.getAnyVariable("CLUTCH_B");
            params.setProperty(MrRacer2013.CLUTCH + Clutch.F + GeneralisedLogisticFunction.GROWTH_RATE_B,
                    String.valueOf(Math.pow(10, v.toDouble())));

            v = (RealVariable) variables.getAnyVariable("CLUTCH_M");
            params.setProperty(MrRacer2013.CLUTCH + Clutch.F + GeneralisedLogisticFunction.M,
                    String.valueOf(v.toDouble()));

            v = (RealVariable) variables.getAnyVariable("CLUTCH_V");
            params.setProperty(MrRacer2013.CLUTCH + Clutch.F + GeneralisedLogisticFunction.V,
                    String.valueOf(v.toDouble()));

            v = (RealVariable) variables.getAnyVariable("CLUTCH_Q");
            params.setProperty(MrRacer2013.CLUTCH + Clutch.F + GeneralisedLogisticFunction.Q,
                    String.valueOf(v.toDouble()));

            v = (RealVariable) variables.getAnyVariable("CLUTCH_SPEED");
            params.setProperty(MrRacer2013.CLUTCH + Clutch.MS,
                    String.valueOf(v.toDouble() * 300.0));

            params.setProperty(MrRacer2013.CLUTCH + Clutch.F + GeneralisedLogisticFunction.LOWER_ASYMPTOTE_A, String.valueOf(0.0));
            params.setProperty(MrRacer2013.CLUTCH + Clutch.F + GeneralisedLogisticFunction.UPPER_ASYMPTOTE_K, String.valueOf(1.0));
        }

        // target speeds
        if (optimizeTargetspeeds) {
            System.out.println("optimizeTargetspeeds not supported right now");
            System.exit(1);
            /*
             int speed = 0;

             for (int i = 0; i < trackModels[offset].size(); ++i) {
             TrackSegment seg = trackModels[offset].getSegment(i);
             if (seg.isCorner()) {
             for (int k = 0; k < seg.getApexes().length; ++k) {
             v = (RealVariable) variables.getAnyVariable("TS_CORNER_" + speed);
             seg.setTargetSpeed(k, v.toDouble() * 300.0);
             ++speed;
             }
             }
             }
             try {
             trackModels[offset].save("ind-id" + variables.id + TrackModel.TM_EXT);
             } catch (Exception e) {
             e.printStackTrace(System.out);
             System.exit(-1);
             }*/
        }

        // wait a bit
        /*try {
         Thread.sleep(1000);
         } catch (Exception e) {
         e.printStackTrace(System.out);
         }*/

        MrRacer2013 controller = new MrRacer2013();
        //ClutchTester controller = new ClutchTester();
        //MrSimpleRacer controller = new MrSimpleRacer();

        defaultProperties.setProperty("-MrRacer2012.Plan--PLAN.MU-", String.valueOf(track.getWeightedFriction()));

        Properties controllerParams = new Properties();
        controllerParams.putAll(defaultProperties);
        controllerParams.putAll(params);

        controller.setParameters(controllerParams);
        controller.setStage(scr.Controller.Stage.QUALIFYING);
        if (optimizeTargetspeeds) {
            controller.setTrackName("ind-id" + variables.id);
        } else {
            controller.setTrackName(track.controllerTrackName);
        }

        String dir = "." + File.separator + "inds" + (run < 10 ? "0" : "") + String.valueOf(run);
        final String saveTo = dir + File.separator + "ind-id" + variables.id + track.controllerTrackName + BaseController.PARAMETER_EXT;
        String comment = "Individual " + variables.id + " track " + track.controllerTrackName + " from experiment " + experimentName
                + " run " + run + " ("
                + (new Date().toString().replace(':', '-').replace(' ', '-'))
                + "), before the evaluation";

        Properties realParams = new Properties();
        controller.getParameters(realParams);

        try {
            File f = new File(dir);
            f.mkdirs();
            FileWriter out = new FileWriter(new File(saveTo));
            out.write(comment + "\n");
            out.write(Utils.list(realParams, "\n"));
            out.close();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        Torcs134Executor torcsExec = new Torcs134Executor(torcsPath);



        if (runTorcsTextOnly) {
            TorcsExecConfig cfg = new TorcsExecConfig();
            cfg.port = port;
            cfg.restart_behavior = 's';
            cfg.timeout = torcsTimeout;
            cfg.nolaptime = true;
            cfg.nodamage = true;
            cfg.noisy = noisy;
            System.out.print("Starting torcs @" + cfg.port + "...");
            torcsExec.startTorcs(track, cfg);
            System.out.println("done");
        }

        FitnessEvaluator fe = new FitnessEvaluator(host, port + offset,
                new Evaluator(controller, maxTicks), maxLaps);

        try {
            fe.join();

        } catch (InterruptedException e) {
            e.printStackTrace(System.out);
        }

        System.out.println("Timeouts: " + torcsExec.getTimeoutCtr());

        if (runTorcsTextOnly && !fe.finishedNormally()) {
            System.out.println("Torcs " + (offset + 1) + " seems to have crashed, destroying process");
            torcsExec.stopTorcs();
        }



        variables.evaluated = true;

        Fitness[] results;
        if (optimizeClutch) {
            results = new Fitness[1];
        } else {
            results = new Fitness[3];
        }
        if (optimizeClutch) {
            results[0] = new Fitness(-fe.getResults().getDistanceRaced(), "Distance", true, "Dist");
        } else {
            results[0] = new Fitness(fe.getResults().getAdjustedTime(EvaluationData.BY_DAMAGE), "Overalltime", true, "Time");
        }

        if (results.length > 1) {
            results[1] = new Fitness(fe.getResults().getDamage(), "Damage", true, "Dam");
            double value = 10.0*(fe.getResults().getLateralSpeedIntegral()/track.length);
            results[2] = new Fitness(value, "LateralSpeed", true, "LatS");
        }
        //results[2] = new Fitness(fe.getLatSpeedIntegral(), "LatSpeed", true, "LatS");

        realParams = new Properties();
        controller.getParameters(realParams);

        comment = "Individual " + variables.id + " track " + track.controllerTrackName + " from experiment " + experimentName
                + " run " + run + " ("
                + (new Date().toString().replace(':', '-').replace(' ', '-'))
                + "), fitness ";
        for(int i=0; i < results.length; ++i){
            if(i > 0){
                comment += ", ";
            }
            comment += Utils.toString(results[i]);
        }

        try {
            File f = new File(dir);
            f.mkdirs();
            FileWriter out = new FileWriter(new File(saveTo));
            out.write("# " + comment + "\n");
            out.write(Utils.list(realParams, "\n"));
            out.close();

            if (optimizeTargetspeeds) {
                java.nio.file.Path source = new File("ind-id" + variables.id + TrackModel.TM_EXT).toPath();
                java.nio.file.Path newdir = new File(dir).toPath();
                java.nio.file.Files.move(source, newdir.resolve(source.getFileName()), java.nio.file.StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        return results;
    }

    public Individual getTemplate(Parameters par) {
        Individual result = new Individual();

        VariableGroup genotype = new VariableGroup("genotype");

        if (optimizePlan && !optimizeTargetspeeds) {
            // growth rate b of the target speed
            genotype.add(new RealVariable("TS_B", 0, 1, true));
            // m of the target speed
            genotype.add(new RealVariable("TS_M", 0, 1, true));
            // v of the target speed
            genotype.add(new RealVariable("TS_V", 0.01, 1, true));
            // q of the target speed
            genotype.add(new RealVariable("TS_Q", 0.01, 1, true));
        }

        if (optimizeACCDamp) {
            // growth rate b of the acc damp
            genotype.add(new RealVariable("AD_B", 0, 1, true));
            // m of the acc damp
            genotype.add(new RealVariable("AD_M", 0, 1, true));
            // v of the acc damp
            genotype.add(new RealVariable("AD_V", 0.01, 1, true));
            // q of the acc damp
            genotype.add(new RealVariable("AD_Q", 0.01, 1, true));
        }

        if (optimizeBrakeDamp) {
            // growth rate b of the brake damp
            genotype.add(new RealVariable("BD_B", 0, 1, true));
            // m of the brake damp
            genotype.add(new RealVariable("BD_M", 0, 1, true));
            // v of the brake damp
            genotype.add(new RealVariable("BD_V", 0.01, 1, true));
            // q of the brake damp
            genotype.add(new RealVariable("BD_Q", 0.01, 1, true));
        }

        // brake corner coefficient && corner fraction
        if (optimizePlan) {
            if (includePlanBCC) {
                genotype.add(new RealVariable("P_BCC", 0, 1, true));
            }
            if (includePlanPCF) {
                genotype.add(new RealVariable("P_CPF", 0, 1, true));
            }
        }

        if (optimizeClutch) {
            genotype.add(new RealVariable("CLUTCH_B", 0, 1, true));
            genotype.add(new RealVariable("CLUTCH_M", 0, 1, true));
            genotype.add(new RealVariable("CLUTCH_V", 0.01, 1, true));
            genotype.add(new RealVariable("CLUTCH_Q", 0.01, 1, true));
            genotype.add(new RealVariable("CLUTCH_SPEED", 0, 1, true));
        }

        if (optimizeTargetspeeds) {
            int speed = 0;

            for (int i = 0; i < trackModels[0].size(); ++i) {
                TrackSegment seg = trackModels[0].getSegment(i);
                if (seg.isCorner()) {
                    for (int k = 0; k < seg.getApexes().length; ++k) {
                        genotype.add(new RealVariable("TS_CORNER_" + speed, 0, 1, true));
                        ++speed;
                    }
                }
            }
        }

        result.importGroup(genotype);

        // one mutation strength:
        RealMetaVariable mutationStrength = new RealMetaVariable("mutationStrength", result.variables,
                Operators.CMUTATION_STRENGTH, par.minSigma, par.maxSigma);
        if (par.initSigmas > 0) {
            mutationStrength.setInitGaussian(par.initSigmas, par.initSigmaRange);
        }
        result.importMetadata(mutationStrength);

        return result;
    }

    public int dimensions() {
        return dimensions;
    }

    /**
     * Number of objective functions defined in this problem
     */
    public int objectiveFunctions() {
        return objectiveFunctions;
    }

    @Override
    public void setProblemNo(int i) {
        run = i;
        //System.out.println("Run set to " + run);
    }

    public static void main(String[] args) {
        //String paramName = "F:\\Quad\\Experiments\\Torcs-Test\\torcs-config.properties";
        String paramName = null;
        if (args.length == 1) {
            paramName = args[ 0];
        }
        Parameters par = new Parameters(args, paramName);

        BlackBoxProblem prob = new TorcsProblem(par);
        Individual ind = prob.getTemplate(par);
        BatchEA ea = new BatchEA(ind, par, prob);
        ea.run();
    }
}
