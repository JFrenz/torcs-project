/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo.tools;

import de.janquadflieg.mrracer.torcs.SurfaceData;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import de.janquadflieg.mrracer.torcs.TrackData;
import de.janquadflieg.mrracer.torcs.TrackDataList;
import de.janquadflieg.mrracer.track.TrackModel;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

/**
 *
 * @author quad
 */
public class ExperimentSetup {

    private Properties settings;
    private static final String DEFAULT_PARAMETERS_DIRECTORY_KEY = "DEFAULT_PARAMETERS_DIRECTORY";
    private final File DEFAULT_PARAMETERS_DIRECTORY;
    private static final String TRACK_MODELS_DIRECTORY_KEY = "TRACK_MODELS_DIRECTORY";
    private final File TRACK_MODELS_DIRECTORY;
    private static final String BASE_PATH_KEY = "BASE_PATH";
    private final String BASE_PATH;
    private static final String ANALYSE_SCRIPT_KEY = "ANALYSE_SCRIPT";
    private final File ANALYSE_SCRIPT;
    private static final String EA_TEMPLATE_FILE_KEY = "EA_TEMPLATE_FILE";
    private final File EA_TEMPLATE_FILE;
    private final static String CONTROLLER_DIRECTORY_KEY = "CONTROLLER_DIRECTORY";
    private final File CONTROLLER_DIRECTORY;
    
    public static final String[] CONTROLLER_FILES = {"MrRacer.jar",
        "lib" + File.separator + "commons-math3-3.2.jar",
        "lib" + File.separator + "ealib.jar",
        "lib" + File.separator + "flanagan.jar",
        "lib" + File.separator + "Jama-1.0.2.jar",
        "lib" + File.separator + "RaceclientLib.jar",
        "lib" + File.separator + "SupportTools.jar",
        "lib" + File.separator + "vecmath.jar"
    };
    private static final String DEFAULT_PARAMETERS_FILES_KEY = "DEFAULT_PARAMETERS_FILES";
    private ArrayList<String> DEFAULT_PARAMETERS_FILES = new ArrayList<>();
    private static final String USE_TRACK_SPECIFIC_DEFAULTS_KEY = "USE_TRACK_SPECIFIC_DEFAULTS";
    private final boolean USE_TRACK_SPECIFIC_DEFAULTS;
    private static final HashMap<SurfaceData, String> DEFAULTS = new HashMap<>();
    private static final TrackDataList trackList = new TrackDataList();
    //public static final String[] TRACKS = {"A-Speedway", "Kumbharli-mountain-snow"};
    private String[] TRACKS;

    static {
        DEFAULTS.put(SurfaceData.createCityReference(), "basecity.params");
        DEFAULTS.put(SurfaceData.createDesertReference(), "basedesert.params");
        DEFAULTS.put(SurfaceData.createHillReference(), "basehill.params");
        DEFAULTS.put(SurfaceData.createMountainReference(), "basemountain.params");
    }

    public ExperimentSetup(Properties p) {
        this.settings = p;

        DEFAULT_PARAMETERS_DIRECTORY = new File(settings.getProperty(ExperimentSetup.DEFAULT_PARAMETERS_DIRECTORY_KEY));
        TRACK_MODELS_DIRECTORY = new File(settings.getProperty(ExperimentSetup.TRACK_MODELS_DIRECTORY_KEY));
        BASE_PATH = settings.getProperty(ExperimentSetup.BASE_PATH_KEY);
        ANALYSE_SCRIPT = new File(settings.getProperty(ExperimentSetup.ANALYSE_SCRIPT_KEY));
        EA_TEMPLATE_FILE = new File(settings.getProperty(ExperimentSetup.EA_TEMPLATE_FILE_KEY));
        CONTROLLER_DIRECTORY = new File(settings.getProperty(ExperimentSetup.CONTROLLER_DIRECTORY_KEY));

        if (settings.containsKey(ExperimentSetup.DEFAULT_PARAMETERS_FILES_KEY)) {
            String s = settings.getProperty(ExperimentSetup.DEFAULT_PARAMETERS_FILES_KEY);
            StringTokenizer tokenizer = new StringTokenizer(s, ",");

            while (tokenizer.hasMoreTokens()) {
                DEFAULT_PARAMETERS_FILES.add(tokenizer.nextToken().trim());
            }
        }
        USE_TRACK_SPECIFIC_DEFAULTS = Boolean.parseBoolean(settings.getProperty(ExperimentSetup.USE_TRACK_SPECIFIC_DEFAULTS_KEY, "false"));
        TRACKS = new String[TrackDataList.TRACKS_TCIAIG2014.length];
        System.arraycopy(TrackDataList.TRACKS_TCIAIG2014, 0, TRACKS, 0, TrackDataList.TRACKS_TCIAIG2014.length);
    }

    private void copyController(File targetDir)
            throws Exception {
        File libsDir = new File(targetDir.getAbsolutePath() + File.separator + "lib");
        libsDir.mkdir();
        for (String filestring : CONTROLLER_FILES) {
            Path src = Paths.get(CONTROLLER_DIRECTORY + File.separator + filestring);
            Path dst = Paths.get(targetDir.getAbsolutePath() + File.separator + filestring);
            Files.copy(src, dst, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    private static void appendAnalyseEntry(OutputStreamWriter w, File subDir)
            throws Exception {
        w.write("if [ -d " + subDir.getName() + " ]\n");
        w.write("then\n");
        w.write("cd " + subDir.getName() + "\n");
        w.write("echo " + subDir.getName() + "\n");
        //w.write("R --vanilla < list_best.r\n");
        w.write("echo \"  Analysing...\"\n");
        w.write("R CMD BATCH list_best.r\n");
        w.write("echo \"  Zipping...\"\n");
        w.write("zip -rq " + subDir.getName() + ".zip inds01/ inds02/ inds03/ inds04/ inds05/ inds06/ inds07/ inds08/ inds09/ inds10/\n");
        w.write("echo \"  Deleting...\"\n");
        w.write("rm -r inds01/\n");
        w.write("rm -r inds02/\n");
        w.write("rm -r inds03/\n");
        w.write("rm -r inds04/\n");
        w.write("rm -r inds05/\n");
        w.write("rm -r inds06/\n");
        w.write("rm -r inds07/\n");
        w.write("rm -r inds08/\n");
        w.write("rm -r inds09/\n");
        w.write("rm -r inds10/\n");
        for (String filestring : CONTROLLER_FILES) {
            w.write("rm " + filestring + "\n");
        }
        w.write("rmdir lib\n");
        w.write("echo \"  Done.\"\n");
        w.write("cd ..\n");
        w.write("else\n");
        w.write("echo " + subDir.getName() + " has been deleted.\n");
        w.write("fi\n");
    }

    public void run(boolean setup, boolean analyse)
            throws Exception {
        System.out.println("Setting up experiment");
        System.out.println("Base path: " + BASE_PATH);

        // create directories
        File baseDir = new File(BASE_PATH);
        if (!baseDir.exists() || !baseDir.isDirectory()) {
            System.out.println("Error with base directory " + BASE_PATH);
            System.exit(1);
        }

        // copy controller jar to base directory, so we can use it in the clean up script
        if (analyse) {
            copyController(baseDir);
        }

        // create analyse and cleanup scrip
        File analyseScriptFile;
        OutputStreamWriter analyseScriptWriter = null;
        if (analyse) {
            analyseScriptFile = new File(BASE_PATH + File.separator + "analyse");
            analyseScriptWriter = new OutputStreamWriter(new FileOutputStream(analyseScriptFile), "UTF-8");
            analyseScriptWriter.write("#!/bin/sh\n");
            analyseScriptWriter.write("echo Disk spaced used:\n");
            analyseScriptWriter.write("du -sh\n");
            analyseScriptWriter.write("echo Running Java Cleanup Program\n");
            analyseScriptWriter.write("java -cp MrRacer.jar de.janquadflieg.mrracer.evo.tools.ExperimentCleanUp\n");
            analyseScriptWriter.write("echo Done.\n");
            analyseScriptWriter.write("echo Disk spaced used:\n");
            analyseScriptWriter.write("du -sh\n");
        }

        for (String s : TRACKS) {
            TrackData track = trackList.getTrack(s);
            File trackDir = new File(BASE_PATH + File.separator + track.controllerTrackName);
            if (setup) {
                trackDir.mkdir();

                // copy track model
                Path srcTrackModel = Paths.get(TRACK_MODELS_DIRECTORY.getAbsolutePath()
                        + File.separator + track.controllerTrackName + "-1" + TrackModel.TM_EXT);
                //System.out.println(srcTrackModel.toString()+" ? "+Files.exists(srcTrackModel));
                Path dstTrackModel = Paths.get(trackDir.getAbsolutePath()
                        + File.separator + track.controllerTrackName + TrackModel.TM_EXT);
                //System.out.println(dstTrackModel.toString()+" ? "+Files.exists(dstTrackModel));
                Files.copy(srcTrackModel, dstTrackModel, StandardCopyOption.REPLACE_EXISTING);
            }

            if (analyse) {
                // copy r script
                Path srcRScript = Paths.get(ANALYSE_SCRIPT.getAbsolutePath());
                Path dstRScript = Paths.get(trackDir.getAbsolutePath() + File.separator + "list_best.r");
                Files.copy(srcRScript, dstRScript, StandardCopyOption.REPLACE_EXISTING);
            }

            if (setup) {
                // copy controller
                copyController(trackDir);
                
                ArrayList<String> defaultParameters = new ArrayList<>();

                if (USE_TRACK_SPECIFIC_DEFAULTS) {
                    System.out.print("Looking for best match of default parameters for track " + track.torcsTrackName + "... ");
                    Set<SurfaceData> entries = DEFAULTS.keySet();
                    SurfaceData bestMatch = null;
                    double bestValue = Double.POSITIVE_INFINITY;
                    Iterator<SurfaceData> it = entries.iterator();
                    while (it.hasNext()) {
                        SurfaceData candidate = it.next();
                        double v = track.calcSurfaceDifference(candidate);
                        if (v < bestValue) {
                            bestMatch = candidate;
                            bestValue = v;
                        }
                    }
                    System.out.println(bestMatch.name+" "+DEFAULTS.get(bestMatch));
                    defaultParameters.add(DEFAULTS.get(bestMatch));
                    
                } else {
                    defaultParameters.addAll(DEFAULT_PARAMETERS_FILES);
                }

                // create properties file
                File eaConfigFile = new File(trackDir.getAbsolutePath() + File.separator + track.controllerTrackName + ".properties");

                OutputStreamWriter eaConfigWriter = new OutputStreamWriter(new FileOutputStream(eaConfigFile), "UTF-8");

                eaConfigWriter.write("# name of experiment\n");
                eaConfigWriter.write("experiment=" + track.controllerTrackName + "\n");
                eaConfigWriter.write("\n");
                eaConfigWriter.write("Torcs.TRACKS = " + track.trackName + "\n");
                eaConfigWriter.write("Torcs.PARAM_FILES = ");
                for(int i=0; i < defaultParameters.size(); ++i){
                    eaConfigWriter.write(defaultParameters.get(i));
                    if(i > 0){
                        eaConfigWriter.write(",");
                    }
                }
                eaConfigWriter.write("\n");

                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(EA_TEMPLATE_FILE), "UTF-8"));

                String line = null;
                while ((line = reader.readLine()) != null) {
                    eaConfigWriter.write(line + "\n");
                }
                reader.close();
                eaConfigWriter.flush();
                eaConfigWriter.close();



                for (String filestring : defaultParameters) {
                    Path src = Paths.get(DEFAULT_PARAMETERS_DIRECTORY.getAbsolutePath() + File.separator + filestring);
                    Path dst = Paths.get(trackDir.getAbsolutePath() + File.separator + filestring);
                    Files.copy(src, dst, StandardCopyOption.REPLACE_EXISTING);
                }

                // write ea start script
                File eaScriptFile = new File(trackDir.getAbsolutePath() + File.separator + "r" + track.controllerTrackName);
                OutputStreamWriter eaScriptWriter = new OutputStreamWriter(new FileOutputStream(eaScriptFile), "UTF-8");
                eaScriptWriter.write("#!/bin/sh\n");
                eaScriptWriter.write("java -Xmx4G -cp MrRacer.jar de.janquadflieg.mrracer.evo.adapter.TorcsProblem paramFile=" + track.controllerTrackName + ".properties");
                eaScriptWriter.close();
            }

            // append entry to the analyse script
            if (analyse) {
                ExperimentSetup.appendAnalyseEntry(analyseScriptWriter, trackDir);
            }

            System.out.println(track.trackName + " done");
        }
        // nothing more to do
        if (analyse) {
            analyseScriptWriter.write("echo Disk spaced used:\n");
            analyseScriptWriter.write("du -sh\n");
            analyseScriptWriter.close();
        }
        if (setup) {
            // write slurm start script
            File slurmScriptFile = new File(BASE_PATH + File.separator + "queuejobs");
            OutputStreamWriter slurmScriptWriter = new OutputStreamWriter(new FileOutputStream(slurmScriptFile), "UTF-8");
            slurmScriptWriter.write("#!/bin/sh\n");
            for (String s : TRACKS) {
                TrackData track = trackList.getTrack(s);
                slurmScriptWriter.write("chmod 0777 " + track.controllerTrackName + "/r" + track.controllerTrackName + "\n");
                slurmScriptWriter.write("cd " + track.controllerTrackName + "\n");
                slurmScriptWriter.write("sbatch --output=ea" + track.controllerTrackName + ".log " + "r" + track.controllerTrackName + "\n");
                slurmScriptWriter.write("cd ..\n");
            }

            slurmScriptWriter.close();
        }
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Missing parameter file for the Experiment setup");
            System.out.println("A parameter file can contain the following settings:");
            System.out.println("DEFAULT_PARAMETERS_DIRECTORY = /somepath");
            System.out.println("TRACK_MODELS_DIRECTORY = /home/quad/Experimente/Data/trackmodels/mrracer2013");
            System.out.println("BASE_PATH = /scratch/quad/Experimente/2015-09-20");
            System.out.println("ANALYSE_SCRIPT = /home/quad/Experimente/Data/scripts/list_best_mo.r");
            System.out.println("DEFAULT_PARAMETERS_FILES = clutch-0.params, plan.params, recovery.params");
            System.exit(1);
        }


        Properties p = new Properties();

        try {
            p.load(new FileInputStream(args[0]));

        } catch (Exception e) {
            System.out.println("Failed to load properties from " + args[0]);
            e.printStackTrace(System.out);
            System.exit(1);
        }

        if (!p.containsKey(ExperimentSetup.BASE_PATH_KEY)) {
            System.out.println("Missing " + ExperimentSetup.BASE_PATH_KEY);
            System.exit(1);

        } else {
            System.out.println("Base directory: " + p.getProperty(ExperimentSetup.BASE_PATH_KEY));
        }
        
        if (!p.containsKey(ExperimentSetup.CONTROLLER_DIRECTORY_KEY)) {
            System.out.println("Missing " + ExperimentSetup.CONTROLLER_DIRECTORY_KEY);
            System.exit(1);

        } else {
            System.out.println("Controller directory: " + p.getProperty(ExperimentSetup.CONTROLLER_DIRECTORY_KEY));
        }

        if (!p.containsKey(ExperimentSetup.DEFAULT_PARAMETERS_DIRECTORY_KEY)) {
            System.out.println("Missing " + ExperimentSetup.DEFAULT_PARAMETERS_DIRECTORY_KEY);
            System.exit(1);

        } else {
            System.out.println("Directory for default parameters: " + p.getProperty(ExperimentSetup.DEFAULT_PARAMETERS_DIRECTORY_KEY));
        }

        if (!p.containsKey(ExperimentSetup.TRACK_MODELS_DIRECTORY_KEY)) {
            System.out.println("Missing " + ExperimentSetup.TRACK_MODELS_DIRECTORY_KEY);
            System.exit(1);

        } else {
            System.out.println("Directory for track models: " + p.getProperty(ExperimentSetup.TRACK_MODELS_DIRECTORY_KEY));
        }

        if (!p.containsKey(ExperimentSetup.EA_TEMPLATE_FILE_KEY)) {
            System.out.println("Missing " + ExperimentSetup.EA_TEMPLATE_FILE_KEY);
            System.exit(1);

        } else {
            System.out.println("EA properties template: " + p.getProperty(ExperimentSetup.EA_TEMPLATE_FILE_KEY));
        }

        try {
            ExperimentSetup setup = new ExperimentSetup(p);
            setup.run(true, true);

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
}
