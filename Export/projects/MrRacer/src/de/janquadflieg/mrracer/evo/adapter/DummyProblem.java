/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo.adapter;



import optimizer.ea.*;

/**
 * Interface betweeen Mike's ea lib and Torcs.
 *
 * @author quad
 */
public class DummyProblem
        extends Problem implements ParallelEvaluableProblem {

    /**
     * Dimensions.
     */
    private int dimensions = 5;
    private int objectiveFunctions = 4;

    /**
     * Creates a new instance of Torcs Problem.
     */
    public DummyProblem(Parameters par) {
    }

    @Override
    public String problemRunHeader() {
        StringBuilder result = new StringBuilder();

        result.append("Dummy problem to debug mo runs\n");

        return result.toString();
    }

    /**
     * A method called before the next round of parallel evaluations start. This
     * can be used to setup some metadata used to evaluate the n individuals
     * (determine port numbers for network connections of the individuals,
     * prepare different temp directories for the individuals, etc).
     *
     * @param n Number of individuals to be evaluated in parallel during the
     * next batch.
     */
    public void preParallelEvaluations(int n) {
        // nothing to do here
    }

    /**
     * Method called after the parallel evaluation of a number of individuals
     * has finished. Can be used to clean up the stuff generated during the call
     * to preParallelEvaluation().
     */
    public void postParallelEvaluations() {
        // nothing to do here
    }

    /**
     * Method called to start the evaluation of one individual.
     *
     * @param ind The individual to evaluate.
     * @param i The index of the individual, a number between 1 and n.
     * @return The results of the evaluation.
     */
    @Override
    public Evaluation parallelEvaluation(Individual ind, int i) {
        return evaluate(ind, i - 1);
    }

    @Override
    public Evaluation evaluate(Individual variables) {
        return evaluate(variables, 0);
    }

    public Evaluation evaluate(Individual variables, int offset) {
        System.out.println("-- " + variables.id + " / " + offset + " -----------------------------");


        variables.evaluated = true;

        Fitness[] results = new Fitness[objectiveFunctions()];
        results[0] = new Fitness(Math.random()*100, "Fit1", true, "Fit1");
        results[1] = new Fitness(5000+Math.random()*100, "Fit2", true, "Fit2");
        results[2] = new Fitness(100+Math.random()*100, "Fit3", true, "Fit3");
        results[3] = new Fitness(10000+Math.random()*100, "Fit4", true, "Fit4");
        
        
        /*        if (optimizeClutch) {
         results[0] = new Fitness(-fe.getResult().distance, "Distance", true, "Dist");
         } else {
         results[0] = new Fitness(fe.getOverallTime(), "Overalltime", true, "Time");
         }*/


        synchronized (this) {
            ++evals;
            ++validEvals;
        }

        return new Evaluation(results);
    }

    public Individual getTemplate(Parameters par) {
        Individual result = new Individual();

        VariableGroup genotype = new VariableGroup("genotype");

        genotype.add(new RealVariable("V1", 0, 1, true));
        genotype.add(new RealVariable("V2", 0, 1, true));
        genotype.add(new RealVariable("V3", 0, 1, true));
        genotype.add(new RealVariable("V4", 0, 1, true));
        genotype.add(new RealVariable("V5", 0, 1, true));        

        result.importGroup(genotype);

        // one mutation strength:
        RealMetaVariable mutationStrength = new RealMetaVariable("mutationStrength", result.variables,
                Operators.CMUTATION_STRENGTH, par.minSigma, par.maxSigma);
        if (par.initSigmas > 0) {
            mutationStrength.setInitGaussian(par.initSigmas, par.initSigmaRange);
        }
        result.importMetadata(mutationStrength);

        return result;
    }

    public int dimensions() {
        return dimensions;
    }

    /**
     * Number of objective functions defined in this problem
     */
    public int objectiveFunctions() {
        return objectiveFunctions;
    }

    @Override
    public void setProblemNo(int i) {
        
        //System.out.println("Run set to " + run);
    }

    public static void main(String[] args) {
        //String paramName = "F:\\Quad\\Experiments\\Torcs-Test\\torcs-config.properties";
        String paramName = null;
        if (args.length == 1) {
            paramName = args[ 0];
        }
        Parameters par = new Parameters(args, paramName);

        BlackBoxProblem prob = new DummyProblem(par);
        Individual ind = prob.getTemplate(par);
        BatchEA ea = new BatchEA(ind, par, prob);
        ea.run();
    }
}
