/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo.tools;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.plan.ParameterRecommender;
import de.janquadflieg.mrracer.plan.Plan2013;
import de.janquadflieg.mrracer.controller.MrRacer2013;
import de.janquadflieg.mrracer.evo.DriveTool2014Torcs134;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import de.janquadflieg.mrracer.torcs.TrackData;
import de.janquadflieg.mrracer.torcs.TrackDataList;
import de.janquadflieg.mrracer.track.TrackModel;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;

import scr.Controller.Stage;
import de.janquadflieg.mrracer.evo.EvaluationType;
import de.janquadflieg.mrracer.evo.tools.TestScenario;
import de.janquadflieg.mrracer.evo.tools.TestScenario;

/**
 * Tool to setup a comparison of different controller variants (usually
 * optimization results). This version is specifically designed to sample a
 * pareto front.
 *
 * @author quad
 */
public class DriveToolSetupMOFrontCheck {

    
    private final String OPTIMIZATION_BASE_PATH;
    private final String BASE_PATH;
    private final File CONTROLLER_DIRECTORY;
    private final File TOOLS_DIRECTORY;
    public static final String[] CONTROLLER_FILES = {"MrRacer.jar",
        "lib" + "/" + "commons-math3-3.2.jar",
        "lib" + "/" + "ealib.jar",
        "lib" + "/" + "flanagan.jar",
        "lib" + "/" + "Jama-1.0.2.jar",
        "lib" + "/" + "RaceclientLib.jar",
        "lib" + "/" + "vecmath.jar"
    };   
    public static final String[] TOOLS_FILES = {"SupportTools.jar"};
    
    private static final TrackDataList trackList = new TrackDataList();
    private Properties p;

    public DriveToolSetupMOFrontCheck(Properties p) {
        this.p = p;        
        OPTIMIZATION_BASE_PATH = p.getProperty("drivetoolsetup.OPTIMIZATION_BASE_PATH").trim();
        BASE_PATH = p.getProperty("drivetoolsetup.BASE_PATH").trim();
        CONTROLLER_DIRECTORY = new File(p.getProperty("drivetoolsetup.CONTROLLER_DIRECTORY").trim());
        TOOLS_DIRECTORY = new File(p.getProperty("drivetoolsetup.TOOLS_DIRECTORY").trim());
    }

    public static ArrayList<MOVariantData> listVariants(File dir)
            throws Exception {
        File[] directoryContents = dir.listFiles();
        ArrayList<MOVariantData> result = new ArrayList<>();

        for (File f : directoryContents) {
            if (f.isFile()) {
                String fileName = f.getName();
                //System.out.println(f.getName());
                String runAsString = "";
                for (int index = fileName.indexOf("run") + 3; Character.isDigit(fileName.charAt(index)); ++index) {
                    runAsString += fileName.charAt(index);
                }
                //System.out.println(runAsString);
                //System.out.println(Integer.parseInt(runAsString));

                String idAsString = "";
                for (int index = fileName.indexOf("id") + 2; Character.isDigit(fileName.charAt(index)); ++index) {
                    idAsString += fileName.charAt(index);
                }
                //System.out.println(idAsString);
                //System.out.println(Integer.parseInt(idAsString));

                MOVariantData d = new MOVariantData();
                d.bestEverFile = f;
                d.id = Integer.parseInt(idAsString);
                d.run = Integer.parseInt(runAsString);
                result.add(d);
            }
        }

        return result;
    }

    private static void appendToShellScript(File f, String s)
            throws Exception {
        OutputStreamWriter scriptWriter = new OutputStreamWriter(new FileOutputStream(f, true), "UTF-8");
        scriptWriter.write(s);
        scriptWriter.close();
    }

    private static void createShellScript(File f, String cmd)
            throws Exception {
        initShellScript(f);
        appendToShellScript(f, cmd);
    }

    private static String createBaseName(EvaluationType evalType,
            Stage stage, boolean noisy, String suffix) {
        String result = "";
        if (stage == Stage.WARMUP) {
            result += "w";

        } else {
            result += "q";
            if (evalType == EvaluationType.DISTANCE_IN_CONSTANT_TIME) {
                result += "d";

            } else {
                result += "t";
            }
        }

        result += suffix;

        result += (noisy ? "noisy" : "");
        return result;
    }

    private static String createPropertiesName(EvaluationType evalType,
            Stage stage, boolean noisy, String suffix) {
        return createBaseName(evalType, stage, noisy, suffix) + ".properties";
    }

    private static String createSlurmScriptName(EvaluationType evalType,
            Stage stage, boolean noisy, String suffix) {
        return "run" + createBaseName(evalType, stage, noisy, suffix);
    }

    private static void copyController(File srcDir, File targetDir)
            throws Exception {
        File libsDir = new File(targetDir.getAbsolutePath() + "/" + "lib");
        libsDir.mkdir();
        for (String filestring : CONTROLLER_FILES) {
            Path src = Paths.get(srcDir + "/" + filestring);
            Path dst = Paths.get(targetDir.getAbsolutePath() + "/" + filestring);
            Files.copy(src, dst, StandardCopyOption.REPLACE_EXISTING);
        }
    }
    
    private static void copyTools(File srcDir, File targetDir)
            throws Exception {      
        
        for (String filestring : TOOLS_FILES) {
            Path src = Paths.get(srcDir + "/" + filestring);
            Path dst = Paths.get(targetDir.getAbsolutePath() + "/" + filestring);
            Files.copy(src, dst, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    private int setupQualifing(MOVariantData d, File variantDir, TestScenario scenario, int port, ArrayList<String> tracks)
            throws Exception {

        int portsUsed = 0;
        int qualifyingPort = port;

        for (boolean noisy : new boolean[]{false, true}) {
            for (EvaluationType evalType : new EvaluationType[]{EvaluationType.DISTANCE_IN_CONSTANT_TIME, EvaluationType.TIME_FOR_LAPS}) {
                String[] suffixes;
                if (evalType == EvaluationType.DISTANCE_IN_CONSTANT_TIME) {
                    suffixes = new String[]{""};
                } else {
                    suffixes = new String[]{"2laps", "5laps"};
                }

                for (String suffix : suffixes) {
                    File propertiesFile = new File(variantDir.getAbsolutePath() + "/"
                            + createPropertiesName(evalType, Stage.QUALIFYING, noisy, suffix));

                    String expDirectory = variantDir.getParentFile().getAbsolutePath();

                    int laps = 0;
                    if (suffix.contains("2")) {
                        laps = 2;

                    } else if (suffix.contains("5")) {
                        laps = 5;
                    }

                    //System.out.println(expDirectory);

                    // File outFile, boolean noisy, Stage stage, int port,
                    // String controllerJVMOptions, ArrayList<String> tracks, String expDirectory,
                    // EvaluationType evalType, TestScenario scenario                    
                    writePropertiesFile(propertiesFile, noisy, Stage.QUALIFYING, qualifyingPort,
                            "", tracks, expDirectory, evalType, null, laps);
                    ++qualifyingPort;
                    ++portsUsed;

                    // create shell script                        
                    String scriptName = createSlurmScriptName(evalType, Stage.QUALIFYING, noisy, suffix);
                    File scriptFile = new File(variantDir.getAbsolutePath() + "/" + scriptName);
                    createShellScript(scriptFile, "java -Xmx2G -cp SupportTools.jar de.janquadflieg.mrracer.evo.DriveTool2014Torcs134 paramFile=" + propertiesFile.getName());

                    // extend queue jobs scripts
                    File queueJobsScript = new File(BASE_PATH + "/queue_qualifying_jobs");
                    OutputStreamWriter queueJobsWriter = new OutputStreamWriter(new FileOutputStream(queueJobsScript, true), "UTF-8");
                    queueJobsWriter.write("cd " + variantDir.getParentFile().getParentFile().getName() + "\n");
                    queueJobsWriter.write("cd " + scenario.getDirectoryName() + "\n");
                    queueJobsWriter.write("cd MrRacer \n");
                    String batchCmd = "sbatch";                    
                    batchCmd += " --nodelist=" + p.getProperty("drivetoolsetup.NODE_LIST", "snail11");                    
                    batchCmd += " --output=" + createBaseName(evalType, Stage.QUALIFYING, noisy, suffix) + ".log";
                    batchCmd += " " + scriptName;
                    queueJobsWriter.write(batchCmd + "\n");
                    queueJobsWriter.write("cd ..\n");
                    queueJobsWriter.write("cd ..\n");
                    queueJobsWriter.write("cd ..\n");
                    queueJobsWriter.close();
                    
                    // Alternative: directely enqueue batch job
                    // sbatch --output=bla --dependency=afterok:625653 --workdir=/scratch/quad...
                }
            }
        }
        
        return portsUsed;
    }

    private int setupVariantDirectory(MOVariantData d, TestScenario scenario, int port, ArrayList<String> tracks)
            throws Exception {
        //File variantDir = new File(BASE_PATH + "/" + scenario.getDirectoryName() + "/" + d.track.controllerTrackName + "/" + "MrRacer");
        String variantDirectoryName = "r" + d.run + "-id" + d.id;
        File variantDir = new File(BASE_PATH + "/" + variantDirectoryName + "/" + scenario.getDirectoryName() + "/" + "MrRacer");
        if (!variantDir.exists()) {
            if (!variantDir.mkdirs()) {
                System.out.println("Failed to create directory: " + variantDir.getAbsolutePath());
                System.exit(1);
            }
        }

        // copy the parameter file            
        Path srcParamFile = Paths.get(d.bestEverFile.getAbsolutePath());
        Path dstParamFile = Paths.get(variantDir.getAbsolutePath()
                + "/" + d.bestEverFile.getName());
        Files.copy(srcParamFile, dstParamFile, StandardCopyOption.REPLACE_EXISTING);

        // copy controller files & tools
        DriveToolSetupMOFrontCheck.copyController(CONTROLLER_DIRECTORY, variantDir);
        DriveToolSetupMOFrontCheck.copyTools(TOOLS_DIRECTORY, variantDir);

        int portsUsed = 0;

        switch (scenario) {
            case RACE_WEEKEND_2015:
            case RACE_WEEKEND: {
                System.out.println(scenario.name());

                // warmup                
                for (boolean noisy : new boolean[]{false, true}) {
                    int warmupPort = noisy ? port + 3 : port;

                    System.out.println("Warmup " + noisy + ", " + warmupPort);

                    String controllerJVMOptions = "-D" + ParameterRecommender.PARAM_FILES + "="
                            + dstParamFile.toString() + " -D"
                            + MrRacer2013.LOAD_FROM_CP + "=false";

                    File propertiesFile = new File(variantDir.getAbsolutePath() + "/"
                            + createPropertiesName(null, Stage.WARMUP, noisy, ""));

                    writePropertiesFile(propertiesFile, noisy, Stage.WARMUP, warmupPort,
                            controllerJVMOptions, tracks,
                            BASE_PATH + "/" + variantDirectoryName + "/" + scenario.getDirectoryName(),
                            null, scenario, 0);

                    // create job shell script                        
                    String scriptName = createSlurmScriptName(null, Stage.WARMUP,
                            noisy, "");
                    File scriptFile = new File(variantDir.getAbsolutePath() + "/" + scriptName);
                    createShellScript(scriptFile, "java -Xmx2G -cp SupportTools.jar de.janquadflieg.mrracer.evo.DriveTool2014Torcs134 paramFile=" + propertiesFile.getName());

                    // extend queue jobs scripts
                    File queueJobsScript = new File(BASE_PATH + "/queue_warmup_jobs");
                    OutputStreamWriter queueJobsWriter = new OutputStreamWriter(new FileOutputStream(queueJobsScript, true), "UTF-8");
                    queueJobsWriter.write("cd " + variantDirectoryName + "\n");
                    queueJobsWriter.write("cd " + scenario.getDirectoryName() + "\n");
                    queueJobsWriter.write("cd MrRacer \n");
                    queueJobsWriter.write("sbatch --nodelist="
                            + p.getProperty("drivetoolsetup.NODE_LIST", "snail11")
                            + " --output="
                            + createBaseName(null, Stage.WARMUP, noisy, "") + ".log"
                            + " " + scriptName + "\n");
                    queueJobsWriter.write("cd ..\n");
                    queueJobsWriter.write("cd ..\n");
                    queueJobsWriter.write("cd ..\n");
                    queueJobsWriter.close();
                }

                // qualifying                
                portsUsed += setupQualifing(d, variantDir, scenario, port, tracks);

                break;
            }
        }

        return portsUsed;
    }

    private void writePropertiesFile(File outFile, boolean noisy, Stage stage, int port,
            String controllerJVMOptions, ArrayList<String> tracks, String expDirectory,
            EvaluationType evalType, TestScenario scenario, int qualifyingLaps)
            throws Exception {
        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8");

        writer.write("ipport = 127.0.0.1:" + String.valueOf(port) + "\n");
        writer.write("controller = MRRACER\n");
        writer.write("controllerJVMOptions = " + controllerJVMOptions + "\n");
        writer.write("noisy = " + String.valueOf(noisy) + "\n");
        writer.write("stage = " + stage.toString() + "\n");
        writer.write("torcs.134.exppath=" + expDirectory + "\n");

        if (stage == Stage.QUALIFYING) {
            switch (evalType) {
                case TIME_FOR_LAPS: {
                    writer.write("EVALUATION_TYPE = TIME_FOR_LAPS\n");
                    writer.write("qualifyinglaps = " + qualifyingLaps + "\n");
                    break;
                }
                case DISTANCE_IN_CONSTANT_TIME: {
                    writer.write("EVALUATION_TYPE = DISTANCE_IN_CONSTANT_TIME\n");
                    writer.write("qualifyingticks = " + p.getProperty("drivetoolsetup.qualifyingticks", "10000") + "\n");
                    break;
                }
            }
        } else if (stage == Stage.WARMUP) {
            if (scenario == TestScenario.RACE_WEEKEND) {
                writer.write("EVALUATION_TYPE = DISTANCE_IN_CONSTANT_TIME\n");
                writer.write("warmupticks = " + p.getProperty("drivetoolsetup.warmupticks", "100000") + "\n");

            } else {
                writer.write("EVALUATION_TYPE = TIME_FOR_LAPS\n");
                writer.write("warmuplaps = 5\n");
            }
        }

        if (tracks != null) {
            writer.write("tracks = ");
            for (int i = 0; i < tracks.size(); ++i) {
                writer.write(tracks.get(i));
                if (i < tracks.size() - 1) {
                    writer.write(", ");
                }
            }
            writer.write("\n");
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(
                p.getProperty("drivetoolsetup.TEMPLATE")), "UTF-8"));

        String line = null;
        while ((line = reader.readLine()) != null) {
            writer.write(line + "\n");
        }
        reader.close();
        writer.flush();
        writer.close();
    }

    private static void initShellScript(File f)
            throws Exception {
        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(f), "UTF-8");
        writer.write("#!/bin/sh\n");
        writer.close();
    }

    public void run(ArrayList<TestScenario> scenarios)
            throws Exception {
        File optbaseDir = new File(OPTIMIZATION_BASE_PATH);
        File baseDir = new File(BASE_PATH);

        System.out.println("Setting up drive tool runs");
        System.out.println("Base path: " + BASE_PATH);
        System.out.println("Optimization base path: " + OPTIMIZATION_BASE_PATH);

        // check directories        
        if (!baseDir.exists() || !baseDir.isDirectory()) {
            System.out.println("Error with base directory " + BASE_PATH);
            System.exit(1);
        }
        if (!optbaseDir.exists() || !optbaseDir.isDirectory()) {
            System.out.println("Error with optimization base directory " + OPTIMIZATION_BASE_PATH);
            System.exit(1);
        }

        Properties templateProperties = new Properties();
        FileInputStream in = new FileInputStream(p.getProperty("drivetoolsetup.TEMPLATE"));
        templateProperties.load(in);
        in.close();

        // copy the controller to the base directory, so we can use it for the cleanup programs
        DriveToolSetupMOFrontCheck.copyController(CONTROLLER_DIRECTORY, baseDir);

        // gather information about optimization results
        ArrayList<MOVariantData> list = listVariants(optbaseDir);

        System.out.println("Found " + list.size() + " variants to test");
        if (list.isEmpty()) {
            System.exit(1);
        }


        ArrayList<String> tracks = null;

        if (!templateProperties.containsKey("tracks")) {
            System.out.println("Error: No tracks given in template");
            System.exit(1);
        }

        // prepare queue jobs scripts
        initShellScript(new File(BASE_PATH + "/queue_warmup_jobs"));
        initShellScript(new File(BASE_PATH + "/queue_qualifying_jobs"));

        int port = 3001;

        for (int i = 0; i < list.size(); ++i) {
            MOVariantData d = list.get(i);

            if (scenarios.contains(TestScenario.RACE_WEEKEND)) {
                port += setupVariantDirectory(d, TestScenario.RACE_WEEKEND, port + i, tracks);
            }

            if (scenarios.contains(TestScenario.RACE_WEEKEND_2015)) {
                port += setupVariantDirectory(d, TestScenario.RACE_WEEKEND_2015, port + i, tracks);
            }
        }
        System.out.println("Next free port: "+port);
    }

    public static void main(String[] args) {
        try {
            Properties p = new Properties();

            if (args.length > 0 && args[0].startsWith("paramFile")) {
                try {
                    String filename = "./" + args[0].substring(10, args[0].length());
                    FileInputStream in = new FileInputStream(filename);
                    p.load(in);
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else {
                System.out.println("Error: Missing param file");
                System.exit(1);
            }

            /*if (!p.containsKey("drivetoolsetup.NODE_LIST")) {
             System.out.println("Missing batch system node list in param file");
             System.exit(1);
             }*/

            ArrayList<TestScenario> scenarios = new ArrayList<>();
            StringTokenizer tokenizer = new StringTokenizer(p.getProperty("drivetoolsetup.SCENARIO", "RACE_WEEKEND"), ",");

            while (tokenizer.hasMoreTokens()) {
                scenarios.add(TestScenario.valueOf(tokenizer.nextToken().trim()));
            }

            if (scenarios.contains(TestScenario.REEVALUATE) && !p.containsKey("drivetoolsetup.TRACK_MODELS_DIRECTORY")) {
                System.out.println("Error: Scenarios contain REEVALUATE, but missing directory for track models");
                System.exit(1);
            }

            if (!p.containsKey("drivetoolsetup.OPTIMIZATION_BASE_PATH")) {
                System.out.println("Missing path for the optimization results");
                System.exit(1);
            }

            if (!p.containsKey("drivetoolsetup.BASE_PATH")) {
                System.out.println("Missing base directory path");
                System.exit(1);
            }

            if (!p.containsKey("drivetoolsetup.CONTROLLER_DIRECTORY")) {
                System.out.println("Missing controller directory path");
                System.exit(1);
            }

            if (!p.containsKey("drivetoolsetup.TEMPLATE")) {
                System.out.println("Missing template properties file");
                System.exit(1);
            }

            DriveToolSetupMOFrontCheck prog = new DriveToolSetupMOFrontCheck(p);

            prog.run(scenarios);

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    public static class MOVariantData {

        File bestEverFile;
        int run;
        int id;
        double fitness;
        TrackData track;
    }
}
