/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo.tools;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.plan.ParameterRecommender;
import de.janquadflieg.mrracer.plan.Plan2013;
import de.janquadflieg.mrracer.controller.MrRacer2013;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import de.janquadflieg.mrracer.torcs.TrackData;
import de.janquadflieg.mrracer.torcs.TrackDataList;
import de.janquadflieg.mrracer.track.TrackModel;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;

import scr.Controller.Stage;
import de.janquadflieg.mrracer.evo.EvaluationType;
import static de.janquadflieg.mrracer.evo.EvaluationType.DISTANCE_IN_CONSTANT_TIME;
import static de.janquadflieg.mrracer.evo.EvaluationType.TIME_FOR_LAPS;

/**
 * Tool to setup a comparison of different controller variants (usually
 * optimization results).
 *
 * @author quad
 */
public class DriveToolSetup {

    private final File TRACK_MODELS_DIRECTORY;
    private final String SAMPLES_DIRECTORY;
    private final String BASE_PATH;
    private final File CONTROLLER_DIRECTORY;
    private static final TrackDataList trackList = new TrackDataList();
    private Properties p;
    private ArrayList<TestScenario> scenarios = new ArrayList<>();

    //"drivetoolsetup.SCENARIO"
    //private 
    public DriveToolSetup(Properties p) {
        this.p = p;

        if (!p.containsKey("drivetoolsetup.TRACK_MODELS_DIRECTORY")) {
            System.out.println("Missing directory for track models");
            System.exit(1);
        }

        if (!p.containsKey("drivetoolsetup.SAMPLES_DIRECTORY")) {
            System.out.println("Missing path for the samples");
            System.exit(1);
        }

        if (!p.containsKey("drivetoolsetup.SAMPLES")) {
            System.out.println("Missing file which contains the samples to test");
            System.exit(1);
        }

        if (!p.containsKey("drivetoolsetup.BASE_PATH")) {
            System.out.println("Missing base directory path");
            System.exit(1);
        }

        if (!p.containsKey("drivetoolsetup.CONTROLLER_DIRECTORY")) {
            System.out.println("Missing controller directory path");
            System.exit(1);
        }

        if (!p.containsKey("drivetoolsetup.SCENARIO")) {
            System.out.println("Missing drivetoolsetup.SCENARIO");
            System.exit(1);
        }

        if (!p.containsKey("TEXT_DEBUG")) {
            System.out.println("Parameters do not contain\"TEXT_DEBUG\", setting TEXT_DEBUG=false");
            p.setProperty("TEXT_DEBUG", "false");
        }

        if (!p.containsKey("torcs.134.timeout")) {
            System.out.println("Parameters do not contain\"torcs.134.timeout\", setting torcs.134.timeout=1000000");
            p.setProperty("torcs.134.timeout", "1000000");
        }

        if (!p.containsKey("trackrepeats")) {
            System.out.println("Parameters do not contain\"trackrepeats\", setting trackrepeats=1");
            p.setProperty("trackrepeats", "1");
        }

        if (!p.containsKey("evalrepeats")) {
            System.out.println("Parameters do not contain\"evalrepeats\", setting evalrepeats=1");
            p.setProperty("evalrepeats", "1");
        }

        if (!p.containsKey("torcs.134.path")) {
            System.out.println("Parameters do not contain\"torcs.134.path\", setting torcs.134.path=/scratch/quad/torcs");
            p.setProperty("torcs.134.path", "/scratch/quad/torcs");
        }

        TRACK_MODELS_DIRECTORY = new File(p.getProperty("drivetoolsetup.TRACK_MODELS_DIRECTORY").trim());
        SAMPLES_DIRECTORY = p.getProperty("drivetoolsetup.SAMPLES_DIRECTORY").trim();
        BASE_PATH = p.getProperty("drivetoolsetup.BASE_PATH").trim();
        CONTROLLER_DIRECTORY = new File(p.getProperty("drivetoolsetup.CONTROLLER_DIRECTORY").trim());
        StringTokenizer tokenizer = new StringTokenizer(p.getProperty("drivetoolsetup.SCENARIO"), ",");
        while (tokenizer.hasMoreTokens()) {
            scenarios.add(TestScenario.valueOf(tokenizer.nextToken().trim()));
        }
    }

    public ArrayList<VariantData> parseSamplesFile(File list)
            throws Exception {
        // example line
        //aalborg;aalborg/best/run01-ind-id1469aalborg.params;1;1469;179.946;Aalborg
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(list), "UTF-8"));

        ArrayList<VariantData> result = new ArrayList<>();

        String line = reader.readLine();

        while ((line = reader.readLine()) != null) {
            VariantData v = new VariantData();
            StringTokenizer tokenizer = new StringTokenizer(line, ";");
            v.name = tokenizer.nextToken();
            v.bestEverFile = new File(SAMPLES_DIRECTORY + File.separator + tokenizer.nextToken());
            v.run = Integer.parseInt(tokenizer.nextToken());
            v.id = Integer.parseInt(tokenizer.nextToken());
            v.fitness = Double.parseDouble(tokenizer.nextToken());
            v.track = trackList.getTrack(tokenizer.nextToken());

            result.add(v);
        }

        reader.close();
        return result;
    }

    private static String getNodeForPort(int port) {
        if (Utils.even(port)) {
            return "snail11";
        } else {
            return "snail07";
        }
    }

    private void setupQualifing(VariantData d, File variantDir,
            TestScenario scenario, int port, ArrayList<TrackData> tracks,
            String controllerJVMOptions, boolean nodeByPort)
            throws Exception {
        for (EvaluationType evalType : EvaluationType.values()) {
            for (boolean noisy : new boolean[]{true, false}) {
                String[] suffixes = (evalType == EvaluationType.TIME_FOR_LAPS)
                        ? new String[]{"2laps", "5laps"}
                        : new String[]{"10000ticks", "75000ticks"};

                for (String suffix : suffixes) {
                    int stopValue = 0;

                    // create shell script                        
                    String scriptName = "re_";
                    scriptName += (noisy ? "n" : "");
                    scriptName += (evalType == EvaluationType.TIME_FOR_LAPS ? "t" : "d");
                    switch (suffix) {
                        case "2laps":
                            scriptName += "2L";
                            stopValue = 2;
                            break;
                        case "5laps":
                            scriptName += "5L";
                            stopValue = 5;
                            break;
                        case "10000ticks":
                            scriptName += "10";
                            stopValue = 10000;
                            break;
                        case "75000ticks":
                            scriptName += "75";
                            stopValue = 75000;
                            break;
                        default:
                            System.out.println("WARNING: Unhandled suffix: " + suffix);
                            stopValue = 2;
                            break;
                    }
                    scriptName += d.track.controllerTrackName.substring(0, Math.min(8, d.track.controllerTrackName.length()));
                    File scriptFile = new File(variantDir.getAbsolutePath() + "/" + scriptName);

                    File propertiesFile = new File(variantDir.getAbsolutePath() + "/"
                            + IOUtils.createToolScriptName(scenario, evalType, Stage.QUALIFYING, noisy, suffix) + ".properties");

                    IOUtils.createShellScript(scriptFile, "java -Xmx2G -cp .:MrRacer.jar de.janquadflieg.mrracer.evo.DriveTool2014Torcs134 paramFile=" + propertiesFile.getName());



                    writePropertiesFile(propertiesFile, noisy, Stage.QUALIFYING, port,
                            controllerJVMOptions, tracks,
                            BASE_PATH + "/" + scenario.getDirectoryName() + "/" + d.getName(),
                            evalType, stopValue);

                    // extend queue jobs scripts
                    File slurmScriptFile = new File(BASE_PATH + "/" + IOUtils.createSlurmScriptName("qj", scenario, evalType, Stage.QUALIFYING, noisy, suffix));
                    OutputStreamWriter slurmScriptWriter = new OutputStreamWriter(new FileOutputStream(slurmScriptFile, true), "UTF-8");
                    slurmScriptWriter.write("cd " + scenario.getDirectoryName() + "\n");
                    slurmScriptWriter.write("cd " + d.getName() + "\n");
                    slurmScriptWriter.write("cd MrRacer \n");
                    String batchCmd = "sbatch";

                    if (p.containsKey("drivetoolsetup.NODE_LIST")) {
                        batchCmd += " --nodelist=" + p.getProperty("drivetoolsetup.NODE_LIST");

                    } else {
                        if (nodeByPort) {
                            batchCmd += " --nodelist=" + getNodeForPort(port);
                        }
                    }

                    batchCmd += " --output=" + IOUtils.createToolScriptName(scenario, evalType, Stage.QUALIFYING, noisy, suffix) + ".log";
                    batchCmd += " " + scriptName;

                    slurmScriptWriter.write(batchCmd + "\n");
                    slurmScriptWriter.write("cd ..\n");
                    slurmScriptWriter.write("cd ..\n");
                    slurmScriptWriter.write("cd ..\n");
                    slurmScriptWriter.close();
                }
            }
        }
    }

    private void setupVariantDirectory(VariantData d, TestScenario scenario, int port, ArrayList<TrackData> tracks)
            throws Exception {
        File variantDir = new File(BASE_PATH + "/" + scenario.getDirectoryName() + "/" + d.getName() + "/" + "MrRacer");
        if (!variantDir.mkdirs()) {
            System.out.println("Failed to create directory: " + variantDir.getAbsolutePath());
            System.exit(1);
        }

        // copy the parameter file            
        Path srcParamFile = Paths.get(d.bestEverFile.getAbsolutePath());

        Path dstParamFile = Paths.get(variantDir.getAbsolutePath()
                + "/" + "opton_" + d.track.controllerTrackName + ".params");
        Files.copy(srcParamFile, dstParamFile, StandardCopyOption.REPLACE_EXISTING);

        // copy controller files
        IOUtils.copyController(CONTROLLER_DIRECTORY, variantDir, ExperimentSetup.CONTROLLER_FILES);

        switch (scenario) {
            case REEVALUATE: {
                String controllerJVMOptions = "-D" + MrRacer2013.ENFORCE_PARAMETERS + "="
                        + dstParamFile.toString().replace('\\', '/') + " -D"
                        + MrRacer2013.LOAD_FROM_CP + "=false -D" + Plan2013.ALWAYS_USE_GLF_FOR_TARGET_SPEEDS;

                // copy track models
                ArrayList<TrackData> tracksNeeded = tracks;

                for (int noise = 1; noise <= 2; ++noise) {
                    boolean noisy = noise % 2 == 0;
                    for (TrackData trackData : tracksNeeded) {
                        Path srcTrackModel = Paths.get(TRACK_MODELS_DIRECTORY.getAbsolutePath()
                                + "/" + trackData.controllerTrackName
                                + (noisy ? "-noisy" : "")
                                + "-1" + TrackModel.TM_EXT);
                        Path dstTrackModel = Paths.get(variantDir.getAbsolutePath()
                                + "/" + trackData.controllerTrackName
                                + (noisy ? "-noisy" : "")
                                + "-1" + TrackModel.TM_EXT);
                        Files.copy(srcTrackModel, dstTrackModel, StandardCopyOption.REPLACE_EXISTING);
                    }
                }

                // do the hard work
                setupQualifing(d, variantDir, scenario, port, tracks, controllerJVMOptions, false);

                break;
            }
            case RACE_WEEKEND_2015:
            case RACE_WEEKEND: {
                // warmup
                boolean nodeByPort = true;
                for (boolean noisy : new boolean[]{false, true}) {
                    String controllerJVMOptions = "-D" + ParameterRecommender.PARAM_FILES + "="
                            + dstParamFile.toString() + " -D"
                            + MrRacer2013.LOAD_FROM_CP + "=false";

                    String suffix = scenario == TestScenario.RACE_WEEKEND ? "100K" : "5L";
                    int stopValue = scenario == TestScenario.RACE_WEEKEND ? 100000 : 5;
                    EvaluationType evalType = scenario == TestScenario.RACE_WEEKEND ? EvaluationType.DISTANCE_IN_CONSTANT_TIME : EvaluationType.TIME_FOR_LAPS;

                    File propertiesFile = new File(variantDir.getAbsolutePath() + "/"
                            + IOUtils.createToolScriptName(scenario, null, Stage.WARMUP, noisy, suffix) + ".properties");

                    writePropertiesFile(propertiesFile, noisy, Stage.WARMUP, port,
                            controllerJVMOptions, tracks,
                            BASE_PATH + "/" + scenario.getDirectoryName() + "/" + d.getName(),
                            evalType, stopValue);

                    // create shell script                        
                    String scriptName = "rw_w";
                    scriptName += (noisy ? "n" : "");
                    scriptName += d.track.controllerTrackName.substring(0, Math.min(8, d.track.controllerTrackName.length()));
                    File scriptFile = new File(variantDir.getAbsolutePath() + "/" + scriptName);
                    IOUtils.createShellScript(scriptFile, "java -Xmx2G -cp .:MrRacer.jar de.janquadflieg.mrracer.evo.DriveTool2014Torcs134 paramFile=" + propertiesFile.getName());                   
                    

                    // extend queue jobs scripts
                    File slurmScriptFile = new File(BASE_PATH + "/" + IOUtils.createSlurmScriptName("qj", scenario, null, Stage.WARMUP, noisy, suffix));
                    OutputStreamWriter slurmScriptWriter = new OutputStreamWriter(new FileOutputStream(slurmScriptFile, true), "UTF-8");
                    slurmScriptWriter.write("cd " + scenario.getDirectoryName() + "\n");
                    slurmScriptWriter.write("cd " + d.getName() + "\n");
                    slurmScriptWriter.write("cd MrRacer \n");                    
                    String batchCmd = "sbatch";
                    if (p.containsKey("drivetoolsetup.NODE_LIST")) {
                        batchCmd += " --nodelist=" + p.getProperty("drivetoolsetup.NODE_LIST");

                    } else {
                        if (nodeByPort) {
                            batchCmd += " --nodelist=" + getNodeForPort(port);
                        }
                    }
                    batchCmd += " --output=" + IOUtils.createToolScriptName(scenario, evalType, Stage.WARMUP, noisy, suffix) + ".log";
                    batchCmd += " " + scriptName;
                    slurmScriptWriter.write(batchCmd+"\n");                    
                    slurmScriptWriter.write("cd ..\n");
                    slurmScriptWriter.write("cd ..\n");
                    slurmScriptWriter.write("cd ..\n");
                    slurmScriptWriter.close();
                }

                // qualifying                
                setupQualifing(d, variantDir, scenario, port, tracks, "", nodeByPort);

                break;
            }
        }
    }

    private void writePropertiesFile(File outFile, boolean noisy, Stage stage, int port,
            String controllerJVMOptions, ArrayList<TrackData> tracks, String expDirectory,
            EvaluationType evalType, int stopValue)
            throws Exception {
        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8");

        writer.write("ipport = 127.0.0.1:" + String.valueOf(port) + "\n");
        writer.write("controller = MRRACER\n");
        writer.write("controllerJVMOptions = " + controllerJVMOptions + "\n");
        writer.write("noisy = " + String.valueOf(noisy) + "\n");
        writer.write("stage = " + stage.toString() + "\n");
        writer.write("torcs.134.exppath=" + expDirectory + "\n");

        writer.write("TEXT_DEBUG=" + p.getProperty("TEXT_DEBUG") + "\n");
        writer.write("torcs.134.timeout=" + p.getProperty("torcs.134.timeout") + "\n");
        writer.write("trackrepeats=" + p.getProperty("trackrepeats") + "\n");
        writer.write("evalrepeats=" + p.getProperty("evalrepeats") + "\n");
        writer.write("torcs.134.path=" + p.getProperty("torcs.134.path") + "\n");

        if (stage == Stage.QUALIFYING) {
            switch (evalType) {
                case TIME_FOR_LAPS: {
                    writer.write("EVALUATION_TYPE = TIME_FOR_LAPS\n");
                    writer.write("qualifyinglaps = " + String.valueOf(stopValue) + "\n");
                    break;
                }
                case DISTANCE_IN_CONSTANT_TIME: {
                    writer.write("EVALUATION_TYPE = DISTANCE_IN_CONSTANT_TIME\n");
                    writer.write("qualifyingticks = " + String.valueOf(stopValue) + "\n");
                    break;
                }
            }
        } else if (stage == Stage.WARMUP) {
            switch (evalType) {
                case TIME_FOR_LAPS: {
                    writer.write("EVALUATION_TYPE = TIME_FOR_LAPS\n");
                    writer.write("warmuplaps = " + String.valueOf(stopValue) + "\n");
                    break;
                }
                case DISTANCE_IN_CONSTANT_TIME: {
                    writer.write("EVALUATION_TYPE = DISTANCE_IN_CONSTANT_TIME\n");
                    writer.write("warmupticks = " + String.valueOf(stopValue) + "\n");
                    break;
                }
            }
        }

        writer.write("tracks = ");
        for (int i = 0; i < tracks.size(); ++i) {
            if (i > 0) {
                writer.write(", ");
            }
            writer.write(tracks.get(i).trackName);
        }
        writer.write("\n");

        writer.flush();
        writer.close();
    }

    public void run()
            throws Exception {
        File samplesDir = new File(SAMPLES_DIRECTORY);
        File baseDir = new File(BASE_PATH);

        System.out.println("*********** DriveToolSetup **********************");

        System.out.println("Setting up drive tool runs");
        System.out.println("Base path: " + BASE_PATH);
        System.out.println("Samples Directory: " + SAMPLES_DIRECTORY);
        System.out.print("Scenarios: ");
        for (int i = 0; i < scenarios.size(); ++i) {
            if (i > 0) {
                System.out.print(", ");
            }
            System.out.print(scenarios.get(i));
        }
        System.out.println("");

        // check directories        
        if (!baseDir.exists() || !baseDir.isDirectory()) {
            System.out.println("Error with base directory " + BASE_PATH);
            System.exit(1);
        }
        if (!samplesDir.exists() || !samplesDir.isDirectory()) {
            System.out.println("Error with optimization base directory " + SAMPLES_DIRECTORY);
            System.exit(1);
        }

        // copy the controller to the base directory, so we can use it for the cleanup programs
        IOUtils.copyController(CONTROLLER_DIRECTORY, baseDir, ExperimentSetup.CONTROLLER_FILES);

        ArrayList<VariantData> list = parseSamplesFile(new File(p.getProperty("drivetoolsetup.SAMPLES")));

        java.util.Collections.sort(list, new java.util.Comparator<VariantData>() {
            @Override
            public int compare(VariantData a1, VariantData a2) {
                return a1.track.trackName.compareToIgnoreCase(a2.track.trackName);
            }
        });

        System.out.println("Found " + list.size() + " variants to test");

        ArrayList<TrackData> tracks = new ArrayList<>();

        if (!p.containsKey("tracks")) {
            System.out.println("No tracks given in parameter file, creating list from samples");
            tracks = new ArrayList<>();
            for (VariantData d : list) {
                tracks.add(d.track);
            }

        } else {
            boolean ok = IOUtils.parseTrackList(p.getProperty("tracks"), true, tracks);
            if (!ok) {
                System.exit(1);
            }
        }

        System.out.println("Found " + tracks.size() + " tracks");

        for (TestScenario scenario : scenarios) {
            System.out.println("Setting up environment for " + scenario);
            // prepare queue jobs scripts
            for (boolean noisy : new boolean[]{true, false}) {

                if (scenario == TestScenario.RACE_WEEKEND
                        || scenario == TestScenario.RACE_WEEKEND_2015) {
                    String suffix = scenario == TestScenario.RACE_WEEKEND ? "100K" : "5L";
                    IOUtils.initShellScript(new File(BASE_PATH + "/" + IOUtils.createSlurmScriptName("qj", scenario, null, Stage.WARMUP, noisy, suffix)));
                }

                for (EvaluationType evalType : EvaluationType.values()) {
                    String[] suffixes = (evalType == EvaluationType.TIME_FOR_LAPS)
                            ? new String[]{"2laps", "5laps"}
                            : new String[]{"10000ticks", "75000ticks"};
                    for (String suffix : suffixes) {
                        IOUtils.initShellScript(
                                new File(BASE_PATH + "/"
                                + IOUtils.createSlurmScriptName("qj",
                                scenario, evalType,
                                Stage.QUALIFYING, noisy, suffix)));
                    }
                }
            }
            // setup variants
            for (int i = 0; i < list.size(); ++i) {
                VariantData d = list.get(i);
                setupVariantDirectory(d, scenario, 3001 + i, tracks);
            }
        }
    }

    public static void main(String[] args) {
        try {
            Properties p = new Properties();

            if (args.length > 0 && args[0].startsWith("paramFile")) {
                try {
                    String filename = args[0].substring(10, args[0].length());
                    if (!(new File(filename).isAbsolute())) {
                        filename = "./" + filename;
                    }
                    FileInputStream in = new FileInputStream(filename);
                    p.load(in);
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else {
                System.out.println("Missing param file");
                System.exit(1);
            }

            DriveToolSetup prog = new DriveToolSetup(p);

            prog.run();

        } catch (Exception e) {
            e.printStackTrace(System.out);


        }
    }

    public static class VariantData {

        File bestEverFile;
        int run;
        int id;
        double fitness;
        TrackData track;
        String name;

        public String getName() {
            return (name != null ? name : track.controllerTrackName);
        }
    }
}
