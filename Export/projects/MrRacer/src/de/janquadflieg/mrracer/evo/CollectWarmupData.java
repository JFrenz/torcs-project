/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.torcs.TrackDataList;
import de.janquadflieg.mrracer.torcs.TrackData;
import de.janquadflieg.mrracer.controller.BaseController;
import java.io.*;
import java.util.*;

/**
 *
 * @author quad
 */
public class CollectWarmupData {

    public static void run()
            throws Exception {
        //String path = "Z:\\Experimente\\2014-12-19-Evaluation\\Repeat15V";
        String path = "Z:\\Experimente\\2014-12-19-Evaluation\\AllTracksV";
        String propertiesFile = "Z:\\Experimente\\2014-12-19-Evaluation\\Environment\\drivetoolallV1.properties";
        String[] PARAMS = {"Comp", "Buka", "Wheel2", "Dirt-3"};
        int[][] VARIANTEN = {{0}, {1}, {2}, {3}, {1, 2}, {2, 3}, {1, 3}, {1, 2, 3},
            {0, 1}, {0, 2}, {0, 3}, {0, 1, 2}, {0, 2, 3}, {0, 1, 3}, {0, 1, 2, 3}
        };


        Properties p = new Properties();
        FileInputStream in = new FileInputStream(propertiesFile);
        p.load(in);
        in.close();

        ArrayList<String> tracks = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer(p.getProperty("tracks"), ",");

        while (tokenizer.hasMoreTokens()) {
            tracks.add(tokenizer.nextToken().trim());
        }

        TrackDataList tracklist = new TrackDataList();
        ArrayList<ArrayList<String>> result = new ArrayList<>();

        for (int track = 0; track < tracks.size(); ++track) {
            ArrayList<String> chosenSets = new ArrayList<>();
            for (int variante = 1; variante <= 15; ++variante) {
                String logFile = path + variante + File.separator + "MrRacer" + File.separator
                        + tracklist.getTrack(tracks.get(track)).controllerTrackName
                        + "-1_warmup_process.log";
                System.out.println(logFile + " exists? " + new File(logFile).exists());

                BufferedReader reader = new BufferedReader(new FileReader(logFile));

                String line = reader.readLine();

                // Find information about parameter sets
                while (line != null && !line.startsWith("Done testing all parameter sets:")) {
                    line = reader.readLine();
                }
                while (line != null && !line.startsWith("Best:")) {
                    line = reader.readLine();
                }

                int set = -1;

                if (line != null) {
                    set = Integer.parseInt(line.substring(6).substring(0, 1)) - 1;

                } else {
                }

                String setDesc = "";
                if (set == -1) {
                    setDesc += "def-";
                    set = 0;
                }
                setDesc += PARAMS[VARIANTEN[variante - 1][set]];
                System.out.println(setDesc);
                chosenSets.add(setDesc);
                reader.close();
                System.out.println("------------------------");
                //bukavumountainsnow-1_warmup_process
            }
            result.add(chosenSets);
        }

        // Header
        System.out.print("Track;");
        for (int variante = 1; variante <= 15; ++variante) {
            System.out.print("V" + variante + "(");
            for(int i=0; i < VARIANTEN[variante-1].length; ++i){
                System.out.print(PARAMS[VARIANTEN[variante-1][i]]);
                if(i != VARIANTEN[variante-1].length-1){
                    System.out.print(",");
                }
            }
            System.out.print(");");
        }
        System.out.println("");
        
        for (int track = 0; track < tracks.size(); ++track) {
            System.out.print(tracks.get(track)+";");
            
            ArrayList<String> chosen = result.get(track);
            
            for(int i=0; i < chosen.size(); ++i){
                System.out.print(chosen.get(i)+";");
            }
            
            System.out.println("");
        }
    }

    public static void main(String[] args) {
        try {
            CollectWarmupData.run();

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
}
