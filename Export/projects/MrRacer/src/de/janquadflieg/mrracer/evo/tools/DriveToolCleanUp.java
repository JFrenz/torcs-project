/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo.tools;

import de.janquadflieg.mrracer.evo.tools.TestScenario;
import java.io.*;

/**
 *
 * @author quad
 */
public class DriveToolCleanUp {

    private static void deleteRecursive(File path) {
        File[] c = path.listFiles();
        //System.out.println("Cleaning out folder:" + path.toString());
        for (File file : c) {
            if (file.isDirectory()) {
                //System.out.println("Deleting file:" + file.toString());
                deleteRecursive(file);
                file.delete();
            } else {
                file.delete();
            }
        }

        path.delete();
    }

    private static void deleteController(File srcDir)
            throws Exception {
        for (String filestring : ExperimentSetup.CONTROLLER_FILES) {
            File f = new File(srcDir.getAbsolutePath() + "/" + filestring);
            f.delete();
        }
        File libsDir = new File(srcDir.getAbsolutePath() + "/" + "lib");
        libsDir.delete();
    }

    public static void run(String s, boolean deleteMetaFiles)
            throws Exception {
        TestScenario ts = TestScenario.valueOf(s);
        System.out.println("Scenario: "+ts+", delete meta files: "+deleteMetaFiles);

        File scenarioDirectory = new File("./" + ts.getDirectoryName());

        File[] variants = scenarioDirectory.listFiles();

        for (File variant : variants) {
            if (!variant.isDirectory()) {
                continue;
            }

            File subDir = new File(variant.getAbsolutePath() + "/" + "MrRacer");
            System.out.print("Checking directory \"" + variant.getName() + "/" + subDir.getName() + "\"...");

            boolean used = false;
            File[] dirContents = subDir.listFiles();
            for (int i = 0; i < dirContents.length; ++i) {
                if (dirContents[i].isFile() && dirContents[i].getName().endsWith(".cvs")) {
                    String oldName = dirContents[i].getAbsolutePath();
                    File newName = new File(oldName.substring(0, oldName.length() - 4) + ".csv");
                    boolean b = dirContents[i].renameTo(newName);
                    /*if(b){
                     System.out.println("renamed");
                     } else {
                     System.out.println("failed");
                     }*/
                }
            }
            dirContents = subDir.listFiles();
            for (int i = 0; i < dirContents.length && !used; ++i) {
                used = dirContents[i].isFile() && dirContents[i].getName().endsWith(".csv");
                if (used) {
                    System.out.print(" found csv results file: " + dirContents[i].getName());
                }
            }
            if (used) {
                System.out.println(" ok");

                if (deleteMetaFiles) {
                    if (ts == TestScenario.REEVALUATE) {
                        System.out.print("\tDeleting track models...");
                        dirContents = subDir.listFiles();

                        for (int i = 0; i < dirContents.length; ++i) {
                            if (dirContents[i].isFile() && dirContents[i].getName().endsWith(".saved_model")) {
                                boolean success = dirContents[i].delete();
                                if (!success) {
                                    System.out.print(" WARNING failed \"" + dirContents[i].getName() + "\"");
                                }
                            }
                        }
                        System.out.println(" done");
                    }

                    System.out.print("\tDeleting shell scripts...");
                    dirContents = subDir.listFiles();

                    for (int i = 0; i < dirContents.length; ++i) {
                        if (dirContents[i].isFile()
                                && (dirContents[i].getName().startsWith("re_d")
                                || dirContents[i].getName().startsWith("re_nd")
                                || dirContents[i].getName().startsWith("re_t")
                                || dirContents[i].getName().startsWith("re_nt"))) {
                            boolean success = dirContents[i].delete();
                            if (!success) {
                                System.out.print(" WARNING failed \"" + dirContents[i].getName() + "\"");
                            }
                        }
                    }
                    System.out.println(" done");

                    System.out.print("\tDeleting controller files");
                    deleteController(subDir);
                    System.out.println(" done");

                }

            } else {
                System.out.println(" not used, deleting whole directory");
                DriveToolCleanUp.deleteRecursive(variant);
            }
        }
    }

    public static void main(String[] args) {
        try {
            if (args.length == 0) {
                System.out.println("DriveToolCleanUp usage:");
                System.out.println("DriveToolCleanUp TEST_SCENARIO [DELETE_META_FILES]");
                System.out.println("where TEST_SCENARIO can be one of: ");
                TestScenario[] scenarios = TestScenario.values();
                for (int i = 0; i < scenarios.length; ++i) {
                    if (i > 0) {
                        System.out.print("|");
                    }
                    System.out.print(scenarios[i].name());
                }
                System.out.println("");

                System.out.println("If you add DELETE_META_FILES to the call, metafiles will be deleted.");
                System.out.println("Otherwise only empty (unused) directories will be deleted.");

                System.exit(1);
            }
            boolean deleteMeta = false;
            if (args.length > 1 && args[1].equals("DELETE_META_FILES")) {
                deleteMeta = true;
            }
            DriveToolCleanUp.run(args[0], deleteMeta);

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

    }
}
