/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.opponents;

import static de.janquadflieg.mrracer.data.CarConstants.*;
import de.janquadflieg.mrracer.track.TrackSegment;


import java.io.Serializable;
import java.util.*;

/**
 *
 * @author Verlage
 *
 * New in 2013: Saves distance on neighbour sensors, too First tests with
 * sensors +-1
 *
 */
public class Opponent2013S implements Serializable {

    private int id = 0;
    private boolean active = false;
    /**
     * Do i need to block this opponent?
     */
    private boolean needToBlock = false;
    /**
     * Ahead of me?
     */
    private boolean ahead = false;
    /**
     * This car is responsable for behaviour
     */
    private boolean responsable = false;
    /**
     * Angle of the sensor belonging to this Opponent in radiants.
     */
    private double angleR = 0.0;
    /**
     * criticle Oppo?
     */
    private boolean critical = false;
    
    /**
     * Number of positions used to calculate the relative speed vector.
     */
    private static final int NUM_POSITIONS = 4;
    /**
     * Time in seconds between the positions.
     */
    private static final double DELTA_TIME = 0.02;
    /**
     * Speed difference vector in m/s.
     */
   
    private double timeToCrash = 0.0;
    /**
     * Time in seconds until we reach the minimum save distance, thats the
     * important information!
     */
    private double timeToMinDistance = 0.0;
    /**
     * Rectangle of this car.
     */
    
    private int nummer = 0;
       //Time on which Opponent was last seen
    private int lastseen;
    /**
     * Saves last 10 distances and its angle X = Distance Y = sensorid
     */
   
    
    private double x = 0.0;
    private double y = 0.0;
    private double VdifX = 0.0;
    private double VdifY = 0.0;
    /*
     * Distance to Trackend or next Oppo
     * if distance is distance to trackend then oppo=null
     * else oppo= opponent, which has that distance
     */
    private double leftGap = 0.0;
    private double rightGap = 0.0;
    private Opponent2013S leftOppo;
    private Opponent2013S rightOppo;


    public Opponent2013S(int id, int nummer, int lastseen, double x, double y, double VdifX, double VdifY,boolean active, boolean crit, boolean ahead, double leftgap, double rightgap) {
        this.id = id;
        this.angleR = Math.toRadians(id * 10.0);
        this.nummer = nummer;
        this.lastseen = lastseen;
        this.x = x;
        this.y = y;
        this.VdifX = VdifX;
        this.VdifY = VdifY;
        this.active = active;
        this.critical = crit;
        this.ahead = ahead;
        this.leftGap = leftgap;
        this.rightGap = rightgap;

        //System.out.println("Neuer Gegner "+nummer+"\n ID: "+id+" ;");
    }

    public double getLeftGap() {
        return leftGap;
    }

    public void setLeftGap(double leftgap) {
        this.leftGap = leftgap;
    }

    public double getRightGap() {
        return rightGap;
    }

    public void setRightGap(double rightgap) {
        this.rightGap = rightgap;
    }

    public int getLastSeen() {
        return lastseen;
    }

    public Opponent2013S getLeftOppo() {
        return leftOppo;
    }

    public void setLeftOppo(Opponent2013S leftOppo) {
        this.leftOppo = leftOppo;
    }

    public Opponent2013S getRightOppo() {
        return rightOppo;
    }

    public void setRightOppo(Opponent2013S rightOppo) {
        this.rightOppo = rightOppo;
    }

    public void resetLeftGap() {
        leftOppo = null;
    }

    public void resetRightGap() {
        rightOppo = null;
    }

    public boolean isResponsable() {
        return responsable;
    }

    public void setResponsable(boolean responsable) {
        this.responsable = responsable;
    }

    /*
     * returns last angle Id on which car was seen
     */
    public int getID() {
        return id;
    }

    public double getXPosition() {
        return x;
    }
    
    public double getYPosition() {
        return y;
    }

  

    public double getVdifX() {
        return VdifX;
    }
    
    public double getVdifY() {
        return VdifY;
    }

    public double getTimeToCrash() {
        return timeToCrash;
    }

    public double getTimeToMinDistance() {
        return timeToMinDistance;
    }

    public boolean isActive() {
        return active;
    }

    public boolean isAhead() {
        return ahead;
    }

    public boolean shouldBeBlocked() {
        return needToBlock;
    }

    public void reset(double d, int sensorid, int lastseen) {
        active = false;
        needToBlock = false;
        ahead = false;
        x = 0.0;
        y = 0.0;
        VdifX = 0.0;
        VdifY = 0.0;                
        timeToCrash = 0.0;
        timeToMinDistance = 0.0;
        
        lastseen = 0;
        

    }

    

    public int getNumber() {
        return nummer;
    }

    public boolean isCritical() {
        return critical;
    }

    public void setCritical(boolean crit) {
        this.critical = crit;
    }
}
