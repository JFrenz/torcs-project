/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MathX;

import de.delbrueg.math.MathUtil;
import Jama.Matrix;
import de.delbrueg.math.Circle.RotationDirection;
import java.security.InvalidParameterException;
import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tim
 */
public class MathUtilTest {

    public MathUtilTest() {
    }

    /**
     * Test of normalize method, of class MathUtil.
     */
    @Test
    public void testNormalize() {
        System.out.println("normalize");
        {
            Point2d p = new Point2d(0,3000);
            Point2d expResult = new Point2d(0,1);
            Point2d result = MathUtil.normalize(p);
            assertTrue("normalisation is wrong!!!",expResult.epsilonEquals(result,0.000001));
        }
        {
            Point2d p = new Point2d(3330,0);
            Point2d expResult = new Point2d(1,0);
            Point2d result = MathUtil.normalize(p);
            assertTrue("normalisation is wrong!!!",expResult.epsilonEquals(result,0.000001));
        }
    }

    /**
     * Test of getOrthogonalDirection method, of class MathUtil.
     */
    @Test
    public void testGetOrthogonalDirection() {
        System.out.println("getOrthogonalDirection");

        Vector2d dir = new Vector2d(1,1);
        Vector2d orth = MathUtil.getOrthogonalDirection(dir);
        double result = orth.dot(dir);

        assertEquals("scalar product of orthogonal vectors not zero !!!", 0, result, 0.000001);
    }

    /**
     * Test of rotatePoint method, of class MathUtil.
     */
    @Test
    public void testRotatePoint() {
            System.out.println("rotatePoint");
        {
            System.out.println(" - PI clockwise");
            Point2d point = new Point2d(1,1);
            Point2d center = new Point2d(2,1);
            RotationDirection dir = RotationDirection.clockwise;
            double angle = Math.PI;
            Point2d expResult = new Point2d(3,1);

            Point2d result = MathUtil.rotatePoint(point, center, dir, angle);

            assertTrue(expResult.epsilonEquals(result, 0.00000000000001));
        }

        {
            System.out.println(" - PI counter-clockwise");
            Point2d point = new Point2d(1,1);
            Point2d center = new Point2d(2,1);
            RotationDirection dir = RotationDirection.counter_clockwise;
            double angle = Math.PI;
            Point2d expResult = new Point2d(3,1);

            Point2d result = MathUtil.rotatePoint(point, center, dir, angle);

            assertTrue(expResult.epsilonEquals(result, 0.00000000000001));
        }

        {
            System.out.println(" - 1/2*PI counter-clockwise");
            Point2d point = new Point2d(-1,-1);
            Point2d center = new Point2d(0,0);
            RotationDirection dir = RotationDirection.counter_clockwise;
            double angle = Math.PI/2;
            Point2d expResult = new Point2d(1,-1);

            Point2d result = MathUtil.rotatePoint(point, center, dir, angle);

            assertTrue(expResult.epsilonEquals(result, 0.00000000000001));
        }

        {
            System.out.println(" - 1/2*PI clockwise");
            Point2d point = new Point2d(-1,-1);
            Point2d center = new Point2d(0,0);
            RotationDirection dir = RotationDirection.clockwise;
            double angle = Math.PI/2;
            Point2d expResult = new Point2d(-1,1);

            Point2d result = MathUtil.rotatePoint(point, center, dir, angle);

            assertTrue(expResult.epsilonEquals(result, 0.00000000000001));
        }
    }

    /**
     * Test of rotatePoint method, of class MathUtil.
     */
    @Test
    public void testRotateVector() {
            System.out.println("rotateVector");
        {
            System.out.println(" - PI clockwise");
            Vector2d point = new Vector2d(1,1);
            RotationDirection dir = RotationDirection.clockwise;
            double angle = Math.PI;
            Vector2d expResult = new Vector2d(-1,-1);

            Vector2d result = MathUtil.rotateVector(point, dir, angle);

            assertTrue(expResult.epsilonEquals(result, 0.00000000000001));
        }

        {
            System.out.println(" - PI counter-clockwise");
            Vector2d point = new Vector2d(1,1);
            RotationDirection dir = RotationDirection.counter_clockwise;
            double angle = Math.PI;
            Vector2d expResult = new Vector2d(-1,-1);

            Vector2d result = MathUtil.rotateVector(point, dir, angle);

            assertTrue(expResult.epsilonEquals(result, 0.00000000000001));
        }

        {
            System.out.println(" - PI clockwise negative");
            Vector2d point = new Vector2d(1,1);
            RotationDirection dir = RotationDirection.clockwise;
            double angle = -Math.PI;
            Vector2d expResult = new Vector2d(-1,-1);

            Vector2d result = MathUtil.rotateVector(point, dir, angle);

            assertTrue(expResult.epsilonEquals(result, 0.00000000000001));
        }

        {
            System.out.println(" - 1/2*PI counter-clockwise");
            Vector2d point = new Vector2d(-1,-1);
            RotationDirection dir = RotationDirection.counter_clockwise;
            double angle = Math.PI/2;
            Vector2d expResult = new Vector2d(1,-1);

            Vector2d result = MathUtil.rotateVector(point, dir, angle);

            assertTrue(expResult.epsilonEquals(result, 0.00000000000001));
        }

        {
            System.out.println(" - 1/2*PI clockwise");
            Vector2d point = new Vector2d(-1,-1);
            RotationDirection dir = RotationDirection.clockwise;
            double angle = Math.PI/2;
            Vector2d expResult = new Vector2d(-1,1);

            Vector2d result = MathUtil.rotateVector(point, dir, angle);

            assertTrue(expResult.epsilonEquals(result, 0.00000000000001));
        }

        {
            System.out.println(" - 1/2*PI counter-clockwise negative");
            Vector2d point = new Vector2d(-1,-1);
            RotationDirection dir = RotationDirection.counter_clockwise;
            double angle = -Math.PI/2;
            Vector2d expResult = new Vector2d(-1,1);

            Vector2d result = MathUtil.rotateVector(point, dir, angle);

            assertTrue(expResult.epsilonEquals(result, 0.00000000000001));
        }
    }

    /**
     * Test of matrixToPoint method, of class MathUtil.
     */
    @Test
    public void testMatrixToPoint() {
        System.out.println("matrixToPoint");

        {
            double[][] values = {{13},{47}};

            Matrix m = new Matrix(values);
            Point2d expResult = new Point2d(13,47);
            Point2d result = MathUtil.matrixToPoint(m);
            assertTrue(expResult.epsilonEquals(result,0.00000000001));
        }
        {
            double[][] values = {{13,47}};  // row vector instead of column

            // test exception
            boolean catched = false;
            try{
                Matrix m = new Matrix(values);
                MathUtil.matrixToPoint(m);
            }
            catch(InvalidParameterException e){
                catched = true;
            }
            assertTrue("no exception thrown for wrong dimensions!!",catched);
        }
    }

    /**
     * Test of pointToMatrix method, of class MathUtil.
     */
    @Test
    public void testTupleToMatrix() {
        System.out.println("tupleToMatrix");

        // test correct values and dimension
        double[][] values = {{13},{47}};
        Matrix m = new Matrix(values);

        Point2d p = new Point2d(13,47);
        Matrix result = MathUtil.tupleToMatrix(p);

        assertEquals("Matrix has wrong num of columns!", 1, result.getColumnDimension());
        assertEquals("Matrix has wrong num of rows!", 2, result.getRowDimension());

        double dist = result.minus(m).norm1();

        assertEquals(0, dist, 0.0001 );
    }

    /**
     * Test of getRotationMatrix method, of class MathUtil.
     */
    @Test
    public void testGetRotationMatrix() {
        System.out.println("getRotationMatrix");

        {
            System.out.println(" - PI clockwise");
            Matrix rotMatrix = MathUtil.getRotationMatrix(Math.PI, RotationDirection.clockwise);

            assertEquals(2, rotMatrix.getColumnDimension());
            assertEquals(2, rotMatrix.getRowDimension());

            assertEquals(-1, rotMatrix.get(0, 0), 0.0000001 );
            assertEquals(0, rotMatrix.get(0, 1), 0.0000001 );
            assertEquals(0, rotMatrix.get(1, 0), 0.0000001 );
            assertEquals(-1, rotMatrix.get(1, 1), 0.0000001 );
        }
        {
            System.out.println(" - PI/2 clockwise");
            Matrix rotMatrix = MathUtil.getRotationMatrix(Math.PI/2, RotationDirection.clockwise);

            assertEquals(2, rotMatrix.getColumnDimension());
            assertEquals(2, rotMatrix.getRowDimension());

            assertEquals(0, rotMatrix.get(0, 0), 0.000000001 );
            assertEquals(1, rotMatrix.get(0, 1), 0.000000001 );
            assertEquals(-1, rotMatrix.get(1, 0), 0.000000001 );
            assertEquals(0, rotMatrix.get(1, 1), 0.000000001 );
        }
        {
            System.out.println(" - PI counter-clockwise");
            Matrix rotMatrix = MathUtil.getRotationMatrix(Math.PI, RotationDirection.counter_clockwise);

            assertEquals(2, rotMatrix.getColumnDimension());
            assertEquals(2, rotMatrix.getRowDimension());

            assertEquals(-1, rotMatrix.get(0, 0), 0.000000001 );
            assertEquals(0, rotMatrix.get(0, 1), 0.000000001 );
            assertEquals(0, rotMatrix.get(1, 0), 0.000000001 );
            assertEquals(-1, rotMatrix.get(1, 1), 0.000000001 );
        }
        {
            System.out.println(" - PI/2 counter-clockwise");
            Matrix rotMatrix = MathUtil.getRotationMatrix(Math.PI/2, RotationDirection.counter_clockwise);

            assertEquals(2, rotMatrix.getColumnDimension());
            assertEquals(2, rotMatrix.getRowDimension());

            assertEquals(0, rotMatrix.get(0, 0), 0.000000001 );
            assertEquals(-1, rotMatrix.get(0, 1), 0.000000001 );
            assertEquals(1, rotMatrix.get(1, 0), 0.000000001 );
            assertEquals(0, rotMatrix.get(1, 1), 0.000000001 );
        }
        {
            System.out.println(" - just a silly number test clockwise");
            Matrix rotMatrix = MathUtil.getRotationMatrix(0.23564123512312234526523, RotationDirection.clockwise);

            assertEquals(2, rotMatrix.getColumnDimension());
            assertEquals(2, rotMatrix.getRowDimension());

            assertEquals(rotMatrix.get(0, 0), rotMatrix.get(1, 1), 0.000000001 );
            assertEquals(rotMatrix.get(0, 1), -rotMatrix.get(1, 0), 0.000000001 );
        }
        {
            System.out.println(" - just a silly number test counter-clockwise");
            Matrix rotMatrix = MathUtil.getRotationMatrix(0.23564123512312234526523, RotationDirection.counter_clockwise);

            assertEquals(2, rotMatrix.getColumnDimension());
            assertEquals(2, rotMatrix.getRowDimension());

            assertEquals(rotMatrix.get(0, 0), rotMatrix.get(1, 1), 0.000000001 );
            assertEquals(rotMatrix.get(0, 1), -rotMatrix.get(1, 0), 0.000000001 );
        }
    }

}