/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MathX;

import de.delbrueg.math.PseudoCircle;
import de.delbrueg.exceptions.PointsCollinearException;
import javax.vecmath.Point2d;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tim
 */
public class PseudoCircleTest {

    public PseudoCircleTest() {
    }

    /**
     * Test of getMeanCircle method, of class PseudoCircle.
     */
    @Test
    public void testGetMeanCircle() {
        System.out.println("getMeanCircle");

        // test straight line
        PseudoCircle straight = new PseudoCircle();
        straight.addPoint(0, 1);
        straight.addPoint(1, 0);
        straight.addPoint(-1, 2);

        boolean catched = false;
        try{
            straight.getMeanCircle();
        }
        catch(PointsCollinearException e)
        {
            catched = true;
        }

        assertTrue("straight line doesnt throw exception", catched);

        // test left exact circle
        System.out.println(" - exact circle to the left");
        PseudoCircle left = new PseudoCircle();
        left.addPoint(-1, 1);
        left.addPoint(-1, -1);
        left.addPoint(0, 0);
        try{
            de.delbrueg.math.Circle c = left.getMeanCircle();
            Point2d p = c.getCenter();
            assertTrue(" - exact circle center x coordinate is false", p.getX() == -1);
            assertTrue(" - exact circle center y coordinate is false", p.getY() == 0);
            assertTrue(" - exact circle radius is false", c.getRadius() == 1.0d);
        }
        catch(PointsCollinearException _)
        {
            fail(" - unexpected exception: points are considered collinear but should not!!!");
        }

        // test right exact circle
        System.out.println(" - exact circle to the right");
        PseudoCircle right = new PseudoCircle();
        right.addPoint(2, 2);
        right.addPoint(2, -2);
        right.addPoint(0, 0);
        try{
            de.delbrueg.math.Circle c = right.getMeanCircle();
            Point2d p = c.getCenter();
            assertTrue(" - exact circle center x coordinate is false", p.getX() == 2);
            assertTrue(" - exact circle center y coordinate is false", p.getY() == 0);
            assertTrue(" - exact circle radius is false", c.getRadius() == 2.0d);
        }
        catch(PointsCollinearException _)
        {
            fail(" - unexpected exception: points are considered collinear but should not!!!");
        }

        // test mean of all before this
        System.out.println(" - mean circle");
        PseudoCircle ps = new PseudoCircle();
        ps.addPoint(0, 1);
        ps.addPoint(1, 0);
        ps.addPoint(0, 0);
        ps.addPoint(-1, 1);
        ps.addPoint(-1, -1);
        ps.addPoint(0, 0);
        ps.addPoint(2, -2);
        ps.addPoint(2, 2);
        ps.addPoint(0, 0);

        try{
            de.delbrueg.math.Circle c = ps.getMeanCircle();
            Point2d p = c.getCenter();
//            assertTrue(" - circle center x coordinate is false", p.getX() == 0);
//            assertTrue(" - circle center y coordinate is false", p.getY() == 0);
//            assertTrue(" - circle radius is false", c.getRadius() == 1.5d);
        }
        catch(PointsCollinearException _)
        {
            fail("unexpected exception: points are considered collinear but should not!!!");
        }

                // test mean of all before this
        {
            System.out.println(" - mean circle");
            PseudoCircle ps2 = new PseudoCircle();
            ps2.addPoint(-4.58, -3.44);
            ps2.addPoint(-6.56, -1.86);
            ps2.addPoint(-7.27, 1.39);
            ps2.addPoint(-7.33, 4.25);
            ps2.addPoint(-6.59, 6.11);
            ps2.addPoint(-4.36, 8);
            ps2.addPoint(-1.54, 8.46);

            try{
                de.delbrueg.math.Circle c = ps2.getMeanCircle();
                Point2d p = c.getCenter();
    //            assertTrue(" - circle center x coordinate is false", p.getX() == 0);
    //            assertTrue(" - circle center y coordinate is false", p.getY() == 0);
    //            assertTrue(" - circle radius is false", c.getRadius() == 1.5d);

                System.out.println("center: " + p + " and radius: " + c.getRadius());
            }
            catch(PointsCollinearException _)
            {
                fail("unexpected exception: points are considered collinear but should not!!!");
            }
        }
    }

}