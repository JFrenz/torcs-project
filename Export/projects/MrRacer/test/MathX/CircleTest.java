/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MathX;

import de.delbrueg.math.Circle;
import de.delbrueg.exceptions.PointsCollinearException;
import java.util.ArrayList;
import java.util.List;
import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tim
 */
public class CircleTest {

    public CircleTest() {
    }


    /**
     * Test of constructor method, of class Circle.
     */
    @Test
    public void testConstruct_3points() throws Exception {
        System.out.println("set by 3 points");

        // test straight line
        System.out.println(" - test straight line");

        boolean catched = false;
        try{
            Circle straight = new Circle(new Point2d(0, 1),new Point2d(1, 0),new Point2d(-1, 2));
        }
        catch(PointsCollinearException e)
        {
            catched = true;
        }
        assertTrue("-- straight line does not throw exception!!!", catched);
    }

    /**
     * Test of constructor method, of class Circle.
     */
    @Test
    public void testConstruct_2points_vector() throws Exception {
        System.out.println("set by 2 points and a direction");

        {
            System.out.println(" - test 1-circle");

            Circle c = new Circle(new Point2d(0, 1),new Vector2d(1, 0),new Point2d(0, -1));

            assertEquals(1, c.getRadius(), 0.000000000001);
            assertEquals(0, c.getCenter().x, 0.000000000001);
            assertEquals(0, c.getCenter().y, 0.000000000001);
        }
        {
            System.out.println(" - test 1-circle displaced");

            Circle c = new Circle(new Point2d(1, 1),new Vector2d(1, 0),new Point2d(1, -1));

            assertEquals(1, c.getRadius(), 0.000000000001);
            assertEquals(1, c.getCenter().x, 0.000000000001);
            assertEquals(0, c.getCenter().y, 0.000000000001);
        }
        {
            System.out.println(" - test 1-circle other points");

            Circle c = new Circle(new Point2d(-Math.sqrt(0.5), -Math.sqrt(0.5)),new Vector2d(-1, 1),new Point2d(0, -1));

            assertEquals(1, c.getRadius(), 0.000000000001);
            assertEquals(0, c.getCenter().x, 0.000000000001);
            assertEquals(0, c.getCenter().y, 0.000000000001);
        }
    }

    /**
     * Test of getCenter method, of class Circle.
     */
    @Test
    public void testGetCenter() {
        System.out.println("getCenter, getRadius, set(center,radius)");
        Circle c = new Circle(new Point2d(3.7,-12.45), 3.77);

        assertTrue("set/getCenter is mistaken.", c.getCenter().distance(new Point2d(3.7,-12.45)) < 0.0000001);
        assertEquals("set/getRadius is mistaken.", 3.77, c.getRadius(), 0.0000001);
    }

    /**
     * Test of getLength method, of class Circle.
     */
    @Test
    public void testGetLength() {
        System.out.println("getLength");
        
        Circle c = new Circle(new Point2d(0,0), 1);
        double l = c.getLength();

        assertTrue("length of 1-circle is not 2Pi!!!", l == 2*Math.PI);
    }

    /**
     * Test of getMeanCircle method, of class Circle.
     */
    @Test
    public void testGetMeanCircle_List() {
        System.out.println("getMeanCircle");

        Circle c = new Circle(new Point2d(1.0,0.0), 3.3);
        Circle d = new Circle(new Point2d(0.0,1.0), 1.1);
        Circle e = new Circle(new Point2d(2.0,2.0), 5.0);

        List<Circle> list = new ArrayList<Circle>(3);
        list.add(c);
        list.add(d);
        list.add(e);

        Circle m = Circle.getMeanCircle(list);
        assertTrue("circle center not right",m.getCenter().distance(new Point2d(1.0,1.0)) < 0.000001);
        assertTrue("circle radius not right",Math.abs(m.getRadius() - 3.1333333333333333) < 0.000001);
    }

    /**
     * Test of getNearestPointOnCircle method, of class Circle.
     */
    @Test
    public void testGetNearestPointOnCircle() {
        System.out.println("getNearestPointOnCircle");

        Circle c = new Circle(new Point2d(1.0,2.0), 1.5);
        Point2d p = new Point2d(1,30);
        Point2d nearest = c.getNearestPointOnCircle(p);
        Point2d desired = new Point2d (1.0, 3.5);

        assertTrue("nearest point is false", nearest.distance(desired) < 0.00001);
    }

    /**
     * Test of goAlong method, of class Circle.
     */
    @Test
    public void testGoAlong() {
        System.out.println("goAlong");

        System.out.println(" - r=1 (0,0) full clockwise");
        {
            Circle c = new Circle(new Point2d(0.0,0.0), 1.0);
            double d = 2*Math.PI;
            Point2d start = new Point2d (-1,0);
            Point2d end = c.goAlong(start, Circle.RotationDirection.clockwise, d);

            assertTrue("point is false", end.distance(start) < 0.00001);
        }

        System.out.println(" - r=2 (0,0) full counter-clockwise");
        {
            Circle c = new Circle(new Point2d(0.0,0.0), 2.0);
            double d = 4*Math.PI;   // full circle (2*PI*2=full)
            Point2d start = new Point2d (-2,0);
            Point2d end = c.goAlong(start, Circle.RotationDirection.counter_clockwise, d);

            assertTrue("point is false", end.distance(start) < 0.00001);
        }

        System.out.println(" - r=1 (0,0) half clockwise");
        {
            Circle c = new Circle(new Point2d(0.0,0.0), 1.0);
            double d = Math.PI;
            Point2d desired = new Point2d (1.0, 0.0);
            Point2d start = new Point2d (-1,0);
            Point2d end = c.goAlong(start, Circle.RotationDirection.clockwise, d);

            assertTrue("point is false", end.distance(desired) < 0.00001);
        }

        System.out.println(" - r=2 (0,0) half counter-clockwise");
        {
            Circle c = new Circle(new Point2d(0.0,0.0), 2.0);
            double d = 2*Math.PI;   // half circle (2*PI*2=full)
            Point2d desired = new Point2d (2.0, 0.0);
            Point2d start = new Point2d (-2,0);
            Point2d end = c.goAlong(start, Circle.RotationDirection.counter_clockwise, d);

            assertTrue("point is false", end.distance(desired) < 0.00001);
        }

        System.out.println(" - r=2 (1,-1) quarter clockwise");
        {
            Circle c = new Circle(new Point2d(1.0,-1.0), 2.0);
            double d = Math.PI;   // quarter circle (2*PI*2=full)
            Point2d desired = new Point2d (1, 1);
            Point2d start = new Point2d (-1,-1);
            Point2d end = c.goAlong(start, Circle.RotationDirection.clockwise, d);

            assertTrue("point is false", end.distance(desired) < 0.00001);
        }

        System.out.println(" - r=2 (1,-1) quarter counter-clockwise");
        {
            Circle c = new Circle(new Point2d(1.0,-1.0), 2.0);
            double d = Math.PI;   // quarter circle (2*PI*2=full)
            Point2d desired = new Point2d (1, -3);
            Point2d start = new Point2d (-1,-1);
            Point2d end = c.goAlong(start, Circle.RotationDirection.counter_clockwise, d);

            assertTrue("point is false", end.distance(desired) < 0.00001);
        }


    }

    /**
     * Test of getScaled method, of class Circle.
     */
    @Test
    public void testGetScaled() {
        System.out.println("getScaled");

        double scale = 2.0;
        Point2d center = new Point2d(13, 25);
        double radius = 37;


        Circle c = new Circle(center,radius);
        Circle result = c.getScaled(scale);


        assertTrue(result.getCenter().distance(center) < 0.00001);
        assertTrue(Math.abs(result.getRadius() - scale*radius) < 0.00001);
    }

    /**
     * Test of getRadius method, of class Circle.
     */
    @Test
    public void testGetRadius() {
        Circle c = new Circle(new Point2d(1,-1), 2.543);
        double result = c.getRadius();

        assertEquals(2.543, result, 0.0000000001);
    }

    /**
     * Test of getWeightedCircle method, of class Circle.
     */
    @Test
    public void testGetWeightedCircle_3args() {
        System.out.println("getWeightedMeanCircle");

        {
            System.out.println(" - equal weight");
            Circle circle1 = new Circle(new Point2d(1, 1), 1);
            Circle circle2 = new Circle(new Point2d(-1, 1), 1);
            double weight = 0.5;
            Point2d exp_center = new Point2d(0,1);
            double exp_radius = 1;

            Circle result = Circle.getWeightedMeanCircle(circle1, circle2, weight);

            assertTrue("center is false. expected: "+exp_center.toString()+", but was: "+result.getCenter().toString()
                    ,exp_center.epsilonEquals(result.getCenter(), 0.0000001));
            assertEquals("radius is false", exp_radius,result.getRadius(), 0.0000001);
        }
        {
            System.out.println(" - quarter weight");
            Circle circle1 = new Circle(new Point2d(1, 1), 1);
            Circle circle2 = new Circle(new Point2d(-1, -1), 1);
            double weight = 0.25;
            Point2d exp_center = new Point2d(0.5,0.5);
            double exp_radius = 1;

            Circle result = Circle.getWeightedMeanCircle(circle1, circle2, weight);

            assertTrue("center is false. expected: "+exp_center.toString()+", but was: "+result.getCenter().toString()
                    ,exp_center.epsilonEquals(result.getCenter(), 0.0000001));
            assertEquals("radius is false", exp_radius,result.getRadius(), 0.0000001);
        }
    }
}