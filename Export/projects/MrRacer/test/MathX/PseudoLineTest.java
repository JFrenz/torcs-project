/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MathX;

import org.junit.Test;
import static org.junit.Assert.*;
import de.delbrueg.math.PseudoLine;
import de.delbrueg.math.Line;

/**
 *
 * @author Tim
 */
public class PseudoLineTest {

    /**
     * Test of getMeanLine method, of class PseudoLine.
     */
    @Test
    public void testGetMeanLine() {
        System.out.println("getMeanLine");

        Line result1;
        double m = 1;
        {
            PseudoLine instance = new PseudoLine();


            instance.addPoint(m*3, 0);
            instance.addPoint(m*2.8, 0.5);
            instance.addPoint(m*2.95, 1.1);
            instance.addPoint(m*2.7, 1.78);
            instance.addPoint(m*2.6, 2.9);
        
            result1 = instance.getMeanLine();
        }
        Line result2;
        m = -1;
        {
            PseudoLine instance = new PseudoLine();


            instance.addPoint(m*3, 0);
            instance.addPoint(m*2.8, 0.5);
            instance.addPoint(m*2.95, 1.1);
            instance.addPoint(m*2.7, 1.78);
            instance.addPoint(m*2.6, 2.9);

            result2 = instance.getMeanLine();
            System.out.println(result2.getPoint() + "and dir: " + result2.getDirection());
        }

        result1.getPoint().x *= -1;
        result1.getDirection().x *= -1;

        assertEquals("Points dont match!",0,result1.getPoint().distance(result2.getPoint()), 0.00001);
        assertEquals("Directions dont match!",0,result1.getDirection().angle(result2.getDirection()), 0.00001);

    }
}
