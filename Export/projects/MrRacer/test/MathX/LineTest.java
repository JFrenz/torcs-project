/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MathX;

import de.delbrueg.math.Line;
import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tim
 */
public class LineTest {


    /**
     * Test of getOrthogonalLine method, of class Line.
     */
    @Test
    public void testGetOrthogonalLine() {
        System.out.println("getOrthogonalLine");

        {
            System.out.println(" - p=(0,0) dir=(1,1)");
            Line instance = new Line(new Point2d(0,0), new Vector2d(1,1));
            Line orth = instance.getOrthogonalLine();

            assertEquals(0, instance.getDirection().dot(orth.getDirection()), 0.0000001);
            assertTrue(instance.getPoint().epsilonEquals(orth.getPoint(), 0.0000001));
        }
        {
            System.out.println(" - p=(13,47) dir=(-13,1)");
            Line instance = new Line(new Point2d(13,47), new Vector2d(-13,1));
            Line orth = instance.getOrthogonalLine();

            assertEquals(0, instance.getDirection().dot(orth.getDirection()), 0.0000001);
            assertTrue(instance.getPoint().epsilonEquals(orth.getPoint(), 0.0000001));
        }

    }

    /**
     * Test of intersectionWith method, of class Line.
     */
    @Test
    public void testIntersectionWith() {
        System.out.println("intersectionWith");
        {
            System.out.println(" - parallel");
            Point2d p = new Point2d(13,47);
            Point2d q = new Point2d(13,44);
            Vector2d v = new Vector2d(13,44);
            Vector2d t = new Vector2d(26,88);

            Line instance = new Line(p, v);
            Line other = new Line(q, t);

            Point2d expResult = null;
            Point2d result = instance.intersectionWith(other);
            assertEquals(expResult, result);
        }
        {
            System.out.println(" - not parallel");
            Point2d p = new Point2d(0,0);
            Point2d q = new Point2d(0,0);
            Vector2d v = new Vector2d(13,44);
            Vector2d t = new Vector2d(6,88);

            Line instance = new Line(p, v);
            Line other = new Line(q, t);

            Point2d expResult = new Point2d(0,0);
            Point2d result = instance.intersectionWith(other);
            
            assertTrue(result.epsilonEquals(expResult, 0.0000000001));
        }
    }

    /**
     * Test of isParallelTo method, of class Line.
     */
    @Test
    public void testIsParallelTo() {
        System.out.println("isParallelTo");

        {
            System.out.println(" - parallel");
            Point2d p = new Point2d(13,47);
            Point2d q = new Point2d(13,44);
            Vector2d v = new Vector2d(13,44);
            Vector2d t = new Vector2d(26,88);

            Line instance = new Line(p, v);
            Line other = new Line(q, t);

            boolean expResult = true;
            boolean result = instance.isParallelTo(other);
            assertEquals(expResult, result);
        }
        {
            System.out.println(" - not parallel");
            Point2d p = new Point2d(13,47);
            Point2d q = new Point2d(13,44);
            Vector2d v = new Vector2d(13,44);
            Vector2d t = new Vector2d(13,14);

            Line instance = new Line(p, v);
            Line other = new Line(q, t);

            boolean expResult = false;
            boolean result = instance.isParallelTo(other);
            assertEquals(expResult, result);
        }
    }

    /**
     * Test of getPointAt method, of class Line.
     */
    @Test
    public void testGetPointAt() {
        System.out.println("getPointAt");
        double lambda = 47.3;
        Line instance = new Line(new Point2d(0,0), new Vector2d(1,1));

        Point2d expResult = new Point2d(47.3,47.3);
        Point2d result = instance.getPointAt(lambda);

        assertEquals(expResult, result);
    }

    @Test
    public void testIsLeftOf(){
        System.out.println("IsPointLeftOf");
        {
            // on the left
            Point2d p = new Point2d (-1.0, 2.0);
            Line instance = new Line(new Point2d(0,0), new Vector2d(0,1));

            assertTrue(instance.isPointOnTheLeft(p));
        }
        {
            // on the line
            Point2d p = new Point2d (0.0, 2.0);
            Line instance = new Line(new Point2d(0,0), new Vector2d(0,1));
            Line instance2 = new Line(new Point2d(0,0), new Vector2d(0,-1));

            assertTrue(!instance.isPointOnTheLeft(p));
            assertTrue(!instance2.isPointOnTheLeft(p));
        }
        {
            // on the right
            Point2d p = new Point2d (1.0, 2.0);
            Line instance = new Line(new Point2d(0,0), new Vector2d(0,1));

            assertTrue(!instance.isPointOnTheLeft(p));
        }
        {
            // on the left
            Point2d p = new Point2d (-2.0, -13);
            Line instance = new Line(new Point2d(0,0), new Vector2d(-1,1));

            assertTrue(instance.isPointOnTheLeft(p));
        }
        {
            // on the right
            Point2d p = new Point2d (-2.0, 13);
            Line instance = new Line(new Point2d(0,0), new Vector2d(-1,1));

            assertTrue(!instance.isPointOnTheLeft(p));
        }
        {
            // on the right
            Point2d p = new Point2d (2.0, 13);
            Line instance = new Line(new Point2d(0,0), new Vector2d(-1,1));

            assertTrue(!instance.isPointOnTheLeft(p));
        }
        {
            // on the right
            Point2d p = new Point2d (2.0, 1);
            Line instance = new Line(new Point2d(-2,0), new Vector2d(-1,1));

            assertTrue(!instance.isPointOnTheLeft(p));
        }
        {
            // on the right
            Point2d p = new Point2d (2.0, -1);
            Line instance = new Line(new Point2d(-2,0), new Vector2d(-1,1));

            assertTrue(!instance.isPointOnTheLeft(p));
        }

    }

    @Test
    public void testCut(){
        {
            Line l1 = new Line(new Point2d(1,1), new Vector2d(-2,0));
            Line l2 = new Line(new Point2d(0,10), new Vector2d(0,10));

            Point2d c = l1.cut(l2);
            assertEquals(0, c.x, 0.000000000001);
            assertEquals(1, c.y, 0.000000000001);
        }
        {
            Line l1 = new Line(new Point2d(1,1), new Vector2d(-2,2));
            Line l2 = new Line(new Point2d(0,10), new Vector2d(0,10));

            Point2d c = l1.cut(l2);
            assertEquals(0, c.x, 0.000000000001);
            assertEquals(2, c.y, 0.000000000001);
        }
        {   // parallel
            Line l1 = new Line(new Point2d(1,1), new Vector2d(-2,2));
            Line l2 = new Line(new Point2d(0,-3), new Vector2d(-1,1));

            Point2d c = l1.cut(l2);
            assertEquals(null, c);
        }
        {   // identical
            Line l1 = new Line(new Point2d(1,1), new Vector2d(-2,-2));
            Line l2 = new Line(new Point2d(-4,-4), new Vector2d(1,1));

            Point2d c = l1.cut(l2);
            assertEquals(null, c);
        }
    }

}