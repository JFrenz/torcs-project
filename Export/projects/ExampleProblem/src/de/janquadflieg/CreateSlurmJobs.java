/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Locale;

/**
 *
 * @author quad
 */
public class CreateSlurmJobs {

    public static void main(String[] args) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("F:/Quad/svn/Dissertation/Code/RExampleProblem/b-values2.txt"), "UTF-8"));

            OutputStreamWriter w = new OutputStreamWriter(new FileOutputStream("batch_jobs.sh"), "UTF-8");
            w.write("#!/bin/sh\n");

            String line = reader.readLine();

            while (line != null && line.trim().length() > 0) {
                double b = Double.parseDouble(line);
                double maxTime = 1;

                String cmd = "sbatch --output=process_b" + String.format(Locale.ENGLISH,"%.5f", b)
                        + ".txt --wrap \"Rscript calculateProbabilities.R "
                        + String.valueOf(b) + " " + String.valueOf(maxTime) + "\"";

                w.write(cmd + "\n");

                line = reader.readLine();
            }

            // 

            //

            //w.write("java -Xmx4G -cp MrRacer.jar de.janquadflieg.mrracer.evo.adapter.TorcsProblem paramFile=" + track.controllerTrackName + ".properties");
            w.close();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
}
