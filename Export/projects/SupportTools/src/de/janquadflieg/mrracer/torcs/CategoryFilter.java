/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.torcs;

import java.util.Arrays;
import java.util.ArrayList;

/**
 *
 * @author quad
 */
public class CategoryFilter implements TrackMatcher {
    
    private ArrayList<String> whiteList = new ArrayList<>();
    private ArrayList<String> blackList = new ArrayList<>();
    
    public CategoryFilter(String[] whiteList, String[] blackList){
        if(whiteList != null){
            this.whiteList.addAll(Arrays.asList(whiteList));
        }
        if(blackList != null){
            this.blackList.addAll(Arrays.asList(blackList));        
        }
    }

    public boolean matches(TrackData d) {
        boolean white = false;
        for(String s: whiteList){
            if(d.trackName.contains(s)){
                white = true;
            }
        }
        boolean black = true;
        for(String s: blackList){
            if(d.trackName.contains(s)){
                black = false;
            }
        }
        return black && white;
    }
}
