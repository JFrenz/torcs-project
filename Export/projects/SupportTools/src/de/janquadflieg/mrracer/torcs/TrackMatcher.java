/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.torcs;

/**
 *
 * @author quad
 */
public interface TrackMatcher {
    public boolean matches(TrackData d);    
}