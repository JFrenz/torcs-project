/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.torcs;

import java.util.Arrays;
import java.util.ArrayList;

/**
 *
 * @author quad
 */
public class SurfaceFilter implements TrackMatcher {
    private static final double EPSILON = 0.000001;
    
    private double friction = 0.0;
    private double rolling = 0.0;
    private double roughness = 0.0;
    private double wavelength = 0.0;    
    
    public SurfaceFilter(double a, double b, double c, double d){
        friction = a;
        rolling = b;
        roughness = c;
        wavelength = d;        
    }
    
    public SurfaceFilter(SurfaceData sd){
        friction = sd.friction;
        rolling = sd.rollingResistance;
        roughness = sd.roughness;
        wavelength = sd.roughnessWavelength;        
    }

    public boolean matches(TrackData d) {
        boolean a = Math.abs(d.getWeightedFriction()-friction) < EPSILON;
        boolean b = Math.abs(d.getWeightedRollingResistance() - rolling) < EPSILON;
        boolean c = Math.abs(d.getWeightedRoughness() - roughness) < EPSILON;
        boolean e = Math.abs(d.getWeightedRoughnessWavelength() - wavelength) < EPSILON;
               
        return a && b && c && e;
    }
}
