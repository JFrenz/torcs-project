/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.torcs;

import de.janquadflieg.mrracer.Utils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Helper class to read torcs xml files.
 *
 * @author quad
 */
public class TrackXMLReader {

    public static void scanSurfaces(ArrayList<SurfaceData> result)
            throws Exception {
        String TORCS_PATH = "F:/torcs/1_3_4/test-linux-patch_original/runtime/";
        

        FileInputStream fi = new FileInputStream(TORCS_PATH + "\\data\\tracks\\surfaces.xml");
        InputStreamReader r = new InputStreamReader(fi);
        BufferedReader b = new BufferedReader(r);
        String line = b.readLine();
        SurfaceData data = null;
        while (line != null) {
            //System.out.println(line);
            line = line.trim();
            if (line.contains("<section")) {
                data = new SurfaceData();
                line = line.substring(line.indexOf("name=\"") + 6);
                line = line.substring(0, line.indexOf("\""));
                data.name = line;
                //System.out.println("New surface found: "+data.name);
            }
            if (line.contains("</section>")) {
                if (data != null) {
                    result.add(data);
                }
            }

            scanSurfaceData(line, data);

            line = b.readLine();
        }

        b.close();
    }

    private static void scanSurfaceData(String line, SurfaceData data) {        
        if (line.contains("\"friction\"")) {
            double factor = 1.0;
            if(line.contains("unit=")){
                String unit = line.substring(line.indexOf("unit=")+5, line.indexOf("unit=")+9);
                factor = getFactor(unit);
                //System.out.println("UNIT: "+unit);
            }            
            line = line.substring(line.indexOf("val=\"") + 5);
            line = line.substring(0, line.indexOf("\""));            
            data.friction = factor*Double.parseDouble(line);            
        }

        if (line.contains("\"rolling resistance\"")) {
            double factor = 1.0;
            if(line.contains("unit=")){
                String unit = line.substring(line.indexOf("unit=")+5, line.indexOf("unit=")+9);
                factor = getFactor(unit);
                //System.out.println("UNIT: "+unit);
            }
            line = line.substring(line.indexOf("val=\"") + 5);
            line = line.substring(0, line.indexOf("\""));
            data.rollingResistance = factor * Double.parseDouble(line);            
        }

        if (line.contains("\"roughness\"")) {
            double factor = 1.0;
            if(line.contains("unit=")){
                String unit = line.substring(line.indexOf("unit=")+5, line.indexOf("unit=")+9);
                factor = getFactor(unit);
                //System.out.println("UNIT: "+unit);
            }
            line = line.substring(line.indexOf("val=\"") + 5);
            line = line.substring(0, line.indexOf("\""));
            data.roughness = factor * Double.parseDouble(line);            
        }        
        if (line.contains("\"roughness wavelength\"")) {        
            double factor = 1.0;
            if(line.contains("unit=")){
                String unit = line.substring(line.indexOf("unit=")+5, line.indexOf("unit=")+9);
                factor = getFactor(unit);
                //System.out.println("UNIT: "+unit);
            }
            line = line.substring(line.indexOf("val=\"") + 5);
            line = line.substring(0, line.indexOf("\""));
            data.roughnessWavelength = factor * Double.parseDouble(line);
        }
    }
    
    private static double getFactor(String unit){
        if(unit.equalsIgnoreCase("\"cm\"")){
            return 0.01;
            
        } else if(unit.equalsIgnoreCase("\"mm\"")){
            return 0.001;
        }
        System.out.println("UNKNOWN UNIT");
        return 0.0;
    }

    private static double convertM(double value, String unit) {
        if (unit.equalsIgnoreCase("ft")) {
            return value * 0.304801f;
        } else {
            return -1;
        }
    }

    private static void scanSegmentData(String line, TrackData.Segment data) {
        if (line.contains("type")) {
            line = line.substring(line.indexOf("val=\"") + 5);
            line = line.substring(0, line.indexOf("\""));
            data.direction = line;
        }
        if (line.contains("surface")) {
            line = line.substring(line.indexOf("val=\"") + 5);
            line = line.substring(0, line.indexOf("\""));
            data.surface = line;
        }
        if (line.contains("<attnum name=\"lg\"")) {
            String unit = line.substring(line.indexOf("unit=\"") + 6);
            unit = unit.substring(0, unit.indexOf("\""));
            //System.out.println(unit);
            String value = line.substring(line.indexOf("val=\"") + 5);
            value = value.substring(0, value.indexOf("\""));
            //System.out.println(value);
            if (!unit.equalsIgnoreCase("m")) {
                System.exit(-1);
            }
            data.length = Double.parseDouble(value);
        }
        if (line.contains("<attnum name=\"arc\"")) {
            String unit = line.substring(line.indexOf("unit=\"") + 6);
            unit = unit.substring(0, unit.indexOf("\""));
            //System.out.println(unit);
            String value = line.substring(line.indexOf("val=\"") + 5);
            value = value.substring(0, value.indexOf("\""));
            //System.out.println(value);
            if (!unit.equalsIgnoreCase("deg")) {
                System.exit(-1);
            }
            data.arc = Double.parseDouble(value);
        }
        if (line.contains("<attnum name=\"radius\"")) {
            String unit = line.substring(line.indexOf("unit=\"") + 6);
            unit = unit.substring(0, unit.indexOf("\""));
            //System.out.println(unit);
            String value = line.substring(line.indexOf("val=\"") + 5);
            value = value.substring(0, value.indexOf("\""));
            //System.out.println(value);
            data.radius = Double.parseDouble(value);
            if (!unit.equalsIgnoreCase("m")) {
                data.radius = convertM(data.radius, unit);
            }

        }
        if (line.contains("<attnum name=\"end radius\"")) {
            String unit = line.indexOf("unit=\"") != -1 ? line.substring(line.indexOf("unit=\"") + 6) : "m\"";
            unit = unit.substring(0, unit.indexOf("\""));
            //System.out.println(unit);
            String value = line.substring(line.indexOf("val=\"") + 5);
            value = value.substring(0, value.indexOf("\""));
            //System.out.println(value);
            data.endRadius = Double.parseDouble(value);
            if (!unit.equalsIgnoreCase("m")) {
                data.endRadius = convertM(data.endRadius, unit);
            }

        }
        if (line.contains("<attnum name=\"profil steps length\"")) {
            String unit = line.indexOf("unit=\"") != -1 ? line.substring(line.indexOf("unit=\"") + 6) : "m\"";
            unit = unit.substring(0, unit.indexOf("\""));
            //System.out.println(unit);
            String value = line.substring(line.indexOf("val=\"") + 5);
            value = value.substring(0, value.indexOf("\""));
            //System.out.println(value);
            if (!unit.equalsIgnoreCase("m")) {
                System.exit(-1);
            }
            data.profilStepsLength = Double.parseDouble(value);
        }
        if (line.contains("<attnum name=\"profil steps\"")) {
            String value = line.substring(line.indexOf("val=\"") + 5);
            value = value.substring(0, value.indexOf("\""));
            //System.out.println(value);            
            data.steps = Integer.parseInt(value);
        }
    }

    public static void scanTrack(ArrayList<SurfaceData> surfaces, BufferedReader b, TrackData trackData)
            throws Exception {
        ArrayList<TrackData.Segment> trackSegments = new ArrayList<>();
        String trackDefaultSurface = null;
        ArrayList<String> sections = new ArrayList<>(); // Stack
        SurfaceData surfaceData = null;
        double trackProfilStepsLength = 0.0;

        String line = b.readLine();
        boolean comment = false;

        while (line != null) {
            line = line.trim();

            if (line.contains("<!--")) {
                String front = line.substring(0, line.indexOf("<!--")).trim();
                String remainder = line.substring(line.indexOf("<!--")).trim();

                //System.out.println("[FRONT]:"+front);
                //System.out.println("[REMAINDER]:"+remainder);

                if (remainder.contains("-->")) {
                    String back = remainder.substring(remainder.indexOf("-->") + 3);
                    //System.out.println("[BACK]:"+back);
                    line = front + back;

                } else {
                    comment = true;
                    line = front;
                }

            } else if (line.contains("-->")) {
                comment = false;
                line = line.substring(line.indexOf("-->") + 3);

            } else if (comment) {
                line = "";
            }

            if (line.contains("<section")) {
                String name = line.substring(line.indexOf("name=\"") + 6);
                name = name.substring(0, name.indexOf("\""));
                sections.add(name);
//                            for (int i = 0; i < sections.size() - 1; ++i) {
//                                System.out.print("  ");
//                            }
//                            System.out.println(name);
            }

            if (line.contains("</section")) {
                /*for (int i = 0; i < sections.size() - 1; ++i) {
                 System.out.print("  ");
                 }*/
                if (sections.isEmpty()) {
                    System.out.println("Error: Sections Stack underflow" + trackData.torcsTrackName);

                } else {
                    if (sections.size() == 2 && sections.get(0).equalsIgnoreCase("Surfaces")) {
                        //System.out.println("Saving surface "+surfaceData);
                        surfaces.add(surfaceData);
                    }

                    //System.out.println("/" + sections.get(sections.size() - 1));
                    sections.remove(sections.size() - 1);
                }
            }

            if (sections.size() == 1 && sections.get(0).equalsIgnoreCase("Header")) {
                if (line.trim().contains("<attstr name=\"name\"")) {
                    //System.out.println("Found name entry");
                    line = line.trim();
                    line = line.substring(line.indexOf("val="));
                    line = line.substring(line.indexOf("\"") + 1);
                    line = line.substring(0, line.indexOf("\""));
                    String ourName = line.replace(" ", "").replace("-", "").toLowerCase();
                    System.out.print(line + ";" + trackData.torcsCategory+ ";" + trackData.torcsTrackName+ ";" + ourName);
                    trackData.trackName = line;                    
                    trackData.controllerTrackName = ourName;
                }
            }

            // beginning of a surface
            if (sections.size() == 2 && sections.get(0).equalsIgnoreCase("Surfaces")
                    && line.contains("<section")) {
                surfaceData = new SurfaceData();
                surfaceData.name = trackData.torcsTrackName + sections.get(sections.size() - 1);
                //System.out.println("Found new surface: " + surfaceData.name);
            }
            // surface data
            if (sections.size() == 2 && sections.get(0).equalsIgnoreCase("Surfaces")
                    && (line.contains("<attstr") || line.contains("<attnum"))) {
                scanSurfaceData(line, surfaceData);
            }

            // beginning of a track segment
            if (sections.size() == 3
                    && (sections.get(1).equalsIgnoreCase("Track Segments") || sections.get(1).equalsIgnoreCase("segments"))
                    && line.contains("<section")) {
                //System.out.println("Start of track segment");
                TrackData.Segment s = new TrackData.Segment();
                s.name = sections.get(2);
                trackSegments.add(s);
            }

            // track segment data
            if (sections.size() == 3
                    && (sections.get(1).equalsIgnoreCase("Track Segments") || sections.get(1).equalsIgnoreCase("segments"))
                    && (line.contains("<attstr") || line.contains("<attnum"))) {
                scanSegmentData(line, trackSegments.get(trackSegments.size() - 1));
            }



            // Track Segments abschnitt main track
            //<attnum name="width" unit="m" val="12.0" />
            if (sections.size() == 1 && sections.get(0).equalsIgnoreCase("Main Track")
                    && line.trim().contains("<attnum name=\"width\"")) {
                //System.out.println("Found track width");
                line = line.trim();
                line = line.substring(line.indexOf("val="));
                line = line.substring(line.indexOf("\"") + 1);
                line = line.substring(0, line.indexOf("\""));
                //System.out.println(line);
                trackData.width = Double.parseDouble(line);
            }

            if (sections.size() == 1 && sections.get(0).equalsIgnoreCase("Main Track")
                    && line.trim().contains("<attstr name=\"surface\"")) {
                //System.out.println("Found main track surface");
                line = line.trim();
                line = line.substring(line.indexOf("val="));
                line = line.substring(line.indexOf("\"") + 1);
                line = line.substring(0, line.indexOf("\""));
                //System.out.println(line);
                trackDefaultSurface = line;
            }

            if (sections.size() == 1 && sections.get(0).equalsIgnoreCase("Main Track")
                    && line.contains("<attnum name=\"profil steps length\"")) {
                String unit = line.substring(line.indexOf("unit=\"") + 6);
                unit = unit.substring(0, unit.indexOf("\""));
                //System.out.println(unit);
                String value = line.substring(line.indexOf("val=\"") + 5);
                value = value.substring(0, value.indexOf("\""));
                //System.out.println(value);
                if (!unit.equalsIgnoreCase("m")) {
                    System.exit(-1);
                }
                trackProfilStepsLength = Double.parseDouble(value);
            }

            line = b.readLine();
        }

        System.out.print("Done with track " + trackData.torcsTrackName
                + " " + trackSegments.size() + " segments ");
        trackData.length = 0.0;
        for (int ts = 0; ts < trackSegments.size(); ++ts) {
            TrackData.Segment seg = trackSegments.get(ts);
            seg.calcLength(trackProfilStepsLength);
            if (seg.surface == null) {
                seg.surface = trackDefaultSurface;
                if (seg.surface == null) {
                    seg.surface = trackSegments.get(0).surface;
                }
            }
//                        System.out.println((ts + 1) + " " +seg.name + " " + seg.direction + " "
//                                + seg.getLength(trackProfilStepsLength) + " "
//                                + seg.surface);
            trackData.length += seg.length;
        }
        System.out.println(Utils.dTS(trackData.length) + ", " + trackData.width);

        //System.out.print("Used Surfaces: ");

        //double friction = 0.0;
        HashMap<SurfaceData, Double> usedSurfaces = new HashMap<>();
        for (int i = 0; i < trackSegments.size(); ++i) {
            String sname = trackSegments.get(i).surface;
            SurfaceData search = new SurfaceData();
            search.name = trackData.torcsTrackName + sname;
            if (surfaces.contains(search)) {
                search = surfaces.get(surfaces.indexOf(search));
                //System.out.print("(T)");
            } else {
                search.name = sname;

                if (surfaces.contains(search)) {
                    search = surfaces.get(surfaces.indexOf(search));
                } else {
                    System.out.println("SURFACE NOT FOUND: " + search.name);
                    for (SurfaceData bla : surfaces) {
                        System.out.print(bla.name + ", ");
                    }
                    System.out.println("");
                    System.exit(-1);
                }
            }
            if (usedSurfaces.containsKey(search)) {
                double d = usedSurfaces.get(search).doubleValue();
                d += trackSegments.get(i).length;
                usedSurfaces.remove(search);
                usedSurfaces.put(search, d);

            } else {
                usedSurfaces.put(search, trackSegments.get(i).length);
            }
        }

        // check if different surfaces are really different
        if (usedSurfaces.keySet().size() > 1) {
            for (int i = 0; i < usedSurfaces.keySet().size() - 1; ++i) {
                final double EPS = 0.001;
                // von hinten durch die brust...                            
                SurfaceData s2 = (SurfaceData) usedSurfaces.keySet().toArray()[i + 1];
                for (int j = 0; j < i + 1; ++j) {
                    SurfaceData s1 = (SurfaceData) usedSurfaces.keySet().toArray()[j];
                    if (Math.abs(s1.friction - s2.friction) < EPS
                            && Math.abs(s1.rollingResistance - s2.rollingResistance) < EPS
                            && Math.abs(s1.roughness - s2.roughness) < EPS
                            && Math.abs(s1.roughnessWavelength - s2.roughnessWavelength) < EPS) {
                        System.out.println("Redundant: " + s2);
                        double d = usedSurfaces.get(s1).doubleValue();
                        d += usedSurfaces.get(s2).doubleValue();
                        usedSurfaces.remove(s1);
                        usedSurfaces.remove(s2);
                        usedSurfaces.put(s1, d);
                        i = 0;
                    }
                    //System.out.println(s1);
                    //System.out.println(s2);                                
                }
                //System.out.println("-----------------");
            }
        }

        //System.out.println(frictions.entrySet());
        System.out.println(usedSurfaces.keySet().size() + " surfaces");
        if (usedSurfaces.keySet().size() > 1) {
            System.out.println(usedSurfaces.entrySet());
        }

        trackData.surfaces.putAll(usedSurfaces);

        System.out.println("");
    }

    public static void main(String[] args) {
        try {
            String TORCS_PATH = "F:/torcs/1_3_4/test-linux-patch_original/runtime/";

            ArrayList<SurfaceData> surfaces = new ArrayList<>();
            scanSurfaces(surfaces);

            File trackDir = new File(TORCS_PATH + File.separator + "tracks");
            File[] categories = trackDir.listFiles();

            ArrayList<TrackData> allTracks = new ArrayList<>();

            for (File catDir : categories) {
                File[] tracks = catDir.listFiles();
                for (File track : tracks) {


//                    System.out.println(catDir.getName() + " " + track.getName());

                    /*if (!track.getName().equalsIgnoreCase("C-Speedway")) {
                        continue;
                    }*/

                    FileInputStream fi = new FileInputStream(track.getAbsolutePath() + File.separator + track.getName() + ".xml");
                    InputStreamReader r = new InputStreamReader(fi);
                    BufferedReader b = new BufferedReader(r);

                    TrackData trackData = new TrackData();
                    trackData.torcsCategory = catDir.getName();
                    trackData.torcsTrackName = track.getName();

                    scanTrack(surfaces, b, trackData);

                    b.close();
                    r.close();
                    fi.close();


                    allTracks.add(trackData);
                }
            }

            System.out.println(allTracks.size() + " tracks:");
            int[] ctr = {0, 0, 0, 0, 0};
            for (TrackData track : allTracks) {
                System.out.println(track);
                int numSurfaces = track.surfaces.keySet().size();
                if (numSurfaces > 4) {
                    ctr[4]++;

                } else {
                    ctr[numSurfaces - 1]++;
                }
            }
            System.out.println(allTracks.size() + " tracks, short analytical version:");
            System.out.println(TrackData.getAnalyticalStringHeader());
            for (TrackData track : allTracks) {
                System.out.println(track.toShortAnalyticalString());                
            }            
            System.out.println("1 Surface: " + ctr[0] + " tracks");
            System.out.println("2 Surfaces: " + ctr[1] + " tracks");
            System.out.println("3 Surfaces: " + ctr[2] + " tracks");
            System.out.println("4 Surfaces: " + ctr[3] + " tracks");
            System.out.println("> 4 Surfaces: " + ctr[4] + " tracks");
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
}