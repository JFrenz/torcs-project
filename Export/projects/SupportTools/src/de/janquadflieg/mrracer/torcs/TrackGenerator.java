/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.torcs;

import de.janquadflieg.mrracer.Utils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

/**
 *
 * @author quad
 */
public class TrackGenerator {

    private static final String TORCS_BASE_PATH = "F:\\torcs\\1_3_4\\test-linux-patch_original\\runtime";
    private static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    private static final String ENTITIES =
            "<!DOCTYPE params SYSTEM \"../../../../src/libs/tgf/params.dtd\" [\n"
            + "<!-- general definitions for tracks -->\n"
            + "<!ENTITY default-surfaces SYSTEM \"../../../data/tracks/surfaces.xml\">\n"
            + "<!ENTITY default-objects SYSTEM \"../../../data/tracks/objects.xml\">\n"
            + "]>";
    private static final String LEFT_PART_OF_TRACK_SIDE =
            "<section name=\"Left Side\">\n"
            + "      <attnum name=\"start width\" unit=\"m\" val=\"4.0\" />\n"
            + "      <attnum name=\"end width\" unit=\"m\" val=\"4.0\" />\n"
            + "      <attstr name=\"surface\" val=\"grass\" />\n"
            + "    </section>\n"
            + "    <section name=\"Left Border\">\n"
            + "      <attnum name=\"width\" unit=\"m\" val=\"3.5\" />\n"
            + "      <attnum name=\"height\" unit=\"m\" val=\"0.05\" />\n"
            + "      <attstr name=\"surface\" val=\"curb-5cm-r\" />\n"
            + "      <attstr name=\"style\" val=\"plan\" />\n"
            + "    </section>\n"
            + "    <section name=\"Left Barrier\">\n"
            + "      <attnum name=\"width\" unit=\"m\" val=\"0.1\" />\n"
            + "      <attnum name=\"height\" unit=\"m\" val=\"1.0\" />\n"
            + "      <attstr name=\"surface\" val=\"barrier\" />\n"
            + "      <attstr name=\"style\" val=\"curb\" />\n"
            + "    </section>";
    private static final String RIGHT_PART_OF_TRACK_SIDE =
            "<section name=\"Right Side\">\n"
            + "      <attnum name=\"start width\" unit=\"m\" val=\"4.0\" />\n"
            + "      <attnum name=\"end width\" unit=\"m\" val=\"4.0\" />\n"
            + "      <attstr name=\"surface\" val=\"grass\" />\n"
            + "    </section>\n"
            + "    <section name=\"Right Border\">\n"
            + "      <attnum name=\"width\" unit=\"m\" val=\"0.5\" />\n"
            + "      <attnum name=\"height\" unit=\"m\" val=\"0.05\" />\n"
            + "      <attstr name=\"surface\" val=\"curb-5cm-r\" />\n"
            + "      <attstr name=\"style\" val=\"plan\" />\n"
            + "    </section>\n"
            + "    <section name=\"Right Barrier\">\n"
            + "      <attnum name=\"width\" unit=\"m\" val=\"0.1\" />\n"
            + "      <attnum name=\"height\" unit=\"m\" val=\"0.3\" />\n"
            + "      <attstr name=\"surface\" val=\"barrier\" />\n"
            + "      <attstr name=\"style\" val=\"curb\" />\n"
            + "    </section>";
    private static final String PITS =
            "<section name=\"Pits\">\n"
            + "      <attstr name=\"side\" val=\"right\" />\n"
            + "      <attstr name=\"entry\" val=\"\" />\n"
            + "      <attstr name=\"start\" val=\"\" />\n"
            + "      <attstr name=\"end\" val=\"\" />\n"
            + "      <attstr name=\"exit\" val=\"\" />\n"
            + "      <attnum name=\"length\" unit=\"m\" val=\"0.0\" />\n"
            + "      <attnum name=\"width\" unit=\"m\" val=\"0.0\" />\n"
            + "    </section>";

    public void generateTrack(TrackData trackData, double cornerRadius) {
        try {
            File trackDir = new File(TORCS_BASE_PATH + File.separator + "tracks" + File.separator
                    + trackData.torcsCategory + File.separator + trackData.torcsTrackName);
            System.out.print(trackDir.getAbsolutePath());
            if (trackDir.exists()) {
                System.out.println(" ... exists");

            } else {
                boolean success = trackDir.mkdirs();
                if (!success) {
                    System.out.println(" ... failed to create!");
                    System.exit(1);
                } else {
                    System.out.println(" ... created");    
                }
                
            }





            String xml = generateTrackXML(trackData, cornerRadius);

            OutputStreamWriter trackXMLWriter = new OutputStreamWriter(new FileOutputStream(new File(TORCS_BASE_PATH + File.separator + "tracks" + File.separator
                    + trackData.torcsCategory + File.separator + trackData.torcsTrackName + File.separator + trackData.torcsTrackName + ".xml")), "UTF-8");
            trackXMLWriter.write(xml);
            trackXMLWriter.close();
            
            ProcessBuilder pb = new ProcessBuilder(TORCS_BASE_PATH+File.separator+"trackgen.exe","-c",trackData.torcsCategory,
                    "-n",trackData.torcsTrackName,"-a");
            
            pb.directory(new File(TORCS_BASE_PATH));
            pb.redirectErrorStream(true);
            pb.inheritIO();
            
            
            
            Process p = pb.start();
            
            p.waitFor();

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    public String generateTrackXML(TrackData trackData, double cornerRadius) {
        StringBuilder result = new StringBuilder();

        result.append(XML_HEADER).append("\n");
        result.append(ENTITIES).append("\n");

        result.append("<params name=\"").append(trackData.trackName).append("\" type=\"param\" mode=\"mw\">").append("\n");

        result.append("<section name=\"Surfaces\">&default-surfaces;</section>").append("\n");

        // Header
        result.append("<section name=\"Header\">").append("\n");
        result.append("  <attstr name=\"name\" val=\"").append(trackData.trackName).append("\" />").append("\n");
        result.append("  <attstr name=\"category\" val=\"").append(trackData.torcsCategory).append("\" />").append("\n");
        result.append("  <attnum name=\"version\" val=\"4\" />").append("\n");
        result.append("  <attstr name=\"author\" val=\"Jan Quadflieg\" />").append("\n");
        result.append("  <attstr name=\"description\" val=\"" + "Corner radius: ").append(Utils.dTS(cornerRadius)).append("m, surface:TODO"+"\" />").append("\n");
        result.append("</section>").append("\n");

        // Graphic section
        result.append("<section name=\"Graphic\">").append("\n");
        result.append("  <attstr name=\"3d description\" val=\"").append(trackData.torcsTrackName).append(".ac").append("\" />").append("\n");
        result.append("  <section name=\"Terrain Generation\">").append("\n");
        result.append("    <attnum name=\"track step\" unit=\"m\" val=\"20\" />").append("\n");
        result.append("    <attnum name=\"border margin\" unit=\"m\" val=\"50\" />").append("\n");
        result.append("    <attnum name=\"border step\" unit=\"m\" val=\"30\" />").append("\n");
        result.append("    <attnum name=\"border height\" unit=\"m\" val=\"15\" />").append("\n");
        result.append("    <attstr name=\"orientation\" val=\"clockwise\" />").append("\n");
        result.append("  </section>").append("\n");
        result.append("</section>").append("\n");

        // Main track section
        result.append("<section name=\"Main Track\">").append("\n");
        result.append("<attnum name=\"width\" unit=\"m\" val=\"").append(String.valueOf(trackData.width)).append("\" />").append("\n");
        result.append("<attnum name=\"profil steps length\" unit=\"m\" val=\"4.0\" />").append("\n");
        result.append("<attstr name=\"surface\" val=\"asphalt2-lines\" />").append("\n");
        result.append(LEFT_PART_OF_TRACK_SIDE).append("\n");
        result.append(RIGHT_PART_OF_TRACK_SIDE).append("\n");

        // Pits
        result.append(PITS).append("\n");

        // Track Segments
        result.append("<section name=\"Track Segments\">").append("\n");

        // Track Segment 1
        result.append("<section name=\"Straight1-2\">").append("\n");
        result.append("<attstr name=\"type\" val=\"str\" />").append("\n");
        result.append("<attnum name=\"lg\" unit=\"m\" val=\"800.0\" />").append("\n");
        result.append("<attnum name=\"z start\" unit=\"m\" val=\"0.0\" />").append("\n");
        result.append("<attnum name=\"z end\" unit=\"m\" val=\"0.0\" />").append("\n");
        result.append("<attstr name=\"surface\" val=\"asphalt2-lines\" />").append("\n");
        result.append(LEFT_PART_OF_TRACK_SIDE).append("\n");
        result.append(RIGHT_PART_OF_TRACK_SIDE).append("\n");
        result.append("</section>").append("\n");

        // Track Segment 2
        result.append("<section name=\"Right1\">").append("\n");
        result.append("<attstr name=\"type\" val=\"rgt\" />").append("\n");
        result.append("<attnum name=\"arc\" unit=\"deg\" val=\"180.0\" />").append("\n");
        result.append("<attnum name=\"radius\" unit=\"m\" val=\"").append(String.valueOf(cornerRadius)).append("\" />").append("\n");
        result.append("<attnum name=\"z start\" unit=\"m\" val=\"0.0\" />").append("\n");
        result.append("<attnum name=\"z end\" unit=\"m\" val=\"0.0\" />").append("\n");
        result.append("<attstr name=\"surface\" val=\"asphalt2-lines\" />").append("\n");
        result.append(LEFT_PART_OF_TRACK_SIDE).append("\n");
        result.append(RIGHT_PART_OF_TRACK_SIDE).append("\n");
        result.append("</section>").append("\n");

        // Track Segment 3
        result.append("<section name=\"Straight2-1\">").append("\n");
        result.append("<attstr name=\"type\" val=\"str\" />").append("\n");
        result.append("<attnum name=\"lg\" unit=\"m\" val=\"1000.0\" />").append("\n");
        result.append("<attnum name=\"z start\" unit=\"m\" val=\"0.0\" />").append("\n");
        result.append("<attnum name=\"z end\" unit=\"m\" val=\"0.0\" />").append("\n");
        result.append("<attstr name=\"surface\" val=\"asphalt2-lines\" />").append("\n");
        result.append(LEFT_PART_OF_TRACK_SIDE).append("\n");
        result.append(RIGHT_PART_OF_TRACK_SIDE).append("\n");
        result.append("</section>").append("\n");

        // Track Segment 4
        result.append("<section name=\"Right2\">").append("\n");
        result.append("<attstr name=\"type\" val=\"rgt\" />").append("\n");
        result.append("<attnum name=\"arc\" unit=\"deg\" val=\"180.0\" />").append("\n");
        result.append("<attnum name=\"radius\" unit=\"m\" val=\"").append(String.valueOf(cornerRadius)).append("\" />").append("\n");
        result.append("<attnum name=\"z start\" unit=\"m\" val=\"0.0\" />").append("\n");
        result.append("<attnum name=\"z end\" unit=\"m\" val=\"0.0\" />").append("\n");
        result.append("<attstr name=\"surface\" val=\"asphalt2-lines\" />").append("\n");
        result.append(LEFT_PART_OF_TRACK_SIDE).append("\n");
        result.append(RIGHT_PART_OF_TRACK_SIDE).append("\n");
        result.append("</section>").append("\n");

        // Track Segment 5
        result.append("<section name=\"Straight1-1\">").append("\n");
        result.append("<attstr name=\"type\" val=\"str\" />").append("\n");
        result.append("<attnum name=\"lg\" unit=\"m\" val=\"200.0\" />").append("\n");
        result.append("<attnum name=\"z start\" unit=\"m\" val=\"0.0\" />").append("\n");
        result.append("<attnum name=\"z end\" unit=\"m\" val=\"0.0\" />").append("\n");
        result.append("<attstr name=\"surface\" val=\"asphalt2-lines\" />").append("\n");
        result.append(LEFT_PART_OF_TRACK_SIDE).append("\n");
        result.append(RIGHT_PART_OF_TRACK_SIDE).append("\n");
        result.append("</section>").append("\n");

        // End Track Segments
        result.append("</section>").append("\n");

        // End Main Track Section
        result.append("</section>").append("\n");


        //result.append("").append("\n");




        result.append("</params>").append("\n");
        return result.toString();
    }

    public static void main(String[] args) {
        // MetaData for generating the track
        double width = 15.0;
        double radius = 250.0;
        String surfaceName = "Default";
        
        
        TrackData trackData = new TrackData();
        trackData.torcsTrackName = "testtrack"+String.valueOf(width)+"r"+String.valueOf(radius)+surfaceName;
        trackData.torcsTrackName = trackData.torcsTrackName.replace(".", "_");
        trackData.trackName = "Testtrack-W"+Utils.dTS(width)+"-R"+Utils.dTS(radius)+"m-SurfaceTODO";
        trackData.torcsCategory = "jqtest";
        trackData.width = width;


        TrackGenerator generator = new TrackGenerator();
        generator.generateTrack(trackData, radius);
    }
}