/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.torcs;

import de.janquadflieg.mrracer.Utils;
import java.io.*;
import java.util.*;

/**
 *
 * @author quad
 */
public class TrackDataList {

    public static final String[] TRACKS_TCIAIG2014_REPEAT = {"Forza", "Wheel 2",
        "C-Speedway", "Kerang-desert", "Mueda-city", "Pinneo-city", "Watorowo-city",
        "E-Track 5", "Westplex-mountain", "Ponnesen-desert", "Kumbharli-desert",
        "Makowiec-hill", "Volcan-mountain", "CG track 3", "Bukavu-mountain-snow"};
    public static final String[] TRACKS_TCIAIG2014_SCRC = {"Kerang-desert",
        "Mueda-city", "Zvolenovice-mountain", "Alsoujlak-hill", "Arraias-desert",
        "Sancassa-city"};
    public static final String[] TRACKS_TORCS_134 = {"A-Speedway", "Aalborg", "Alpine 1",
        "Alpine 2", "B-Speedway", "Brondehach", "C-Speedway",
        "CG Speedway number 1", "CG track 2", "CG track 3", "Corkscrew",
        "D-Speedway", "Dirt 1", "Dirt 2", "Dirt 3", "Dirt 4", "Dirt 5",
        "Dirt 6", "E-Road", "E-Speedway", "E-Track 1", "E-Track 2", "E-Track 3",
        "E-Track 4", "E-Track 5", "E-Track 6", "F-Speedway", "Forza",
        "G-Speedway", "Michigan Speedway", "Mixed 1", "Mixed 2",
        "Olethros Road 1", "Ruudskogen", "Spring", "Street 1",
        "Wheel 1", "Wheel 2"};
    public static final String[] TRACKS_TCIAIG2014 = {"A-Speedway", "Aalborg", "Alpine 1",
        "Alpine 2", "Alsoujlak-hill", "Arraias-desert", "B-Speedway",
        "Berhet-hill", "Berhet-mountain", "Berhet-mountain-snow", "Brondehach",
        "Bukavu-hill", "Bukavu-mountain", "Bukavu-mountain-snow", "C-Speedway",
        "CG Speedway number 1", "CG track 2", "CG track 3", "Corkscrew",
        "D-Speedway", "Dirt 1", "Dirt 2", "Dirt 3", "Dirt 4", "Dirt 5",
        "Dirt 6", "E-Road", "E-Speedway", "E-Track 2", "E-Track 3",
        "E-Track 4", "E-Track 5", "E-Track 6", "F-Speedway", "Forza",
        "G-Speedway", "Kerang-desert", "Kumbharli-city", "Kumbharli-desert",
        "Kumbharli-hill", "Kumbharli-mountain-snow", "Makowiec-city",
        "Makowiec-desert", "Makowiec-hill", "Makowiec-mountain",
        "Makowiec-mountain-snow", "Michigan Speedway", "Mixed 1", "Mixed 2",
        "Mueda-city", "Noye-desert", "Noye-hill", "Noye-mountain",
        "Noye-mountain-snow", "Olethros Road 1", "Petrinja-desert",
        "Petrinja-hill", "Petrinja-mountain-snow", "Pinneo-city",
        "Pinneo-desert", "Pinneo-hill", "Pinneo-mountain",
        "Pinneo-mountain-snow", "Ponnesen-desert", "Ponnesen-hill",
        "Quedam-city", "Quedam-desert", "Quedam-hill", "Quedam-mountain",
        "Quedam-mountain-snow", "R491-city", "R491-desert", "R491-hill",
        "R491-mountain", "Rauleswor-desert", "Rauleswor-hill",
        "Rauleswor-mountain-snow", "Revercombs-city", "Revercombs-hill",
        "Ruudskogen", "Sancassa-city", "Sandager-city", "Sandager-desert",
        "Sandager-hill", "Sandager-mountain", "Shenkursk-hill",
        "Shenkursk-mountain", "Sirevag-city", "Sirevag-desert", "Sirevag-hill",
        "Sirevag-mountain", "Sirevag-mountain-snow", "Spring",
        "Starlight-desert", "Starlight-hill", "Starlight-mountain", "Street 1",
        "Tailwinds-city", "Tailwinds-hill", "Tailwinds-mountain",
        "Teruoka-city", "Teruoka-hill", "Thornbridge-hill",
        "Thornbridge-mountain", "TiasNimbas-hill", "TiasNimbas-mountain",
        "Torovo-city", "Torovo-mountain", "Tsaritsani-hill", "Volcan-hill",
        "Volcan-mountain", "Watorowo-city", "Watorowo-hill",
        "Watorowo-mountain", "Westplex-hill", "Westplex-mountain", "Wheel 1",
        "Wheel 2", "Wildno-desert", "Wildno-hill", "Wildno-mountain",
        "Zbydniow-hill", "Zdzieszulice-hill", "Zdzieszulice-mountain",
        "Zdzieszulice-mountain-snow", "Zihoun-hill", "Zongxoi-city",
        "Zongxoi-hill", "Zongxoi-mountain-snow", "Zorgvlied-hill",
        "Zorgvlied-mountain", "Zvolenovice-mountain"};
    public static final String[] ALL_WORKING_TRACKS = {"A-Speedway", "Aalborg", "Alpine 1",
        "Alpine 2", "Alsoujlak-hill", "Arraias-desert", "B-Speedway",
        "Berhet-hill", "Berhet-mountain", "Berhet-mountain-snow", "Brondehach",
        "Bukavu-hill", "Bukavu-mountain", "Bukavu-mountain-snow", "C-Speedway",
        "CG Speedway number 1", "CG track 2", "CG track 3", "Corkscrew",
        "D-Speedway", "Dirt 1", "Dirt 2", "Dirt 3", "Dirt 4", "Dirt 5",
        "Dirt 6", "E-Road", "E-Speedway", "E-Track 2", "E-Track 3",
        "E-Track 4", "E-Track 5", "E-Track 6", "F-Speedway", "Forza",
        "G-Speedway", "Kerang-desert", "Kumbharli-city", "Kumbharli-desert",
        "Kumbharli-hill", "Kumbharli-mountain-snow", "Makowiec-city",
        "Makowiec-desert", "Makowiec-hill", "Makowiec-mountain",
        "Makowiec-mountain-snow", "Michigan Speedway", "Mixed 1", "Mixed 2",
        "Mueda-city", "Noye-desert", "Noye-hill", "Noye-mountain",
        "Noye-mountain-snow", "Olethros Road 1", "Petrinja-desert",
        "Petrinja-hill", "Petrinja-mountain-snow", "Pinneo-city",
        "Pinneo-desert", "Pinneo-hill", "Pinneo-mountain",
        "Pinneo-mountain-snow", "Ponnesen-desert", "Ponnesen-hill",
        "Quedam-city", "Quedam-desert", "Quedam-hill", "Quedam-mountain",
        "Quedam-mountain-snow", "R491-city", "R491-desert", "R491-hill",
        "R491-mountain", "Rauleswor-desert", "Rauleswor-hill",
        "Rauleswor-mountain-snow", "Revercombs-city", "Revercombs-hill",
        "Ruudskogen", "Sancassa-city", "Sandager-city", "Sandager-desert",
        "Sandager-hill", "Sandager-mountain", "Shenkursk-hill",
        "Shenkursk-mountain", "Sirevag-city", "Sirevag-desert", "Sirevag-hill",
        "Sirevag-mountain", "Sirevag-mountain-snow", "Spring",
        "Starlight-desert", "Starlight-hill", "Starlight-mountain", "Street 1",
        "Tailwinds-city", "Tailwinds-hill", "Tailwinds-mountain",
        "Teruoka-city", "Teruoka-hill", "Thornbridge-hill",
        "Thornbridge-mountain", "TiasNimbas-hill", "TiasNimbas-mountain",
        "Torovo-city", "Torovo-mountain", "Tsaritsani-hill", "Volcan-hill",
        "Volcan-mountain", "Watorowo-city", "Watorowo-hill",
        "Watorowo-mountain", "Westplex-hill", "Westplex-mountain", "Wheel 1",
        "Wheel 2", "Wildno-desert", "Wildno-hill", "Wildno-mountain",
        "Zbydniow-hill", "Zdzieszulice-hill", "Zdzieszulice-mountain",
        "Zdzieszulice-mountain-snow", "Zihoun-hill", "Zongxoi-city",
        "Zongxoi-hill", "Zongxoi-mountain-snow", "Zorgvlied-hill",
        "Zorgvlied-mountain", "Zvolenovice-mountain"};
    private ArrayList<TrackData> list = new ArrayList<>();

    public TrackDataList() {
        try {
            InputStream in = getClass().getResourceAsStream("/de/janquadflieg/mrracer/data/torcstrackdata");

            InputStreamReader r = new InputStreamReader(in);
            BufferedReader b = new BufferedReader(r);
            String line = b.readLine();

            while (line != null) {
                TrackData data = new TrackData();
                data.readMetaData(line);
                //System.out.println(data);
                list.add(data);
                line = b.readLine();
            }

            b.close();

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
    
    public Iterator<TrackData> getAll(){
        return list.iterator();
    }

    public int getTrackIndex(TrackData data) {
        return list.indexOf(data);
    }

    public int getTrackIndex(String track) {
        return list.indexOf(getTrack(track));
    }

    public TrackData getTrackByControllerName(String s) {
        for (TrackData d : list) {
            if (d.controllerTrackName.equalsIgnoreCase(s)) {
                return d;
            }
        }
        return null;
    }

    public TrackData getTrack(String s) {
        for (TrackData d : list) {
            if (d.trackName.equalsIgnoreCase(s)) {
                return d;
            }
        }
        return null;
    }

    public String[] listTracks(TrackMatcher m, boolean output) {
        ArrayList<String> names = new ArrayList<>();

        for (TrackData d : list) {
            
            if (m.matches(d)) {
                names.add(d.trackName);
            }
        }
        if (output) {
            System.out.println(names.size() + " Tracks");
        }
        Collections.sort(names);
        if (output) {
            for (String s : names) {
                System.out.print(s + ";");
            }
            System.out.println("");
            System.out.print("String[] TRACKS = {");
            for (String s : names) {
                System.out.print("\"" + s + "\",");
            }
            System.out.println("};");
            System.out.print("Torcs.TRACKS = ");
            for(int i=0; i < names.size(); ++i){
                System.out.print((i>0?",":"")+names.get(i));                
            }
            System.out.println("");
        }


        String[] result = new String[names.size()];
        for (int i = 0; i < result.length; ++i) {
            result[i] = names.get(i);
        }
        return result;
    }

    public String[] listTracks(String[] categories, boolean output) {
        ArrayList<String> names = new ArrayList<>();
        for (TrackData d : list) {
            boolean match = false;
            for (String category : categories) {
                if (category.equalsIgnoreCase(d.torcsCategory)) {
                    match = true;
                }
            }
            if (match) {
                names.add(d.trackName);
            }
        }
        if (output) {
            System.out.println(names.size() + " Tracks");
        }
        Collections.sort(names);
        if (output) {
            for (String s : names) {
                System.out.print(s + ";");
            }
            System.out.println("");
            System.out.print("String[] TRACKS = {");
            for (String s : names) {
                System.out.print("\"" + s + "\",");
            }
            System.out.println("};");
            System.out.println("");
            System.out.print("Torcs.TRACKS = ");
            for(int i=0; i < names.size(); ++i){
                System.out.print((i>0?",":"")+names.get(i));                
            }
            System.out.println("");
        }

        String[] result = new String[names.size()];
        for (int i = 0; i < result.length; ++i) {
            result[i] = names.get(i);
        }
        return result;
    }

    public void listTracks() {
        ArrayList<String> names = new ArrayList<>();
        for (TrackData d : list) {
            names.add(d.trackName);
        }
        Collections.sort(names);
        for (String s : names) {
            System.out.print(s + ";");
        }
        System.out.println("");
        System.out.print("String[] TRACKS = {");
        for (String s : names) {
            System.out.print("\"" + s + "\",");
        }
        System.out.println("};");
    }

    public static void main(String[] args) {
        System.out.print(TRACKS_TCIAIG2014[0]);
        for (int i = 1; i < TRACKS_TCIAIG2014.length; ++i) {
            System.out.print("," + TRACKS_TCIAIG2014[i]);
        }
        System.out.println("");


        // Streckenliste
//        TrackDataList list = new TrackDataList();
//        //list.listTracks();
//        String[] tracks = list.listTracks(new String[]{"road", "dirt", "oval"}, true);
//        System.out.println("name;category;tname;cname;length;width;minf;wf;maxf;minrr;wrr;maxrr;minr;wr;maxr;minrw;wrw;maxrw");
//        for (String s : tracks) {
//            System.out.println(list.getTrack(s).toShortAnalyticalString());
//        }

        // Dump der Streckendaten
        TrackDataList list = new TrackDataList();
        System.out.println(TrackDataList.TRACKS_TCIAIG2014.length + " tracks");
        System.out.println("name;category;tname;cname;length;width;minf;wf;maxf;minrr;wrr;maxrr;minr;wr;maxr;minrw;wrw;maxrw;source");
        for (String s : TrackDataList.TRACKS_TCIAIG2014_REPEAT) {
            System.out.print(list.getTrack(s).toShortAnalyticalString());
            if (Arrays.asList(TrackDataList.TRACKS_TCIAIG2014_SCRC).contains(s)) {
                System.out.println(";SCRC");

            } else if (Arrays.asList(TrackDataList.TRACKS_TORCS_134).contains(s)) {
                System.out.println(";TORCS");

            } else {
                System.out.println(";PACK");
            }
        }
        
        list.listTracks(new WhiteBlackList(new String[]{"mountain"},new String[]{"snow"}), true);
        list.listTracks(new WhiteBlackList(new String[]{"snow"}, null), true);
        list.listTracks(new WhiteBlackList(new String[]{"city"}, null), true);
        list.listTracks(new WhiteBlackList(new String[]{"desert"}, null), true);
        list.listTracks(new WhiteBlackList(new String[]{"hill"}, null), true);
        
        System.out.println("------- City Surface ---------");
        list.listTracks(new SurfaceFilter(1.2, 0.001, 0.0, 1.0), true);
        
        System.out.println("------- Desert Surface ---------");
        list.listTracks(new SurfaceFilter(0.85, 0.005, 0.02, 30.0), true);
        
        System.out.println("------- Hill Surface ---------");
        list.listTracks(new SurfaceFilter(1.15, 0.001, 0.0, 1.0), true);
        
        System.out.println("------- Mountain Surface ---------");
        list.listTracks(new SurfaceFilter(0.9, 0.12, 0.02, 4.0), true);
        
        
        
        list.listTracks(new String[]{"jqtest"}, true);
        
        System.out.println(list.getTrack("Overtaking SW 15 Mountain"));
        
        //SurfaceData hill = SurfaceData.createHillReference();
        
        Iterator<TrackData> it = list.getAll();
        
        while(it.hasNext()){
            TrackData data = it.next();
            
            SurfaceData bestMatch = null;
            double distance = Double.POSITIVE_INFINITY;
            
            for(SurfaceData reference: SurfaceData.getAllReferences()){
                double d = data.calcSurfaceDifference(reference);
                if(d < distance){
                    bestMatch = reference;
                    distance = d;
                }
            }
            
            System.out.println(data.trackName+" "+Utils.dTS(distance)+" "+bestMatch.name);
        }

        // Teststrecken fuer Wiederholungen
//        ArrayList<String> list = new ArrayList<>(Arrays.asList(TrackDataList.TRACKS_TCIAIG2014));
//        String result = "String[] TRACKS_TCIAIG2014_REPEAT = {";
//        result += "\"Forza\"";
//        list.remove("Forza");
//        result += ",\"Wheel 2\"";
//        list.remove("Wheel 2");
//        result += ",\"C-Speedway\"";
//        list.remove("C-Speedway");
//        result += ",\"Kerang-desert\"";
//        list.remove("Kerang-desert");
//        result += ",\"Mueda-city\"";
//        list.remove("Mueda-city");
//
//        for (int i = 0; i < 10; ++i) {
//            int index = (int)Math.floor(Math.random()*list.size());
//            result +=",\""+list.get(index)+"\"";
//            list.remove(index);
//        }
//        System.out.println(result);
    }
}
