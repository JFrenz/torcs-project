/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.torcs;

import java.util.*;

/**
 * Class to gather data from torcs race track xml files.
 *
 * @author quad
 */
public class TrackData {
    /**
     * Width of the track.
     */
    public double width;
    /**
     * Length of the track.
     */
    public double length;
    /**
     * Track name as read from the xml file.
     */
    public String trackName;
    /**
     * Track name from the file system (track directory name).
     */
    public String torcsTrackName;
    /**
     * Category, same as the directory name.
     */
    public String torcsCategory;
    /**
     * Controller friendly name without any special characters.
     */
    public String controllerTrackName;
    /**
     * Surfaces and length of the track which uses the surface.
     */
    public HashMap<SurfaceData, Double> surfaces = new HashMap<>();    

    public void writeMetaData(java.io.OutputStreamWriter w)
            throws Exception {
        w.write(trackName + ";");
        w.write(torcsCategory + ";");
        w.write(torcsTrackName + ";");
        w.write(controllerTrackName + ";");
        w.write(String.valueOf(length) + ";");
        w.write(String.valueOf(width) + ";");
        w.write(String.valueOf(surfaces.keySet().size()) + ";");
        for (SurfaceData surface : surfaces.keySet()) {
            w.write(surface.name + ";");
            w.write(String.valueOf(surface.friction) + ";");
            w.write(String.valueOf(surface.rollingResistance) + ";");
            w.write(String.valueOf(surface.roughness) + ";");
            w.write(String.valueOf(surface.roughnessWavelength) + ";");
            w.write(String.valueOf(surfaces.get(surface)) + ";");
        }        
    }
    
    public void readMetaData(String line)
            throws Exception {        
        StringTokenizer tokenizer = new StringTokenizer(line, ";");
        trackName = tokenizer.nextToken();
        torcsCategory = tokenizer.nextToken();
        torcsTrackName = tokenizer.nextToken();
        controllerTrackName = tokenizer.nextToken();
        length = Double.parseDouble(tokenizer.nextToken());
        width = Double.parseDouble(tokenizer.nextToken());
        int num = Integer.parseInt(tokenizer.nextToken());
        for(int i=0; i < num; ++i){
            SurfaceData surface = new SurfaceData();
            surface.name = tokenizer.nextToken();
            surface.friction = Double.parseDouble(tokenizer.nextToken());
            surface.rollingResistance = Double.parseDouble(tokenizer.nextToken());
            surface.roughness = Double.parseDouble(tokenizer.nextToken());
            surface.roughnessWavelength = Double.parseDouble(tokenizer.nextToken());
            double d = Double.parseDouble(tokenizer.nextToken());
            surfaces.put(surface, d);
        }
//        for (SurfaceData surface : surfaces.keySet()) {
//            w.write(surface.name + ";");
//            w.write(String.valueOf(surface.friction) + ";");
//            w.write(String.valueOf(surface.rollingResistance) + ";");
//            w.write(String.valueOf(surface.roughness) + ";");
//            w.write(String.valueOf(surface.roughnessWavelength) + ";");
//            w.write(String.valueOf(surfaces.get(surface)) + ";");
//        }
//        w.write("\n");
    }
    
    public double getMinimumFriction(){
        double result = Double.POSITIVE_INFINITY;
        
        for (SurfaceData surface : surfaces.keySet()) {
            result = Math.min(result, surface.friction);
        }
        
        return result;
    }
    
    public double getMaximumFriction(){
        double result = Double.NEGATIVE_INFINITY;
        
        for (SurfaceData surface : surfaces.keySet()) {
            result = Math.max(result, surface.friction);
        }
        
        return result;
    }

    public double getWeightedFriction() {
        double result = 0.0;
        for (SurfaceData surface : surfaces.keySet()) {
            result += surface.friction * surfaces.get(surface).doubleValue();
        }
        result /= length;
        return result;
    }
    
    public double getMinimumRollingResistance(){
        double result = Double.POSITIVE_INFINITY;
        
        for (SurfaceData surface : surfaces.keySet()) {
            result = Math.min(result, surface.rollingResistance);
        }
        
        return result;
    }
    
    public double getMaximumRollingResistance(){
        double result = Double.NEGATIVE_INFINITY;
        
        for (SurfaceData surface : surfaces.keySet()) {
            result = Math.max(result, surface.rollingResistance);
        }
        
        return result;
    }

    public double getWeightedRollingResistance() {
        double result = 0.0;
        for (SurfaceData surface : surfaces.keySet()) {
            result += surface.rollingResistance * surfaces.get(surface).doubleValue();
        }
        result /= length;
        return result;
    }
    
    public double getMinimumRoughness(){
        double result = Double.POSITIVE_INFINITY;
        
        for (SurfaceData surface : surfaces.keySet()) {
            result = Math.min(result, surface.roughness);
        }
        
        return result;
    }
    
    public double getMaximumRoughness(){
        double result = Double.NEGATIVE_INFINITY;
        
        for (SurfaceData surface : surfaces.keySet()) {
            result = Math.max(result, surface.roughness);
        }
        
        return result;
    }

    public double getWeightedRoughness() {
        double result = 0.0;
        for (SurfaceData surface : surfaces.keySet()) {
            result += surface.roughness * surfaces.get(surface).doubleValue();
        }
        result /= length;
        return result;
    }
    
    public double getMinimumRoughnessWavelength(){
        double result = Double.POSITIVE_INFINITY;
        
        for (SurfaceData surface : surfaces.keySet()) {
            result = Math.min(result, surface.roughnessWavelength);
        }
        
        return result;
    }
    
    public double getMaximumRoughnessWavelength(){
        double result = Double.NEGATIVE_INFINITY;
        
        for (SurfaceData surface : surfaces.keySet()) {
            result = Math.max(result, surface.roughnessWavelength);
        }
        
        return result;
    }
    
    public double calcSurfaceDifference(SurfaceData other){
        double result = 0.0;
        
        result += Math.abs(getWeightedFriction()-other.friction);
        result += Math.abs(getWeightedRollingResistance()-other.rollingResistance);
        
        if(getWeightedRoughness() != 0.0 || other.roughness != 0.0){
            double normalizedWavelength = this.getWeightedRoughnessWavelength()/30.0;
            double otherNormalizedWavelength = other.roughnessWavelength/30.0;
            if(getWeightedRoughness() != 0.0 && other.roughness != 0.0){
                result += Math.abs(getWeightedRoughness()-other.roughness);
                result += Math.abs(normalizedWavelength-otherNormalizedWavelength);
                
            } else if(getWeightedRoughness() == 0.0 && other.roughness != 0.0){
                result += Math.abs(other.roughness);
                result += Math.abs(otherNormalizedWavelength);
                
            } else if(getWeightedRoughness() != 0.0 && other.roughness == 0.0){
                result += Math.abs(getWeightedRoughness());
                result += Math.abs(normalizedWavelength);                        
            }            
        }
        
        return result;
    }

    public double getWeightedRoughnessWavelength() {
        double result = 0.0;
        for (SurfaceData surface : surfaces.keySet()) {
            result += surface.roughnessWavelength * surfaces.get(surface).doubleValue();
        }
        result /= length;
        return result;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append(trackName).append(";");
        result.append(torcsCategory).append(";");
        result.append(torcsTrackName).append(";");
        result.append(controllerTrackName).append(";");
        result.append(length).append(";");
        result.append(width).append(";");
        result.append(surfaces.keySet().size()).append(";");
        for (SurfaceData surface : surfaces.keySet()) {
            result.append(surface.name).append(";");
            result.append(surface.friction).append(";");
            result.append(surface.rollingResistance).append(";");
            result.append(surface.roughness).append(";");
            result.append(surface.roughnessWavelength).append(";");
            result.append(surfaces.get(surface)).append(";");
        }       

        return result.toString();
    }
    
    public static String getAnalyticalStringHeader(){
        StringBuilder result = new StringBuilder();
        
        result.append("trackName").append(";");
        result.append("torcsCategory").append(";");
        result.append("torcsTrackName").append(";");
        result.append("controllerTrackName").append(";");
        result.append("length").append(";");
        result.append("width").append(";");
        // friction
        result.append("MinimumFriction").append(";");
        result.append("WeightedFriction").append(";");
        result.append("MaximumFriction").append(";");
        // rolling resistance
        result.append("MinimumRollingResistance").append(";");
        result.append("WeightedRollingResistance").append(";");
        result.append("MaximumRollingResistance").append(";");
        // roughness
        result.append("MinimumRoughness").append(";");
        result.append("WeightedRoughness").append(";");
        result.append("MaximumRoughness").append(";");
        // roughness wavelength
        result.append("MinimumRoughnessWavelength").append(";");
        result.append("WeightedRoughnessWavelength").append(";");
        result.append("MaximumRoughnessWavelength()");        
        
        return result.toString();
    }
    
    public String toShortAnalyticalString(){
        StringBuilder result = new StringBuilder();
        
        result.append(trackName).append(";");
        result.append(torcsCategory).append(";");
        result.append(torcsTrackName).append(";");
        result.append(controllerTrackName).append(";");
        result.append(length).append(";");
        result.append(width).append(";");
        // friction
        result.append(getMinimumFriction()).append(";");
        result.append(getWeightedFriction()).append(";");
        result.append(getMaximumFriction()).append(";");
        // rolling resistance
        result.append(getMinimumRollingResistance()).append(";");
        result.append(getWeightedRollingResistance()).append(";");
        result.append(getMaximumRollingResistance()).append(";");
        // roughness
        result.append(getMinimumRoughness()).append(";");
        result.append(getWeightedRoughness()).append(";");
        result.append(getMaximumRoughness()).append(";");
        // roughness wavelength
        result.append(getMinimumRoughnessWavelength()).append(";");
        result.append(getWeightedRoughnessWavelength()).append(";");
        result.append(getMaximumRoughnessWavelength());        
        
        return result.toString();
    }

    public static class Segment {

        /**
         * Surface used in this segment.
         */
        public String surface;
        /**
         * Direction of this segment.
         */
        public String direction;
        /**
         * Length of this segment.
         */
        public double length;
        /**
         * Arc, if this is a corner.
         */
        public double arc;
        /**
         * Radius, if this is a corner.
         */
        public double radius = -1;
        /**
         * End radius, if this is a corner with varying radius.
         */
        public double endRadius = -1;
        /**
         * Profil steps length.
         */
        public double profilStepsLength = -1;
        /**
         * Steps.
         */
        public int steps = 1;
        /**
         * Name.
         */
        public String name = "";

        public void calcLength(double globalStepsLength) {
            if (direction.equalsIgnoreCase("str")) {
                // nothing todo
            } else if (endRadius == -1 || endRadius == radius) {
                length = 2.0 * Math.PI * radius / 360.0 * arc;

            } else {
                double arcRad = arc / 180.0 * Math.PI;
                length = (radius + endRadius) / 2.0 * arcRad;
                //System.out.println("Length: "+length);
                if (steps == 1) {
                    //  System.out.println(profilStepsLength+" - "+globalStepsLength);
                    double stepslg = profilStepsLength != -1 ? profilStepsLength : globalStepsLength;
                    //System.out.println("Step length: "+stepslg);
                    if (stepslg != 0.0) {
                        steps = (int) (length / stepslg) + 1;
                    } else {
                        steps = 1;
                    }
                }

                //double curArc = arcRad / (double) steps;
                float curLength = (float) length / (float) steps;
                if (steps != 1) {
                    float dRadius = (float) (endRadius - radius) / (float) (steps - 1);
                    float tmpAngle = 0.0f;
                    float tmpRadius = (float) radius;
                    for (int curStep = 0; curStep < steps; ++curStep) {
                        tmpAngle += curLength / tmpRadius;
                        tmpRadius += dRadius;
                    }
                    curLength *= arcRad / tmpAngle;
                }
                //System.out.println(name+" Current Length: "+curLength+", step: "+
                //       steps+", overall = "+(curLength * steps));
                length = curLength * steps;
            }
        }
    }
}
