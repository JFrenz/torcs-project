/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.torcs;

import java.util.Objects;

/**
 * Class to gather surface data from torcs race track xml files.
 * @author quad
 */
public class SurfaceData {

    public String name = "";
    public double friction = 0.0;
    public double rollingResistance = 0.0;
    public double roughness = 0.0;
    public double roughnessWavelength = -19;

    @Override
    public boolean equals(Object o) {
        if (o instanceof SurfaceData) {
            return name.equalsIgnoreCase(((SurfaceData)o).name);            
            
        } else {
            return false;
        }        
    }
    
    public static SurfaceData createCityReference(){
        SurfaceData result = new SurfaceData();
        result.name = "CityReferenceSurface";
        result.friction = 1.2;
        result.rollingResistance = 0.001;
        result.roughness = 0.0;
        result.roughnessWavelength = 1.0;        
        return result;
    }
    
    public static SurfaceData createDesertReference(){
        SurfaceData result = new SurfaceData();
        result.name = "DesertReferenceSurface";
        result.friction = 0.85;
        result.rollingResistance = 0.005;
        result.roughness = 0.02;
        result.roughnessWavelength = 30.0;
        return result;
    }
    
    public static SurfaceData createHillReference(){
        SurfaceData result = new SurfaceData();
        result.name = "HillReferenceSurface";        
        result.friction = 1.15;
        result.rollingResistance = 0.001;
        result.roughness = 0.0;
        result.roughnessWavelength = 1.0;
        return result;
    }
    
    public static SurfaceData createMountainReference(){
        SurfaceData result = new SurfaceData();
        result.name = "MountainReferenceSurface";        
        result.friction = 0.9;
        result.rollingResistance = 0.12;
        result.roughness = 0.02;
        result.roughnessWavelength = 4.0;        
        return result;
    }
    
    public static SurfaceData[] getAllReferences(){
        SurfaceData[] result = new SurfaceData[]{
            createCityReference(),
            createDesertReference(),
            createHillReference(),
            createMountainReference()
        };
        
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append("Surface ").append(name).append(":");
        result.append(" Friction=").append(friction);
        result.append(", RollingResistance=").append(rollingResistance);
        result.append(", Roughness=").append(roughness);
        result.append(", RoughnessWavelength=").append(roughnessWavelength);

        return result.toString();
    }
}