/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo.tools;

import java.io.*;

/**
 *
 * @author quad
 */
public class ExperimentCleanUp {   
    
    private static void deleteRecursive(File path){
        File[] c = path.listFiles();
        //System.out.println("Cleaning out folder:" + path.toString());
        for (File file : c){
            if (file.isDirectory()){
                //System.out.println("Deleting file:" + file.toString());
                deleteRecursive(file);
                file.delete();
            } else {
                file.delete();
            }
        }

        path.delete();
  }
    
    public static void run()
    throws Exception{
        File currentDir = new File(".");
        File[] files = currentDir.listFiles();        
        
        for(File subDir: files){
            if(!subDir.isDirectory() || (subDir.isDirectory() && subDir.getName().equals("lib"))){
                continue;
            }
            System.out.print("Checking directory \""+subDir.getName()+"\"...");
            boolean used = false;
            File[] dirContents = subDir.listFiles();
            for(int i=0; i < dirContents.length && !used; ++i){
                used = dirContents[i].isFile() && dirContents[i].getName().endsWith(".log");
                if(used){
                    System.out.print(" found ea log file: "+dirContents[i].getName());
                }
            }
            if(used){
                System.out.println(" ok");                
                
            } else {
                System.out.println(" not used, deleting");
                ExperimentCleanUp.deleteRecursive(subDir);
            }
        }        
    }
    
    public static void main(String[] args){
        try{
            ExperimentCleanUp.run();
            
        } catch(Exception e){
            e.printStackTrace(System.out);
        }
        
    }    
}
