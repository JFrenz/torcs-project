/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo;

import java.io.InputStream;
import java.util.ArrayList;

/**
 *
 * @author quad
 */
public class TorcsStreamReader
        implements Runnable {

    private String name;
    private InputStream in;
    private boolean run = true;
    private boolean reportNAN = true;
    private ArrayList<RaceStateListener> listeners = new ArrayList<>();
    private boolean TEXT_OUT;

    public TorcsStreamReader(InputStream in, String s, boolean text) {
        this.in = in;
        this.name = s;
        TEXT_OUT = text;
    }

    public void addListener(RaceStateListener l) {
        listeners.add(l);
    }

    public void removeListener(RaceStateListener l) {
        listeners.remove(l);
    }

    private void notifyWaitingForRequest() {
        for (RaceStateListener l : listeners) {
            l.waitingForRequest();
        }
    }

    private void notifyRaceEnded() {
        for (RaceStateListener l : listeners) {
            l.raceEnded();
        }
    }

    private void notifyTimeout() {
        for (RaceStateListener l : listeners) {
            l.timeout();
            
        }
    }
    
    private void notifyReachedMaxTicks() {
        for (RaceStateListener l : listeners) {
            l.reachedMaxTicks();
        }
    }

    private void notifyDistance(double d) {
        for (RaceStateListener l : listeners) {
            l.distanceRaced(d);
        }
    }
    
    private void notifyDamage(double d) {
        for (RaceStateListener l : listeners) {
            l.damage(d);
        }
    }
    
    private void notifyTicks(int i) {
        for (RaceStateListener l : listeners) {
            l.ticks(i);

        }
    }
    
    private void notifyOverallTime(double d) {
        for (RaceStateListener l : listeners) {
            l.overallTime(d);
        }
    }

    private void notifyLapTime(int lap, double time, boolean b) {
        for (RaceStateListener l : listeners) {
            l.lapTime(lap, time, b);
        }
    }

    @Override
    public void run() {
        if (TEXT_OUT) {
            System.out.println("[TORCS " + name + "] Streamreader running");
        }
        String line = "";
        try {
            while (run) {
                if (name.contains("cerr")) {
                    int available = in.available();
                    if (available == 0) {
                        Thread.sleep(500);
                        continue;
                    }
                }
                /*if(name.equals("1-cerr")){
                 System.out.println("vor read");
                 }*/
                int i = in.read();
                /*if(name.equals("1-cerr")){
                 System.out.println("read: "+i);
                 }*/
                if (i != -1) {
                    char c = (char) i;
                    if (c == '\n') {
                        if (TEXT_OUT && !line.startsWith("Dist_raced") &&
                                !line.startsWith("Damage") &&
                                !line.startsWith("Overall_time") &&
                                !line.startsWith("Ticks")) {
                            System.out.println("[TORCS " + name + "] " + line);
                        }
                        //System.out.println(line);
                        if (line.contains("Timeout for client answer")) {
                            //System.out.println(line);
                            // todo? count?/
                            notifyTimeout();

                        } else if (line.startsWith("Waiting for request")) {
                            notifyWaitingForRequest();

                        } else if (line.startsWith("lap")) {
                            try {
                                //System.out.println(line.substring(line.indexOf(' '), line.indexOf(':')));
                                //System.out.println(line.substring(line.indexOf(':') + 1));
                                notifyLapTime(Integer.parseInt(line.substring(line.indexOf(' '), line.indexOf(':')).trim()), Double.parseDouble(line.substring(line.indexOf(':') + 1).trim()), false);
                            } catch (NumberFormatException e) {
                                if (reportNAN) {
                                    System.out.println("[TORCS " + name + "] NumberFormatException" + line);
                                    reportNAN = false;
                                }
                            }
                            
                        } else if(line.startsWith("Ticks")) {
                            notifyTicks(Integer.parseInt(line.substring(line.indexOf(' ') + 1).trim()));     
                            
                        } else if(line.startsWith("Damage")){
                            try {                                
                                notifyDamage(Double.parseDouble(line.substring(line.indexOf(' ') + 1).trim()));
                                
                            } catch (NumberFormatException e) {                                
                            }
                            
                        } else if(line.startsWith("Overall_time")){
                            try {                                
                                notifyOverallTime(Double.parseDouble(line.substring(line.indexOf(' ') + 1).trim()));
                                
                            } catch (NumberFormatException e) {                                
                            }
                            
                        } else if(line.startsWith("Reached Tick Limit")){
                            notifyReachedMaxTicks();

                        } else if (line.startsWith("Sim Time")) {
                            //System.out.println(line.substring(line.indexOf(':')+1, line.indexOf('[')).trim());
                            //System.out.println(line.substring(line.indexOf("Laps:")+5, line.indexOf(',', line.indexOf("Laps:"))).trim());
                            try {
                                int lap = Integer.parseInt(line.substring(line.indexOf("Laps:") + 5, line.indexOf(',', line.indexOf("Laps:"))).trim());
                                double time = Double.parseDouble(line.substring(line.indexOf(':') + 1, line.indexOf('[')).trim());
                                notifyLapTime(lap, time, true);
                            } catch (NumberFormatException e) {
                                if (reportNAN) {
                                    System.out.println("[TORCS " + name + "] NumberFormatException" + line);
                                    reportNAN = false;
                                }
                            }

                        } else if (line.startsWith("Winner")) {
                            //System.out.println("Race ended normally");
                            notifyRaceEnded();

                        } else if (line.startsWith("Dist_raced")) {
                            try {
                                double distance = Double.parseDouble(line.substring(10));
                                //System.out.println("Distance: "+distance);
                                notifyDistance(distance);

                            } catch (NumberFormatException e) {
                                if (reportNAN) {
                                    System.out.println("[TORCS " + name + "] NumberFormatException" + line);
                                    reportNAN = false;
                                }
                            }

                        } else if (line.endsWith("cannot bind socket")) {
                            System.out.println("Cannot bind socket!");
                        }
                        line = "";
                    } else if (c == '\r') {
                        //System.out.println("cr");
                    } else {
                        line += c;
                    }
                } else {
                    /*if(name.equals("1-cerr")){
                     System.out.println("Got -1, stopping thread");
                     }*/
                    run = false;
                }
            }
        } catch (Exception e) {
            System.out.println("[TORCS " + name + "] " + e.getMessage());
            if (TEXT_OUT) {
                e.printStackTrace(System.out);
            }
        }
        if (TEXT_OUT) {
            System.out.println("[TORCS " + name + "] Stream Reader terminated, " + run);
        }
    }

    public void stop() {
        run = false;
        try {
            in.close();
        } catch (Exception e) {
            if (TEXT_OUT) {
                e.printStackTrace(System.out);
            }
        }
    }
}
