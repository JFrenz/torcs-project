/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo;

/**
 *
 * @author Jonas
 */
public class CarData {
    
    public static final String[] TRB = {"car1-trb1","car2-trb1",
        "car3-trb1","car4-trb1","car5-trb1","car6-trb1","car7-trb1",
        "car8-trb1","car1-trb3"};
    public static final String[] KC = {"kc-2000gt","kc-5300gt",
        "kc-a110","kc-alfatz2","kc-bigh","kc-cobra","kc-coda",
        "kc-conrero","kc-corvette-ttop","kc-daytona","kc-db4z",
        "kc-dbs","kc-dino","kc-ghibli","kc-giulietta","kc-grifo",
        "kc-gt40","kc-gto","kc-p4"};
    public static final String[] PW = {"pw-206wrc","pw-306wrc",
        "pw-corollawrc","pw-evoviwrc","pw-focuswrc","pw-imprezawrc"};
    public static final String[] OTHER = {"155-DTM","acura-nsx-sz",
        "baja-bug","buggy","car1-ow1","car1-stock1","p406"};
    public static final String[] ALL = {"car1-trb1","car2-trb1",
        "car3-trb1","car4-trb1","car5-trb1","car6-trb1","car7-trb1",
        "car8-trb1","car1-trb3","kc-2000gt","kc-5300gt",
        "kc-a110","kc-alfatz2","kc-bigh","kc-cobra","kc-coda",
        "kc-conrero","kc-corvette-ttop","kc-daytona","kc-db4z",
        "kc-dbs","kc-dino","kc-ghibli","kc-giulietta","kc-grifo",
        "kc-gt40","kc-gto","kc-p4","pw-206wrc","pw-306wrc",
        "pw-corollawrc","pw-evoviwrc","pw-focuswrc","pw-imprezawrc",
        "155-DTM","acura-nsx-sz","baja-bug","buggy","car1-ow1",
        "car1-stock1","p406"};

    private String carName;
    
    public CarData(String name){
        for (String c : ALL) {
            if (c.equalsIgnoreCase(name)) {
                carName = c;
            }
        }
    }
    
    public String getCarName(){
        return carName;
    }
            
}
