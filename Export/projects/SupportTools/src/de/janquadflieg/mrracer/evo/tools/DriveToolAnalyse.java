/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo.tools;

import de.janquadflieg.mrracer.evo.DriveTool2014Torcs134;
import java.io.*;

import de.janquadflieg.mrracer.evo.tools.TestScenario;
import de.janquadflieg.mrracer.evo.EvaluationType;
import de.janquadflieg.mrracer.evo.tools.IOUtils;
import de.janquadflieg.mrracer.evo.tools.TestScenario;
import de.janquadflieg.mrracer.torcs.TrackData;
import de.janquadflieg.mrracer.torcs.TrackDataList;
import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;
import scr.Controller;

/**
 *
 * @author quad
 */
public class DriveToolAnalyse {

    private static TrackDataList trackDataList = new TrackDataList();

    private static void appendResults(File resultsSrc, String resultsDst,
            String rowName, ArrayList<String> trackList)
            throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(resultsSrc), "UTF-8"));
        OutputStreamWriter writer = new OutputStreamWriter(
                new FileOutputStream("./" + resultsDst, true), "UTF-8");

        writer.write(rowName);

        String line = reader.readLine();
        int lineCtr = 0;

        while ((line = reader.readLine()) != null) {
            StringTokenizer tokenizer = new StringTokenizer(line, ";");
            String track = tokenizer.nextToken();
            if (!track.equalsIgnoreCase(trackList.get(lineCtr))) {
                System.out.println("Error: Expected result for track "
                        + trackList.get(lineCtr) + " in line " + lineCtr
                        + " but found " + track);
                System.exit(1);
            }

            String result = tokenizer.nextToken();
            writer.write(";" + result);

            ++lineCtr;
        }
        writer.write("\n");
        reader.close();
        writer.close();
    }

    private static String createTrackModelLearningTimeFileName(boolean noisy) {
        String fileName = "warmup_time_trackmodel" + (noisy ? "-noisy" : "") + ".csv";
        return fileName;
    }

    private static String createResultsFileName(TestScenario scenario,
            boolean noisy, EvaluationType eType, String suffix) {
        String fileName = scenario.getPropertiesName() + "_"
                + DriveTool2014Torcs134.createResultsFileName(noisy, eType, suffix);
        return fileName;
    }

    private static String createDamageFileName(TestScenario scenario,
            boolean noisy, EvaluationType eType, String suffix) {
        String fileName = scenario.getPropertiesName() + "_"
                + DriveTool2014Torcs134.createDamageFileName(noisy, eType, suffix);
        return fileName;
    }

    private static void initResultsFile(String name, ArrayList<String> trackList)
            throws Exception {
        OutputStreamWriter writer = new OutputStreamWriter(
                new FileOutputStream("./" + name), "UTF-8");
        writer.write("variant");
        for (String track : trackList) {
            writer.write(";" + track);
        }
        writer.write("\n");
        writer.close();
    }

    private static void analyse(TestScenario scenario, boolean noisy,
            EvaluationType eType, String suffix)
            throws Exception {
        File scenarioDirectory = new File("./" + scenario.getDirectoryName());
        File[] directoryContents = scenarioDirectory.listFiles();
        boolean initResultFile = true;
        ArrayList<String> trackList = new ArrayList<>();
        ArrayList<String> variantList = new ArrayList<>();

        // check, if there are any results of the given type at all
        boolean resultsAvailable = false;
        for (File variant : directoryContents) {
            if (!variant.isDirectory()) {
                continue;
            }
            variantList.add(variant.getName());

            File resultsFile = new File(variant.getAbsolutePath() + "/MrRacer/" + DriveTool2014Torcs134.createResultsFileName(noisy, eType, suffix));
            if (resultsFile.exists()) {
                resultsAvailable = true;
            }
        }

        if (!resultsAvailable) {
            System.out.println("No results available for scenario " + scenario + ", "
                    + eType + ", noisy: " + noisy+", suffix: "+suffix);
            return;
        } else {
            System.out.println("Merging results for scenario " + scenario + ", "
                    + eType + ", noisy: " + noisy+", suffix: "+suffix);
        }

        java.util.Collections.sort(variantList, new java.util.Comparator<String>() {
            public int compare(String v1, String v2) {
                return v1.compareToIgnoreCase(v2);
            }
        });

        for (String variant : variantList) {
            File variantDirectory = new File(scenarioDirectory.getAbsolutePath()
                    + "/" + variant);

            if (initResultFile) {
                Properties templateProperties = new Properties();
                String propertiesFile = IOUtils.createToolScriptName(scenario, eType, Controller.Stage.QUALIFYING, noisy, suffix) + ".properties";
                FileInputStream in = new FileInputStream(variantDirectory.getAbsolutePath() + "/MrRacer/" + propertiesFile);
                templateProperties.load(in);
                in.close();

                StringTokenizer tokenizer = new StringTokenizer(templateProperties.getProperty("tracks"), ",");

                while (tokenizer.hasMoreTokens()) {
                    trackList.add(tokenizer.nextToken().trim());
                }

                initResultsFile(createResultsFileName(scenario, noisy, eType, suffix), trackList);
                initResultsFile(createDamageFileName(scenario, noisy, eType, suffix), trackList);

                if (scenario == TestScenario.RACE_WEEKEND && eType == EvaluationType.TIME_FOR_LAPS) {
                    initResultsFile(createTrackModelLearningTimeFileName(noisy), trackList);
                }

                initResultFile = false;
            }
            File resultsFile = new File(variantDirectory.getAbsolutePath() + "/MrRacer/" + DriveTool2014Torcs134.createResultsFileName(noisy, eType, suffix));
            File damageFile = new File(variantDirectory.getAbsolutePath() + "/MrRacer/" + DriveTool2014Torcs134.createDamageFileName(noisy, eType, suffix));

            if (!resultsFile.exists() || !damageFile.exists()) {
                System.out.println("*** WARNING ***");
                if (!resultsFile.exists()) {
                    System.out.println(resultsFile.getAbsolutePath() + " does not exist");
                }
                if (!damageFile.exists()) {
                    System.out.println(damageFile.getAbsolutePath() + " does not exist");
                }
                continue;
            }

            if (resultsFile.exists()) {
                appendResults(resultsFile, createResultsFileName(scenario, noisy, eType, suffix),
                        variant, trackList);
            }
            if (damageFile.exists()) {
                appendResults(damageFile, createDamageFileName(scenario, noisy, eType, suffix),
                        variant, trackList);
            }

            if (scenario == TestScenario.RACE_WEEKEND && eType == EvaluationType.TIME_FOR_LAPS) {
                OutputStreamWriter writer = new OutputStreamWriter(
                        new FileOutputStream("./" + createTrackModelLearningTimeFileName(noisy), true), "UTF-8");

                String rowName = variant;
                writer.write(rowName);

                for (String s : trackList) {
                    File warmupLogFile = new File(variantDirectory.getAbsolutePath() + "/MrRacer/"
                            + trackDataList.getTrack(s).controllerTrackName
                            + (noisy ? "-noisy" : "") + "-1_warmup_process.log");
                    if (!warmupLogFile.exists()) {
                        System.out.println("*** WARNING ***");
                        System.out.println(warmupLogFile.getAbsolutePath() + " does not exist");
                        writer.write(";0");
                        continue;
                    }
                    BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(warmupLogFile), "UTF-8"));
                    String line = reader.readLine();
                    boolean found = false;
                    while ((line = reader.readLine()) != null && !found) {
                        if (line.startsWith("Learning the trackmodel took")) {
                            String subString = line.substring("Learning the trackmodel took ".length());
                            double time = 0.0;
                            time += 60.0 * Double.parseDouble(subString.substring(0, subString.indexOf(":")));
                            time += Double.parseDouble(subString.substring(subString.indexOf(":") + 1));
                            if (time != 0.0) {
                                writer.write(";" + String.valueOf(time));
                                found = true;
                            }
                        }
                        if (line.startsWith("Waiting took")) {
                            String subString = line.substring("Waiting took ".length());
                            double time = 0.0;
                            time += 60.0 * Double.parseDouble(subString.substring(0, subString.indexOf(":")));
                            time += Double.parseDouble(subString.substring(subString.indexOf(":") + 1));
                            if (time != 0.0) {
                                writer.write(";" + String.valueOf(time));
                                found = true;
                            }
                        }
                    }
                    if (!found) {
                        //System.out.println("Variant: "+variantDirectory);
                        //System.out.println(s);
                        //System.exit(1);
                        writer.write(";2400");
                    }
                    reader.close();
                }
                writer.write("\n");
                writer.close();
            }
        }
    }

    public static void run(String s)
            throws Exception {
        TestScenario ts = TestScenario.valueOf(s);

        for (EvaluationType evalType : EvaluationType.values()) {
            for (boolean noisy : new boolean[]{true, false}) {
                String[] suffixes = (evalType == EvaluationType.TIME_FOR_LAPS)
                        ? new String[]{"2laps", "5laps"}
                        : new String[]{"10000ticks", "75000ticks"};

                for (String suffix : suffixes) {
                    analyse(ts, noisy, evalType, suffix);
                }
            }
        }
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Missing Parameter for the test scenario");
            System.out.println("Usage:");
            System.out.print("DriveToolAnalyse ");
            TestScenario[] scenarios = TestScenario.values();
            for (int i = 0; i < scenarios.length; ++i) {
                if (i > 0) {
                    System.out.print("|");
                }
                System.out.print(scenarios[i].name());
            }
            System.out.println("");
            System.exit(1);
        }
        try {
            DriveToolAnalyse.run(args[0]);

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

    }
}
