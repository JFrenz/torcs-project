/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo;

import de.janquadflieg.mrracer.torcs.*;

import java.io.File;
import java.util.HashMap;

/**
 *
 * @author quad
 */
public enum ControllerData {    

    MRRACERGECCO2013(" -Xmx2G -cp .;MrRacer.jar scr.Client de.janquadflieg.mrracer.controller.MrRacer2013",
    "MrRacerGecco2013", "MrRacerGecco2013", ControllerType.JAVA, "trackName:") {
    },
    MRRACERCIG2012(" -Xmx2G -cp .;MrRacer.jar scr.Client de.janquadflieg.mrracer.controller.MrRacer2012",
    "MrRacerCig2012", "MrRacerCig2012", ControllerType.JAVA, "trackName:") {
    },
    BERNIW("", "BerniW", "BerniW", ControllerType.NATIVE_TORCS, "trackName:") {
    },
    INFERNO("", "Inferno", "Inferno", ControllerType.NATIVE_TORCS, "trackName:") {
    },    
    TITA("", "Tita", "Tita", ControllerType.NATIVE_TORCS, "trackName:") {
    },
    BT("", "BT", "BT", ControllerType.NATIVE_TORCS, "trackName:") {
    },
    DAMNED("", "Damned", "Damned", ControllerType.NATIVE_TORCS, "trackName:") {
    },
    LLIAW("", "Lliaw", "Lliaw", ControllerType.NATIVE_TORCS, "trackName:") {
    },
    OLETHROS("", "Olethros", "Olethros", ControllerType.NATIVE_TORCS, "trackName:") {
    },
    MRRACER(" -Xmx2G -cp .;MrRacer.jar scr.Client de.janquadflieg.mrracer.controller.MrRacer2013",
    "MrRacer", "MrRacer", ControllerType.JAVA, "trackName:") {
    },
    GRNDRIVER2015(
    "client",
    "GRNDriver2015", "GRNDriver2015", ControllerType.JAVA, "track:") {
    },
    AUTOPIA2013(
    "client",
    "Autopia2013", "Autopia2013", ControllerType.EXECUTABLE, "trackName:") {
    },
    MRRACERGECCO2015(" -Xmx2G -cp .;MrRacer.jar scr.Client de.janquadflieg.mrracer.controller.MrRacer2013",
    "MrRacerGecco2015", "MrRacerGecco2015", ControllerType.JAVA, "trackName:") {
    },
    MARISCALFERNANDEZ2012(" -Xmx2G -cp . champ2010client.Client champ2010client.SimpleDriver",
    "MariscalFernandez2012", "MariscalF2012", ControllerType.JAVA, "trackName:") {
    },
    READY2WIN2012(" -Xmx2G -jar Ready2WinController.jar controller.Ready2WinController",
    "Ready2Win2012", "Ready2Win2012", ControllerType.JAVA, "trackName:") {
    },
    ICERIDDFS2013(" -Xmx2G -jar ICER_IDDFS.jar",
    "IcerIddfs2013", "ICER-IDDFS", ControllerType.JAVA, "trackName:") {
    },
    BERNIWLOWFUEL("", "BerniWLowFuel", "BerniWLowFuel", ControllerType.NATIVE_TORCS, "trackName:") {
    };
    private String path;
    private String cmd;
    private String displayName;
    private String driverName;
    private ControllerType type;
    private String jvmOptions = "";
    private HashMap<String, String> argumentNames = new HashMap<>();
    private int index;

    ControllerData(String cmd, String path, String displayName, ControllerType t,
            String trackNameArgument) {
        this.path = path;
        this.cmd = cmd;
        this.displayName = displayName;
        this.driverName = displayName;
        this.type = t;
        this.argumentNames.put("TRACKNAME", trackNameArgument);
    }
    
    public ControllerType getType(){
        return type;
    }

    public void setJVMOptions(String s) {
        this.jvmOptions = s;
    }
    
    /*public static ControllerData fromString(String s){
        return new ControllerData("cmd", "path", "displayName", ControllerType.EXECUTABLE, "args");
    }*/

    public String getDisplayName() {
        return displayName;
    }

    public String getPath(String baseDir) {
        return baseDir + File.separator + path;
    }

    public String getPath() {
        return path;
    }

    public static String getJavaExecutablePath() {
        StringBuilder result = new StringBuilder();
        result.append(System.getProperty("java.home"));

        if (System.getProperty("os.name").startsWith("Windows")) {
            result.append(File.separator).append("bin").append(File.separator).append("java.exe");

        } else if (System.getProperty("os.name").startsWith("Linux")) {
            result.append(File.separator).append("bin").append(File.separator).append("java");

        } else {
            System.out.println("Unknown OS: " + System.getProperty("os.name"));
            System.exit(-1);
        }

        return result.toString();
    }

    public String getCMDLine(scr.Controller.Stage stage, TrackData trackData, String baseDir, String host, int port) {
        boolean linux = System.getProperty("os.name").startsWith("Linux");
        StringBuilder result = new StringBuilder();
        if (type == ControllerType.JAVA) {
            result.append(getJavaExecutablePath());
            if (!jvmOptions.isEmpty()) {
                result.append(" ").append(jvmOptions);
            }
        } else {
            result.append(getPath(baseDir)).append(File.separator);
        }
        String modifiedCmd;
        if (type == ControllerType.JAVA) {
            modifiedCmd = (linux ? cmd.replace(';', ':') : cmd);
        } else {
            modifiedCmd = (linux ? cmd : (cmd + ".exe"));
        }
        result.append(modifiedCmd);
        result.append(" host:").append(host).append(" port:").append(port).append(" stage:");
        result.append(String.valueOf(stage.ordinal())).append(" id:SCR");
        result.append(" ").append(argumentNames.get("TRACKNAME"));
        result.append(trackData.controllerTrackName);
        if (stage == scr.Controller.Stage.WARMUP) {
            // now controlled by torcs
            //result.append(" maxSteps:100000");
        } else {
            // a controller which is not able to complete an evaluation blocks
            // by setting a step limit, we avoid this
            // maximum time: 10km/h, one lap
            //int steps = (int)Math.round((trackData.length/(10.0*1000.0)) * 3600 * 50);
            //result.append(" maxSteps:").append(steps);
            // now controlled by torcs
        }
        result.append(" maxEpisodes:1");

        return result.toString();
    }
    
    public void setIndex(int index){
        if(this.driverName == "BT" || this.driverName == "Damned" || this.driverName == "Olethros")
            this. index = index - 1;
        else this.index = index;
        this.displayName = driverName + "_" + this.index;
    }
    
    public int getIndex(){
        return index;
    }
    
    public String getDriverName(){
        return driverName;
    }

    public static void main(String[] args) {
        //System.out.println(MRRACER.getCMDLine(Controller.Stage.WARMUP, new TrackDataList().getTrack("forza")));
        //System.out.println(AUTOPIA.getCMDLine(Controller.Stage.WARMUP, new TrackDataList().getTrack(TrackDataList.TRACKS_TCIAIG2014_REPEAT[11])));
        //System.out.println(READY2WIN.getCMDLine(Controller.Stage.WARMUP, new TrackDataList().getTrack(TrackDataList.TRACKS_TCIAIG2014_REPEAT[11])));
    }
}