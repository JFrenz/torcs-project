/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo.tools;

import de.janquadflieg.mrracer.evo.ControllerData;
import de.janquadflieg.mrracer.evo.DriveTool2014Torcs134;
import java.io.*;

import de.janquadflieg.mrracer.evo.tools.TestScenario;
import de.janquadflieg.mrracer.evo.EvaluationType;
import de.janquadflieg.mrracer.evo.tools.AnalyseReferenceEvaluation;
import de.janquadflieg.mrracer.evo.tools.TestScenario;
import de.janquadflieg.mrracer.torcs.TrackData;
import de.janquadflieg.mrracer.torcs.TrackDataList;
import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;
import scr.Controller;

/**
 *
 * @author quad
 */
public class DriveToolAnalyseMOFrontCheck {

    private static TrackDataList trackDataList = new TrackDataList();

    private static void appendResults(File resultsSrc, String resultsDst,
            String rowName, ArrayList<String> trackList)
            throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(resultsSrc), "UTF-8"));
        OutputStreamWriter writer = new OutputStreamWriter(
                new FileOutputStream("./" + resultsDst, true), "UTF-8");

        writer.write(rowName);

        String line = reader.readLine();
        int lineCtr = 0;

        while ((line = reader.readLine()) != null) {
            StringTokenizer tokenizer = new StringTokenizer(line, ";");
            String track = tokenizer.nextToken();
            if (!track.equalsIgnoreCase(trackList.get(lineCtr))) {
                System.out.println("Error: Expected result for track "
                        + trackList.get(lineCtr) + " in line " + lineCtr
                        + " but found " + track);
                System.exit(1);
            }

            String result = tokenizer.nextToken();
            writer.write(";" + result);

            ++lineCtr;
        }
        writer.write("\n");
        reader.close();
        writer.close();
    }

    private static String createTrackModelLearningTimeFileName(boolean noisy) {
        String fileName = "warmup_time_trackmodel" + (noisy ? "-noisy" : "") + ".csv";
        return fileName;
    }

    private static String createResultsFileName(TestScenario scenario,
            boolean noisy, EvaluationType eType, String suffix) {
        String fileName = scenario.getPropertiesName() + "_"
                + DriveTool2014Torcs134.createResultsFileName(noisy, eType, suffix);
        return fileName;
    }

    private static String createDamageFileName(TestScenario scenario,
            boolean noisy, EvaluationType eType, String suffix) {
        String fileName = scenario.getPropertiesName() + "_"
                + DriveTool2014Torcs134.createDamageFileName(noisy, eType, suffix);
        return fileName;
    }

    private static void initResultsFile(String name, ArrayList<String> trackList)
            throws Exception {
        OutputStreamWriter writer = new OutputStreamWriter(
                new FileOutputStream("./" + name), "UTF-8");
        writer.write("variant");
        for (String track : trackList) {
            writer.write(";" + track);
        }
        writer.write("\n");
        writer.close();
    }

    private static void analyse()
            throws Exception {
        
        File[] directoryContents = new File("./Variants").listFiles();
        ArrayList<String> variantList = new ArrayList<>();
        for (File variant : directoryContents) {
            if (!variant.isDirectory()) {
                continue;
            }
            variantList.add(variant.getName());            
        }
        
        System.out.println("Found "+variantList.size()+" variants");
        
        for(String variant: variantList){
            AnalyseReferenceEvaluation.analyse(new File("./Variants/"+variant), ControllerData.MRRACER);
        }
        
        /*
        File scenarioDirectory = new File("./" + scenario.getDirectoryName());
        
        boolean initResultFile = true;
        ArrayList<String> trackList = new ArrayList<>();
        
        
        //String suffix = (eType == TIME_FOR_LAPS)
        //        ? (properties.getProperty("qualifyinglaps", "2") + "laps")
        //        : properties.getProperty("qualifyingticks", "10000") + "ticks";
        
        String suffix = "todo";

        // check, if there are any results of the given type at all
        boolean resultsAvailable = false;
        for (File variant : directoryContents) {
            if (!variant.isDirectory()) {
                continue;
            }
            variantList.add(trackDataList.getTrackByControllerName(variant.getName()));

            File resultsFile = new File(variant.getAbsolutePath() + "/MrRacer/" + DriveTool2014Torcs134.createResultsFileName(noisy, eType, suffix));
            if (resultsFile.exists()) {
                resultsAvailable = true;
            }
        }

        if (!resultsAvailable) {
            System.out.println("No results available for scenario " + scenario + ", "
                    + eType + ", noisy: " + noisy);
            return;
        } else {
            System.out.println("Merging results for scenario " + scenario + ", "
                    + eType + ", noisy: " + noisy);
        }

        java.util.Collections.sort(variantList, new java.util.Comparator<TrackData>() {
            public int compare(TrackData a1, TrackData a2) {
                return a1.trackName.compareToIgnoreCase(a2.trackName);
            }
        });

        for (TrackData trackData : variantList) {
            File variantDirectory = new File(scenarioDirectory.getAbsolutePath()
                    + "/" + trackData.controllerTrackName);

            if (initResultFile) {
                Properties templateProperties = new Properties();
                String propertiesFile = IOUtils.createToolScriptName(scenario, eType, Controller.Stage.QUALIFYING, noisy) + ".properties";
                FileInputStream in = new FileInputStream(variantDirectory.getAbsolutePath() + "/MrRacer/" + propertiesFile);
                templateProperties.load(in);
                in.close();

                StringTokenizer tokenizer = new StringTokenizer(templateProperties.getProperty("tracks"), ",");

                while (tokenizer.hasMoreTokens()) {
                    trackList.add(tokenizer.nextToken().trim());
                }

                initResultsFile(createResultsFileName(scenario, noisy, eType, suffix), trackList);
                initResultsFile(createDamageFileName(scenario, noisy, eType, suffix), trackList);

                if (scenario == TestScenario.RACE_WEEKEND && eType == EvaluationType.TIME_FOR_LAPS) {
                    initResultsFile(createTrackModelLearningTimeFileName(noisy), trackList);
                }

                initResultFile = false;
            }
            File resultsFile = new File(variantDirectory.getAbsolutePath() + "/MrRacer/" + DriveTool2014Torcs134.createResultsFileName(noisy, eType, suffix));
            File damageFile = new File(variantDirectory.getAbsolutePath() + "/MrRacer/" + DriveTool2014Torcs134.createDamageFileName(noisy, eType, suffix));

            if (!resultsFile.exists() || !damageFile.exists()) {
                System.out.println("*** WARNING ***");
                if (!resultsFile.exists()) {
                    System.out.println(resultsFile.getAbsolutePath() + " does not exist");
                }
                if (!damageFile.exists()) {
                    System.out.println(damageFile.getAbsolutePath() + " does not exist");
                }
                continue;
            }

            if (resultsFile.exists()) {
                appendResults(resultsFile, createResultsFileName(scenario, noisy, eType, suffix),
                        trackData.trackName, trackList);
            }
            if (damageFile.exists()) {
                appendResults(damageFile, createDamageFileName(scenario, noisy, eType, suffix),
                        trackData.trackName, trackList);
            }

            if (scenario == TestScenario.RACE_WEEKEND && eType == EvaluationType.TIME_FOR_LAPS) {
                OutputStreamWriter writer = new OutputStreamWriter(
                        new FileOutputStream("./" + createTrackModelLearningTimeFileName(noisy), true), "UTF-8");

                String rowName = trackData.trackName;
                writer.write(rowName);

                for (String s : trackList) {
                    File warmupLogFile = new File(variantDirectory.getAbsolutePath() + "/MrRacer/"
                            + trackDataList.getTrack(s).controllerTrackName
                            + (noisy ? "-noisy" : "") + "-1_warmup_process.log");
                    if (!warmupLogFile.exists()) {
                        System.out.println("*** WARNING ***");
                        System.out.println(warmupLogFile.getAbsolutePath() + " does not exist");
                        writer.write(";0");
                        continue;
                    }
                    BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(warmupLogFile), "UTF-8"));
                    String line = reader.readLine();
                    boolean found = false;
                    while ((line = reader.readLine()) != null && !found) {
                        if (line.startsWith("Learning the trackmodel took")) {
                            String subString = line.substring("Learning the trackmodel took ".length());
                            double time = 0.0;
                            time += 60.0 * Double.parseDouble(subString.substring(0, subString.indexOf(":")));
                            time += Double.parseDouble(subString.substring(subString.indexOf(":") + 1));
                            if(time != 0.0){
                                writer.write(";" + String.valueOf(time));
                                found = true;
                            }
                        }
                        if (line.startsWith("Waiting took")) {
                            String subString = line.substring("Waiting took ".length());
                            double time = 0.0;
                            time += 60.0 * Double.parseDouble(subString.substring(0, subString.indexOf(":")));
                            time += Double.parseDouble(subString.substring(subString.indexOf(":") + 1));
                            if(time != 0.0){
                                writer.write(";" + String.valueOf(time));
                                found = true;
                            }
                        }
                    }
                    if(!found){
                        //System.out.println("Variant: "+variantDirectory);
                        //System.out.println(s);
                        //System.exit(1);
                        writer.write(";2400");
                    }
                    reader.close();
                }                
                writer.write("\n");
                writer.close();
            }
        }*/
    }

    
    public static void main(String[] args) {      
        
        try {
            DriveToolAnalyseMOFrontCheck.analyse();

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

    }
}
