/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo;

import de.janquadflieg.mrracer.torcs.TrackData;
import de.janquadflieg.mrracer.torcs.TrackDataList;
import java.io.*;

/**
 *
 * @author quad
 */
public class Torcs134Executor
        implements RaceStateListener, ProcessListener {

    private static final boolean TEXT_DEBUG = false;
    private static final String CONFIG_FILE_1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<!DOCTYPE params SYSTEM \"params.dtd\">\n"
            + "\n"
            + "\n"
            + "<params name=\"Quick Race\">\n"
            + "  <section name=\"Header\">\n"
            + "    <attstr name=\"name\" val=\"Quick Race\"/>\n"
            + "    <attstr name=\"description\" val=\"Quick Race\"/>\n"
            + "    <attnum name=\"priority\" val=\"10\"/>\n"
            + "    <attstr name=\"menu image\" val=\"data/img/splash-qr.png\"/>\n"
            + "  </section>\n"
            + "\n"
            + "  <section name=\"Tracks\">\n"
            + "    <attnum name=\"maximum number\" val=\"1\"/>\n"
            + "    <section name=\"1\">\n";
    private static final String CONFIG_FILE_2 = "   </section>\n"
            + "\n"
            + "  </section>\n"
            + "\n"
            + "  <section name=\"Races\">\n"
            + "    <section name=\"1\">\n"
            + "      <attstr name=\"name\" val=\"Quick Race\"/>\n"
            + "    </section>\n"
            + "\n"
            + "  </section>\n"
            + "\n"
            + "  <section name=\"Quick Race\">\n"
            + "    <attnum name=\"distance\" unit=\"km\" val=\"0\"/>\n"
            + "    <attstr name=\"type\" val=\"race\"/>\n"
            + "    <attstr name=\"starting order\" val=\"drivers list\"/>\n"
            + "    <attstr name=\"display mode\" val=\"normal\"/>\n"
            + "    <attstr name=\"display results\" val=\"yes\"/>\n"
            + "    <attstr name=\"restart\" val=\"yes\"/>\n";
    private static final String CONFIG_FILE_3 = "<section name=\"Starting Grid\">\n"
            + "      <attnum name=\"rows\" val=\"2\"/>\n"
            + "      <attnum name=\"distance to start\" val=\"25\"/>\n"
            + "      <attnum name=\"distance between columns\" val=\"20\"/>\n"
            + "      <attnum name=\"offset within a column\" val=\"10\"/>\n"
            + "      <attnum name=\"initial speed\" val=\"0\"/>\n"
            + "      <attnum name=\"initial height\" val=\"0.2\"/>\n"
            + "    </section>\n"
            + "\n"
            + "  </section>\n"
            + "\n"
            + "  <section name=\"Drivers\">\n"
            + "    <attnum name=\"maximum number\" val=\"40\"/>\n"
            + "    <attnum name=\"focused idx\" val=\"0\"/>\n"
            + "    <attstr name=\"focused module\" val=\"scr_server\"/>\n"
            + "    <section name=\"1\">\n";
    //+ "      <attnum name=\"idx\" val=\"0\"/>\n";
    private static final String CONFIG_FILE_4 = "      <attstr name=\"module\" val=\"scr_server\"/>\n"
            + "    </section>\n"
            + "\n"
            + "  </section>\n"
            + "\n"
            + "  <section name=\"Configuration\">\n"
            + "    <attnum name=\"current configuration\" val=\"4\"/>\n"
            + "    <section name=\"1\">\n"
            + "      <attstr name=\"type\" val=\"track select\"/>\n"
            + "    </section>\n"
            + "\n"
            + "    <section name=\"2\">\n"
            + "      <attstr name=\"type\" val=\"drivers select\"/>\n"
            + "    </section>\n"
            + "\n"
            + "    <section name=\"3\">\n"
            + "      <attstr name=\"type\" val=\"race config\"/>\n"
            + "      <attstr name=\"race\" val=\"Quick Race\"/>\n"
            + "      <section name=\"Options\">\n"
            + "        <section name=\"1\">\n"
            + "          <attstr name=\"type\" val=\"race length\"/>\n"
            + "        </section>\n"
            + "\n"
            + "        <section name=\"2\">\n"
            + "          <attstr name=\"type\" val=\"display mode\"/>\n"
            + "        </section>\n"
            + "\n"
            + "      </section>\n"
            + "\n"
            + "    </section>\n"
            + "\n"
            + "  </section>\n"
            + "\n"
            + "</params>";
    private int waitedForRequest = 0;
    private Process torcsProcess;
    private String torcsBasePath;    
    private int timeoutCtr = 0;
    TorcsStreamReader srCerr;
    TorcsStreamReader srCout;

    public Torcs134Executor(String torcsBasePath) {
        this.torcsBasePath = torcsBasePath;        
    }

    public static String getExecutablePath(String basePath) {
        StringBuilder result = new StringBuilder();
        result.append(basePath);

        if (System.getProperty("os.name").startsWith("Windows")) {
            result.append(File.separator).append("wtorcs.exe");

        } else if (System.getProperty("os.name").startsWith("Linux")) {
            result.append(File.separator).append("bin").append(File.separator).append("torcs");

        } else {
            System.out.println("Unknown OS: " + System.getProperty("os.name"));
            System.exit(-1);
        }

        return result.toString();
    }

    @Override
    public void processTerminated(int result) {
        if (TEXT_DEBUG) {
            System.out.println("TORCS Terminated: " + result);
        }
        srCerr.stop();
        srCout.stop();
    }

    @Override
    public void lapTime(int lap, double time, boolean b) {
        //System.out.println("Laptime");
    }

    public void distanceRaced(double d) {
    }
    
    public void overallTime(double d) {
    }
    
    public void reachedMaxTicks(){
        
    }

    public void ticks(int i){
        
    }
    
    @Override
    public void raceEnded() {
    }
    
    @Override
    public void damage(double d) {
    }

    @Override
    public void timeout() {
        timeoutCtr++;
    }

    @Override
    public void waitingForRequest() {
        synchronized (this) {
            ++waitedForRequest;
            if (waitedForRequest == 2) {
                if (TEXT_DEBUG) {
                    System.out.println("Evaluation ended, destroying Torcs process");
                }
                torcsProcess.destroy();
                if (srCout != null) {
                    srCout.stop();
                }
                if (srCerr != null) {
                    srCerr.stop();
                }

                if (TEXT_DEBUG) {
                    System.out.println("Process destroyed");
                }
            }
        }
        synchronized (this) {
            notifyAll();
        }
    }
    
    public int getTimeoutCtr(){
        return timeoutCtr;
    }

    public void stopTorcs() {
        torcsProcess.destroy();
    }    
     
    public static String createConfigFile(TrackData trackData, int torcsInstance,
            int laps) {
        String quickraceXml = CONFIG_FILE_1;
        quickraceXml += "<attstr name=\"name\" val=\"" + trackData.torcsTrackName + "\"/>\n";
        quickraceXml += "<attstr name=\"category\" val=\"" + trackData.torcsCategory + "\"/>\n";
        quickraceXml += CONFIG_FILE_2;
        quickraceXml += "<attnum name=\"laps\" val=\"";
        quickraceXml += String.valueOf(laps);
        quickraceXml += "\"/>\n";
        quickraceXml += CONFIG_FILE_3;
        quickraceXml += "<attnum name=\"idx\" val=\"";
        quickraceXml += String.valueOf(torcsInstance - 1);
        quickraceXml += "\"/>\n";
        quickraceXml += CONFIG_FILE_4;
        return quickraceXml;
    }

    public void startTorcs(TrackData trackData, TorcsExecConfig cfg) {
        try {
            //scr.Controller.Stage stage = scr.Controller.Stage.QUALIFYING;
            if (TEXT_DEBUG) {
                System.out.println("[TORCS-" + cfg.port + "/"+cfg.scr_server_instance+ "]" + "Building xml file");
            }
            String quickraceXml = createConfigFile(trackData, cfg.scr_server_instance, 100);

            if (TEXT_DEBUG) {
                //System.out.println(quickraceXml);
            }

            String TORCS_PATH = torcsBasePath;

            if (TEXT_DEBUG) {
                System.out.println("[TORCS-" + cfg.port + "/"+cfg.scr_server_instance+ "]" + TORCS_PATH);
            }

            File raceConfig = new File(TORCS_PATH + File.separator + "quickrace" + cfg.port + "-"+cfg.scr_server_instance+ ".xml");
            FileOutputStream fo = new FileOutputStream(raceConfig);
            OutputStreamWriter w = new OutputStreamWriter(fo, "UTF-8");

            w.write(quickraceXml);

            w.flush();
            fo.flush();
            w.close();
            fo.close();

            

            //System.out.println(raceConfig.getAbsolutePath());

            String torcsParams = " -r " + raceConfig.getAbsolutePath();                        
            torcsParams += cfg.toArgumentString();

            String torcsExecDirectory = TORCS_PATH;

            if (TEXT_DEBUG) {
                System.out.println("[TORCS-" + cfg.port + "/"+cfg.scr_server_instance+ "]Starting Process");
                System.out.println(getExecutablePath(TORCS_PATH) + torcsParams);
                System.out.println("in directory");
                System.out.println(torcsExecDirectory);
            }

            torcsProcess = Runtime.getRuntime().exec(getExecutablePath(TORCS_PATH) + torcsParams, null, new File(torcsExecDirectory));

            ProcessObserver observer = new ProcessObserver(torcsProcess, this);

            if (TEXT_DEBUG) {
                System.out.println("[TORCS-" + cfg.port + "/"+cfg.scr_server_instance+ "]done");
            }

            srCerr = new TorcsStreamReader(torcsProcess.getErrorStream(), String.valueOf(cfg.port) + "-cerr", TEXT_DEBUG);
            srCerr.addListener(this);
            Thread t1 = new Thread(srCerr, "TORCS-" + String.valueOf(cfg.port) + "-cerr");
            t1.start();

            srCout = new TorcsStreamReader(torcsProcess.getInputStream(), String.valueOf(cfg.port) + "-cout", TEXT_DEBUG);
            srCout.addListener(this);
            Thread t2 = new Thread(srCout, "TORCS-" + String.valueOf(cfg.port) + "-cout");
            t2.start();

            synchronized (this) {
                if (TEXT_DEBUG) {
                    System.out.println("[TORCS-" + cfg.port + "/"+cfg.scr_server_instance+ "]" + "Waiting");
                }
                while (waitedForRequest < 1) {
                    wait();
                }
                if (TEXT_DEBUG) {
                    System.out.println("[TORCS-" + cfg.port + "/"+cfg.scr_server_instance+ "]" + "Torcs up and running");
                }
            }

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    public static void main(String[] args) {
        //Torcs134Executor bla = new Torcs134Executor();
        // bla.startTorcs(new TrackDataList().getTrack(TrackDataList.TRACKS_TCIAIG2014_REPEAT[11]), TEXT_DEBUG, 1);
    }
}