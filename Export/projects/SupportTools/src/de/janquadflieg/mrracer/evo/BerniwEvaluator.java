/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo;

import de.janquadflieg.mrracer.torcs.TrackData;

/**
 *
 * @author quad
 */
public class BerniwEvaluator       {

    
    private static final String CONFIG_FILE_1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<!DOCTYPE params SYSTEM \"params.dtd\">\n"
            + "\n"
            + "\n"
            + "<params name=\"Quick Race\">\n"
            + "  <section name=\"Header\">\n"
            + "    <attstr name=\"name\" val=\"Quick Race\"/>\n"
            + "    <attstr name=\"description\" val=\"Quick Race\"/>\n"
            + "    <attnum name=\"priority\" val=\"10\"/>\n"
            + "    <attstr name=\"menu image\" val=\"data/img/splash-qr.png\"/>\n"
            + "  </section>\n"
            + "\n"
            + "  <section name=\"Tracks\">\n"
            + "    <attnum name=\"maximum number\" val=\"1\"/>\n"
            + "    <section name=\"1\">\n";
    private static final String CONFIG_FILE_2 = "   </section>\n"
            + "\n"
            + "  </section>\n"
            + "\n"
            + "  <section name=\"Races\">\n"
            + "    <section name=\"1\">\n"
            + "      <attstr name=\"name\" val=\"Quick Race\"/>\n"
            + "    </section>\n"
            + "\n"
            + "  </section>\n"
            + "\n"
            + "  <section name=\"Quick Race\">\n"
            + "    <attnum name=\"distance\" unit=\"km\" val=\"0\"/>\n"
            + "    <attstr name=\"type\" val=\"race\"/>\n"
            + "    <attstr name=\"starting order\" val=\"drivers list\"/>\n"
            + "    <attstr name=\"display mode\" val=\"normal\"/>\n"
            + "    <attstr name=\"display results\" val=\"yes\"/>\n"
            + "    <attstr name=\"restart\" val=\"yes\"/>\n";
    private static final String CONFIG_FILE_3 = "<section name=\"Starting Grid\">\n"
            + "      <attnum name=\"rows\" val=\"2\"/>\n"
            + "      <attnum name=\"distance to start\" val=\"25\"/>\n"
            + "      <attnum name=\"distance between columns\" val=\"20\"/>\n"
            + "      <attnum name=\"offset within a column\" val=\"10\"/>\n"
            + "      <attnum name=\"initial speed\" val=\"0\"/>\n"
            + "      <attnum name=\"initial height\" val=\"0.2\"/>\n"
            + "    </section>\n"
            + "\n"
            + "  </section>\n"
            + "\n"
            + "  <section name=\"Drivers\">\n"
            + "    <attnum name=\"maximum number\" val=\"40\"/>\n"
            + "    <attnum name=\"focused idx\" val=\"0\"/>\n"
            + "    <attstr name=\"focused module\" val=\"scr_server\"/>\n"
            ;
    private static final String CONFIG_FILE_4 = ""
            + "\n"
            + "  </section>\n"
            + "\n"
            + "  <section name=\"Configuration\">\n"
            + "    <attnum name=\"current configuration\" val=\"4\"/>\n"
            + "    <section name=\"1\">\n"
            + "      <attstr name=\"type\" val=\"track select\"/>\n"
            + "    </section>\n"
            + "\n"
            + "    <section name=\"2\">\n"
            + "      <attstr name=\"type\" val=\"drivers select\"/>\n"
            + "    </section>\n"
            + "\n"
            + "    <section name=\"3\">\n"
            + "      <attstr name=\"type\" val=\"race config\"/>\n"
            + "      <attstr name=\"race\" val=\"Quick Race\"/>\n"
            + "      <section name=\"Options\">\n"
            + "        <section name=\"1\">\n"
            + "          <attstr name=\"type\" val=\"race length\"/>\n"
            + "        </section>\n"
            + "\n"
            + "        <section name=\"2\">\n"
            + "          <attstr name=\"type\" val=\"display mode\"/>\n"
            + "        </section>\n"
            + "\n"
            + "      </section>\n"
            + "\n"
            + "    </section>\n"
            + "\n"
            + "  </section>\n"
            + "\n"
            + "</params>";
    
    
    public static String createConfigFile(TrackData trackData, ControllerData controllerData, int maxLaps){
        String quickraceXml = CONFIG_FILE_1;
            quickraceXml += "<attstr name=\"name\" val=\"" + trackData.torcsTrackName + "\"/>\n";
            quickraceXml += "<attstr name=\"category\" val=\"" + trackData.torcsCategory + "\"/>\n";
            quickraceXml += CONFIG_FILE_2;
            quickraceXml += "<attnum name=\"laps\" val=\"";
            quickraceXml += String.valueOf(maxLaps);
            quickraceXml += "\"/>\n";
            quickraceXml += CONFIG_FILE_3;
            quickraceXml += "    <section name=\"1\">\n";
            quickraceXml += "<attnum name=\"idx\" val=\"" + controllerData.getIndex() + "\"/>\n";
            quickraceXml += "<attstr name=\"module\" val=\"" + controllerData.getDisplayName().split("_")[0].toLowerCase() + "\"/>\n";     //automatic convertion to lowercase drivername, as it is standard in current TORCS-version
            quickraceXml += "</section>\n";
            quickraceXml += CONFIG_FILE_4;        
        return quickraceXml;
    }   
    
    public static String createConfigFile(TrackData trackData, ControllerData [] controllerData, int maxLaps){
        String quickraceXml = CONFIG_FILE_1;
            quickraceXml += "<attstr name=\"name\" val=\"" + trackData.torcsTrackName + "\"/>\n";
            quickraceXml += "<attstr name=\"category\" val=\"" + trackData.torcsCategory + "\"/>\n";
            quickraceXml += CONFIG_FILE_2;
            quickraceXml += "<attnum name=\"laps\" val=\"";
            quickraceXml += String.valueOf(maxLaps);
            quickraceXml += "\"/>\n";
            quickraceXml += CONFIG_FILE_3;
            for(int i=0; i < controllerData.length; i++){
                quickraceXml += "    <section name=\""+(i+1)+"\">\n";
                quickraceXml += "<attnum name=\"idx\" val=\"" + controllerData[i].getIndex() + "\"/>\n";
                quickraceXml += "<attstr name=\"module\" val=\"" + controllerData[i].getDisplayName().split("_")[0].toLowerCase() + "\"/>\n";     //automatic convertion to lowercase drivername, as it is standard in current TORCS-version
                quickraceXml += "</section>\n";
            }
            quickraceXml += CONFIG_FILE_4;        
        return quickraceXml;
    }   
}