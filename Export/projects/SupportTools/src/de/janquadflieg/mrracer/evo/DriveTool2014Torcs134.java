/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo;

import de.janquadflieg.mrracer.Utils;
import de.janquadflieg.mrracer.torcs.TrackDataList;
import de.janquadflieg.mrracer.torcs.TrackData;
import static de.janquadflieg.mrracer.evo.EvaluationType.DISTANCE_IN_CONSTANT_TIME;
import static de.janquadflieg.mrracer.evo.EvaluationType.TIME_FOR_LAPS;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author quad
 */
public class DriveTool2014Torcs134 {

    private boolean TEXT_DEBUG = false;
    private static final String[] ALL_TRACKS = {"A-Speedway", "Aalborg", "Alpine 1",
        "Alpine 2", "Alsoujlak-hill", "Ardennen-Spa", "Arraias-desert", "B-Speedway",
        "Brondehach", "C-Speedway", "CG Speedway number 1", "CG track 2",
        "CG track 3", "D-Speedway", "Dirt 1", "Dirt 2", "Dirt 3", "Dirt 4", "Dirt 5",
        "Dirt 6", "E-Road", "E-Speedway", "E-Track 2", "E-Track 3",
        "E-Track 4", "E-Track 5", "E-Track 6", "F-Speedway", "Forza", "G-Speedway",
        "Kerang-desert", "Michigan Speedway", "Mixed 1", "Mixed 2", "Mueda-city",
        "Olethros Road 1", "Ruudskogen", "Sancassa-city", "Spring", "Street 1",
        "Valparaiso-city", "Wheel 1", "Wheel 2", "Wheel 2a", "Zvolenovice-mountain",
        "changay", "curburgring", "f1orza", "goldstone", "monandgo", "petit", "polimi"};
    private static final String[] NEW_TRACKS = {"Alsoujlak-hill", "Arraias-desert",
        "Sancassa-city", "Valparaiso-city"};
    private static final String[] TEST_SET = {"Aalborg", "Alpine 1",
        "Alpine 2", "Ardennen-Spa", "Brondehach", "CG Speedway number 1", "CG track 2",
        "CG track 3", "E-Road", "E-Track 2", "E-Track 3",
        "E-Track 4", "E-Track 5", "E-Track 6", "Forza", "Mueda-city",
        "Olethros Road 1", "Ruudskogen", "Street 1",
        "Wheel 1", "Wheel 2", "changay", "curburgring", "f1orza", "goldstone", "monandgo", "petit", "polimi"};
    private static final String[] TEST_SET2 = {"Forza", "Mueda-city",
        "Wheel 2"};
    private String torcsPath;
    private String expPath;
    private String host;
    private int port;
    private String javaControllerJVMOptions = "";
    private Properties properties;

    public DriveTool2014Torcs134(String torcs, String exp, boolean b, String h, int p,
            String jvmOptions, Properties props) {
        this.torcsPath = torcs;
        this.expPath = exp;
        this.TEXT_DEBUG = b;
        this.host = h;
        this.port = p;
        javaControllerJVMOptions = jvmOptions;
        this.properties = props;
    }

    public static String createResultsFileName(boolean noisy,
            EvaluationType evalType, String suffix) {
        String result = "results" + (noisy ? "-noisy" : "");
        switch (evalType) {
            case TIME_FOR_LAPS: {
                result += "-time";
                break;
            }
            case DISTANCE_IN_CONSTANT_TIME: {
                result += "-distance";
                break;
            }
        }
        return result + suffix + ".csv";
    }

    public static String createDamageFileName(boolean noisy,
            EvaluationType evalType, String suffix) {
        String result = "damage" + (noisy ? "-noisy" : "");
        switch (evalType) {
            case TIME_FOR_LAPS: {
                result += "-time";
                break;
            }
            case DISTANCE_IN_CONSTANT_TIME: {
                result += "-distance";
                break;
            }
        }
        return result + suffix + ".csv";
    }   

    private static void write(OutputStreamWriter w, String s) {
        if (w != null) {
            try {
                w.write(s);
            } catch (Exception e) {
            }
        }
    }

    private void printAndSave(HashMap<String, HashMap<String, HashMap<String, ArrayList<Double>>>> resultsPerDriver,
            OutputStreamWriter resultsWriter,
            final String[] TRACKS, CarData[] cars, int trackRepeats, int repeats, boolean noisy) {
        TrackDataList trackDataList = new TrackDataList();
        /*System.out.print("Track");
        write(resultsWriter, "Track");
        for (int tr = 1; tr <= trackRepeats; ++tr) {
            for (int r = 1; r <= repeats; ++r) {
                System.out.print(";T" + tr + "E" + r);
                write(resultsWriter, ";T" + tr + "E" + r);
            }
        }*/
        
        System.out.print("Car\t");
        write(resultsWriter, "Car");
        for(int i = 0; i < TRACKS.length; i ++){
            System.out.print(" \t" + TRACKS[i]);
            write(resultsWriter, ";" + TRACKS[i]);
        }
        
        System.out.println("");
        write(resultsWriter, "\n");  
        
        for (CarData c : cars){
            
            System.out.print(c.getCarName() + ":");
            write(resultsWriter, c.getCarName());
            HashMap<String, HashMap<String, ArrayList<Double>>> resultsPerCar = resultsPerDriver.get(c.getCarName());
            for (String s : TRACKS) {
                //System.out.print(s);
                //write(resultsWriter, s);

                TrackData d = trackDataList.getTrack(s);
                        
                HashMap<String, ArrayList<Double>> tracksResults = resultsPerCar.get(s);

                String backup = d.controllerTrackName;

                for (int tr = 1; tr <= trackRepeats; ++tr) {
                    d.controllerTrackName = d.controllerTrackName + "-"
                            + (noisy ? "noisy-" : "")
                            + String.valueOf(tr);

                    ArrayList<Double> values = tracksResults.get(d.controllerTrackName);

                    for (int r = 1; r <= repeats; ++r) {
                        System.out.print("\t" + values.get(r - 1) + ";\t");
                        write(resultsWriter, "; " + values.get(r - 1));
                    }

                d.controllerTrackName = backup;
                }
            }
            System.out.println("");
            write(resultsWriter, "\n");
        }

        if (resultsWriter != null) {
            try {
                resultsWriter.close();
            } catch (Exception e) {
            }
        }
    }

    public void runQualifying(final String[] TRACKS,
            final ControllerData[] controllerData, CarData[] carData, int trackRepeats, int repeats, boolean noisy,
            long timeout, EvaluationType eType) {
        TrackDataList trackDataList = new TrackDataList();
        HashMap<String, HashMap<String, HashMap<String, HashMap<String, ArrayList<Double>>>>> results = new HashMap<>();
        HashMap<String, HashMap<String, HashMap<String, HashMap<String, ArrayList<Double>>>>> damages = new HashMap<>();
        boolean[] ok = new boolean[carData.length * controllerData.length * TRACKS.length * trackRepeats * repeats];
        long startTime = System.currentTimeMillis();
        long timeouts = 0;

        // TODO: Maybe the best point for entering parallelization

        for(int i = 0; i < controllerData.length; i++) {
            controllerData[i].setJVMOptions(javaControllerJVMOptions);
            HashMap<String, HashMap<String, HashMap<String, ArrayList<Double>>>> resultsPerDriver = new HashMap<>();
            HashMap<String, HashMap<String, HashMap<String, ArrayList<Double>>>> damagesPerDriver = new HashMap<>();
            String[] driverXml = new String[(carData.length - 1)/10 + 1];
            
            for(int k = 0; k <= ((carData.length - 1)/10) - 1; k++){
                driverXml[k] = CarChanger.createDriverFile(controllerData[i], Arrays.copyOfRange(carData, k * 10, ((k + 1) * 10)));
            }
            driverXml[((carData.length - 1)/10)] = CarChanger.createDriverFile(controllerData[i], Arrays.copyOfRange(carData, ((carData.length - 1)/10)*10, carData.length));
            
            for (int c = 0; c < carData.length; c++){
                
                if(c%10==0){
                    File carConfig = new File(torcsPath + "/drivers/" + controllerData[i].getDriverName().toLowerCase() + File.separator + controllerData[i].getDriverName().toLowerCase() + ".xml");

                    try {
                        OutputStreamWriter w = new OutputStreamWriter(new FileOutputStream(carConfig), "UTF-8");
                
                        w.write(driverXml[c/10]);
                        w.flush();
                        w.close();
                    } catch (Exception e) {
                        e.printStackTrace(System.out);
                    }
                }
                
                controllerData[i].setIndex(c%10 + 1);
                
                HashMap<String, HashMap<String, ArrayList<Double>>> resultsPerCar = new HashMap<>();
                HashMap<String, HashMap<String, ArrayList<Double>>> damagesPerCar = new HashMap<>();
                
                for (int t = 0; t < TRACKS.length; ++t) {
                    System.out.println("---------- " + controllerData[i].getDriverName() + " " + carData[c].getCarName() + " " + TRACKS[t] + " " 
                            +  ((i * carData.length * TRACKS.length) + (c * TRACKS.length)+ (t + 1)) + "/" 
                            + (controllerData.length * carData.length * TRACKS.length) + " ----------");
                    String s = TRACKS[t];
                    TrackData d = trackDataList.getTrack(s);

                    HashMap<String, ArrayList<Double>> trackResults = new HashMap<>();
                    HashMap<String, ArrayList<Double>> trackDamages = new HashMap<>();

                    for (int tr = 1; tr <= trackRepeats; ++tr) {
                        String backup = d.controllerTrackName;
                        d.controllerTrackName = d.controllerTrackName + "-"
                                + (noisy ? "noisy-" : "")
                                + String.valueOf(tr);

                        ArrayList<Double> trackRepeatResults = new ArrayList<>();
                        ArrayList<Double> trackRepeatDamages = new ArrayList<>();

                        for (int k = 0; k < repeats; ++k) {
                            TorcsExecConfig cfg = new TorcsExecConfig();
                            cfg.set(properties);
                            cfg.noisy = noisy;
                            cfg.port = port;
                            cfg.timeoutthreshold = 100;

                            final int MAX_LAPS;

                            switch (eType) {
                                case TIME_FOR_LAPS: {
                                    MAX_LAPS = Integer.parseInt(properties.getProperty("qualifyinglaps", "2"));
                                    int steps = (int) Math.round((d.length / (10.0 * 1000.0)) * 3600 * 50 * MAX_LAPS);
                                    cfg.step_limit = steps;
                                    cfg.nolaptime = true;
                                    cfg.restart_behavior = 's';
                                    break;
                                }
                                case DISTANCE_IN_CONSTANT_TIME: {
                                    MAX_LAPS = 1000;
                                    cfg.step_limit = Integer.parseInt(properties.getProperty("qualifyingticks", "10000"));
                                    cfg.restart_behavior = 's';
                                    cfg.nolaptime = true;
                                    break;
                                }
                                default: {
                                    MAX_LAPS = 2;
                                }
                            }
                    
                            DirectTorcs134Evaluator eval = new DirectTorcs134Evaluator(torcsPath, expPath,
                                    TEXT_DEBUG, eType);
                            eval.startTorcs(d, controllerData[i], cfg,
                                    scr.Controller.Stage.QUALIFYING, host, MAX_LAPS);


                            timeouts += eval.timeouts();

                            switch (eType) {
                                case TIME_FOR_LAPS: {
                                    ok[i * repeats * trackRepeats * TRACKS.length * carData.length
                                            + c * repeats * trackRepeats * TRACKS.length 
                                            + t * repeats * trackRepeats + (tr - 1) * repeats + k] = eval.ok();
                                    if (eval.ok()) {
                                        trackRepeatResults.add(eval.getTime());
                                    } else {
                                        trackRepeatResults.add(-1.0);
                                    }
                                    trackRepeatDamages.add(eval.getDamage());
                                    break;
                                }
                                case DISTANCE_IN_CONSTANT_TIME: {
                                    ok[i * repeats * trackRepeats * TRACKS.length * carData.length
                                            + c * repeats * trackRepeats * TRACKS.length 
                                            + t * repeats * trackRepeats + (tr - 1) * repeats + k] = eval.ok();
                                    trackRepeatResults.add(eval.getDistanceRaced());
                                    trackRepeatDamages.add(eval.getDamage());
                                    break;
                                }
                            }
                        }

                        trackResults.put(d.controllerTrackName, trackRepeatResults);
                        trackDamages.put(d.controllerTrackName, trackRepeatDamages);

                        d.controllerTrackName = backup;
                    }

                    resultsPerCar.put(s, trackResults);
                    damagesPerCar.put(s, trackDamages);
                }
                resultsPerDriver.put(carData[c].getCarName(), resultsPerCar);
                damagesPerDriver.put(carData[c].getCarName(), damagesPerCar);
            }
            results.put(controllerData[i].getDriverName(), resultsPerDriver);
            damages.put(controllerData[i].getDriverName(), damagesPerDriver);
        }
        System.out.println("-------------------------------------------------------");

        int ctr = 0;
        
        for (int i = 0; i < controllerData.length * carData.length * TRACKS.length * repeats * trackRepeats; ++i) {
            if (ok[i]) {
                ++ctr;
            }
        }
        System.out.println(ctr + " of " + (controllerData.length * carData.length * TRACKS.length * repeats * trackRepeats) + " runs ok");
        long endTime = System.currentTimeMillis();
        System.out.println("This took " + Utils.timeToString((endTime - startTime) / 1000.0));
        System.out.println("Timeouts: " + timeouts);

        OutputStreamWriter resultsWriter = null;
        OutputStreamWriter damagesWriter = null;
        
        for(int i = 0; i < controllerData.length; i++){

            String suffix = (eType == TIME_FOR_LAPS)
                ? (properties.getProperty("qualifyinglaps", "2") + controllerData[i].getDriverName() + "laps")
                : properties.getProperty("qualifyingticks", "10000") + controllerData[i].getDriverName() + "ticks";

            try {
                resultsWriter = new OutputStreamWriter(new FileOutputStream("./" + createResultsFileName(noisy, eType, suffix)), "UTF-8");

            } catch (Exception e) {
                e.printStackTrace(System.out);
            }
            try {
                damagesWriter = new OutputStreamWriter(new FileOutputStream("./" + createDamageFileName(noisy, eType, suffix)), "UTF-8");

            } catch (Exception e) {
                e.printStackTrace(System.out);
            }
        
        
            /*String[] DRIVERS = new String[controllerData.length];
            for (int i = 0; i < controllerData.length; i++){
                DRIVERS[i] = controllerData[i].getDisplayName();
            }*/
            System.out.println("Complete results for " + controllerData[i].getDriverName()
                    + ", noisy? " + noisy);
            printAndSave(results.get(controllerData[i].getDriverName()), resultsWriter, TRACKS, carData, trackRepeats, repeats, noisy);
            System.out.println("");
            System.out.println("Damage data");
            printAndSave(damages.get(controllerData[i].getDriverName()), damagesWriter, TRACKS, carData, trackRepeats, repeats, noisy);
            System.out.println("");
            
            if (resultsWriter != null) {
                try {
                    resultsWriter.close();
                } catch (Exception e) {
                }
            }
            if (damagesWriter != null) {
                try {
                    damagesWriter.close();
                } catch (Exception e) {
                }
            }
        }
    }
    
    public void runVSmode(final String[] TRACKS,
            final ControllerData[] controllerData,CarData[] carData, int trackRepeats, int repeats, boolean noisy,
            long timeout, EvaluationType eType) {
        TrackDataList trackDataList = new TrackDataList();
        HashMap<String, HashMap<String, ArrayList<Double>>> results = new HashMap<>();
        HashMap<String, HashMap<String, ArrayList<Double>>> damages = new HashMap<>();
        boolean[] ok = new boolean[TRACKS.length * trackRepeats * repeats * carData.length];
        long startTime = System.currentTimeMillis();
        long timeouts = 0;
        String[][] driverXml = new String[controllerData.length][(carData.length - 1)/10 + 1];    
        
        for(int i=0; i < controllerData.length; i++){
            driverXml[i] = new String[(carData.length - 1)/10 + 1]; 
            controllerData[i].setJVMOptions(javaControllerJVMOptions);
            for(int k = 0; k <= ((carData.length - 1)/10) - 1; k++){
                driverXml[i][k] = CarChanger.createDriverFile(controllerData[i], Arrays.copyOfRange(carData, k * 10, ((k + 1) * 10)));
            }
            driverXml[i][((carData.length - 1)/10)] = CarChanger.createDriverFile(controllerData[i], Arrays.copyOfRange(carData, ((carData.length - 1)/10)*10, carData.length));
        }
        
        for (int c = 0; c < carData.length; c++){
            for(int i=0; i < controllerData.length;i++){
                if(c%10==0){
                    File carConfig = new File(torcsPath + "/drivers/" + controllerData[i].getDriverName().toLowerCase() + File.separator + controllerData[i].getDriverName().toLowerCase() + ".xml");

                    try {
                        OutputStreamWriter w = new OutputStreamWriter(new FileOutputStream(carConfig), "UTF-8");
                        
                        w.write(driverXml[i][c/10]);
                        w.flush();
                        w.close();
                    } catch (Exception e) {
                        System.out.println(driverXml[c/10]);
                        e.printStackTrace(System.out);
                    }
                }
                
                controllerData[i].setIndex(c%10 + 1);
            }
               

            for (int i = 0; i < TRACKS.length; ++i) {
                System.out.println("---------- " + TRACKS[i] + " " + (i + 1) + "/" + (TRACKS.length) + " ----------");
                String s = TRACKS[i];
                TrackData d = trackDataList.getTrack(s);

                HashMap<String, ArrayList<Double>> trackResults = new HashMap<>();
                HashMap<String, ArrayList<Double>> trackDamages = new HashMap<>();

                for (int tr = 1; tr <= trackRepeats; ++tr) {
                    String backup = d.controllerTrackName;
                    d.controllerTrackName = d.controllerTrackName + "-"
                            + (noisy ? "noisy-" : "")
                            + String.valueOf(tr);

                    ArrayList<Double> trackRepeatResults = new ArrayList<>();
                    ArrayList<Double> trackRepeatDamages = new ArrayList<>();

                    for (int k = 0; k < repeats; ++k) {
                        TorcsExecConfig cfg = new TorcsExecConfig();
                        cfg.set(properties);
                        cfg.noisy = noisy;
                        cfg.port = port;
                        cfg.timeoutthreshold = 100;

                        final int MAX_LAPS;

                        switch (eType) {
                            case TIME_FOR_LAPS: {
                                MAX_LAPS = Integer.parseInt(properties.getProperty("qualifyinglaps", "2"));
                                int steps = (int) Math.round((d.length / (10.0 * 1000.0)) * 3600 * 50 * MAX_LAPS);
                                cfg.step_limit = steps;
                                cfg.nolaptime = true;
                                cfg.restart_behavior = 's';
                                break;
                            }
                            case DISTANCE_IN_CONSTANT_TIME: {
                                MAX_LAPS = 1000;
                                cfg.step_limit = Integer.parseInt(properties.getProperty("qualifyingticks", "10000"));
                                cfg.restart_behavior = 's';
                                cfg.nolaptime = true;
                                break;
                            }
                            default: {
                                MAX_LAPS = 2;
                            }
                        }

                        DirectTorcs134Evaluator eval = new DirectTorcs134Evaluator(torcsPath, expPath,
                                TEXT_DEBUG, eType);
                        eval.startTorcs(d, controllerData, cfg,
                                scr.Controller.Stage.QUALIFYING, host, MAX_LAPS);


                        timeouts += eval.timeouts();

                        switch (eType) {
                            case TIME_FOR_LAPS: {
                                ok[TRACKS.length * c + i * repeats * trackRepeats + (tr - 1) * repeats + k] = eval.ok();
                                if (eval.ok()) {
                                    trackRepeatResults.add(eval.getTime());
                                } else {
                                    trackRepeatResults.add(-1.0);
                                }
                                trackRepeatDamages.add(eval.getDamage());
                                break;
                            }
                            case DISTANCE_IN_CONSTANT_TIME: {
                                ok[TRACKS.length * c + i * repeats * trackRepeats + (tr - 1) * repeats + k  ] = eval.ok();
                                trackRepeatResults.add(eval.getDistanceRaced());
                                trackRepeatDamages.add(eval.getDamage());
                                break;
                            }
                        }
                    }

                    trackResults.put(d.controllerTrackName, trackRepeatResults);
                    trackDamages.put(d.controllerTrackName, trackRepeatDamages);

                    d.controllerTrackName = backup;
                }

                results.put(s, trackResults);
                damages.put(s, trackDamages);
            }
        }
        System.out.println("-------------------------------------------------------");
        int ctr = 0;
        for (int i = 0; i < TRACKS.length * repeats * trackRepeats * carData.length; ++i) {
            System.out.println(ok[i]);
            if (ok[i]) {
                ++ctr;
            }
        }
        System.out.println(ctr + " of " + (TRACKS.length * repeats * trackRepeats * carData.length) + " runs ok");
        long endTime = System.currentTimeMillis();
        System.out.println("This took " + Utils.timeToString((endTime - startTime) / 1000.0));
        System.out.println("Timeouts: " + timeouts);

        /*OutputStreamWriter resultsWriter = null;
        OutputStreamWriter damagesWriter = null;

        String suffix = (eType == TIME_FOR_LAPS)
                ? (properties.getProperty("qualifyinglaps", "2") + "laps")
                : properties.getProperty("qualifyingticks", "10000") + "ticks";

        try {
            resultsWriter = new OutputStreamWriter(new FileOutputStream("./" + createResultsFileName(noisy, eType, suffix)), "UTF-8");

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        try {
            damagesWriter = new OutputStreamWriter(new FileOutputStream("./" + createDamageFileName(noisy, eType, suffix)), "UTF-8");

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }


        System.out.println("Complete results for " + controllerData.getDisplayName()
                + ", noisy? " + noisy);
        printAndSave(results.get(controllerData[0].getDriverName()), resultsWriter, TRACKS, carData, trackRepeats, repeats, noisy);
        System.out.println("");
        System.out.println("Damage data");
        printAndSave(damages.get(controllerData[0].getDriverName()), damagesWriter, TRACKS, carData, trackRepeats, repeats, noisy);

        if (resultsWriter != null) {
            try {
                resultsWriter.close();
            } catch (Exception e) {
            }
        }
        if (damagesWriter != null) {
            try {
                damagesWriter.close();
            } catch (Exception e) {
            }
        }*/
    }

    public void runWarmup(String[] TRACKS, final ControllerData controllerData,
            int repeats, boolean noisy, long timeout, EvaluationType eType) {
        TrackDataList trackDataList = new TrackDataList();
        boolean[] ok = new boolean[TRACKS.length * repeats];
        long startTime = System.currentTimeMillis();
        long timeouts = 0;
        controllerData.setJVMOptions(javaControllerJVMOptions);
        
        if(controllerData == ControllerData.BERNIW){
            System.out.println("Warmup for BerniW does not make sense!");
            System.exit(1);
        }

        for (int i = 0; i < TRACKS.length; ++i) {
            System.out.println("---------- " + TRACKS[i] + " " + (i + 1) + "/" + (TRACKS.length) + " ----------");
            String s = TRACKS[i];
            TrackData d = trackDataList.getTrack(s);

            for (int k = 1; k <= repeats; ++k) {
                String backup = d.controllerTrackName;
                d.controllerTrackName = d.controllerTrackName + "-"
                        + (noisy ? "noisy-" : "")
                        + String.valueOf(k);

                TorcsExecConfig cfg = new TorcsExecConfig();
                cfg.set(properties);
                cfg.nodamage = true;
                cfg.nofuel = true;
                cfg.nolaptime = true;
                cfg.noisy = noisy;
                cfg.port = port;
                cfg.restart_behavior = 's';
                cfg.timeoutthreshold = 100;

                final int MAX_LAPS;

                switch (eType) {
                    case TIME_FOR_LAPS: {
                        MAX_LAPS = Integer.parseInt(properties.getProperty("warmuplaps", "5"));
                        int steps = (int) Math.round((d.length / (10.0 * 1000.0)) * 3600 * 50 * MAX_LAPS);
                        cfg.step_limit = steps;
                        break;
                    }
                    case DISTANCE_IN_CONSTANT_TIME: {
                        MAX_LAPS = 1000;
                        cfg.step_limit = Integer.parseInt(properties.getProperty("warmupticks", "100000"));
                        break;
                    }
                    default: {
                        MAX_LAPS = 5;
                    }
                }

                DirectTorcs134Evaluator eval = new DirectTorcs134Evaluator(torcsPath, expPath,
                        TEXT_DEBUG, eType);
                eval.startTorcs(d, controllerData, cfg, scr.Controller.Stage.WARMUP, host, MAX_LAPS);


                ok[i * repeats + k - 1] = eval.ok();


                timeouts += eval.timeouts();
                d.controllerTrackName = backup;
            }
        }
        int ctr = 0;
        for (int i = 0; i < TRACKS.length * repeats; ++i) {
            if (ok[i]) {
                ++ctr;
            }
        }
        System.out.println(ctr + " of " + (TRACKS.length * repeats) + " runs ok");
        long endTime = System.currentTimeMillis();
        System.out.println("This took " + Utils.timeToString((endTime - startTime) / 1000.0));
        System.out.println("Timeouts: " + timeouts);
    }

    public static void checkDirectory(String description, String path) {
        File check = new File(path);
        System.out.print("Checking " + description + ": " + check.getAbsolutePath() + " ...");
        if (check.exists()) {
            System.out.println("ok");
        } else {
            System.out.println("");
            System.out.println("Does not exist!");
            System.exit(-1);
        }
    }

    public static void runCmdLine(String[] args) {
        System.out.println("------------ Drivetool2014 for Torcs 1.3.4 ------------");

        System.out.print("Available controllers: ");
        for (ControllerData d : ControllerData.values()) {
            System.out.print(d.name() + ", ");
        }
        System.out.println("");

        Properties p = new Properties();
        if (args.length > 0 && args[0].startsWith("paramFile")) {
            try {
                String filename = "." + java.io.File.separator + args[0].substring(10, args[0].length());
                FileInputStream in = new FileInputStream(filename);
                p.load(in);
                in.close();
            } catch (Exception e) {
                e.printStackTrace(System.out);
            }
        } else {
            System.out.println("Missing param file");
            System.exit(1);
        }

        ArrayList<String> list = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer(p.getProperty("tracks"), ",");

        while (tokenizer.hasMoreTokens()) {
            list.add(tokenizer.nextToken().trim());
        }

        String[] TRACKS = new String[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            TRACKS[i] = list.get(i);
        }

        // check if all tracks are available
        TrackDataList tracklist = new TrackDataList();
        boolean success = true;
        for (String s : TRACKS) {
            TrackData td = tracklist.getTrack(s);
            if (td == null) {
                System.out.println("FAILURE: Track \"" + s + "\" not found!");
                success = false;
            }
        }
        if (!success) {
            System.exit(-1);
        }

        if (!p.containsKey("controller")) {
            System.out.println("No controller given, checking directory name");
            File currentDirectory = new File(".").getAbsoluteFile();
            File parentDirectory = currentDirectory.getParentFile();

            for (ControllerData d : ControllerData.values()) {
                if (d.getPath().contentEquals(parentDirectory.getName())) {
                    System.out.println("Assuming you mean the controller " + d.name());
                    p.setProperty("controller", d.name());
                    break;
                }
            }
        }
        
        list = new ArrayList<>();
        tokenizer = new StringTokenizer(p.getProperty("controller"), ",");

        while (tokenizer.hasMoreTokens()) {
            list.add(tokenizer.nextToken().trim());
        }

        ControllerData[] controller = new ControllerData[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            controller[i] = ControllerData.valueOf(list.get(i));
        }
        
        list = new ArrayList<>();
        tokenizer = new StringTokenizer(p.getProperty("cars"), ",");

        while (tokenizer.hasMoreTokens()) {
            list.add(tokenizer.nextToken().trim());
        }
        CarData[] carData = new CarData[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            carData[i] = new CarData(list.get(i));
        }
        
        scr.Controller.Stage stage = scr.Controller.Stage.valueOf(p.getProperty("stage"));
        int repeats = Integer.parseInt(p.getProperty("trackrepeats"));
        int evalrepeats = Integer.parseInt(p.getProperty("evalrepeats"));
        boolean textDebug = Boolean.parseBoolean(p.getProperty("TEXT_DEBUG", "false"));
        boolean noisy = Boolean.parseBoolean(p.getProperty("noisy"));
        long timeout = Long.parseLong(p.getProperty("torcs.134.timeout", "10000"));

        String ipport = p.getProperty("ipport", "127.0.0.1:3001");
        String host = ipport.substring(0, ipport.indexOf(':'));
        int port = Integer.parseInt(ipport.substring(ipport.indexOf(':') + 1));

        String jvmOptions = p.getProperty("controllerJVMOptions", "");

        EvaluationType evalType = EvaluationType.valueOf(p.getProperty("EVALUATION_TYPE", "TIME_FOR_LAPS"));
        System.out.println("Evaluation type: " + evalType);


        System.out.println("IPPort: " + host + ":" + port);

        for (int i = 0; i < controller.length; i++){
        System.out.println(controller[i].name() + " " + stage.name() + " TrackRepeats:" + repeats
                + " EvalRepeats:" + evalrepeats + " noisy:" + noisy);
        }
        System.out.println("ControllerJVMOptions: " + jvmOptions);
        String tracksOut = TRACKS.length + " Tracks: \"" + TRACKS[0] + "\"";
        for (int i = 1; i < TRACKS.length; ++i) {
            if (tracksOut.length() + TRACKS[i].length() + 2 > 79) {
                System.out.println(tracksOut);
                tracksOut = "\"" + TRACKS[i] + "\"";

            } else {
                tracksOut += ", \"" + TRACKS[i] + "\"";
            }

        }
        System.out.println(tracksOut);

        if (!p.containsKey("torcs.134.exppath")) {
            System.out.println("Properties file does not contain an experiment path");
            File currentDirectory = new File(".").getAbsoluteFile();
            //System.out.println("Name of current Directory: " + currentDirectory.getName());
            File parentDirectory = currentDirectory.getParentFile();
            //System.out.println("Name of parent: " + parentDirectory.getName());
            // check if the parent directory name matches the controller path
            if (controller[0].getPath().contentEquals(parentDirectory.getName())) {     //TODO: Look at this again, may need change
                System.out.println("Parent Directory \"" + parentDirectory.getName() + "\" matches the path name of the "
                        + controller[0].getDisplayName() + " controller.");
                File experimentDirectory = parentDirectory.getParentFile();
                System.out.println("Setting " + experimentDirectory.getAbsolutePath() + " as the experiment directory.");

                p.setProperty("torcs.134.exppath", experimentDirectory.getAbsolutePath());

            } else {
                System.out.println("Unable to automatically determine the experiment directory");
                System.exit(1);
            }
        }



        System.out.println("Torcs Path: " + p.getProperty("torcs.134.path"));
        System.out.println("Exp Path: " + p.getProperty("torcs.134.exppath"));
        System.out.println("TEXT_DEBUG: " + textDebug);
        System.out.println("Timeout: " + timeout);

        // check all the paths
        for (int i = 0; i < controller.length; i++){ 
            if (controller[i].getType() == ControllerType.JAVA) {
                checkDirectory("Java Executable Path", ControllerData.getJavaExecutablePath());
            }
        }
        checkDirectory("experiment Path", p.getProperty("torcs.134.exppath"));
        for (int i = 0; i < controller.length; i++){ 
            checkDirectory("controller path", controller[i].getPath(p.getProperty("torcs.134.exppath")));
        }
        checkDirectory("torcs path", p.getProperty("torcs.134.path"));
        checkDirectory("torcs executable path", DirectTorcs134Evaluator.getExecutablePath(p.getProperty("torcs.134.path")));

        DriveTool2014Torcs134 dtool = new DriveTool2014Torcs134(p.getProperty("torcs.134.path"),
                p.getProperty("torcs.134.exppath"), textDebug, host, port, jvmOptions, p);        

        switch (stage) {
            case WARMUP:
                //dtool.runWarmup(TRACKS, controller, repeats, noisy, timeout, evalType);       //TODO: Ignored because too lazy to alter this function too, if we need in in the future we have to alter it then
                break;
            case QUALIFYING:
                dtool.runQualifying(TRACKS, controller, carData, repeats, evalrepeats, noisy, timeout, evalType);
                break;
            case VS:
                dtool.runVSmode(TRACKS, controller, carData,  evalrepeats, repeats, noisy, timeout, evalType);
                break;
            default:
                System.out.println("Unknown stage");
                System.exit(1);
        }
    }

    public static void main(String[] args) {
        runCmdLine(args);
    }
}