/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo.tools;

import de.janquadflieg.mrracer.evo.EvaluationType;
import de.janquadflieg.mrracer.torcs.TrackData;
import de.janquadflieg.mrracer.torcs.TrackDataList;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.StringTokenizer;
import scr.Controller;
import scr.Controller.Stage;

/**
 *
 * @author quad
 */
public class IOUtils {

    /*public String createAnalyseScriptName(TestScenario ts) {
    String result = "analyse";
    result += "_" + ts.getPropertiesName();
    return result;
    }*/
    public static void createShellScript(File f, String cmd) throws Exception {
        initShellScript(f);
        appendToShellScript(f, cmd);
    }

    public static void appendToShellScript(File f, String s) throws Exception {
        OutputStreamWriter scriptWriter = new OutputStreamWriter(new FileOutputStream(f, true), "UTF-8");
        scriptWriter.write(s);
        scriptWriter.close();
    }

    public static String createToolScriptName(TestScenario ts, EvaluationType evalType, Stage stage, boolean noisy, String suffix) {
        String result = "";
        result += ts.getPropertiesName();
        if (stage == Controller.Stage.QUALIFYING && evalType != null) {
            result += "_" + (evalType == EvaluationType.TIME_FOR_LAPS ? "byTime" : "byDistance");
        }
        result += "_" + (stage == Controller.Stage.WARMUP ? "w" : "q");
        result += (noisy ? "_noisy" : "");
        result += suffix;
        return result;
    }

    public static void copyController(File srcDir, File targetDir, String[] CONTROLLER_FILES) throws Exception {
        File libsDir = new File(targetDir.getAbsolutePath() + "/" + "lib");
        libsDir.mkdir();
        for (String filestring : CONTROLLER_FILES) {
            Path src = Paths.get(srcDir + "/" + filestring);
            Path dst = Paths.get(targetDir.getAbsolutePath() + "/" + filestring);
            Files.copy(src, dst, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    public static void initShellScript(File f) throws Exception {
        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(f), "UTF-8");
        writer.write("#!/bin/sh\n");
        writer.close();
    }

    public static String createSlurmScriptName(String prefix, TestScenario ts, EvaluationType evalType, Stage stage, boolean noisy, String suffix) {
        String result = prefix;
        result += "_" + ts.getPropertiesName();
        if (stage == Controller.Stage.QUALIFYING && evalType != null) {
            result += "_" + (evalType == EvaluationType.TIME_FOR_LAPS ? "byTime" : "byDistance");
        }
        result += "_" + (stage == Controller.Stage.WARMUP ? "w" : "q");
        result += (noisy ? "_noisy" : "");
        result += suffix;
        return result;
    }    
    
    public static boolean parseTrackList(String s, boolean TEXT_OUT,
            ArrayList<TrackData> result){        
        StringTokenizer tokenizer = new StringTokenizer(s, ",");        

        // check if all tracks are available
        TrackDataList tracklist = new TrackDataList();
        boolean success = true;
        while(tokenizer.hasMoreTokens()){
            String token = tokenizer.nextToken().trim();
            TrackData td = tracklist.getTrack(token);
            if (td == null) {
                System.out.println("FAILURE: Track \"" + token + "\" not found!");
                success = false;
            } else {
                result.add(td);
            }
        }
        
        return success;
    }
}