/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo;

/**
 *
 * @author quad
 */
public interface RaceStateListener {
    public void waitingForRequest();
    public void lapTime(int lap, double time, boolean completeTime);
    public void raceEnded();
    public void distanceRaced(double d);
    public void damage(double d);
    public void timeout();
    public void overallTime(double d);
    public void reachedMaxTicks();
    public void ticks(int i);
}
