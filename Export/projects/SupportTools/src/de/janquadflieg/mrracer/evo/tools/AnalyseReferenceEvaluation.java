/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo.tools;

import de.janquadflieg.mrracer.evo.*;
import de.janquadflieg.mrracer.torcs.*;
import java.io.*;
import java.util.*;

/**
 *
 * @author quad
 */
public class AnalyseReferenceEvaluation {

    private static TrackDataList trackDataList = new TrackDataList();

    private static ArrayList<String> parseFile(String fileName, int entries)
            throws Exception {
        ArrayList<String> results = new ArrayList<>();

        if (new File(fileName).exists()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
            String line = reader.readLine();
            for (int i = 0; i < entries; ++i) {
                line = reader.readLine();
                line = line.substring(line.indexOf(';') + 1);
                //System.out.println(line);
                results.add(line);
            }
            
        } else {
            System.out.println("WARNING: " + fileName + " does not exist! Returning Zeros");
            for (int i = 0; i < entries; ++i) {
                results.add("0.0");
            }
        }

        return results;
    }

    public static void analyse(File directory, ControllerData controller)
            throws Exception {
        /*File scenarioDirectory = new File("./" + scenario.getDirectoryName());
         File[] directoryContents = scenarioDirectory.listFiles();
         boolean initResultFile = true;*/
        ArrayList<TrackData> trackList = new ArrayList<>();
        String fileName = directory.getAbsolutePath() + File.separator + TestScenario.RACE_WEEKEND.getDirectoryName() + File.separator
                + controller.getPath() + File.separator + "w.properties";
        Properties p = new Properties();
        p.load(new FileInputStream(fileName));


        StringTokenizer tokenizer = new StringTokenizer(p.getProperty("tracks"), ",");

        while (tokenizer.hasMoreTokens()) {
            trackList.add(trackDataList.getTrack(tokenizer.nextToken().trim()));
        }

        ArrayList<ArrayList<String>> allData = new ArrayList<>();

        for (TestScenario scenario : new TestScenario[]{TestScenario.RACE_WEEKEND_2015, TestScenario.RACE_WEEKEND}) {
            for (boolean noisy : new boolean[]{false, true}) {
                for (EvaluationType evalType : new EvaluationType[]{EvaluationType.DISTANCE_IN_CONSTANT_TIME, EvaluationType.TIME_FOR_LAPS}) {
                    String[] suffixes;
                    if (evalType == EvaluationType.DISTANCE_IN_CONSTANT_TIME) {
                        suffixes = new String[]{"10000ticks", "75000ticks"};
                    } else {
                        suffixes = new String[]{"2laps", "5laps"};
                    }

                    for (String suffix : suffixes) {
                        String resultsFileName = directory.getAbsolutePath() + File.separator + scenario.getDirectoryName() + File.separator
                                + controller.getPath() + File.separator
                                + DriveTool2014Torcs134.createResultsFileName(noisy, evalType, suffix);
                        String damageFileName = directory.getAbsolutePath() + File.separator + scenario.getDirectoryName() + File.separator
                                + controller.getPath() + File.separator
                                + DriveTool2014Torcs134.createDamageFileName(noisy, evalType, suffix);
                        System.out.println(resultsFileName);
                        allData.add(parseFile(resultsFileName, trackList.size()));

                        System.out.println(damageFileName);
                        allData.add(parseFile(damageFileName, trackList.size()));
                    }
                }
            }
        }

        System.out.println(allData.size() + " different results for each track");

        OutputStreamWriter writer = new OutputStreamWriter(
                new FileOutputStream(directory.getAbsolutePath() + File.separator + controller.name() + "_all_data.csv"), "UTF-8");
        for (int i = 0; i < trackList.size(); ++i) {
            writer.write(trackList.get(i).trackName);
            for (int k = 0; k < allData.size(); ++k) {
                writer.write(";");
                writer.write(allData.get(k).get(i));
            }
            writer.write("\n");
        }
        writer.close();

        writer = new OutputStreamWriter(
                new FileOutputStream(directory.getAbsolutePath() + File.separator + controller.name() + "_all_data_excel.csv"), "UTF-8");
        for (int i = 0; i < trackList.size(); ++i) {
            writer.write(trackList.get(i).trackName);
            for (int k = 0; k < allData.size(); ++k) {
                writer.write(";");
                writer.write(allData.get(k).get(i).replace('.', ','));
            }
            writer.write("\n");
        }
        writer.close();
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Missing Parameter for the controller");
            System.out.println("Usage:");
            System.out.print("AnalyseReferenceEvaluation ");
            ControllerData[] controllers = ControllerData.values();
            for (int i = 0; i < controllers.length; ++i) {
                if (i > 0) {
                    System.out.print("|");
                }
                System.out.print(controllers[i].name());
            }
            System.out.println("");
            System.exit(1);
        }
        try {
            //args[0] = ControllerData.MRRACERGECCO2013.name();
            AnalyseReferenceEvaluation.analyse(new File("./"), ControllerData.valueOf(args[0]));

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

    }
}
