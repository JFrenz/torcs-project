/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo;

import java.io.*;

/**
 *
 * @author quad
 */
public class StreamReader
        implements Runnable {

    private InputStream in;
    private boolean run = true;
    private String title;
    private boolean TEXT_OUT = true;
    private String outputFile;

    public StreamReader(String title, InputStream in, boolean b) {
        this(title, in, null, b);
    }

    public StreamReader(String title, InputStream in, String out, boolean b) {
        this.in = in;
        this.title = title;
        this.outputFile = out;
        this.TEXT_OUT = b;
        Thread t = new Thread(this);
        t.start();
    }

    public void run() {

        OutputStreamWriter w = null;
        if (outputFile != null) {
            try {
                w = new OutputStreamWriter(new FileOutputStream(new File(outputFile)), "UTF-8");

            } catch (Exception e) {
                e.printStackTrace(System.out);
                w = null;
            }
        }

        try {
            String line = "";
            while (run) {
                int i = in.read();
                char c = (char) i;
                if (i == -1) {
                    if (TEXT_OUT) {
                        System.out.println("Got -1, stopping thread");
                    }
                    run = false;

                } else if (c == '\n') {
                    if (TEXT_OUT) {
                        System.out.println("[" + title + "] " + line);
                    }
                    if (w != null) {
                        w.write(line + "\n");
                        w.flush();
                    }
                    line = "";

                } else if (c == '\r') {
                } else {
                    line += (char) i;
                }
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        if (w != null) {
            try {
                w.flush();
                w.close();
            } catch (Exception e) {
                e.printStackTrace(System.out);
            }
        }
    }

    public void stop() {
        run = false;
    }
}