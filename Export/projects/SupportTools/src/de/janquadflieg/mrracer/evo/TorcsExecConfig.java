/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo;

/**
 *
 * @author quad
 */
public class TorcsExecConfig {
    public char restart_behavior = 'r'; // ok
    public int port = 3001; // ok
    public int scr_server_instance = 1;
    public int step_limit = -1; // ok
    public int server_behavior = 0; // ok
    public boolean noisy = false; // ok
    public long timeout = 10000; // ok
    public boolean nodamage = false; // ok
    public boolean nolaptime = false; // ok
    public boolean nofuel = false; // ok
    public char reportresults = 'c';
    public boolean udpserververbose = false;
    public int timeoutthreshold = 0;
    public double initialfuel = 0.0;
    
    public void set(java.util.Properties p){
        set(p, "");        
    }
    
    public void set(java.util.Properties p, String prefix){
        timeout = Long.parseLong(p.getProperty(prefix+"torcs.134.timeout", String.valueOf(timeout)));
        restart_behavior = p.getProperty(prefix+"torcs.134.restartbehavior", "r").charAt(0);
        udpserververbose = Boolean.parseBoolean(p.getProperty(prefix+"torcs.134.udpserververbose", "false"));
        initialfuel = Double.parseDouble(p.getProperty(prefix+"torcs.134.initialfuel", "0.0"));
    }    
    
    public String toArgumentString(){
        StringBuilder result = new StringBuilder();
        
        result.append(" -restart ").append(restart_behavior);
        
        result.append(" -udpbaseport ").append(port);
        
        if(step_limit != -1){
            result.append(" -steplimit ").append(step_limit);
        }
        
        result.append(" -serverbehavior ").append(server_behavior);
                
        if(noisy){
            result.append(" -noisy");
        }
        
        result.append(" -t ").append(timeout);
        
        if(nodamage){
            result.append(" -nodamage");
        }
        
        if(nolaptime){
            result.append(" -nolaptime");
        }
        
        if(nofuel){
            result.append(" -nofuel");
        }
        
        result.append(" -reportresults c");
        
        if(udpserververbose){
            result.append(" -udpserververbose");
        }
        
        if(timeoutthreshold > 0){
            result.append(" -timeoutthreshold ").append(timeoutthreshold);
        }
        
        if(initialfuel != 0.0){
            result.append(" -initialfuel ").append(String.valueOf(initialfuel));
        }
        
        return result.toString();
    }
}
