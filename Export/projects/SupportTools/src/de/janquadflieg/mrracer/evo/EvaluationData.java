/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo;

/**
 *
 * @author quad
 */
public interface EvaluationData {
    
    public static final int BY_DAMAGE = 1;
    public static final int BY_LAT_SPEED = 2;
    
    public double getAdjustedTime(int byWhat);
    public double getDamage();
    public double getDistanceRaced();
    public double getFastestLap();
    public int getLapCtr();
    public double getLateralSpeedIntegral();
    public double getOverallTime();
    public int getOvertakingCtr();
    public boolean maxDamageReached();
    
}
