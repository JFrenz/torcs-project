/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo;

/**
 *
 * @author quad
 */
public class ProcessObserver
implements Runnable{
    private int result = -1;
    private Process p;
    private ProcessListener l;
    
    public ProcessObserver(Process p, ProcessListener l){
        this.p = p;
        this.l = l;
        Thread t = new Thread(this, "ProcessObserver: "+p.toString());
        t.start();
    }
    
    public int getResult(){
        return result;
    }
    
    public void run(){
        try{
            result = p.waitFor();
            l.processTerminated(result);
            
        } catch(Exception e){
            e.printStackTrace(System.out);
        }
    }            
}
