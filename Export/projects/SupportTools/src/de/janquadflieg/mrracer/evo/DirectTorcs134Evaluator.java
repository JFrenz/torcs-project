/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo;

import de.janquadflieg.mrracer.evo.EvaluationType;
import de.janquadflieg.mrracer.torcs.TrackDataList;
import de.janquadflieg.mrracer.torcs.TrackData;
import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author quad
 */
public class DirectTorcs134Evaluator
        implements RaceStateListener {

    private boolean TEXT_DEBUG = false;
    private double time = 0.0;
    private double overallTime = 0.0;
    private boolean reachedTickLimit = false;
    private int maxTicks = -1;
    private int maxLaps = -1;
    private int lapsCompleted = 0;
    private int waitedForRequest = 0;
    private Process controllerProcess;
    private Process torcsProcess;
    private boolean controllerTerminated = false;
    private boolean torcsTerminated = false;
    private int torcsResult = 1;
    private int controllerResult = 1;
    private String torcsBasePath;
    private String controllerBasePath;
    private EvaluationType evalType;
    private double [] VSTimes;
    /**
     * Timeout in microseconds.
     */
    //private long timeout = 10000;
    private long timeouts = 0;
    private double distance = 0.0;
    private double damage = 0.0;
    private int ticks = 0;

    public DirectTorcs134Evaluator(String torcsBasePath, String controllerBasePath,
            boolean textDebug, EvaluationType et) {
        this.torcsBasePath = torcsBasePath;
        this.controllerBasePath = controllerBasePath;
        this.TEXT_DEBUG = textDebug;
        this.evalType = et;
    }

    public static String getExecutablePath(String basePath) {
        StringBuilder result = new StringBuilder();
        result.append(basePath);

        if (System.getProperty("os.name").startsWith("Windows")) {
            result.append(File.separator).append("wtorcs.exe");

        } else if (System.getProperty("os.name").startsWith("Linux")) {
            result.append(File.separator).append("bin").append(File.separator).append("torcs");

        } else {
            System.out.println("Unknown OS: " + System.getProperty("os.name"));
            System.exit(-1);
        }

        return result.toString();
    }

    private synchronized void torcsTerminated(int result) {
        torcsResult = result;
        torcsTerminated = true;
        notifyAll();
    }

    private synchronized void controllerTerminated(int result) {
        controllerResult = result;
        controllerTerminated = true;
        notifyAll();
    }

    @Override
    public synchronized void timeout() {
        ++timeouts;
    }

    public synchronized void reachedMaxTicks() {
        reachedTickLimit = true;
    }

    public synchronized long timeouts() {
        return timeouts;
    }

    public synchronized void ticks(int i) {
        ticks = i;
    }

    @Override
    public synchronized void damage(double d) {
        this.damage = d;
    }

    public synchronized void overallTime(double d) {
        this.overallTime = d;
    }

    @Override
    public synchronized void lapTime(int lap, double time, boolean b) {
        lapsCompleted = lap;
        if (b) {
            this.time = time;
            if (TEXT_DEBUG) {
                System.out.println("After lap " + lap + ": " + time);
            }

        } else {
            this.time += time;
            if (TEXT_DEBUG) {
                System.out.println("Result for lap " + lap + ": " + time);
            }
        }
    }

    @Override
    public synchronized void distanceRaced(double d) {
        distance = d;
    }

    @Override
    public synchronized void raceEnded() {
    }

    @Override
    public synchronized void waitingForRequest() {
        ++waitedForRequest;
        notifyAll();
    }

    public double getTime() {
        return time;
    }
    
    public String getVSTimes(){
        String timelist = "";
        for(int i = 0; i< VSTimes.length;i++){
            Double temp = VSTimes[i];
            timelist = timelist.concat(temp.toString())+";";
        }
        return timelist;
    }

    public double getDistanceRaced() {
        return distance;
    }

    public double getOverallTime() {
        return overallTime;
    }

    public double getDamage() {
        return damage;
    }

    public boolean ok() {
        boolean result = true;
        result &= this.controllerResult == 0;

        if (evalType == EvaluationType.DISTANCE_IN_CONSTANT_TIME) {
            result &= reachedTickLimit;

        } else {
            result &= this.lapsCompleted == this.maxLaps;
        }

        return result;
    }

    public void startTorcs(TrackData trackData, ControllerData controllerData,
            TorcsExecConfig cfg,
            final scr.Controller.Stage stage, String host, int maxLaps) {
        this.maxTicks = cfg.step_limit;
        this.maxLaps = maxLaps;

        try {
            //scr.Controller.Stage stage = scr.Controller.Stage.QUALIFYING;
            if (TEXT_DEBUG) {
                System.out.println("Building xml file");
            }

            String quickraceXml;
            if (controllerData.getType() == ControllerType.NATIVE_TORCS) {
                // todo, more generic
                quickraceXml = BerniwEvaluator.createConfigFile(trackData, controllerData, maxLaps);

            } else {
                quickraceXml = Torcs134Executor.createConfigFile(trackData, cfg.scr_server_instance, maxLaps);
            }


            if (TEXT_DEBUG) {
                System.out.println(quickraceXml);
            }

            String TORCS_PATH = torcsBasePath;

            File raceConfig = new File(TORCS_PATH + File.separator + "quickrace" + cfg.port + controllerData.getDriverName()+".xml");
            File raceResultFolder = new File(TORCS_PATH+File.separator+"results"+File.separator+raceConfig.getName().substring(0, raceConfig.getName().lastIndexOf(".")));
            if(!raceResultFolder.exists()){
                raceResultFolder.mkdir();
            }
           // File raceConfig = new File(TORCS_PATH + File.separator + "quickrace" + cfg.port + ".xml");


            OutputStreamWriter w = new OutputStreamWriter(new FileOutputStream(raceConfig), "UTF-8");
            w.write(quickraceXml);
            w.flush();
            w.close();

            String torcsParams = " -r " + raceConfig.getName();
            torcsParams += cfg.toArgumentString();
            int numberOfOldResultFiles = raceResultFolder.listFiles().length;
            String torcsExecDirectory = TORCS_PATH;

            if (TEXT_DEBUG) {
                System.out.println("[TORCS] Starting Process");
                System.out.println(getExecutablePath(TORCS_PATH) + torcsParams);
                System.out.println("in directory");
                System.out.println(torcsExecDirectory);
            }

            torcsProcess = Runtime.getRuntime().exec(getExecutablePath(TORCS_PATH) + torcsParams, null, new File(torcsExecDirectory));

            ProcessObserver poTorcs = new ProcessObserver(torcsProcess, new ProcessListener() {
                @Override
                public void processTerminated(int result) {
                    torcsTerminated(result);
                }
            });

            TorcsStreamReader torcscerr = new TorcsStreamReader(torcsProcess.getErrorStream(), "-cerr", TEXT_DEBUG);
            torcscerr.addListener(this);
            Thread t1 = new Thread(torcscerr, "TORCS-cerr");
            t1.start();

            TorcsStreamReader torcscout = new TorcsStreamReader(torcsProcess.getInputStream(), "-cout", TEXT_DEBUG);
            torcscout.addListener(this);
            Thread t2 = new Thread(torcscout, "TORCS-cout");
            t2.start();
            
            StreamReader s3 = null;
            StreamReader s4 = null;

            if (controllerData.getType() == ControllerType.NATIVE_TORCS) {
                if (TEXT_DEBUG) {
                    System.out.println("Native TORCS controller, no need to wait");
                }

                controllerTerminated = true;
                controllerResult = 0;                       
                
            } else {
                synchronized (this) {
                    if (TEXT_DEBUG) {
                        System.out.println("Waiting");
                    }
                    while (waitedForRequest < 1) {
                        wait();
                    }
                }

                if (TEXT_DEBUG) {
                    System.out.println("Torcs up and running");
                    System.out.println("Starting controller");
                }

                String controllerCmd = controllerData.getCMDLine(stage, trackData, controllerBasePath, host, cfg.port);

                System.out.println(controllerCmd);
                System.out.println(controllerData.getPath(controllerBasePath));

                controllerProcess = Runtime.getRuntime().exec(
                        controllerCmd, null, new File(controllerData.getPath(controllerBasePath)));

                s3 = new StreamReader("Error", controllerProcess.getErrorStream(), TEXT_DEBUG);
                
                if (stage == scr.Controller.Stage.WARMUP) {
                    s4 = new StreamReader("Input", controllerProcess.getInputStream(),
                            controllerData.getPath(controllerBasePath) + File.separator + trackData.controllerTrackName + "_warmup_process.log", TEXT_DEBUG);
                } else {
                    s4 = new StreamReader("Input", controllerProcess.getInputStream(), TEXT_DEBUG);
                }

                ProcessObserver pl = new ProcessObserver(controllerProcess, new ProcessListener() {
                    @Override
                    public void processTerminated(int result) {
                        controllerTerminated(result);
                    }
                });
            }

            synchronized (this) {
                while (!(torcsTerminated && controllerTerminated)) {
                    if (TEXT_DEBUG) {
                        System.out.println("-------------------------------------------------");
                        System.out.println("Waiting for processes to terminate...");
                        System.out.println("Drivername: " + controllerData.getDisplayName().split("_")[0].toLowerCase());
                    }

                    wait();

                    if (TEXT_DEBUG) {
                        System.out.println("controllerTerminated: " + controllerTerminated + " [" + controllerResult + "]");
                        System.out.println("torcsTerminated: " + torcsTerminated + " [" + torcsResult + "]");
                    }

                    if (torcsTerminated && !controllerTerminated) {
                        if (TEXT_DEBUG) {
                            System.out.println("Controller not yet finished, waiting 5s");
                        }

                        long waitTime = 5000;
                        long start = System.currentTimeMillis();
                        long stop = start;
                        while (stop - start < waitTime) {
                            long slept = stop - start;
                            wait(waitTime - slept);
                            stop = System.currentTimeMillis();
                        }

                        if (controllerTerminated) {
                            if (TEXT_DEBUG) {
                                System.out.println("Ok, controller finished in the meantime[" + controllerResult + "]");
                            }
                        } else {
                            if (TEXT_DEBUG) {
                                System.out.println("Controller still not finished, destroying controller process");
                            }
                            controllerProcess.destroy();
                        }
                    }
                }
            }

            System.out.println("laps/maxLaps: " + lapsCompleted + "/" + maxLaps
                    + ", ticks/maxTicks: " + ticks + "/" + maxTicks + ", reachedTickLimit: " + reachedTickLimit);


            System.out.println("Overall time: " + overallTime + ", distance: "
                    + distance + ", laps: " + lapsCompleted + ", ok?" + ok()
                    + ", timeouts: " + timeouts() + ", damage: " + damage);

            System.out.println("Torcs exit code: " + torcsResult + ", controller exit code: " + controllerResult);



            torcscerr.stop();
            torcscout.stop();
            
            if(controllerData.getType() != ControllerType.NATIVE_TORCS){
                s3.stop();
                s4.stop();
            }
            File[] raceFile = raceResultFolder.listFiles();
            if(raceFile.length > numberOfOldResultFiles){
                if(!controllerData.getDriverName().equals(ControllerData.BERNIW.getDriverName())){
                    FileReader in = new FileReader(raceFile[raceFile.length-1].getPath());
                    BufferedReader buf = new BufferedReader(in);
                    for(int i = 0; i < 36; i++){
                        buf.readLine();
                    }
                    String dmg = buf.readLine();
                    dmg = dmg.substring(dmg.indexOf("val=\""), dmg.length());
                    dmg = dmg.substring(dmg.indexOf("\"")+1, dmg.lastIndexOf("\""));
                    this.damage(Double.parseDouble(dmg));
                    in.close();
                }
                if(!TEXT_DEBUG){
                    raceFile[raceFile.length-1].delete();
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
    public void startTorcs(TrackData trackData, ControllerData[] controllerData,
            TorcsExecConfig cfg,
            final scr.Controller.Stage stage, String host, int maxLaps) {
        this.maxTicks = cfg.step_limit;
        this.maxLaps = maxLaps;
        this.VSTimes = new double[controllerData.length];
        for(int i=0; i < controllerData.length; i++){
            if (controllerData[i].getType() != ControllerType.NATIVE_TORCS){
                System.out.println("For VSMode only native AIs are supported yet");
                System.out.println("System exit");
                System.exit(1);
            }
        }
        try {
            //scr.Controller.Stage stage = scr.Controller.Stage.QUALIFYING;
            if (TEXT_DEBUG) {
                System.out.println("Building xml file");
            }

            String quickraceXml;
            if (controllerData[0].getType() == ControllerType.NATIVE_TORCS) {
                // todo, more generic
                quickraceXml = BerniwEvaluator.createConfigFile(trackData, controllerData, maxLaps);

            } else {
                quickraceXml = Torcs134Executor.createConfigFile(trackData, cfg.scr_server_instance, maxLaps);
            }


            if (TEXT_DEBUG) {
                System.out.println(quickraceXml);
            }

            String TORCS_PATH = torcsBasePath;

            File raceConfig = new File(TORCS_PATH + File.separator + "quickrace" + cfg.port + trackData.trackName+".xml");
            File raceResultFolder = new File(TORCS_PATH+File.separator+"results"+File.separator+raceConfig.getName().substring(0, raceConfig.getName().lastIndexOf(".")));
            if(!raceResultFolder.exists()){
                raceResultFolder.mkdir();
            }

            OutputStreamWriter w = new OutputStreamWriter(new FileOutputStream(raceConfig), "UTF-8");
            w.write(quickraceXml);
            w.flush();
            w.close();

            String torcsParams = " -r \"" +raceConfig.getName()+"\"";
            torcsParams += cfg.toArgumentString();
            int numberOfOldResultFiles = raceResultFolder.listFiles().length;
            String torcsExecDirectory = TORCS_PATH;

            if (TEXT_DEBUG) {
                System.out.println("[TORCS] Starting Process");
                System.out.println(getExecutablePath(TORCS_PATH) + torcsParams);
                System.out.println("in directory");
                System.out.println(torcsExecDirectory);
            }

            torcsProcess = Runtime.getRuntime().exec(getExecutablePath(TORCS_PATH) + torcsParams, null, new File(torcsExecDirectory));

            ProcessObserver poTorcs = new ProcessObserver(torcsProcess, new ProcessListener() {
                @Override
                public void processTerminated(int result) {
                    torcsTerminated(result);
                }
            });

            TorcsStreamReader torcscerr = new TorcsStreamReader(torcsProcess.getErrorStream(), "-cerr", TEXT_DEBUG);
            torcscerr.addListener(this);
            Thread t1 = new Thread(torcscerr, "TORCS-cerr");
            t1.start();

            TorcsStreamReader torcscout = new TorcsStreamReader(torcsProcess.getInputStream(), "-cout", TEXT_DEBUG);
            torcscout.addListener(this);
            Thread t2 = new Thread(torcscout, "TORCS-cout");
            t2.start();
            
            
            if (TEXT_DEBUG) {
                System.out.println("Native TORCS controller, no need to wait");
            }

            controllerTerminated = true;
            controllerResult = 0;                       
                
           
            synchronized (this) {
                while (!(torcsTerminated && controllerTerminated)) {
                    if (TEXT_DEBUG) {
                        System.out.println("-------------------------------------------------");
                        System.out.println("Waiting for processes to terminate...");
                        System.out.print("Drivernames: ");
                        for(int i=0; i < controllerData.length; i++){
                            System.out.print(", " + controllerData[i].getDisplayName().split("_")[0].toLowerCase());
                        }
                        System.out.println();
                    }

                    wait();

                    if (TEXT_DEBUG) {
                        System.out.println("controllerTerminated: " + controllerTerminated + " [" + controllerResult + "]");
                        System.out.println("torcsTerminated: " + torcsTerminated + " [" + torcsResult + "]");
                    }

                    if (torcsTerminated && !controllerTerminated) {
                        if (TEXT_DEBUG) {
                            System.out.println("Controller not yet finished, waiting 5s");
                        }

                        long waitTime = 5000;
                        long start = System.currentTimeMillis();
                        long stop = start;
                        while (stop - start < waitTime) {
                            long slept = stop - start;
                            wait(waitTime - slept);
                            stop = System.currentTimeMillis();
                        }

                        if (controllerTerminated) {
                            if (TEXT_DEBUG) {
                                System.out.println("Ok, controller finished in the meantime[" + controllerResult + "]");
                            }
                        } else {
                            if (TEXT_DEBUG) {
                                System.out.println("Controller still not finished, destroying controller process");
                            }
                            controllerProcess.destroy();
                        }
                    }
                }
            }

            System.out.println("laps/maxLaps: " + lapsCompleted + "/" + maxLaps
                    + ", ticks/maxTicks: " + ticks + "/" + maxTicks + ", reachedTickLimit: " + reachedTickLimit);


            System.out.println("Overall time: " + overallTime + ", distance: "
                    + distance + ", laps: " + lapsCompleted + ", ok?" + ok()
                    + ", timeouts: " + timeouts() + ", damage: " + damage);

            System.out.println("Torcs exit code: " + torcsResult + ", controller exit code: " + controllerResult);



            torcscerr.stop();
            torcscout.stop();
            
            File[] raceFile = raceResultFolder.listFiles();
            if(raceFile.length > numberOfOldResultFiles){
                FileReader in = new FileReader(raceFile[raceFile.length-1].getPath());
                BufferedReader buf = new BufferedReader(in);
                int i = 0;
                boolean redy = false;
                int count = 0;
                for(; i < 17; i++){
                    buf.readLine();
                }
                while(!redy){
                    String driver = buf.readLine();
                    System.out.println(driver);
                    i++;
                    try{
                        driver = driver.substring(driver.indexOf("name=\""), driver.length());
                        driver = driver.substring(driver.indexOf("\"")+1, driver.lastIndexOf("\""));
                        Integer.parseInt(driver);
                        count++;
                        for(int k = 0 ; k < 4; k++,i++){
                            buf.readLine();
                        }
                    }catch (NumberFormatException ex){
                        redy = true;
                    }catch (StringIndexOutOfBoundsException ex){
                        redy = true;
                    }
                }
                System.out.println(count);
                buf.readLine();
                i++;
                for(int k = 0 ; k < count; k++){
                    for(int j = 0; j < 9; j++,i++){
                        buf.readLine();
                    }
                    String time = buf.readLine();
                    i++;
                    time = time.substring(time.indexOf("val=\""), time.length());
                    time = time.substring(time.indexOf("\"")+1, time.lastIndexOf("\""));
                    System.out.println(time);
                    for(int j = 0; j < 4; j++,i++){
                        buf.readLine();
                    }
                    String name = buf.readLine();
                    i++;
                    name = name.substring(name.indexOf("val=\""), name.length());
                    name = name.substring(name.indexOf("\"")+1, name.lastIndexOf("\""));
                    for(int c = 0; c < controllerData.length; c++){
                        if(name.equalsIgnoreCase(controllerData[c].getDriverName())){
                            VSTimes[c] = Double.parseDouble(time);
                        }
                    }
                    System.out.println(name);
                }
                in.close();
                if(!TEXT_DEBUG){
                    raceFile[raceFile.length-1].delete();
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    public static void main(String[] args) {
        //DirectTorcs134Evaluator instance = new DirectTorcs134Evaluator();
        //TrackDataList trackDataList = new TrackDataList();
        /*TrackData trackData = new TrackData();
         trackData.trackName = "wheel2";
         trackData.torcsCategory = "road";
         trackData.torcsTrackName = "wheel-2";*/
        //trackData.trackName = "cgtrack2";
        //trackData.controllerTrackName = "cgtrack2";
        //trackData.torcsCategory = "road";
        //trackData.torcsTrackName = "g-track-2";
//        TrackData d = trackDataList.getTrack("Wheel 2");
//        
//        String JAVA_PATH = "C:\\Program Files\\Java\\jdk1.7.0_55\\bin\\java.exe";
//
//        ControllerData CD_MR2014ALPHA = ControllerData.MR2014ALPHA;
//
//
//
//        //String controllerCmd = 
//
//        /*Process controllerProcess = Runtime.getRuntime().exec(AUTOPIA_PATH
//         + "\\client host:127.0.0.1 port:3001 stage:1 track:"
//         + trackData.trackName + " id:SCR", null, new File(AUTOPIA_PATH));*/
//
//        instance.startTorcs(d, CD_MR2014ALPHA, false, scr.Controller.Stage.QUALIFYING);
    }
}