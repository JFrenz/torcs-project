/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo.tools;

/**
 *
 * @author quad
 */
public enum TestScenario {
    REEVALUATE {
        public String getDirectoryName() {
            return "Reevaluate";
        }

        public String getPropertiesName() {
            return "re";
        }
    }, RACE_WEEKEND {
        public String getDirectoryName() {
            return "RaceWeekend";
        }

        public String getPropertiesName() {
            return "rw";
        }
    }, RACE_WEEKEND_2015 {
        public String getDirectoryName() {
            return "RaceWeekend2015";
        }

        public String getPropertiesName() {
            return "rw15";
        }
    };

    public abstract String getDirectoryName();

    public abstract String getPropertiesName();
    
}
