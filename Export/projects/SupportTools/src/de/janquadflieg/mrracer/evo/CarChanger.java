/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.evo;

/**
 *
 * @author Jonas
 */
public class CarChanger {
        private static final String CONFIG_FILE_1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<!DOCTYPE params SYSTEM \"../../libs/tgf/params.dtd\">\n"
            + "\n"
            + "\n";
            
    private static final String CONFIG_FILE_2 = "  <section name=\"Robots\">\n"
            + "    <section name=\"index\">\n"
            + "\n";
       
    private static final String CONFIG_FILE_3 = "      <attstr name=\"desc\" val=\"Altered by Jonas\"/>\n"
            + "      <attstr name=\"author\" val=\"Altered by Jonas\"/>\n";
    
    private static final String CONFIG_FILE_4 = "      <attnum name=\"race number\" val=\"138\"/>\n"
            + "      <attnum name=\"red\" val=\"0.0\"/>\n"
            + "      <attnum name=\"green\" val=\"1.0\"/>\n"
            + "      <attnum name=\"blue\" val=\"1.0\"/>\n"
            + "    </section>\n"
            + "\n";
            
    private static final String CONFIG_FILE_5 = "  </section>\n"
            + "</section>\n"
            + "</params>";
    
    
    public static String createDriverFile(ControllerData controllerData, CarData[] carData){
        String driverXml = CONFIG_FILE_1;
        driverXml += "<params name=\"" + controllerData.getDriverName() + "\" type=\"robotdef\">\n";
        driverXml += CONFIG_FILE_2;
        for (int i = 1; i <= carData.length; i++){
            if(controllerData.getDriverName().equalsIgnoreCase("BT") || controllerData.getDriverName().equalsIgnoreCase("Damned") || controllerData.getDriverName().equalsIgnoreCase("Olethros")){
                driverXml += "<section name=\"" + (i - 1) + "\">\n";
                driverXml += "<attstr name=\"name\" val=\"" + controllerData.getDriverName() + " " + (i - 1) + "\"/>\n";
            }
            else{
                driverXml += "<section name=\"" + i + "\">\n";
                driverXml += "<attstr name=\"name\" val=\"" + controllerData.getDriverName() + " " + i + "\"/>\n";
            }
            driverXml += "<attstr name=\"team\" val=\"" + controllerData.getDriverName() + "\"/>\n";
            driverXml += CONFIG_FILE_3;
            driverXml += "<attstr name=\"car name\" val=\"" + carData[i-1].getCarName() + "\"/>\n";
            driverXml += CONFIG_FILE_4;
        }
        for (int i = carData.length + 1; i <= 10; i++){
            driverXml += "<section name=\"" + i + "\">\n";
            driverXml += "<attstr name=\"name\" val=\"" + controllerData.getDriverName() + " " + i + "\"/>\n";
            driverXml += "<attstr name=\"team\" val=\"" + controllerData.getDriverName() + "\"/>\n";
            driverXml += CONFIG_FILE_3;
            driverXml += "<attstr name=\"car name\" val=\"DummyEntry\"/>";
            driverXml += CONFIG_FILE_4;
        }
        driverXml += CONFIG_FILE_5;        
        return driverXml;
    } 
}
