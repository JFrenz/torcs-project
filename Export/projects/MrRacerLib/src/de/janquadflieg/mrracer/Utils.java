/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import optimizer.ea.Fitness;

/**
 *
 * @author Jan Quadflieg
 */
public class Utils {

    /**
     * NumberFormat to convert double values to strings.
     */
    private final static DecimalFormat FORMAT = new DecimalFormat("#,##0.00;#,##0.00");
    /**
     * A constant indicating that there is no valid data.
     */
    public static final double NO_DATA_D = Double.NEGATIVE_INFINITY;
    /**
     * A constant indicating that there is no valid data.
     */
    public static final int NO_DATA_I = Integer.MIN_VALUE;
    /**
     * A constant indicating that there is no valid data.
     */
    public static final long NO_DATA_L = Long.MIN_VALUE;

    public static String timeToString(double d) {
        if (d == NO_DATA_D) {
            return "No Data";

        } else {
            int minutes = ((int) d) / 60;
            int seconds = ((int) d) - (minutes * 60);

            String result = "";
            if (minutes < 10) {
                result += "0";
            }
            result += String.valueOf(minutes) + ":";
            if (seconds < 10) {
                result += "0";
            }
            result += String.valueOf(seconds);

            return result;
        }
    }

    public static String timeToExactString(double d) {
        if (d == NO_DATA_D) {
            return "No Data";

        } else {
            int minutes = ((int) d) / 60;
            int seconds = ((int) d) - (minutes * 60);
            int hundreds = (int) ((d - ((int) d)) * 100.0);
            String result = String.valueOf(minutes) + ":";
            if (seconds < 10) {
                result += "0";
            }
            result += String.valueOf(seconds) + ".";
            if (hundreds < 10) {
                result += "0";
            }
            result += String.valueOf(hundreds);

            return result;
            //return FORMAT.format(d);
        }
    }

    public static String dTS(double d) {
        if (d == NO_DATA_D) {
            return "No Data";

        } else {
            return FORMAT.format(d);
        }
    }

    public static String iTS(int i) {
        if (i == NO_DATA_I) {
            return "No Data";

        } else {
            return String.valueOf(i);
        }
    }

    public static boolean createPath(String path)
            throws Exception {
        File f = new File(path);

        return f.mkdirs();
    }

    /**
     * Method to check if a given number is even.
     *
     * @ return true if i is an even number.
     */
    public static boolean even(int i) {
        return (i & 1) == 0;
    }

    /**
     * Method to check if a given number is odd.
     *
     * @ return true if i is an odd number.
     */
    public static boolean odd(int i) {
        return (i & 1) != 0;
    }

    public static String[] toArrayOfStrings(String s, String delim) {
        StringTokenizer tokenizer = new StringTokenizer(s, delim);

        String[] result = new String[tokenizer.countTokens()];
        int i = 0;
        while (tokenizer.hasMoreTokens()) {
            result[i] = tokenizer.nextToken();
            ++i;
        }

        return result;
    }

    public static String list(Properties p, String delim) {
        StringBuilder result = new StringBuilder();
        Set<String> set = p.stringPropertyNames();
        ArrayList<String> entries = new ArrayList<>(set);
        Collections.sort(entries);
        for (int i = 0; i < entries.size(); ++i) {
            result.append(entries.get(i)).append("=").append(p.getProperty(entries.get(i))).append(delim);
        }
        return result.toString();
    }

    //assure that value lies in [min,max], cut higher/lower values
    public static double truncate(final double value, final double min, final double max) {
        return Math.min(Math.max(value, min), max);
    }

    //assure that value lies in [min,max], cut higher/lower values
    public static int truncate(final int value, final int min, final int max) {
        return Math.min(Math.max(value, min), max);
    }

    public static String toSHA1(String input) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("SHA1");
            md.update(input.getBytes("UTF-8"));
            byte[] hash = md.digest();

            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.length; ++i) {
                result.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
            }

            return result.toString();

        } catch (Exception e) {
            e.printStackTrace(System.out);
            return "";
        }
    }

    public static Properties loadProperties(String path, boolean fromClassPath)
            throws Exception {
        Properties result = new Properties();

        InputStream in;
        if (fromClassPath) {
            in = new Object().getClass().getResourceAsStream(path);
        } else {
            in = new FileInputStream(path);
        }

        result.load(in);
        in.close();
        return result;
    }

    public static String toString(optimizer.ea.Fitness f) {
        String result = "Fitness [" + f.name + "]: " + f.fit + ", min?" + f.minimization
                + ", id=" + f.id;
        return result;
    }

    public static void main(String[] args) {
        String[] bla = Utils.toArrayOfStrings(System.getProperty("BLABLA"), ",");

        for (int i = 0; i < bla.length; ++i) {
            System.out.println(bla[i]);
        }



//        double forwardAngleD = 31.0;
//
//        //double lb = forwardAngleD - 90.0;        
//
//        /*for(double trackAngleD = 0; trackAngleD < 180; trackAngleD += 1.0){
//         //double steeringD = -45.0 + (90.0 * ((trackAngleD - lb) / 180.0));
//         double steeringD = (trackAngleD - forwardAngleD) / 2.0;
//         System.out.println(Utils.dTS(trackAngleD)+" "+Utils.dTS(steeringD));
//         }*/
//
//        double backwardAngleD = -44.5;
//        double lb = backwardAngleD - 90.0;
//        double ub = backwardAngleD + 90.0;
//        System.out.println(Utils.dTS(ub - lb));
//
//        for (double trackAngleD = -90; trackAngleD < 0; trackAngleD += 1.0) {
//            //double steeringD = 45.0 - (90.0 * ((trackAngleD - backwardAngleD + 90.0) / 180));
//            //double steeringD = -((trackAngleD - backwardAngleD)/2.0);
//
//            double steeringD = (1.0 - ((trackAngleD + 90) / 90)) * -45.0;
//            System.out.println(Utils.dTS(trackAngleD) + " " + Utils.dTS(steeringD));
//        }
//        //steeringD = 45.0 - (90.0 * ((trackAngleD - lb) / (ub - lb)));
//
//        for (double trackAngleD = -180; trackAngleD < 0; trackAngleD += 1.0) {
//            final double BACK_ANGLE_D = -20.0;
//            double distanceAngle = ((trackAngleD - BACK_ANGLE_D) * (trackAngleD - BACK_ANGLE_D))
//                    / ((-180.0 - BACK_ANGLE_D) * (-180.0 - BACK_ANGLE_D));
//            // normierte distanz bei der position                        
//            distanceAngle = Math.sqrt(distanceAngle);
//            
//            System.out.print(Utils.dTS(trackAngleD)+" "+Utils.dTS(distanceAngle));
//            System.out.print(" ");
//            System.out.print(Utils.dTS(Math.abs((trackAngleD - BACK_ANGLE_D)/160.0)));
//            
//            System.out.println("");
//        }
//        
//        
//        for(double beta = 0.0; beta <= 0.52; beta+=0.01){
//            double alpha = (beta - 0.5) * -(1 / 0.5);
//            System.out.println(Utils.dTS(beta) + " " + Utils.dTS(alpha));
//        }
//
//
//        
//
//
//
//        //System.out.println(Utils.timeToExactString(127.20));
//        //java.util.Random r = new java.util.Random(System.currentTimeMillis());
//        //for(int i=0; i < 20; ++i){
//        //    int num = r.nextInt(67);
//        //    System.out.println((1+i)+" - "+num);
//        //}
//
//        /*String salt = "7*X@L\"N:JI";
//         String name = "jan".trim().toLowerCase();
//         String data = "1:56.79-9Punkte";
//         String hash = toSHA1(data+name+salt);
//
//         System.out.println(hash);*/
    }
}
