/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.janquadflieg.mrracer.network;

import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;

/**
 *
 * @author Jan Quadflieg
 */
public class UDPTestConnection 
implements Runnable{

    private static final String CLIENT_ID = "SCR";
    private InetSocketAddress server;    
    private boolean keepRunning = true;
    private Thread thread;
    private DatagramSocket socket;
    private byte[] rcvBuffer = new byte[4096];        

    public UDPTestConnection(String host, int port)
            throws Exception {
        socket = new DatagramSocket();
        server = new InetSocketAddress(host, port);
        

        thread = new Thread(this, "UDP Connection " + host + ":" + port);
        thread.start();
    }       

    private void handShake()
            throws Exception {
        boolean success = false;
        int i = 0;

        /* Build init string */
        
        String initStr = CLIENT_ID + "(init";        
        initStr = initStr + ")";

        for (; !success && i < 100; ++i) {
            try {
                socket.setSoTimeout(1000);
                send(initStr);
                String reply = receive();
                success = reply.contains("identified");
                socket.setSoTimeout(0);
            } catch (java.net.SocketTimeoutException e) {
                //System.out.println("Time out");
            }
        }

        if (success) {
            //System.out.println("Connected, after "+i+" packets");
        } else {
            throw new Exception("Connection failed");
        }
    }

    @Override
    public void run() {
        

        String reply;
        try {
            handShake();
            
            socket.setSoTimeout(10000);

        } catch (Exception e) {
            System.out.println(e.getMessage());            
            return;
        }
        
        int packetCounter = 0;        

        while (keepRunning) {
            ++packetCounter;
            try {                
                reply = receive();
                
                long toSleep = 2000;
                
                if(packetCounter % 100 == 0){
                    toSleep = 20000;
                }
                /*if(packetCounter == 2){                    
                    toSleep = 100000;
                }*/
                long startSleep = System.nanoTime();
                long endSleep = startSleep;
                while((endSleep-startSleep)/1000 < toSleep){
                    endSleep = System.nanoTime();
                }
                

            } catch(SocketTimeoutException e){                                
                return;
                
            } catch (Exception e) {                
                return;
            }            
            
            try {
                String s = "(id "+Integer.parseInt(reply.substring(0, reply.length()-1))+")blablablabla";
                send(s);
                
            } catch (Exception e) {                
                return;
            }
        }
    }

    private String receive()
            throws Exception {
        DatagramPacket packet = new DatagramPacket(rcvBuffer, rcvBuffer.length);        
        socket.receive(packet);
        return new String(packet.getData(), 0, packet.getLength(), "US-ASCII");
    }

    private void send(String toSend)
            throws Exception {
        byte[] outBuffer = toSend.getBytes("US-ASCII");
        socket.send(new DatagramPacket(outBuffer, outBuffer.length, server));
    }    
    
    public static void main(String[] args){
        try{
            new UDPTestConnection("127.0.0.1", 3010);
            //new UDPConnection1("snail07.cs.tu-dortmund.de", 3010);
        } catch(Exception e){
            e.printStackTrace(System.out);
        }
    }
}
