package torcs_ea;

/**
 * Created by JF on 23.08.2016.
 */
public class TestingValues {

    public static void main(String[] args) {

        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;
        double cur;

        TorcsRun torcsRun = new TorcsRun("testing-values", "Tailwinds-city", true);

//        double width = 13.62184;
//        double frict = 1.947106;
//        double rollr = 0.09904457;
//        double rough = 0.01774934;
//        double rwave = 20.96467;

        double width = 14.05002;
        double frict = 1.991133;
        double rollr = 0.1096812;
        double rough = 0.02393701;
        double rwave = 26.98566;


        int totalAmount = 10;

        for (int i = 0; i < totalAmount; i++) {
            torcsRun.valueOf(new double[] {width, frict, rollr, rough, rwave});
            System.out.println("Finished " + (i+1) + "/" + totalAmount);
        }
    }

}
