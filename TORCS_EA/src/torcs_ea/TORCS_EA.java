/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package torcs_ea;

import java.util.ArrayList;
import de.janquadflieg.mrracer.evo.DirectTorcs134Evaluator;
import de.janquadflieg.mrracer.evo.EvaluationType;
import de.janquadflieg.mrracer.evo.ControllerData;
import de.janquadflieg.mrracer.torcs.TrackData;
import de.janquadflieg.mrracer.torcs.TrackDataList;
import de.janquadflieg.mrracer.evo.TorcsExecConfig;
import java.io.OutputStream;
import java.io.PrintStream;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import java.util.Comparator;
import java.util.Random;

/**
 *
 * @author Jonas
 */
public class TORCS_EA {

    static private ArrayList<Chromosome> chromosomes;
    static private ArrayList<ControllerData> controllers;
    static private double latestLapTime;
    static private int numberOfDarts = 5;

    //private int numberOfDarts = 1000;
    public static void main(String[] args) {
        //String torcsPath = "D:/Data/Torcs/test-linux-patch_original/runtime";
        String torcsPath = "D:/test-linux-patch_original/runtime";
        String expPath = "D:/torcs-project/Demo";
        //String expPath = "D:/Data/TORCS-Project/Demo";
        chromosomes = generateDarts(numberOfDarts);
        controllers = new ArrayList<ControllerData>();
        controllers.add(ControllerData.BT);
        controllers.add(ControllerData.DAMNED);
        controllers.add(ControllerData.INFERNO);
        controllers.add(ControllerData.BERNIW);
        controllers.add(ControllerData.OLETHROS);

        TorcsExecConfig cfg = new TorcsExecConfig();
        cfg.nodamage = true;
        cfg.nofuel = true;
        cfg.nolaptime = true;
        cfg.noisy = false;
        cfg.port = 3001;
        cfg.restart_behavior = 's';
        cfg.timeoutthreshold = 100;

        String host = "127.0.0.1";
        int maxLaps = 5;

        TrackDataList tracklist = new TrackDataList(); //TODO: Get the track!
        /*
        double lapTime = 0;
        PrintStream origOut = System.out;
        PrintStream interceptor = new Interceptor(origOut, lapTime);
        System.setOut(interceptor);
         */
        for (int g = 0; g < 1; g++) {
            for (int i = 0; i < chromosomes.size(); i++) {
                TrackUtil.generateTrack(torcsPath + "/tracks/road/Makowiec-desert/Makowiec-desert.xml",
                        chromosomes.get(i).getFriction(), chromosomes.get(i).getRollingReistance(), chromosomes.get(i).getRoughness(),
                        chromosomes.get(i).getRoughnessWavelength(), chromosomes.get(i).getWidth());

                TrackData track = tracklist.getTrack("Makowiec-desert");
                for (int j = 0; j < controllers.size(); j++) {
                    DirectTorcs134Evaluator eval = new DirectTorcs134Evaluator(torcsPath, expPath,
                            true, EvaluationType.TIME_FOR_LAPS);
                    eval.startTorcs(track, controllers.get(j), cfg,
                            scr.Controller.Stage.QUALIFYING, host, maxLaps);
                    switch (controllers.get(j).getDriverName()) {
                        case "BerniW":
                            chromosomes.get(i).setTimeBerniW(eval.getTime());
                            break;
                        case "Inferno":
                            chromosomes.get(i).setTimeInferno(eval.getTime());
                            break;
                        case "BT":
                            chromosomes.get(i).setTimeBT(eval.getTime());
                            break;
                        case "Olethros":
                            chromosomes.get(i).setTimeOlethros(eval.getTime());
                            break;
                        case "Damned":
                            chromosomes.get(i).setTimeDamned(eval.getTime());
                            break;
                    }

                }

            }

            ArrayList<ArrayList<Chromosome>> paretoFronts = new ArrayList<ArrayList<Chromosome>>();
            paretoFronts.add(new ArrayList<Chromosome>());

            for (int i = 0; i < chromosomes.size(); i++) {
                Chromosome p = chromosomes.get(i);
                for (int j = 0; j < chromosomes.size(); j++) {
                    if (i != j) {
                        Chromosome q = chromosomes.get(j);
                        if (p.timeDistance12() < q.timeDistance12()
                                && p.timeDistance23() < q.timeDistance23()
                                && p.timeDistance34() < q.timeDistance34()
                                && p.timeDistance45() < q.timeDistance45()) {
                            p.addToSetOfDominatedChromosomes(q);
                        } else {
                            q.increaseNumberOfDominators();
                        }
                    }
                }
                if (p.getNumberOfDominators() == 0) {
                    p.setParetoLevel(0);
                    paretoFronts.get(0).add(p);
                }
            }

            for (int i = 0;; i++) {
                ArrayList<Chromosome> currentParetoFront = paretoFronts.get(i);
                ArrayList<Chromosome> nextParetoFront = new ArrayList<Chromosome>();
                paretoFronts.add(nextParetoFront);
                for (int j = 0; j < currentParetoFront.size(); j++) {
                    ArrayList<Chromosome> dominatedChromosomes = currentParetoFront.get(j).getSetOfDominatedChromosomes();
                    for (int k = 0; k < dominatedChromosomes.size(); k++) {
                        Chromosome dominatedChromosome = dominatedChromosomes.get(k);
                        dominatedChromosome.decreaseNumberOfDominators();
                        if (dominatedChromosome.getNumberOfDominators() == 0) {
                            dominatedChromosome.setParetoLevel(i + 1);
                            nextParetoFront.add(dominatedChromosome);
                        }
                    }
                }
                if (nextParetoFront.isEmpty()) {
                    paretoFronts.remove(nextParetoFront);
                    break;
                }
            }

            for (int i = 0; i < paretoFronts.size(); i++) {
                System.out.println("-----Current index i: " + i);
                ArrayList<Chromosome> currentParetoFront = paretoFronts.get(i);

                for (int j = 0; j < 4; j++) {
                    double distanceMax = Double.MIN_VALUE;
                    double distanceMin = Double.MAX_VALUE;
                    for (int k = 0; k < chromosomes.size(); k++) {
                        if (distanceMax < chromosomes.get(k).getCriteria(j)) {
                            distanceMax = chromosomes.get(k).getCriteria(j);
                        }
                        if (distanceMin > chromosomes.get(k).getCriteria(j)) {
                            distanceMin = chromosomes.get(k).getCriteria(j);
                        }
                    }
                    final int m = j;
                    currentParetoFront.sort(new Comparator<Chromosome>() {
                        @Override
                        public int compare(Chromosome first, Chromosome second) {
                            return Double.compare(first.getCriteria(m), second.getCriteria(m));
                        }
                    });

                    currentParetoFront.get(0).setCrowdingDistance(Double.MAX_VALUE);
                    currentParetoFront.get(currentParetoFront.size() - 1).setCrowdingDistance(Double.MAX_VALUE);
                    for (int k = 1; k < currentParetoFront.size() - 1; k++) {
                        Chromosome currentChromosome = currentParetoFront.get(k);
                        currentChromosome.setCrowdingDistance(currentChromosome.getCrowdingDistance()
                                + ((currentParetoFront.get(k + 1).getCriteria(j) - currentParetoFront.get(k - 1).getCriteria(j))
                                / (distanceMax - distanceMin)));
                    }
                }
            }
            //chromosomes = (ArrayList)GenerationUtil.nextGen(chromosomes, g);
        }
    }

    public static ArrayList<Chromosome> generateDarts(int numberOfDarts) {
        ArrayList<Chromosome> darts = new ArrayList<Chromosome>();
        Random rnd = new Random();
        for (int i = 0; i < numberOfDarts; i++) {
            darts.add(new Chromosome(0, rnd.nextDouble() * 20 + 10, rnd.nextDouble() * 0.12 + 0.001,
                    rnd.nextDouble() * 0.02, rnd.nextDouble() * 29 + 1, rnd.nextDouble() * 0.6 + 0.8));
        }
        return darts;
    }

}
