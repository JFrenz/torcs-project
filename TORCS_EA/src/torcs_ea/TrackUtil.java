package torcs_ea;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class TrackUtil {

    /**
     * For testing only
     * @param args 
     */
	public static void main(String args[]) {
		TrackUtil.generateTrack("D:\\repositories\\TORCS\\Jan\\Torcs\\runtime\\tracks\\road\\wheel-2\\wheel-2.xml",
			1.15,	//friction
			0.0012,	//rolling resistance
			0,		//roughness
			1,		//roughness wavelength
			12		//width
		);
	}

    /**
     * Generates a new track file based on the given track and the parameters. WARNING: it overwrites the existing xml-file
     * @param filepath The original track file
     * @param p_friction Must be greater than zero
     * @param p_rolling_resistance No restriction, should be between 0 and 0.2
     * @param p_roughness Amplitude of the track in centimeter
     * @param p_roughness_wavelength Wavelength of the track in meter
     * @param p_width of the track in meter
     * @return File-object of the generated xml-file
     */
	public static File generateTrack(String filepath, double p_friction, double p_rolling_resistance, double p_roughness, double p_roughness_wavelength, double p_width) {
		DecimalFormat df = new DecimalFormat("##.#####");
		return generateTrack(filepath, df.format(p_friction), df.format(p_rolling_resistance), df.format(p_roughness), df.format(p_roughness_wavelength), df.format(p_width));
	}

    /**
     * Generates a new track file based on the given track and the parameters. WARNING: it overwrites the existing xml-file
     * @param filepath The original track file
     * @param p_friction Must be greater than zero
     * @param p_rolling_resistance No restriction, should be between 0 and 0.2
     * @param p_roughness Amplitude of the track in centimeter
     * @param p_roughness_wavelength Wavelength of the track in meter
     * @param p_width of the track in meter
     * @return File-object of the generated xml-file
     */
	public static File generateTrack(String filepath, String p_friction, String p_rolling_resistance, String p_roughness, String p_roughness_wavelength, String p_width) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			docFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			XPathFactory xPathFactory = XPathFactory.newInstance();
			XPath xpath = xPathFactory.newXPath();

			//get main surface
			XPathExpression expr = xpath.compile("/params/section[@name='Main Track']/attstr[@name='surface']/@val");
			String surface = (String) expr.evaluate(doc, XPathConstants.STRING);

			//get variables that are subject to balancing
			expr = xpath.compile("/params/section[@name='Surfaces']/section[@name='"+surface+"']/attnum[@name='friction']");
			Element el_friction = (Element) ((NodeList) expr.evaluate(doc, XPathConstants.NODESET)).item(0);

			expr = xpath.compile("/params/section[@name='Surfaces']/section[@name='"+surface+"']/attnum[@name='rolling resistance']");
			Element el_rolling_resistance = (Element) ((NodeList) expr.evaluate(doc, XPathConstants.NODESET)).item(0);

			expr = xpath.compile("/params/section[@name='Surfaces']/section[@name='"+surface+"']/attnum[@name='roughness']");
			Element el_roughness = (Element) ((NodeList) expr.evaluate(doc, XPathConstants.NODESET)).item(0);

			expr = xpath.compile("/params/section[@name='Surfaces']/section[@name='"+surface+"']/attnum[@name='roughness wavelength']");
			Element el_roughness_wavelength = (Element) ((NodeList) expr.evaluate(doc, XPathConstants.NODESET)).item(0);

			expr = xpath.compile("/params/section[@name='Main Track']/attnum[@name='width']");
			Element el_width = (Element) ((NodeList) expr.evaluate(doc, XPathConstants.NODESET)).item(0);

			//Debug
			//System.out.println(friction.getAttribute("val"));

			//set variables
			el_friction.setAttribute			("val", p_friction);
			el_rolling_resistance.setAttribute	("val", p_rolling_resistance);
			el_roughness.setAttribute			("val", p_roughness);
			el_roughness_wavelength.setAttribute("val", p_roughness_wavelength);
			el_width.setAttribute				("val", p_width);

			//generate Hashcode from values
			String hash = p_friction+"_"+p_rolling_resistance+"_"+p_roughness+"_"+p_roughness_wavelength+"_"+p_width;

			//save new XML file
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			DOMSource source = new DOMSource(doc);
			File resultFile = new File(filepath);//+"_"+hash+".xml");
			System.out.println(resultFile);
			StreamResult result = new StreamResult(resultFile);
			transformer.transform(source, result);

			return resultFile;

		} catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException | DOMException | TransformerException e) {
			e.printStackTrace();
		}
		return null;
	}
}