package torcs_ea;

import de.janquadflieg.mrracer.evo.*;
import de.janquadflieg.mrracer.torcs.TrackData;
import de.janquadflieg.mrracer.torcs.TrackDataList;
import fr.inria.optimization.cmaes.fitness.IObjectiveFunction;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DecimalFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.*;

/**
 * Created by COL on 30.06.2016.
 */
public class TorcsRun  implements IObjectiveFunction { // meaning implements methods valueOf and isFeasible

    private ArrayList<ControllerData> controllers;
    private double latestLapTime;
    private TorcsExecConfig cfg;
    private String torcsPath;
    private String expPath;
    private TrackDataList tracklist;
    private String host;
    private int maxLaps;
    private PrintStream fileOut;
    public static TrackData track;
    public static ControllerData controllerData;
    private DecimalFormat df = new DecimalFormat("#.###");

    private String filename;
    private String trackname;
    private boolean multiThreaded;
    int multiplier = 2;
    private boolean vsmode;

    public TorcsRun(boolean multiThreaded) {
        this.multiThreaded = multiThreaded;
        Locale.setDefault(Locale.US);
        //String torcsPath = "D:/Data/Torcs/test-linux-patch_original/runtime";
        torcsPath = "D:/Torcs/test-linux-patch_original/runtime";
        //torcsPath = "D:\\repositories\\TORCS\\Jan\\Torcs\\runtime";
        expPath = "D:/TORCS-Project/Demo";
        //expPath = "D:\\repositories\\TORCS";
        //String expPath = "D:/Data/TORCS-Project/Demo";
        controllers = new ArrayList<ControllerData>();
        controllers.add(ControllerData.BT);
        controllers.add(ControllerData.DAMNED);
        controllers.add(ControllerData.INFERNO);
        controllers.add(ControllerData.BERNIW);
        controllers.add(ControllerData.OLETHROS);

        cfg = new TorcsExecConfig();
        cfg.nodamage = true;
        cfg.nofuel = true;
        cfg.nolaptime = true;
        cfg.noisy = false;
        cfg.port = 3001;
        cfg.restart_behavior = 's';
        cfg.timeoutthreshold = 100;

        CarData[] carData = new CarData[1];
        carData[0] = new CarData("car1-ow1");

        for(int i = 0; i < controllers.size(); i++) {
            String driveXml = CarChanger.createDriverFile(controllers.get(i), carData);
            File carConfig;
            if(multiThreaded) {
                carConfig = new File(torcsPath + i + "/drivers/" + controllers.get(i).getDriverName().toLowerCase() + File.separator + controllers.get(i).getDriverName().toLowerCase() + ".xml");
            }
            else{
                carConfig = new File(torcsPath + "/drivers/" + controllers.get(i).getDriverName().toLowerCase() + File.separator + controllers.get(i).getDriverName().toLowerCase() + ".xml");
            }
            try{
                OutputStreamWriter w = new OutputStreamWriter(new FileOutputStream(carConfig), "UTF-8");
                w.write(driveXml);
                w.flush();
                w.close();
            } catch (Exception e){
                e.printStackTrace(System.out);
            }
        }

        host = "127.0.0.1";
        maxLaps = 5;

        tracklist = new TrackDataList();
        System.out.println("Constructor");
        try {
            fileOut = new PrintStream(new BufferedOutputStream(new FileOutputStream("out.txt")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    public TorcsRun(String prefix, String trackname, boolean multiThreaded,boolean vs) {
        this(multiThreaded);
        this.filename = prefix + "results.csv";
        this.trackname = trackname;
        this.vsmode = vs;
        List a = new ArrayList<String>();
        a.add("width, friction, rolling resistance, roughness, roughness wavelength, BT, Damned, Inferno, BerniW, Olethros, fitness, squarefitness");
        try {Files.write(Paths.get(filename), a, StandardOpenOption.CREATE, StandardOpenOption.APPEND);} catch (IOException e) {e.printStackTrace();}

        try {
            fileOut = new PrintStream(new BufferedOutputStream(new FileOutputStream(prefix+"out.txt")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public TorcsRun(String prefix, String trackname, boolean multiThreaded) {
        this(multiThreaded);
        this.filename = prefix + "results.csv";
        this.trackname = trackname;
        
        this.vsmode = false;
        List a = new ArrayList<String>();
        a.add("width, friction, rolling resistance, roughness, roughness wavelength, BT, Damned, Inferno, BerniW, Olethros, fitness, squarefitness");
        try {Files.write(Paths.get(filename), a, StandardOpenOption.CREATE, StandardOpenOption.APPEND);} catch (IOException e) {e.printStackTrace();}

        try {
            fileOut = new PrintStream(new BufferedOutputStream(new FileOutputStream(prefix+"out.txt")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public double valueOf (double[] x) {
        System.out.println("valueOf");
        System.out.println("Werte: " + Arrays.toString(x));

        track = tracklist.getTrack(trackname);

        double[] times = new double[controllers.size()];
        double min = Double.MAX_VALUE;
        double max = 0;

        PrintStream old = System.out;
        //System.setOut(fileOut);

        final Duration timeout = Duration.ofSeconds(60);

        double[] tempTimes = new double[controllers.size() * multiplier];

        for(int i = 0; i < multiplier; i++) {
            ExecutorService executor = Executors.newWorkStealingPool();
            ArrayList<Future<Double>> futures = new ArrayList<Future<Double>>();
            if (multiThreaded) {
            for (int j = 0; j < controllers.size(); j++) {
                TrackUtil.generateTrack(torcsPath + j + "/tracks/road/" + trackname + "/" + trackname + ".xml", x[1], x[2], x[3], x[4], x[0]);
                controllers.get(j).setIndex(1);
                controllerData = controllers.get(j);
                final String individualTorcsPath = torcsPath + j;
//            ExecutorService executor = Executors.newSingleThreadExecutor();
                final Future<Double> future = executor.submit(() -> {
                            DirectTorcs134Evaluator eval = new DirectTorcs134Evaluator(individualTorcsPath, expPath, false, EvaluationType.TIME_FOR_LAPS);
                            eval.startTorcs(track, controllerData, cfg, scr.Controller.Stage.QUALIFYING, host, maxLaps);
                            return eval.getTime();
                        }
                );
                futures.add(future);
            }
            for (int j = 0; j < controllers.size(); j++) {

                try {
                    //System.out.println("isCanceled: " + futures.get(j).isCancelled());
                    System.setOut(fileOut);
                    times[j] = futures.get(j).get(timeout.toMillis(), TimeUnit.MILLISECONDS);
                    System.out.flush();
                    System.setOut(old);
                } catch (InterruptedException | ExecutionException | TimeoutException e) {
                    System.out.flush();
                    System.setOut(old);
                    System.out.println("!!!!!!!!!!!!!Timeout!!!!!!!!!!!!!");
                    try {
                        Runtime.getRuntime().exec("TASKKILL /F /IM wtorcs.exe");
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    List a = new ArrayList<String>();
                    a.add((Arrays.toString(x) + ", NAN, NAN, NAN, NAN, NAN, NAN").replaceAll("(\\[|\\])", ""));
                    try {
                        Files.write(Paths.get(filename), a, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                    } catch (IOException e2) {
                        e.printStackTrace();
                    }
                    return 10000;//TODO:
                }
                if (times[j] == 0) continue;
                if (times[j] < min) min = times[j];
                if (times[j] > max) max = times[j];
            }

            executor.shutdown();
        }else if(!vsmode){
            TrackUtil.generateTrack(torcsPath + "/tracks/road/" + trackname + "/" + trackname + ".xml", x[1], x[2], x[3], x[4], x[0]);

            for (int j = 0; j < controllers.size(); j++) {
                controllers.get(j).setIndex(1);
                controllerData = controllers.get(j);
                executor = Executors.newSingleThreadExecutor();
                    final Future<Double> future = executor.submit(() -> {
                    DirectTorcs134Evaluator eval = new DirectTorcs134Evaluator(torcsPath, expPath, true, EvaluationType.TIME_FOR_LAPS);
                    eval.startTorcs(track, controllerData, cfg, scr.Controller.Stage.QUALIFYING, host, maxLaps);
                    return eval.getTime();
                }
                );

                try {
                    System.setOut(fileOut);
                    times[j] = future.get(timeout.toMillis(), TimeUnit.MILLISECONDS);
                    System.out.flush();
                    System.setOut(old);
                } catch (InterruptedException | ExecutionException | TimeoutException e) {
                    System.out.flush();
                    System.setOut(old);
                    System.out.println("!!!!!!!!!!!!!Timeout!!!!!!!!!!!!!");
                    try {
                        Runtime.getRuntime().exec("TASKKILL /F /IM wtorcs.exe");
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    List a = new ArrayList<String>();
                    a.add((Arrays.toString(x) + ", NAN, NAN, NAN, NAN, NAN, NAN").replaceAll("(\\[|\\])", ""));
                    try {
                        Files.write(Paths.get(filename), a, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                    } catch (IOException e2) {
                        e.printStackTrace();
                    }
                    return 10000;//TODO:
                }
                executor.shutdown();
                if (times[j] == 0) continue;
                if (times[j] < min) min = times[j];
                if (times[j] > max) max = times[j];
            }
        }else if(vsmode){
            TrackUtil.generateTrack(torcsPath + "/tracks/road/" + trackname + "/" + trackname + ".xml", x[1], x[2], x[3], x[4], x[0]);
            for (int j = 0; j < controllers.size(); j++) {
                controllers.get(j).setIndex(1);}
            executor = Executors.newSingleThreadExecutor();
                final Future<String> future = executor.submit(new Callable<String>() {
                public String call() {
                    DirectTorcs134Evaluator eval = new DirectTorcs134Evaluator(torcsPath, expPath, true, EvaluationType.TIME_FOR_LAPS);
                    eval.startTorcs(track, controllers.toArray(new ControllerData[controllers.size()]), cfg, scr.Controller.Stage.QUALIFYING, host, maxLaps);
                    return eval.getVSTimes();
                }
            });
            try {
                System.setOut(fileOut);
                String timelist = future.get(timeout.toMillis(), TimeUnit.MILLISECONDS);
                for(int j = 0; j < timelist.split(";").length; j++){
                    times[j] = Double.parseDouble(timelist.split(";")[j]);
                }
                System.out.flush();
                System.setOut(old);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                System.out.flush();
                System.setOut(old);
                System.out.println("!!!!!!!!!!!!!Timeout!!!!!!!!!!!!!");
                try {
                    Runtime.getRuntime().exec("TASKKILL /F /IM wtorcs.exe");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                List a = new ArrayList<String>();
                a.add((Arrays.toString(x) + ", NAN, NAN, NAN, NAN, NAN, NAN").replaceAll("(\\[|\\])", ""));
                try {
                    Files.write(Paths.get(filename), a, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                } catch (IOException e2) {
                    e.printStackTrace();
                }
                return 10000;
            }
            executor.shutdown();
            for(int j = 0; j < times.length; j++){
                if (times[j] == 0) continue;
                if (times[j] < min) min = times[j];
                if (times[j] > max) max = times[j];
            }
        }
            for(int j  = 0; j < controllers.size(); j++){
                tempTimes[(i * controllers.size() + j)] = times[j];
            }
        }

        if(multiplier > 1){
            for(int i = 0; i < controllers.size(); i++) {
                boolean differences = false;
                for(int j = 0; j < multiplier; j++) {
                    if (tempTimes[i] != tempTimes[i + controllers.size() * j]) {
                        System.out.println("Verschiedene Zeiten für " + controllers.get(i).getDisplayName() + ": " + tempTimes[i] + " != " + tempTimes[i + controllers.size() * j]);
                        differences = true;
                    }
                }
                if(differences){
                    for(int j = 1; j < multiplier; j++){
                        times[i] += tempTimes[i + controllers.size() * j];
                    }
                    times[i] = times[i] / multiplier;

                }
            }
            min = Double.MAX_VALUE;
            max = 0;
            for(int i = 0; i < controllers.size(); i++){
                if (times[i] == 0) continue;
                if (times[i] < min) min = times[i];
                if (times[i] > max) max = times[i];
            }
        }

        String stTimes = Arrays.toString(times);

        System.out.println("Zeiten: " + Arrays.toString(times));
        System.out.println("return: " + df.format(max-min)); //TODO: squared difference


        double squareDif = 0;
        for (int l=0; l<4; l++) squareDif += Math.pow(times[l]-times[l+1],2);

        List a = new ArrayList<String>();
        a.add((Arrays.toString(x) + ", " + stTimes + ", " + df.format(max-min).replace(',','.') + ", " + df.format(squareDif).replace(',','.')).replaceAll("(\\[|\\])", ""));
        try {Files.write(Paths.get(filename), a, StandardOpenOption.CREATE, StandardOpenOption.APPEND);} catch (IOException e) {e.printStackTrace();}

        if (min > max)
            return 10000;
        return Double.valueOf(df.format(max-min).replace(',','.'));
    }
    public boolean isFeasible(double[] x) {
        if (10 <= x[0] && x[0] <= 30
                && 1 <= x[1] && x[1] <= 2
                && 0.001 <= x[2] && x[2] <= 0.12
                && 0 <= x[3] && x[3] <= 0.04
                && 1 <= x[4] && x[4] <= 30)
            return true;
        return false;
    }
}