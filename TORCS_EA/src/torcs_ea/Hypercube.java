package torcs_ea;

/**
 * Created by COL on 30.06.2016.
 */
public class Hypercube {

    public static void main(String[] args) {

        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;
        double cur;

        TorcsRun torcsRun = new TorcsRun("brute-force-multi-threading", "Makowiec-desert", true);

        //points to evaluate
//        double[] widthPoints = {10, 15, 20, 25, 30};
//        double[] frictPoints = {1, 1.25, 1.5, 1.75, 2};
//        double[] rollrPoints = {0.001, 0.03, 0.06, 0.09, 0.12};
//        double[] roughPoints = {0, 0.01, 0.02, 0.03, 0.04};
//        double[] rwavePoints = {1, 2, 4, 10, 30};

        double[] widthPoints = {11 + (2.0/3), 13 + (1.0/3), 16 + (2.0/3), 18 + (1.0/3), 21 + (2.0/3), 23 + (1.0/3), 26 + (2.0/3), 28 + (1.0/3)};
        double[] frictPoints = {1 + (0.25/3), 1 + (0.5/3), 1.25 + (0.25/3), 1.25 + (0.5/3), 1.5 + (0.25/3), 1.5 + (0.5/3), 1.75 + (0.25/3), 1.75 + (0.5/3)};
        double[] rollrPoints = {0.01, 0.02, 0.04, 0.05, 0.07, 0.08, 0.1, 0.11};
        double[] roughPoints = {0.01/3, 0.02/3, 0.01 + 0.01/3, 0.01 + 0.02/3, 0.02 + 0.01/3, 0.02 + 0.02/3, 0.03 + 0.01/3, 0.03 + 0.02/3};
        double[] rwavePoints = {1 + 1.0/3, 1 + 2.0/3, 2 + 2.0/3, 2 + 4.0/3, 6, 8, 16 + 2.0/3, 23 + 1.0/3};

        int counter = 1;
        int totalAmount = widthPoints.length * frictPoints.length * rollrPoints.length * roughPoints.length * rwavePoints.length;

        for (double width : widthPoints) {
            for (double frict : frictPoints) {
                for (double rollr : rollrPoints) {
                    for (double rough : roughPoints) {
                        for (double rwave : rwavePoints) {
                            //torcsRun.valueOf(new double[] {11.666666666666666, 1.0833333333333333, 0.01, 0.0033333333333333335, 1.3333333333333333});
                            torcsRun.valueOf(new double[] {width, frict, rollr, rough, rwave});
                            System.out.println("Finished " + counter++ + "/" + totalAmount);
                        }
                    }
                }
            }
        }
    }

}
