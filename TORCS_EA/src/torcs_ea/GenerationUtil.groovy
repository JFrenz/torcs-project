/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package torcs_ea;

/**
 *
 * @author COL
 */
public class GenerationUtil {
    
    public static int GENERATION_SIZE = 5//1000
    
    public static List<Chromosome> nextGen(List<Chromosome> oldGen, int generationNumber) {
        List seniors = oldGen.sort()[0..(GENERATION_SIZE-1)]
        List parents = selection(seniors, Math.round(oldGen.size()/2), 2)
        List offsprings = geneticOperations(parents, generationNumber, 20, 20)
        
        return (parents + offsprings)
    }
    
    /**
     * @poolSize is usually chromosomes.size()/2
     * @tourSize is usually 2
     */
    private static List<Chromosome> selection(List<Chromosome> chromosomes, int poolSize, int tourSize) {
        int cSize = chromosomes.size()
        Set candidates = []
        Set result = []
        
        while (result.size() < poolSize) {
            while (candidates.size() < tourSize) {
                candidates << Math.round(cSize*Math.random())
            }
            result << candidates.min()
        }
        
        result as List
    }
    
    private static List<Chromosome> geneticOperations(List<Chromosome> parents, int generationNumber, mu, mum) {
        int N = parents.size()
        int V = Chromosome.getNumberOfDimensions()
        int cross, mutat
        int lLimit = 0
        int uLimit = 1
        
        List result = []
        
        N.times {
            if (Math.random() < 0.9) {//Crossover
                def child1 = new Chromosome(generationNumber)
                def child2 = new Chromosome(generationNumber)
                
                def parent1 = Math.round(N*Math.random())
                def parent2 = Math.round(N*Math.random())
                
                while (parent1 == parent2) {
                    parent2 = Math.round(N*Math.random())
                    if (parent2 < 1) parent2 = 1
                }
                
                parent1 = parents[parent1]
                parent2 = parents[parent2]
                
                V.times { j ->
                    def u = Math.random()
                    def bq = u < 0.5 ? (2*u)**(1/(mu+1)) : 1/((2*(1-u))**(1/(mu+1)))
                    child1.setDimensionValue(j, 0.5*(((1+bq)*parent1.getDimensionValue(j)) + ((1-bq)*parent2.getDimensionValue(j))))
                    child2.setDimensionValue(j, 0.5*(((1+bq)*parent1.getDimensionValue(j)) + ((1-bq)*parent2.getDimensionValue(j))))
                }
                
                result << child1
                result << child2
            } else {//Mutation
                def parent = Math.round(N*Math.random())
                def child = new Chromosome(generationNumber)
                V.times { j ->
                    def r = Math.random()
                    def delta = r < 0.5 ? ((2*r)**(1/(mum+1)))-1 : 1-((2*(1-r))**(1/(mum+1)))
                    child.setDimensionValue(j, parent.getDimensionValue(j))
                }
                result << child
            }
        }
        
        return result
    }
    
}
