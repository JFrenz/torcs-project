/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package torcs_ea;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 *
 * @author Jonas
 */
public class Interceptor extends PrintStream {

        private double lapTime;
        
        public Interceptor(OutputStream out, double lapTime) {
            super(out, true);
        }

        @Override
        public void print(String s) {
            if(s.contains("Overall time"))
                lapTime = Double.parseDouble(s.split(":")[1].split(",")[0]);
            super.print(s);
        }
    }
