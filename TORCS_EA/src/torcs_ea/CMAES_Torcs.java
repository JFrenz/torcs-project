package torcs_ea;

import fr.inria.optimization.cmaes.CMAEvolutionStrategy;
import fr.inria.optimization.cmaes.fitness.IObjectiveFunction;

import java.util.Random;

/** A very short example program how to use the class CMAEvolutionStrategy.  The code is given below, see also the code snippet in the documentation of class {@link CMAEvolutionStrategy}.
 *  For implementation of restarts see {CMAExample2}.
 <pre>
 public class CMAExample1 {
 public static void main(String[] args) {
 IObjectiveFunction fitfun = new Rosenbrock();

 // new a CMA-ES and set some initial values
 CMAEvolutionStrategy cma = new CMAEvolutionStrategy();
 cma.readProperties(); // read options, see file CMAEvolutionStrategy.properties
 cma.setDimension(22); // overwrite some loaded properties
 cma.setInitialX(0.5); // in each dimension, also setTypicalX can be used
 cma.setInitialStandardDeviation(0.2); // also a mandatory setting
 cma.options.stopFitness = 1e-9;       // optional setting

 // initialize cma and get fitness array to fill in later
 double[] fitness = cma.init();  // new double[cma.parameters.getPopulationSize()];

 // initial output to files
 cma.writeToDefaultFilesHeaders(0); // 0 == overwrites old files

 // iteration loop
 while(cma.stopConditions.getNumber() == 0) {

 // core iteration step
 double[][] pop = cma.samplePopulation(); // get a new population of solutions
 for(int i = 0; i < pop.length; ++i) {    // for each candidate solution i
 while (!fitfun.isFeasible(pop[i]))   //    test whether solution is feasible,
 pop[i] = cma.resampleSingle(i);  //       re-sample solution until it is feasible
 fitness[i] = fitfun.valueOf(pop[i]); //    compute fitness value, where fitfun
 }	                                     //    is the function to be minimized
 cma.updateDistribution(fitness);         // pass fitness array to update search distribution

 // output to console and files
 cma.writeToDefaultFiles();
 int outmod = 150;
 if (cma.getCountIter() % (15*outmod) == 1)
 cma.printlnAnnotation(); // might write file as well
 if (cma.getCountIter() % outmod == 1)
 cma.println();
 }
 // evaluate mean value as it is the best estimator for the optimum
 cma.setFitnessOfMeanX(fitfun.valueOf(cma.getMeanX())); // updates the best ever solution

 // final output
 cma.writeToDefaultFiles(1);
 cma.println();
 cma.println("Terminated due to");
 for (String s : cma.stopConditions.getMessages())
 cma.println("  " + s);
 cma.println("best function value " + cma.getBestFunctionValue()
 + " at evaluation " + cma.getBestEvaluationNumber());

 // we might return cma.getBestSolution() or cma.getBestX()

 } // main
 } // class
 </pre>

 *
 * @see CMAEvolutionStrategy
 *
 * @author Nikolaus Hansen, released into public domain.
 */
public class CMAES_Torcs {

    private static Random r;
    private static boolean vsMode = false;

    public static void main(String[] args) {
        vsMode = false;
        //Current one
        for (int i=1; i<=10; i++)
            run("run-1-"+i+"-", "Makowiec-desert", new double[] {20, 1.5, 0.02, 0.02, 20}, new double[] {4, 0.4, 0.024, 0.005, 6});

        //init stddev fixed and random starting point
        r = new Random();
        for (int i=1; i<=10; i++)
            run("run-2-"+i+"-", "Makowiec-desert", new double[] {random(10,30), random(1,2), random(0.001,0.12), random(0,0.04), random(1,30)}, new double[] {4, 0.4, 0.024, 0.005, 6});

        //Different track with current settings
        for (int i=1; i<=10; i++)
            run("run-3-"+i+"-", "Tailwinds-city", new double[] {14, 1.2, 0.001, 0, 1.0}, new double[] {4, 0.4, 0.024, 0.005, 6});

        //Same track as step 3 with settings like step 2
        r = new Random();
        for (int i=1; i<=10; i++)
            run("run-4-"+i+"-", "Tailwinds-city", new double[] {random(10,30), random(1,2), random(0.001,0.12), random(0,0.04), random(1,30)}, new double[] {4, 0.4, 0.024, 0.005, 6});
       
        //Switch to VS-Mode
        vsMode =true;
        
        for (int i=1; i<=10; i++)
            run("run-5-"+i+"-", "Makowiec-desert", new double[] {20, 1.5, 0.02, 0.02, 20}, new double[] {4, 0.4, 0.024, 0.005, 6});

        //init stddev fixed and random starting point
        r = new Random();
        for (int i=1; i<=10; i++)
            run("run-6-"+i+"-", "Makowiec-desert", new double[] {random(10,30), random(1,2), random(0.001,0.12), random(0,0.04), random(1,30)}, new double[] {4, 0.4, 0.024, 0.005, 6});

        //Different track with current settings
        for (int i=1; i<=10; i++)
            run("run-7-"+i+"-", "Tailwinds-city", new double[] {14, 1.2, 0.001, 0, 1.0}, new double[] {4, 0.4, 0.024, 0.005, 6});

        //Same track as step 3 with settings like step 2
        r = new Random();
        for (int i=1; i<=10; i++)
            run("run-8-"+i+"-", "Tailwinds-city", new double[] {random(10,30), random(1,2), random(0.001,0.12), random(0,0.04), random(1,30)}, new double[] {4, 0.4, 0.024, 0.005, 6});
    }

    private static double random(double min, double max) {
        return r.nextDouble()*(max-min)+min;
    }

    public static void run(String prefix, String trackname, double[] initialX, double[] initialStdDev) {
        IObjectiveFunction fitfun = new TorcsRun(prefix, trackname,false,vsMode);

        // new a CMA-ES and set some initial values
        CMAEvolutionStrategy cma = new CMAEvolutionStrategy();
        cma.readProperties(); // read options, see file CMAEvolutionStrategy.properties
        cma.setDimension(5); // overwrite some loaded properties
        //width, friction, rolling resistance, roughness, roughness wavelength
        cma.setInitialX(initialX); // in each dimension, also setTypicalX can be used
        cma.setInitialStandardDeviations(initialStdDev); // also a mandatory setting
        //cma.options.stopFitness = 1e-14;       // optional setting

        System.out.println("Params set");

        // initialize cma and get fitness array to fill in later
        double[] fitness = cma.init();  // new double[cma.parameters.getPopulationSize()];

        // initial output to files
        cma.writeToDefaultFilesHeaders(prefix, 0); // 0 == overwrites old files

        // iteration loop
//        while(cma.stopConditions.getNumber() == 0) {
        for (int k=0; k<625; k++) {
            System.out.println("iteration loop start");

            // --- core iteration step ---
            double[][] pop = cma.samplePopulation(); // get a new population of solutions
            for(int i = 0; i < pop.length; ++i) {    // for each candidate solution i
                // a simple way to handle constraints that define a convex feasible domain
                // (like box constraints, i.e. variable boundaries) via "blind re-sampling"
                // assumes that the feasible domain is convex, the optimum is
                while (!fitfun.isFeasible(pop[i]))     //   not located on (or very close to) the domain boundary,
                    pop[i] = cma.resampleSingle(i);    //   initialX is feasible and initialStandardDeviations are
                //   sufficiently small to prevent quasi-infinite looping here
                // compute fitness/objective value
                fitness[i] = fitfun.valueOf(pop[i]); // fitfun.valueOf() is to be minimized
            }
            cma.updateDistribution(fitness);         // pass fitness array to update search distribution
            // --- end core iteration step ---

            // output to files and console
            cma.writeToDefaultFiles(prefix);
            int outmod = 150;
            if (cma.getCountIter() % (15*outmod) == 1)
                cma.printlnAnnotation(); // might write file as well
            if (cma.getCountIter() % outmod == 1)
                cma.println();
        }
        // evaluate mean value as it is the best estimator for the optimum
        cma.setFitnessOfMeanX(fitfun.valueOf(cma.getMeanX())); // updates the best ever solution

        // final output
        cma.writeToDefaultFiles(prefix);
        cma.println();
        cma.println("Terminated due to");
        for (String s : cma.stopConditions.getMessages())
            cma.println("  " + s);
        cma.println("best function value " + cma.getBestFunctionValue()
                + " at evaluation " + cma.getBestEvaluationNumber());

        // we might return cma.getBestSolution() or cma.getBestX()

    } // main
} // class
