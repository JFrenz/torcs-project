/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package torcs_ea;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.UUID;

/**
 *
 * @author Jonas
 */
class Chromosome implements Comparable {

    private UUID id;
    private int paretoLevel;
    private int generationNumber;
    private String generationType;//TODO:
    private int[] parents;//TODO:
    private double crowdingDistance;
    private double width;
    private double rollingReistance;
    private double roughness;
    private double roughnessWavelength;
    private double friction;
    private double timeBT;
    private double timeInferno;
    private double timeBerniW;
    private double timeOlethros;
    private double timeDamned;
    private ArrayList<Double> sortedTimes;
    private ArrayList<Chromosome> setOfDominatedChromosomes;
    private int numberOfDominators;
    
    public Chromosome(int generationNumber) {
        this(generationNumber, -1, -1, -1, -1, -1);
    }

    public Chromosome(int generationNumber, double width, double rollingReistance, double roughness, double roughnessWavelength, double friction) {
        this.generationNumber = generationNumber;
        this.width = width;
        this.rollingReistance = rollingReistance;
        this.roughness = roughness;
        this.roughnessWavelength = roughnessWavelength;
        this.friction = friction;
        this.id = UUID.randomUUID();
        this.crowdingDistance = 0;
        this.setOfDominatedChromosomes = new ArrayList<Chromosome>();
    }
    
    private void initalizeSortedTimes() {
        sortedTimes = new ArrayList<Double>();
        sortedTimes.add(timeInferno);
        sortedTimes.add(timeBerniW);
        sortedTimes.add(timeDamned);
        sortedTimes.add(timeBT);
        sortedTimes.add(timeOlethros);
        sortedTimes.sort(new Comparator<Double>() {
            @Override
            public int compare(Double first, Double second) {
                return Double.compare(first, second);
            }
        });
    }

    public double getCriteria(int index){
        switch (index){
            case 0: return timeDistance12();
            case 1: return timeDistance23();
            case 2: return timeDistance34();
            case 3: return timeDistance45();
        }
        return 0;
    }
    
    public double timeDistance12() {
        if (sortedTimes == null) {
            initalizeSortedTimes();
        }
        return sortedTimes.get(1) - sortedTimes.get(0);
    }

    public double timeDistance23() {
        if (sortedTimes == null) {
            initalizeSortedTimes();
        }
        return sortedTimes.get(2) - sortedTimes.get(1);
    }

    public double timeDistance34() {
        if (sortedTimes == null) {
            initalizeSortedTimes();
        }
        return sortedTimes.get(3) - sortedTimes.get(2);
    }

    public double timeDistance45() {
        if (sortedTimes == null) {
            initalizeSortedTimes();
        }
        return sortedTimes.get(4) - sortedTimes.get(3);
    }
    
    public static int getNumberOfDimensions() {
        return 5;
    }
    
    public double getDimensionValue(int index) {
        switch (index) {
            case 0: return width;
            case 1: return rollingReistance;
            case 2: return roughness;
            case 3: return roughnessWavelength;
            case 4: return friction;
        }
        throw new IllegalArgumentException();
    }
    
    public void setDimensionValue(int index, double value) {
        switch (index) {
            case 0: width = value;
            case 1: rollingReistance = value;
            case 2: roughness = value;
            case 3: roughnessWavelength = value;
            case 4: friction = value;
        }
        throw new IllegalArgumentException();
    }
    
    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Chromosome)) throw new ClassCastException();
        Chromosome other = (Chromosome) o;
        if (this.paretoLevel == other.paretoLevel) return Double.compare(other.crowdingDistance, this.crowdingDistance);
        return Integer.compare(this.paretoLevel, other.paretoLevel);
    }
    
    
    
    
    /*
        STANDARD GETTERS AND SETTERS
    */

    public UUID getId() {
        return id;
    }

    public int getParetoLevel() {
        return paretoLevel;
    }

    public int getGenerationNumber() {
        return generationNumber;
    }

    public double getCrowdingDistance() {
        return crowdingDistance;
    }

    public double getWidth() {
        return width;
    }

    public double getRollingReistance() {
        return rollingReistance;
    }

    public double getRoughness() {
        return roughness;
    }

    public double getRoughnessWavelength() {
        return roughnessWavelength;
    }

    public double getFriction() {
        return friction;
    }

    public double getTimeBT() {
        return timeBT;
    }

    public double getTimeInferno() {
        return timeInferno;
    }

    public double getTimeBerniW() {
        return timeBerniW;
    }

    public double getTimeOlethros() {
        return timeOlethros;
    }

    public double getTimeDamned() {
        return timeDamned;
    }

    public ArrayList<Chromosome> getSetOfDominatedChromosomes() {
        return setOfDominatedChromosomes;
    }

    public int getNumberOfDominators() {
        return numberOfDominators;
    }

    public void setParetoLevel(int paretoLevel) {
        this.paretoLevel = paretoLevel;
    }

    public void setGenerationNumber(int generationNumber) {
        this.generationNumber = generationNumber;
    }

    public void setCrowdingDistance(double crowdingDistance) {
        this.crowdingDistance = crowdingDistance;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setRollingReistance(double rollingReistance) {
        this.rollingReistance = rollingReistance;
    }

    public void setRoughness(double roughness) {
        this.roughness = roughness;
    }

    public void setRoughnessWavelength(double roughnessWavelength) {
        this.roughnessWavelength = roughnessWavelength;
    }

    public void setFriction(double friction) {
        this.friction = friction;
    }

    public void setTimeBT(double timeBT) {
        this.timeBT = timeBT;
    }

    public void setTimeInferno(double timeInferno) {
        this.timeInferno = timeInferno;
    }

    public void setTimeBerniW(double timeBerniW) {
        this.timeBerniW = timeBerniW;
    }

    public void setTimeOlethros(double timeOlethros) {
        this.timeOlethros = timeOlethros;
    }

    public void setTimeDamned(double timeDamned) {
        this.timeDamned = timeDamned;
    }

    public void addToSetOfDominatedChromosomes(Chromosome dominatedChromosome) {
        this.setOfDominatedChromosomes.add(dominatedChromosome);
    }

    public void increaseNumberOfDominators() {
        this.numberOfDominators++;
    }

    public void decreaseNumberOfDominators() {
        this.numberOfDominators++;
    }
    
}
